<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>Player</title>
	
    <script src="js/resources/jquery-1.12.4.js" ></script>
	<script src="js/yung-full.js"></script>
	
    <script src="Lib/sscompat.js" type="text/javascript"></script> 
    <script src="Lib/sscorlib.js" type="text/javascript"></script>  
    <script src="Lib/ssfx.Core.js" type="text/javascript"></script> 

    <script type="text/javascript" src="Lib/API_BASE.js"></script>    
    
    <script type="text/javascript" src="Lib/API_1484_11.js"></script>
    
    <script type="text/javascript" src="Lib/Controls.js"></script>        
    <script type="text/javascript" src="Lib/Player.js"></script>

    <style type="text/css">
        * { margin: 0px; }
        html, body, iframe { height:100%; }
        html, body { /*overflow-y:hidden;*/ font-family:Helvetica, "Helvetica Neue", Arial; font-size:12px; color:#000000;}
        iframe { display:block; width:100%; }

        html, body, iframe , a, img, table, tbody, tr, td, table td, table th {
            border : 0px none;
            padding: 0px; 
        }
        
                                                 

        a:link { color: #0000FF; }
        a:visited { color: #0000FF; }
        a:hover { color: #000080; }
        a:active { color: #0000FF; }

        #btnExit {margin-left:5px;}
        #btnAbandon {margin-left:5px;}
        #btnSuspendAll {margin-left:5px;}
    </style>
    
    <!--[if IE]>
    <style type="text/css">
        html, body { overflow-y:hidden; }
    </style>
    <![endif]-->
        
    <script type="text/javascript">
	
	   $Y.def("com.yung.util.*");
	   
       var dataMap = new $Y.BasicMap('string', 'string');
	   var debugFlag = false;
	   var paramMap = $Y.StringUtils.parseURL();
	   var mode = paramMap["mode"];
	   if (mode == 'd' || mode == 'D') {
		   debugFlag = true;
	   }
	
       function InitPlayer() {
           
           PlayerConfiguration.TreeMinusIcon = "images/minus.gif";
           PlayerConfiguration.TreePlusIcon = "images/plus.gif";
           PlayerConfiguration.TreeLeafIcon = "images/leaf.gif";
           PlayerConfiguration.TreeActiveIcon = "images/select.gif";

           PlayerConfiguration.BtnPreviousLabel = "Previous";
           PlayerConfiguration.BtnContinueLabel = "Continue";
           PlayerConfiguration.BtnExitLabel = "Exit";
           PlayerConfiguration.BtnExitAllLabel = "Exit All";
           PlayerConfiguration.BtnAbandonLabel = "Abandon";
           PlayerConfiguration.BtnAbandonAllLabel = "Abandon All";
           PlayerConfiguration.BtnSuspendAllLabel = "Suspend All";
           
           
            PlayerRun();
           
       }
       
       function PlayerRun(){
        var contentDiv = document.getElementById('placeholder_contentIFrame');
        contentDiv.innerHTML="";
        Run.ManifestByURL('<c:out value="${url}" escapeXml="false" />', true);
       }
    </script>

</head>
<body onload="InitPlayer()" style="background-color:#FFFFFF;">

  <table width="100%" height="100%" border="1" cellspacing="0" cellpadding="0">
    <tr>
      <td valign="top">
        <table border="0" cellspacing="0" cellpadding="0" width="100%">
         <tr>
            <td width="200" valign="top">SCORM PLAYER DEMO</td>
            <td valign="bottom"><div id="placeholder_navigationContainer" style="padding-top:5px;padding-bottom:5px;"></div></td>
         </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td height="100%" valign="top">
       <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td valign="top" height="100%" style="padding-left:5px;"><div id="placeholder_treeContainer" style="width:200px;height:100%;overflow:auto;"></div></td>
          <td width="100%" height="100%" valign="top"><div id="placeholder_contentIFrame" style="width:100%;height:100%;overflow:auto;-webkit-overflow-scrolling:touch;"></div></td>
          <td valign="top" height="100%">
            <div id="right_Border" style="width:5px;height:100%;"></div>
          </td>
        </tr>
       </table>
      </td>
    </tr>
    <tr>
      <td style="font-size:10px;padding-left:5px;">SCORM player v1.07</td>
    </tr>
  </table>
  <div style="display: none;">
      <form id="scormPostForm" action="scormSetValue" method="POST" >
	      <input type="hidden" id="scormPostForm-element" name="element" value="" />
          <input type="hidden" id="scormPostForm-value" name="value" value="" />
	  </form>
	  <form id="scormCommitForm" action="scormCommit" method="POST" >
          <input type="hidden" id="scormCommitForm-param" name="param" value="" />
      </form>
  </div>
</body>
</html>