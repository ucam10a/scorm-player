<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang='zh-Hant-TW' class="cht">

<head>
    <meta charset="utf-8">
    <!-- gIndex -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- ip Robert meta and canonical setting -->
    <meta name="description" content="台積公司成立於1987年，率先開創了專業積體電路製造服務之商業模式，並一直是全球最大的專業積體電路製造服務公司。台積公司以業界先進的製程技術及設計解決方案組合支援一個蓬勃發展的客戶及夥伴的生態系統，以此釋放全球半導體產業的創新。2019年，台積公司預計提供約1,200萬片之十二吋約當晶圓的年產能，來自台灣、美國和中國晶圓廠區的產能。並提供最廣泛的製程技術，全面涵蓋自（大於）0.5微米製程至最先進的製程技術，即現今的7奈米製程。台積公司係首家提供7奈米製程技術為客戶生產晶片的專業積體電路製造服務公司，其企業總部位於臺灣新竹。"/>
    <!-- ip Robert meta and canonical setting -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="TSMC">
</head>

<body>

    <link rel="shortcut icon" href-off="https://www.tsmc.com/img/logo.ico">
    <link href="https://www.tsmc.com/css/mainStyle.css?v=20200923" rel="stylesheet">
    <link href="https://www.tsmc.com/css/swiper.min.css" rel="stylesheet">
    <link href="https://www.tsmc.com/css/magnific-popup.css" rel="stylesheet">
    <link href="https://www.tsmc.com/css/all.min.css" rel="stylesheet">
    <script src="https://www.tsmc.com/js/scrollreveal.min.js"></script>

    <title>台灣積體電路製造股份有限公司</title>
    <link rel="canonical" href="https://www.tsmc.com/chinese/default.htm" />
    <link rel="alternate" hreflang="x-default" href="https://www.tsmc.com/english/default.htm" />
    <link rel="alternate" hreflang="en" href="https://www.tsmc.com/english/default.htm" />
    <link rel="alternate" hreflang="ja" href="https://www.tsmc.com/japanese/default.htm" />
    <link rel="alternate" hreflang="zh-TW" href="https://www.tsmc.com/chinese/default.htm" />
    <link rel="alternate" hreflang="zh-HK" href="https://www.tsmc.com/chinese/default.htm" />
    <link rel="alternate" hreflang="zh-CN" href="https://www.tsmc.com/schinese/default.htm" />
    <style>
    .fa-linkedin-in:before {
        content: url(data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAApgAAAKYB3X3/OAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAGqSURBVEiJ7ZU/SJRhHMc/v+deu5Baasw2CXQwAyNoEiRoisAuwl0R3Ny6Ew6x7iqkKcSmWhpSHIQItwKRBgc30U108w8OYnCe7/N1MPS9e++EXt6m/G7Pl+/z/fz48cBjkrCxuR7ww8h28cfTKr/YICUZhdkc0kzEC/Fhe1oQhzRS52VwwXAa5acAuBm3fQMvKUBMxfszcS+hArKHn6heyyINAntgUyr3r6QFMElpdTVUYOM/Ayo7TzELAJDfUun50tkEhbkuUGdkpAotq/MqFr29/Hofc30YD4DroHWwZXb3v+jjUBXAyM+8AgoRqEfWq9KzRQDLz/7A6K0ZK/TdOHuE2TvAYmMbK3g3oFL/WoBZG7Vrchi3IuF4QeDeIB433Yu4h/l5G/981zUNXaSLys91h0rraDLAuQ6BX0CTl2J9yQHiA756W69zD3F0AIvxfrqTAn4TZvMqD+wDaCK3jphskLuRFLCgt08OapwsC8BxfTAZwOyg3lIxdwSE6QD+QpeA/wLwjz+EgAzvCWkHWv54m1ypfD9LmC0hXa25Jb/cpO8b0BY5b58AuuGOrrJDtCsAAAAASUVORK5CYII=) !important;
    }
    .fa-twitter:before {
        content: url(data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAApgAAAKYB3X3/OAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAJWSURBVEiJtZbNS1RRGMZ/z5lr5DhhREJh7gzCvpcVLVoKJZHlWP9Bq4Ki2hYUtIkWES2KCgJ1BiIR2tSioF2gUEEfFBWFG6EiZ6x07n1ajBM2Tc5g+azuOe95f895ec+958o2i6mwqHQgWmhi0+DkdkLoMXQJ3tp+WMpmblfiGphciZNYtolyhd5Y0agPLH1bDyxQlCteMBwpD3/TCPYNFA5ZXpe0tWwpG+SLI5jNsUu7nG19M59BKl84LOtynX1MSO5XEibLPTA7gI6UokdRfqrnr7s/TZB1rl6VmClbt0TyrWwgXs2GVmEPR7nigAa/dVTnLVlX6AKW1zUQrcHa/SObeRYAZB5ULelPheRdlC/ejXKFXuULbQBJ0Oq6cEBwbTqbHoXZU1Rq8oVoWvss1s5ZFzDdoO6UIcoVPxnGq7taSwn+dVgCQGpGV43uA/E8eSsEGxqpICQeqzxHABZrhPc0ktyAklLq+xNYVjYDkHXpP8ERvPaBtkJlHADivvR1zND/MDA8njsOs5OOsy0HLY4KXv4Dfya2z/5hAMCdL60yh4zaF0qXuehs5nlNA+9d/gXrHDizQP54KUydqZ787XNdyqaHLY5h3i/A4Pjc5lakWheOQKmhwn6km0BzfbavlPoyh2tF/rgPlJ/IRG4+YelYI3DB+Zm+zKm/xpsGilvjkOyUWU8IG7A3UnlL5tc7rKOlbHp4vkVhuj89FuRRpHbsbfXghqeGk7FauurBoaoHyn/tTCVhk6VO4U5QB/BZ5mOCPySR77l32YsGqqttsBha9L+Knwx46Qtlet5uAAAAAElFTkSuQmCC) !important;
    }
    .fa-facebook-f:before {
        content: url(data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAMAAADXqc3KAAAAA3NCSVQICAjb4U/gAAAACXBIWXMAAACmAAAApgHdff84AAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAAIFQTFRF////AAD/M2aZSW2SQGCAOVWOM2aZM1WIQGCPPF6RNVqPOlqPOV2OOF2POV2ON1yON1uOOF2PN1uOOFyOOFyOOFuOOFyPOFyOOF2OOFyOOVuON1yOOFuOOFyOOF2OOFyOOFyOOFyOOFyOOFyOOFyOOFyOOFyOOFyOOFyOOFyOOFyOPu33wQAAACp0Uk5TAAEFBwgJCg8QHiIwP1Jjb3N2eJ62u8PIyc7U2N/g4uTn6Orw8fP4+/3+a9+b/gAAAHRJREFUKM/dzs0WgWAQgOEpUkhCfiqEEXrv/wKtUmf4zmnt3c08ixmRrunm9IJ6Iqb1AwAis1/Ab9i1EBqoAPLVMvEMXABi+e4KMB8KwVm1AahVb34PIrru4oDCBVkfRtv9AYBjWabm/hjHu38PvgLP2Wd+AwEMGzBvfuGoAAAAAElFTkSuQmCC) !important;
    }
    .fa-paper-plane:before {
        content: url(data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAArQAAAK0BVE7WMAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAItSURBVEiJtdRLiI5hFAfw35kZg3HLAqmRS5Pcr6FYTEmhkJRiRRYmGgtLsrFiyY7IAhsrRC5loVzLpS/JZYEUC5JLMk0xHov3+fQZ88184/LUW+9z/uf2nHP+J1JK/udp+F+OI2IYWqSU/umHOTiEz/jwr5wOxmbcxld8R8KtvypRRExFGzbljI/gOVbhDe7X/YHTxojYEBFX8RCTc4BJGIOVWJbV7/WnDJOwH29zdvswMWN1Oft3mIvhijLN6ctpA9biErpwFRvQWKFTj+M56Mwsa0UnBlRz3Iy9eIWPOIhpVRI4hdeYUiHfibspJZXKdbl+Z/ENd7AFTVWSaMRpvERLN+wkDpdJPBK78AJfcBTz+yjdIFzAM4zvAX+EtnKAM3iCHRhRQ7ObcAVP0dwDPiT3a0FKSV2eimYsxbqIGP37cBYn0/8ixqI1pfSqB7XZeYIelAX12fmB/OQu3Mxlm1GR2QjcQgmjenlhO0rle3TfphExHauxBotyE89hiYL+y1NK73t55TFIKW2Rf3qr9yjFjrmWnR/F4D5sSmj/ea+RxevxSTEdj1WZMgxULLvFZVmtu+izghvzcBk3I2JPRNR305ul4FOpLOhPgKEppU7cQGA7rkdES4XePDxJKXX8SYDGiFiBE9iGaQpyliJia0WA+79Y1tiDCYomd2B3N2wjPuC8okc7f8FrDDBSQZ6DVfBxilF+iDGV2G88qHYiYiHupFoNynb91O/3+QGQz6QeZUkYaAAAAABJRU5ErkJggg==) !important;
    }
    .fa-chevron-up:before {
        content: url(data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAABFklEQVRIie2UvUrEUBBGz5ggi76CVlZ2eQVRAxZxQyCNIMQX8Fl8AAv7QMhPp4KdL2AlCGJvqwj5GRtXduNucgMWCvm6e5k5Z+YWF8b8t4jrupuDGkwLfd/fEZEC2AWyqqpOiqJ4/xXBF/wO2J7dqeptXdfTPkmvYBl8iKRT0AU3lawUmMBNJGtD4CLyrKpnwEfr/sCyrMzzvI1eQRcc2EvT9EpVp6aShScKgmBLVe9XwZMkeZkbxBWRDJi0ZryxbfsojuP6xwaqem4CB0jT9HrZJsBhWZb7s0Nb8GQC75HUTdN81y8IHMe5VNULQIGHLvi8BDgGXkXkTVVP8zx/7OohiqIJA74RgDAMrTAM14f0jBnzR/IJIS6cHEKfkzYAAAAASUVORK5CYII=) !important;
    }
    </style>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="/js/jquery-3.3.1.min.js"><\/script>')</script>
    <script src="https://www.tsmc.com/js/bootstrap.bundle.min.js"></script>
    <script src="https://www.tsmc.com/js/ie10-viewport-bug-workaround.js"></script>
    <script src="https://www.tsmc.com/js/swiper.min.js"></script>
    <script src="https://www.tsmc.com/js/jquery.validate.min.js"></script>
    <script src="https://www.tsmc.com/js/ScrollMagic.min.js"></script>
    <script src="https://www.tsmc.com/js/odometer.min.js"></script>
    <script src="https://www.tsmc.com/js/jquery.magnific-popup.min.js"></script>
    <script src="https://www.tsmc.com/js/velocity.min.js"></script>
    <script src="https://www.tsmc.com/js/plugins/CSSPlugin.min.js"></script>
    <script src="https://www.tsmc.com/js/plugins/animation.gsap.min.js"></script>
    <script src="https://www.tsmc.com/js/easing/EasePack.min.js"></script>
    <script src="https://www.tsmc.com/js/TimelineLite.min.js"></script>
    <script src="https://www.tsmc.com/js/TweenLite.min.js"></script>
    <script src="https://www.tsmc.com/js/bowser.js"></script>
    <script src="https://www.tsmc.com/js/jquery.rwdImageMaps.min.js"></script>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-18588755-28"></script>
    <script src="https://www.tsmc.com/js/global.js?v=20200706"></script>
  
    <header id="gtag-header" class="headerBar fixed-top-off">
    <nav id="nav_main" class="navbar navbar-expand-lg navbar-light bg-white">
    <div class="container align-self-stretch">
      <a class="navbar-brand" href="https://www.tsmc.com/chinese/default.htm" gtag="Header-tsmc_logo"><img src="https://www.tsmc.com/img/logo.png" /></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="bar bar-1 navbar-toggler-icon-off"></span>
        <span class="bar bar-2 navbar-toggler-icon-off"></span>
        <span class="bar bar-3 navbar-toggler-icon-off"></span>
      </button>
      <div class="collapse navbar-collapse align-self-stretch align-items-stretch" id="navbarCollapse">
        <ul class="navbar-nav navbar-nav-center mx-auto">
          <li id="mainNav_Dedicated_IC_Foundry" class="nav-item dropdown mainItem">
            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="https://www.tsmc.com/chinese/dedicatedFoundry/index.htm" role="button" aria-haspopup="true" aria-expanded="false" gtag="Header-nav">
              專業積體電路製造服務
              <span class="underScore"></span>
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <div class="container">
                <div class="row">
                  <div class="title-item_L2 col-12 col-lg-2">
                    <a class="title-link_L2" href="https://www.tsmc.com/chinese/dedicatedFoundry/index.htm" gtag="Header-nav_L2">專業積體電路<br />製造服務 <i class="fas" style="font-size: 100%;">&#10095;</i></a>
                  </div>
                  <div class="col-12 col-lg-10">
                    <div class="row">
                      <div class="col d-lg-none">
                        <a class="title-link_L3 disabled" href="https://www.tsmc.com/chinese/dedicatedFoundry/index.htm" gtag="Header-nav_L3">概述</a>
                      </div>
                      <div class="col">
                        <a class="title-link_L3 disabled" href="#" gtag="Header-nav_L3">專業技術</a>
                        <ul class="contentLinks">
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/dedicatedFoundry/technology/index.htm" gtag="Header-nav_contentlink">概述</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/dedicatedFoundry/technology/logic.htm" gtag="Header-nav_contentlink">邏輯製程</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/dedicatedFoundry/technology/specialty.htm" gtag="Header-nav_contentlink">特殊製程</a></li>
                          <li class="content-item"><a class="content-link" href="https://3dfabric.tsmc.com/chinese/dedicatedFoundry/technology/3DFabric.htm" gtag="Header-nav_contentlink">3DFabric&trade;</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/dedicatedFoundry/technology/platform.htm" gtag="Header-nav_contentlink">平台技術</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/dedicatedFoundry/technology/future_rd.htm" gtag="Header-nav_contentlink">未來研發計劃</a></li>
                        </ul>
                      </div>
                      <div class="col">
                        <a class="title-link_L3 disabled" href="#" gtag="Header-nav_L3">卓越製造</a>
                        <ul class="contentLinks">
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/dedicatedFoundry/manufacturing/index.htm" gtag="Header-nav_contentlink">概述</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/contact_us.htm#TSMC_fabs" gtag="Header-nav_contentlink">晶圓廠區</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/dedicatedFoundry/manufacturing/fab_capacity.htm" gtag="Header-nav_contentlink">產能</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/dedicatedFoundry/manufacturing/gigafab.htm" gtag="Header-nav_contentlink">超大晶圓廠</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/dedicatedFoundry/manufacturing/engineering.htm" gtag="Header-nav_contentlink">工程效能最佳化</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/dedicatedFoundry/manufacturing/intelligent_operations.htm" gtag="Header-nav_contentlink">敏捷與智慧生產</a></li>
                        </ul>
                      </div>
                      <div class="col">
                        <a class="title-link_L3 disabled" href="#" gtag="Header-nav_L3">開放創新平台</a>
                        <ul class="contentLinks">
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/dedicatedFoundry/oip/index.htm" gtag="Header-nav_contentlink">概述</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/dedicatedFoundry/oip/ip_alliance.htm" gtag="Header-nav_contentlink">矽智財聯盟</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/dedicatedFoundry/oip/eda_alliance.htm" gtag="Header-nav_contentlink">電子設計自動化聯盟</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/dedicatedFoundry/oip/design_center_alliance.htm" gtag="Header-nav_contentlink">設計中心聯盟</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/dedicatedFoundry/oip/cloud_alliance.htm" gtag="Header-nav_contentlink">雲端聯盟</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/dedicatedFoundry/oip/value_chain_aggregator.htm" gtag="Header-nav_contentlink">價值鏈聚合聯盟</a></li>
                        </ul>
                      </div>
                      <div class="col">
                        <a class="title-link_L3 disabled" href="#" gtag="Header-nav_L3">晶圓製造服務</a>
                        <ul class="contentLinks">
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/dedicatedFoundry/services/index.htm" gtag="Header-nav_contentlink">概述</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/dedicatedFoundry/services/backend_services.htm" gtag="Header-nav_contentlink">先進封裝解決方案</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/dedicatedFoundry/services/cyberShuttle.htm" gtag="Header-nav_contentlink">晶圓共乘服務</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/dedicatedFoundry/services/mask_services.htm" gtag="Header-nav_contentlink">光罩服務</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/dedicatedFoundry/services/eFoundry.htm" gtag="Header-nav_contentlink">e化晶圓廠</a></li>
                        </ul>
                      </div>
                      <div class="col">
                        <a class="title-link_L3" href="https://www.tsmc.com/chinese/dedicatedFoundry/grandAlliance/index.htm" gtag="Header-nav_L3">台積大同盟</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </li>
          <li id="mainNav_Investors" class="nav-item dropdown mainItem">
            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="https://www.tsmc.com/chinese/investorRelations/index.htm" role="button" aria-haspopup="true" aria-expanded="false" gtag="Header-nav">
              投資人關係
              <span class="underScore"></span>
            </a>
            <div class="dropdown-menu">
              <div class="container">
                <div class="row">
                  <div class="title-item_L2 col-12 col-lg-2">
                    <a class="title-link_L2" href="https://www.tsmc.com/chinese/investorRelations/index.htm" gtag="Header-nav_L2">投資人關係 <i class="fas" style="font-size: 100%;">&#10095;</i></a>
                  </div>
                  <div class="col-12 col-lg-10">
                    <div class="row">
                      <div class="col d-lg-none">
                        <a class="title-link_L3 disabled" href="https://www.tsmc.com/chinese/investorRelations/index.htm" gtag="Header-nav_L3">概述</a>
                      </div>
                      <div class="col">
                        <a class="title-link_L3 disabled" href="#" gtag="Header-nav_L3">財務資訊</a>
                        <ul class="contentLinks">
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/investorRelations/fundamentals.htm" gtag="Header-nav_contentlink">公司概況</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/investorRelations/quarterly_results.htm" gtag="Header-nav_contentlink">每季營運報告</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/investorRelations/annual_reports.htm" gtag="Header-nav_contentlink">公司年報</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/investorRelations/monthly_revenue.htm" gtag="Header-nav_contentlink">每月營業額報告</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/investorRelations/historical_information.htm" gtag="Header-nav_contentlink">歷年財務資訊</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/investorRelations/sec_filings.htm" gtag="Header-nav_contentlink">美國證管會申報文件</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/investorRelations/financial_reports.htm" gtag="Header-nav_contentlink">財務報表</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/investorRelations/financial_calendar.htm" gtag="Header-nav_contentlink">營運報告行事曆</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/investorRelations/credit_rating.htm" gtag="Header-nav_contentlink">信用評等</a></li>
                        </ul>
                      </div>
                      <div class="col">
                        <a class="title-link_L3 disabled" href="#" gtag="Header-nav_L3">公司治理</a>
                        <ul class="contentLinks">
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/investorRelations/corporate_governance.htm" gtag="Header-nav_contentlink">概述</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/investorRelations/board_of_directors.htm" gtag="Header-nav_contentlink">董事會</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/investorRelations/committees.htm" gtag="Header-nav_contentlink">委員會</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/investorRelations/corporate_governance_officer.htm" gtag="Header-nav_contentlink">公司治理主管</a></li>
                          <li class="content-item"><a class="content-link" href="/download/ir/NYSE_Section_303A.pdf" target="_blank" gtag="Header-nav_contentlink">NYSE Section 303A</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/investorRelations/internal_audit.htm" gtag="Header-nav_contentlink">內部稽核</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/investorRelations/major_internal_policies.htm" gtag="Header-nav_contentlink">重要公司內規</a></li>
                        </ul>
                      </div>
                      <div class="col">
                        <a class="title-link_L3 disabled" href="#" gtag="Header-nav_L3">股東專欄</a>
                        <ul class="contentLinks">
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/investorRelations/stock_quotes.htm" gtag="Header-nav_contentlink">股價查詢</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/investorRelations/shareholders_meeting.htm" gtag="Header-nav_contentlink">股東會</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/investorRelations/dividend_history.htm" gtag="Header-nav_contentlink">股利分派</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/investorRelations/analyst_coverage.htm" gtag="Header-nav_contentlink">研究券商</a></li>
                          <li class="content-item"><a class="content-link external-link" href="https://mops.twse.com.tw/mops/web/t05st01" gtag="Header-nav_contentlink">台灣證券交易所公開資訊觀測站</a></li>
                          <li class="content-item"><a class="content-link external-link" href="https://mops.twse.com.tw/mops/web/t05st01?step=1&firstin=true&year=108&co_id=2330" target="_blank" gtag="Header-nav_contentlink">台積電重大訊息公告</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/investorRelations/contacts.htm" gtag="Header-nav_contentlink">聯絡我們</a></li>
                        </ul>
                      </div>
                      <div class="col">
                        <a class="title-link_L3" href="https://www.tsmc.com/chinese/investorRelations/faq.htm" gtag="Header-nav_L3">問答集</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </li>
          <li id="mainNav_Press_Center" class="nav-item dropdown mainItem">
            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="https://www.tsmc.com/chinese/newsEvents/index.htm" role="button" aria-haspopup="true" aria-expanded="false" gtag="Header-nav">
              新聞中心
              <span class="underScore"></span>
            </a>
            <div class="dropdown-menu">
              <div class="container">
                <div class="row">
                  <div class="title-item_L2 col-12 col-lg-2">
                    <a class="title-link_L2" href="https://www.tsmc.com/chinese/newsEvents/index.htm">新聞中心 <i class="fas" style="font-size: 100%;">&#10095;</i></a>
                  </div>
                  <div class="col-12 col-lg-10">
                    <div class="row">
                      <div class="col d-lg-none">
                        <a class="title-link_L3 disabled" href="https://www.tsmc.com/chinese/newsEvents/index.htm" gtag="Header-nav_L3">概述</a>
                      </div>
                      <div class="col">
                        <a class="title-link_L3 disabled" href="#" gtag="Header-nav_L3">新聞</a>
                        <ul class="contentLinks">
                          <li class="content-item"><a class="content-link" href="/tsmcdotcom/PRListingNewsAction.do?language=C" gtag="Header-nav_contentlink">最新消息</a></li>
                          <li class="content-item"><a class="content-link" href="/tsmcdotcom/PRListingNewsArchivesAction.do?language=C" gtag="Header-nav_contentlink">新聞集錦</a></li>
                        </ul>
                      </div>
                      <div class="col">
                        <a class="title-link_L3 disabled" href="#" gtag="Header-nav_L3">活動訊息</a>
                        <ul class="contentLinks">
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/newsEvents/events.htm" gtag="Header-nav_contentlink">所有活動</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/newsEvents/events.htm#investor_meetings" gtag="Header-nav_contentlink">法說會</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/newsEvents/events.htm#TSMC_events" gtag="Header-nav_contentlink">台積公司活動</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/newsEvents/events.htm#industry_events" gtag="Header-nav_contentlink">產業活動</a></li>
                        </ul>
                      </div>
                      <div class="col">
                        <a class="title-link_L3 disabled" href="#" gtag="Header-nav_L3">影像集錦</a>
                        <ul class="contentLinks">
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/newsEvents/photo_gallery.htm#fabs_outside" gtag="Header-nav_contentlink">晶圓廠區：外觀</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/newsEvents/photo_gallery.htm#fabs_inside" gtag="Header-nav_contentlink">晶圓廠區：內部</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/newsEvents/photo_gallery.htm#executives" gtag="Header-nav_contentlink">經營團隊</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/newsEvents/photo_gallery.htm#videos" gtag="Header-nav_contentlink">影片</a></li>
                        </ul>
                      </div>
                      <div class="col">
                        <a class="title-link_L3 disabled" href="#" gtag="Header-nav_L3">企業資訊</a>
                        <ul class="contentLinks">
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/aboutTSMC/index.htm" gtag="Header-nav_contentlink">關於台積公司</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/investorRelations/annual_reports.htm" gtag="Header-nav_contentlink">財務報告</a></li>
                          <li class="content-item"><a class="content-link" href="/csr/ch/index.html" gtag="Header-nav_contentlink">企業社會責任</a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </li>
          <li id="mainNav_Careers" class="nav-item dropdown mainItem">
            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="https://www.tsmc.com/chinese/careers/index.htm" role="button" aria-haspopup="true" aria-expanded="false" gtag="Header-nav">
              人才招募
              <span class="underScore"></span>
            </a>
            <div class="dropdown-menu">
              <div class="container">
                <div class="row">
                  <div class="title-item_L2 col-12 col-lg-2">
                    <a class="title-link_L2" href="https://www.tsmc.com/chinese/careers/index.htm" gtag="Header-nav_L2">人才招募 <i class="fas" style="font-size: 100%;">&#10095;</i></a>
                  </div>
                  <div class="col-12 col-lg-10">
                    <div class="row">
                      <div class="col d-lg-none">
                        <a class="title-link_L3 disabled" href="https://www.tsmc.com/chinese/careers/index.htm" gtag="Header-nav_L3">概述</a>
                      </div>
                      <div class="col">
                        <a class="title-link_L3 disabled" href="#" gtag="Header-nav_L3">搜尋職缺</a>
                        <ul class="contentLinks">
                          <li class="content-item"><a class="content-link" href="https://tsmc.taleo.net/careersection/tsmc_exti/moresearch.ftl?lang=zh-TW" gtag="Header-nav_contentlink">搜尋與申請</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/careers/application_process.htm" gtag="Header-nav_contentlink">應徵流程</a></li>
                        </ul>
                      </div>
                      <div class="col">
                        <a class="title-link_L3 disabled" href="#" gtag="Header-nav_L3">台積資源</a>
                        <ul class="contentLinks">
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/aboutTSMC/values.htm" gtag="Header-nav_contentlink">企業核心價值與經營理念</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/careers/relocation_HQ.htm" gtag="Header-nav_contentlink">移居台灣</a></li>
                        </ul>
                      </div>
                      <div class="col">
                        <a class="title-link_L3 disabled" href="#" gtag="Header-nav_L3">台積生活</a>
                        <ul class="contentLinks">
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/careers/life_at_tsmc.htm" gtag="Header-nav_contentlink">工作環境</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/careers/inclusive_workplace.htm" gtag="Header-nav_contentlink">包容職場</a></li>
                        </ul>
                      </div>
                      <div class="col"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </li>
          <li id="mainNav_CSR" class="nav-item dropdown mainItem">
            <a class="nav-link" href="/csr/ch/index.html" gtag="Header-nav">
              企業社會責任
              <span class="underScore"></span>
            </a>
          </li>
          <li id="mainNav_About_TSMC" class="nav-item dropdown mainItem">
            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="https://www.tsmc.com/chinese/aboutTSMC/index.htm" role="button" aria-haspopup="true" aria-expanded="false" gtag="Header-nav">
              關於台積公司
              <span class="underScore"></span>
            </a>
            <div class="dropdown-menu">
              <div class="container">
                <div class="row">
                  <div class="title-item_L2 col-12 col-lg-2">
                    <a class="title-link_L2" href="https://www.tsmc.com/chinese/aboutTSMC/index.htm" gtag="Header-nav_L2">關於台積公司 <i class="fas" style="font-size: 100%;">&#10095;</i></a>
                  </div>
                  <div class="col-12 col-lg-10">
                    <div class="row">
                      <div class="col d-lg-none">
                        <a class="title-link_L3 disabled" href="https://www.tsmc.com/chinese/aboutTSMC/index.htm" gtag="Header-nav_L3">概述</a>
                      </div>
                      <div class="col">
                        <a class="title-link_L3 disabled" href="#" gtag="Header-nav_L3">公司介紹</a>
                        <ul class="contentLinks">
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/aboutTSMC/company_profile.htm" gtag="Header-nav_contentlink">概述</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/aboutTSMC/mission.htm" gtag="Header-nav_contentlink">願景與使命</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/aboutTSMC/values.htm" gtag="Header-nav_contentlink">企業核心價值與經營理念</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/aboutTSMC/organization.htm" gtag="Header-nav_contentlink">公司組織</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/aboutTSMC/executives.htm" gtag="Header-nav_contentlink">經營團隊</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/investorRelations/board_of_directors.htm" gtag="Header-nav_contentlink">董事會</a></li>
                            <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/aboutTSMC/dc_annual_report.htm" gtag="Header-nav_contentlink">文件中心</a></li>
                        </ul>
                      </div>
                      <div class="col">
                        <a class="title-link_L3 disabled" href="#" gtag="Header-nav_L3">基金會與創新館</a>
                        <ul class="contentLinks">
                          <li class="content-item"><a class="content-link" target="_blank" href="https://www.tsmcmoi.com/ch/" gtag="Header-nav_contentlink">台積創新館</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/aboutTSMC/tsmc_charity.htm" gtag="Header-nav_contentlink">台積電慈善基金會</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/aboutTSMC/tsmc_foundation.htm" gtag="Header-nav_contentlink">台積電文教基金會</a></li>
                        </ul>
                      </div>
                      <div class="col shift-right">
                        <a class="title-link_L3 disabled" href="#" gtag="Header-nav_L3">政策</a>
                        <ul class="contentLinks">
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/aboutTSMC/human_rights.htm" gtag="Header-nav_contentlink">人權政策</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/aboutTSMC/quality_and_reliability.htm" gtag="Header-nav_contentlink">品質政策</a></li>
                        </ul>
                      </div>
                      <div class="col">
                        <a class="title-link_L3 disabled" href="#" gtag="Header-nav_L3">聯絡我們</a>
                        <ul class="contentLinks">
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/contact_us.htm#TSMC_fabs" gtag="Header-nav_contentlink">晶圓廠區</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/contact_us.htm#TSMC_Subsidiaries" gtag="Header-nav_contentlink">子公司</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/aboutTSMC/business_contacts.htm" gtag="Header-nav_contentlink">業務服務</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/contact_us.htm#investor_relations" gtag="Header-nav_contentlink">投資人關係,公共關係,企業社會責任</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/contact_us.htm#procurement" gtag="Header-nav_contentlink">採購</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/contact_us.htm#career_contact" gtag="Header-nav_contentlink">人才招募</a></li>
                          <li class="content-item"><a class="content-link" href="https://www.tsmc.com/chinese/contact_us.htm#business_conduct" gtag="Header-nav_contentlink">從業道德</a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </li>
        </ul>
        <ul class="navbar-nav navbar-nav-right">
          <li id="searchPanel_wrapper_desktop" class="nav-item isIcon dropdown">
            <a id="nav-searchBtn" class="nav-link dropdown-toggle d-none d-lg-block" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" gtag="Header-search_icon">
              <i class="fas" style="font-size: 120%;" >&#128269;</i>
            </a>
            <div id="searchPanel" class="dropdown-menu">
              <div class="container narrow">
                <form id="siteSearch_form" action="https://www.tsmc.com/chinese/search.html">
                  <input type="text" id="input_siteSearch" name="q" placeholder="搜尋">
                  <button type="submit"><i class="fas" style="font-size: 120%;" >&#128269;</i></button>
                </form>
              </div>
            </div>
          </li>
          <li id="langSelect_hover" class="nav-item isIcon">
            <div class="nav-link pr-0 d-none d-lg-block">
              <div id="langSelector">
                <i class="fas" style="font-size: 120%;" >&#127759;</i>
                <a data-dir="english" href="/tsmcdotcom/HomeRedirect.jsp?language=E" gtag="Header-lang_icon">English</a>
                <a data-dir="chinese" href="/tsmcdotcom/HomeRedirect.jsp?language=C" class="active" gtag="Header-lang_icon">繁中</a>
                <a data-dir="schinese" href="/tsmcdotcom/HomeRedirect.jsp?language=SC" gtag="Header-lang_icon">简中</a>
                <a data-dir="japanese" href="/tsmcdotcom/HomeRedirect.jsp?language=JP" gtag="Header-lang_icon">日本語</a>
              </div>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <div id="searchPanel_wrapper_mobile"></div>
</header>

<br>
<br>
<br>
<h2>Content section</h2>
<br>
<br>
<br>
    
<footer id="gtag-footer" class="bg-light">
  <div class="container">
    <div id="nav_footer" class="row">
      <div class="col-12 col-lg">
        <a class="title-link_L2" href="https://www.tsmc.com/chinese/dedicatedFoundry/index.htm" gtag="Footer-nav_L2">專業積體電路製造服務</a>
        <ul class="list-unstyled">
          <li><a class="title-link_L3 d-lg-none" href="https://www.tsmc.com/chinese/dedicatedFoundry/index.htm" gtag="Footer-nav_L3">概述</a></li>
          <li><a class="title-link_L3" href="https://www.tsmc.com/chinese/dedicatedFoundry/technology/index.htm" gtag="Footer-nav_L3">專業技術</a></li>
          <li><a class="title-link_L3" href="https://www.tsmc.com/chinese/dedicatedFoundry/manufacturing/index.htm" gtag="Footer-nav_L3">卓越製造</a></li>
          <li><a class="title-link_L3" href="https://www.tsmc.com/chinese/dedicatedFoundry/oip/index.htm" gtag="Footer-nav_L3">開放創新平台</a></li>
          <li><a class="title-link_L3" href="https://www.tsmc.com/chinese/dedicatedFoundry/services/index.htm" gtag="Footer-nav_L3">晶圓製造服務</a></li>
          <li><a class="title-link_L3" href="https://www.tsmc.com/chinese/dedicatedFoundry/grandAlliance/index.htm" gtag="Footer-nav_L3">台積大同盟</a></li>
        </ul>
      </div>
      <div class="col-12 col-lg">
        <a class="title-link_L2" href="https://www.tsmc.com/chinese/investorRelations/index.htm" gtag="Footer-nav_L2">投資人關係</a>
        <ul class="list-unstyled">
          <li><a class="title-link_L3 d-lg-none" href="https://www.tsmc.com/chinese/investorRelations/index.htm" gtag="Footer-nav_L3">概述</a></li>
          <li><a class="title-link_L3" href="https://www.tsmc.com/chinese/investorRelations/fundamentals.htm" gtag="Footer-nav_L3">公司概況</a></li>
          <li><a class="title-link_L3" href="https://www.tsmc.com/chinese/investorRelations/corporate_governance.htm" gtag="Footer-nav_L3">公司治理</a></li>
          <li><a class="title-link_L3" href="https://www.tsmc.com/chinese/investorRelations/stock_quotes.htm" gtag="Footer-nav_L3">股東專欄</a></li>
          <li><a class="title-link_L3" href="https://www.tsmc.com/chinese/investorRelations/faq.htm" gtag="Footer-nav_L3">問答集</a></li>
        </ul>
      </div>
      <div class="col-12 col-lg">
        <a class="title-link_L2" href="https://www.tsmc.com/chinese/newsEvents/index.htm" gtag="Footer-nav_L2">新聞中心</a>
        <ul class="list-unstyled">
          <li><a class="title-link_L3 d-lg-none" href="https://www.tsmc.com/chinese/newsEvents/index.htm" gtag="Footer-nav_L3">概述</a></li>
          <li><a class="title-link_L3" href="/tsmcdotcom/PRListingNewsAction.do?language=C" gtag="Footer-nav_L3">新聞</a></li>
          <li><a class="title-link_L3" href="https://www.tsmc.com/chinese/newsEvents/events.htm" gtag="Footer-nav_L3">活動訊息</a></li>
          <li><a class="title-link_L3" href="https://www.tsmc.com/chinese/newsEvents/photo_gallery.htm" gtag="Footer-nav_L3">影像集錦</a></li>
          <li><a class="title-link_L3" href="https://www.tsmc.com/chinese/aboutTSMC/index.htm" gtag="Footer-nav_L3">企業資訊</a></li>
        </ul>
      </div>
      <div class="col-12 col-lg">
        <a class="title-link_L2" href="https://www.tsmc.com/chinese/careers/index.htm" gtag="Footer-nav_L2">人才招募</a>
        <ul class="list-unstyled">
          <li><a class="title-link_L3 d-lg-none" href="https://www.tsmc.com/chinese/careers/index.htm" gtag="Footer-nav_L3">概述</a></li>
          <li><a class="title-link_L3" href="https://tsmc.taleo.net/careersection/tsmc_exti/moresearch.ftl?lang=zh-TW" target="_blank" gtag="Footer-nav_L3">搜尋與申請</a></li>
          <li><a class="title-link_L3" href="https://www.tsmc.com/chinese/careers/application_process.htm" gtag="Footer-nav_L3">應徵流程</a></li>
          <li><a class="title-link_L3" href="https://www.tsmc.com/chinese/aboutTSMC/values.htm" gtag="Footer-nav_L3">企業核心價值與經營理念</a></li>
          <li><a class="title-link_L3" href="https://www.tsmc.com/chinese/careers/relocation_HQ.htm" gtag="Footer-nav_L3">移居台灣</a></li>
          <li><a class="title-link_L3" href="https://www.tsmc.com/chinese/careers/life_at_tsmc.htm" gtag="Footer-nav_L3">工作環境</a></li>
          <li><a class="title-link_L3" href="https://www.tsmc.com/chinese/careers/inclusive_workplace.htm" gtag="Footer-nav_L3">包容職場</a></li>
        </ul>
      </div>
      <div class="col-12 col-lg">
        <a class="title-link_L2" href="/csr/ch/index.html" gtag="Footer-nav_L2">企業社會責任</a>
        <ul class="list-unstyled">
          <li><a class="title-link_L3" href="/csr/ch/focus/governanceAndBusiness.html" gtag="Footer-nav_L3">誠信經營</a></li>
          <li><a class="title-link_L3" href="/csr/ch/focus/innovationAndService.html" gtag="Footer-nav_L3">創新與服務</a></li>
          <li><a class="title-link_L3" href="/csr/ch/focus/responsibleSupplyChain.html" gtag="Footer-nav_L3">責任供應鏈</a></li>
          <li><a class="title-link_L3" href="/csr/ch/focus/greenManufacturing.html" gtag="Footer-nav_L3">綠色製造</a></li>
          <li><a class="title-link_L3" href="/csr/ch/focus/inclusiveWorkplace.html" gtag="Footer-nav_L3">包容職場</a></li>
          <li><a class="title-link_L3" href="/csr/ch/focus/socialParticipation.html" gtag="Footer-nav_L3">共好社會</a></li>
        </ul>
      </div>
      <div class="col-12 col-lg">
        <a class="title-link_L2" href="https://www.tsmc.com/chinese/aboutTSMC/index.htm" gtag="Footer-nav_L2">關於台積公司</a>
        <ul class="list-unstyled">
          <li><a class="title-link_L3 d-lg-none" href="https://www.tsmc.com/chinese/aboutTSMC/index.htm" gtag="Footer-nav_L3">概述</a></li>
          <li><a class="title-link_L3" href="https://www.tsmc.com/chinese/aboutTSMC/company_profile.htm" gtag="Footer-nav_L3">公司介紹</a></li>
          <li><a class="title-link_L3" href="http://www.tsmcmoi.com/ch/" target="_blank" gtag="Footer-nav_L3">台積創新館</a></li>
          <li><a class="title-link_L3" href="https://www.tsmc.com/chinese/aboutTSMC/tsmc_charity.htm" gtag="Footer-nav_L3">台積電慈善基金會</a></li>
          <li><a class="title-link_L3" href="https://www.tsmc.com/chinese/aboutTSMC/tsmc_foundation.htm" gtag="Footer-nav_L3">台積電文教基金會</a></li>
          <li><a class="title-link_L3" href="https://www.tsmc.com/chinese/contact_us.htm" gtag="Footer-nav_L3">聯絡我們</a></li>
        </ul>
      </div>
    </div>
    <div id="copyright" class="d-flex flex-column flex-sm-row align-items-center text-muted text-size-small border-secondary pt-4 pb-5">
      <div class="mr-auto d-flex align-items-center pr-5 mb-3 mb-sm-0">
        <a class="navbar-brand" href="/"><img src="https://www.tsmc.com/img/logo.png" height="50" gtag="Footer-tsmc_logo"/></a>
        <div>
          <a href="https://www.tsmc.com/chinese/legal_and_trademark.htm" gtag="Footer-tsmc_legal">法律與商標</a>&nbsp; | &nbsp;<a href="https://www.tsmc.com/chinese/privacy.htm" gtag="Footer-tsmc_privacy">隱私權政策</a>&nbsp; | &nbsp;<a href="https://www.tsmc.com/chinese/cookie.htm" gtag="Footer-tsmc_cookie">Cookie 政策</a>&nbsp; | &nbsp;<a href="https://www.tsmc.com/chinese/site_map.htm" gtag="Footer-tsmc_sitemap">網站地圖</a>
        </div>
      </div>
      <div class="">
        Copyright&copy; 2010-<span id="copyrightYear"></span> 台灣積體電路製造股份有限公司 著作權所有
        <script>
            document.getElementById("copyrightYear").innerHTML = new Date().getFullYear();
        </script>
      </div>
    </div>
  </div>
</footer>
  </body>
</html>