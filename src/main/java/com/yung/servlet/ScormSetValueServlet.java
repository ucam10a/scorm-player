package com.yung.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.yung.scorm.model.ScormData;
import com.yung.tool.ServerDebugPrinter;

public class ScormSetValueServlet extends HttpServlet {

    /**
	 * 
	 */
	private static final long serialVersionUID = -1183069981705363837L;

	private static final Logger logger = LoggerFactory.getLogger(ScormSetValueServlet.class);
	
	private static final ServerDebugPrinter printer = new ServerDebugPrinter();
	
	static {
	    printer.setServerLogger(logger);
	}
	
	@Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        doPost(req, resp);
    }
    
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        
        String element = req.getParameter("element");
        String value = req.getParameter("value");
        
        ScormData scormData = new ScormData();
        scormData.setElement(element);
        scormData.setValue(value);
        
        printer.printObjectParam("scormData", scormData);
        
        Map<String, String> map = new HashMap<String, String>();
        map.put("errormsg", "");
        Gson gson = new Gson();
        String json = gson.toJson(map);
        req.setAttribute("json", json);
        
        RequestDispatcher dispatcher = req.getRequestDispatcher("json.jsp");
        dispatcher.forward(req, resp);
        
    }
    
}
