package com.yung.servlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yung.tool.FileUtil;
import com.yung.tool.MimeTypeTool;

public class GetScormContentServlet extends HttpServlet {

    public static final String CATALINA_HOME = System.getProperty("catalina.base");
    private static final String BASE = CATALINA_HOME;

    private static final Logger logger = LoggerFactory.getLogger(GetScormContentServlet.class);
    
    /**
     * 
     */
    private static final long serialVersionUID = -8911477264847133848L;

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        doPost(req, resp);
    }
    
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        
        try {
            
            HttpSession session = req.getSession();
            String passToken = (String) session.getAttribute(PlayScormServlet.PASS_TOKEN);
            boolean pass = Boolean.valueOf(passToken);
            if (pass == false) {
                throw new Exception("Authenticate Fails");
            }
            
            String path = req.getRequestURL().toString();
            
            String scormName = (String) session.getAttribute(PlayScormServlet.SCORM_NAME);
            if (scormName == null) {
                throw new Exception("scormName is null");
            }
            String name = PlayScormServlet.getScormName(path);
            if (!scormName.equalsIgnoreCase(name)) {
                throw new Exception("scormName is not matched!");
            }
            
            if (path != null) {
                int idx = path.lastIndexOf("getScorm/");
                if (idx > 0) {
                    path = path.substring(idx + "getScorm/".length(), path.length());
                }
                idx = path.lastIndexOf("?");
                if (idx > 0) {
                    path = path.substring(0, idx);
                }
            }
            
            File file = FileUtil.getFile(BASE + "/content/" + path);
            // Get the absolute path of the image
            String filename = getFilename(path);
            
            String mime = guessMimeType(file);
            if (mime == null) {
                resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                return;
            }
            logger.info(filename + ", mime type: " + mime);
            
            resp.setContentType(mime);
            resp.setContentLength((int) file.length());

            FileInputStream in = new FileInputStream(file);
            OutputStream out = resp.getOutputStream();

            // Copy the contents of the file to the output stream
            byte[] buf = new byte[1024];
            int count = 0;
            while ((count = in.read(buf)) >= 0) {
                out.write(buf, 0, count);
            }
            out.close();
            in.close();
        } catch (Exception e) {
            logger.error(e.toString(), e);
            throw new ServletException(e);
        }
        
    }
    
    private static String getFilename(String url) {
        if (url == null) {
            return null;
        }
        int idx = url.lastIndexOf("/");
        if (idx < 0) {
            return url;
        }
        String filename = url.substring(idx + 1);
        idx = filename.lastIndexOf("?");
        if (idx > 0) {
            filename = filename.substring(0, idx);
        }
        return filename;
    }
    
    public static String guessMimeType(File file) {
        return MimeTypeTool.guessMimeType(file);
    }
    
}
