package com.yung.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class PlayScormServlet extends HttpServlet {

    /**
     * 
     */
    private static final long serialVersionUID = -3134113725294025890L;

    public static final String PASS_TOKEN = "SCORM_PASS";
    public static final String SCORM_NAME = "SCORM_NAME";
    
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        doPost(req, resp);
    }
    
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        
        boolean pass = authenticate(req, resp);
        if (pass == false) {
            throw new ServletException("Authenticate Fails");
        }
        HttpSession session = req.getSession();
        session.setAttribute(PASS_TOKEN, "TRUE");
        
        String url = req.getParameter("url");
        req.setAttribute("url", url);
        
        String scormName = getScormName(url);
        session.setAttribute(SCORM_NAME, scormName);
        
        RequestDispatcher dispatcher = req.getRequestDispatcher("demo.jsp");
        dispatcher.forward(req, resp);
        
    }
    
    private static boolean authenticate(HttpServletRequest req, HttpServletResponse resp) {
        // TODO
        
        
        return true;
    }
    
    public static String getScormName(String url) {
        if (url != null) {
            int idx = url.lastIndexOf("getScorm/");
            if (idx > 0) {
                url = url.substring(idx + "getScorm/".length(), url.length());
            }
            idx = url.indexOf("/");
            return url.substring(0, idx);
        }
        return "";
    }
    
}
