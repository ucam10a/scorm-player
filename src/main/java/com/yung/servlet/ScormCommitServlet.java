package com.yung.servlet;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.yung.tool.ServerDebugPrinter;

public class ScormCommitServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7667726915806162325L;
	
	private static final Logger logger = LoggerFactory.getLogger(ScormCommitServlet.class);
	
	private static final ServerDebugPrinter printer = new ServerDebugPrinter();
	
	static {
        printer.setServerLogger(logger);
    }
	
	@Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        doPost(req, resp);
    }
    
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        
        String param = req.getParameter("param");
        Gson gson = new Gson();
        
        if (param != null && !"".equals(param)) {
            Type dataType = new TypeToken<Map<String, String>>() {}.getType();
            Map<String, String> dataMap = gson.fromJson(param, dataType);
            printer.printObjectParam("dataMap", dataMap);
        }
        
        Map<String, String> map = new HashMap<String, String>();
        map.put("errormsg", "");
        String json = gson.toJson(map);
        req.setAttribute("json", json);
        
        RequestDispatcher dispatcher = req.getRequestDispatcher("json.jsp");
        dispatcher.forward(req, resp);
        
    }
    
}
