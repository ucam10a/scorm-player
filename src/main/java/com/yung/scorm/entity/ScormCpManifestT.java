package com.yung.scorm.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Blob;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;

import com.yung.jpa.BasicEntity;
import com.yung.scorm.entity.dao.ScormCpManifestTDAO;

@Entity(name="SCORM_CP_MANIFEST_T")
@EntityListeners(value={ScormCpManifestTDAO.class})
public class ScormCpManifestT extends BasicEntity implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Column(name="MANIFEST_ID")
    @Id
    private BigDecimal manifestId;

    @Column(name="ACT_TREE_PROTOTYPE")
    private Blob actTreePrototype;

    public void setManifestId(BigDecimal manifestId) {
        this.manifestId = manifestId;
    }

    public BigDecimal getManifestId() {
        return manifestId;
    }

    public void setActTreePrototype(Blob actTreePrototype) {
        this.actTreePrototype = actTreePrototype;
    }

    public Blob getActTreePrototype() {
        return actTreePrototype;
    }
}