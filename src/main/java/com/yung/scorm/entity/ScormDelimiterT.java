package com.yung.scorm.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;

import com.yung.jpa.BasicEntity;
import com.yung.scorm.entity.dao.ScormDelimiterTDAO;

@Entity(name="SCORM_DELIMITER_T")
@EntityListeners(value={ScormDelimiterTDAO.class})
public class ScormDelimiterT extends BasicEntity implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Column(name="DELIM_ID")
    @Id
    private BigDecimal delimId;

    @Column(name="DESC_NAME")
    private String descName;

    @Column(name="DEFAULT_VALUE")
    private String defaultValue;

    @Column(name="VALUE_SPM")
    private BigDecimal valueSpm;

    @Column(name="VALIDATOR")
    private BigDecimal validator;

    @Column(name="VALUE")
    private String value;

    public void setDelimId(BigDecimal delimId) {
        this.delimId = delimId;
    }

    public BigDecimal getDelimId() {
        return delimId;
    }

    public void setDescName(String descName) {
        this.descName = descName;
    }

    public String getDescName() {
        return descName;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setValueSpm(BigDecimal valueSpm) {
        this.valueSpm = valueSpm;
    }

    public BigDecimal getValueSpm() {
        return valueSpm;
    }

    public void setValidator(BigDecimal validator) {
        this.validator = validator;
    }

    public BigDecimal getValidator() {
        return validator;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}