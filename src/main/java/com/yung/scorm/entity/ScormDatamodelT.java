package com.yung.scorm.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;

import com.yung.jpa.BasicEntity;
import com.yung.scorm.entity.dao.ScormDatamodelTDAO;

@Entity(name="SCORM_DATAMODEL_T")
@EntityListeners(value={ScormDatamodelTDAO.class})
public class ScormDatamodelT extends BasicEntity implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Column(name="DATAMODEL_ID")
    @Id
    private BigDecimal datamodelId;

    @Column(name="CLASS_TYPE")
    private String classType;

    @Column(name="BINDING")
    private String binding;

    @Column(name="NAV_REQUESTS")
    private BigDecimal navRequests;

    @Column(name="CURRENT_REQUEST")
    private String currentRequest;

    @Column(name="LEARNER_ID")
    private String learnerId;

    @Column(name="SCO_ID")
    private String scoId;

    @Column(name="PROTOCOL")
    private String protocol;

    @Column(name="HOST")
    private String host;

    @Column(name="PORT")
    private BigDecimal port;

    @Column(name="FILE_NAME")
    private String fileName;

    @Column(name="AUTHORITY")
    private String authority;

    @Column(name="REF")
    private String ref;

    @Column(name="COURSE_ID")
    private String courseId;

    @Column(name="ATTEMPT_NUMBER")
    private String attemptNumber;

    public void setDatamodelId(BigDecimal datamodelId) {
        this.datamodelId = datamodelId;
    }

    public BigDecimal getDatamodelId() {
        return datamodelId;
    }

    public void setClassType(String classType) {
        this.classType = classType;
    }

    public String getClassType() {
        return classType;
    }

    public void setBinding(String binding) {
        this.binding = binding;
    }

    public String getBinding() {
        return binding;
    }

    public void setNavRequests(BigDecimal navRequests) {
        this.navRequests = navRequests;
    }

    public BigDecimal getNavRequests() {
        return navRequests;
    }

    public void setCurrentRequest(String currentRequest) {
        this.currentRequest = currentRequest;
    }

    public String getCurrentRequest() {
        return currentRequest;
    }

    public void setLearnerId(String learnerId) {
        this.learnerId = learnerId;
    }

    public String getLearnerId() {
        return learnerId;
    }

    public void setScoId(String scoId) {
        this.scoId = scoId;
    }

    public String getScoId() {
        return scoId;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getHost() {
        return host;
    }

    public void setPort(BigDecimal port) {
        this.port = port;
    }

    public BigDecimal getPort() {
        return port;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    public String getAuthority() {
        return authority;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public String getRef() {
        return ref;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    public String getCourseId() {
        return courseId;
    }

    public void setAttemptNumber(String attemptNumber) {
        this.attemptNumber = attemptNumber;
    }

    public String getAttemptNumber() {
        return attemptNumber;
    }
}
