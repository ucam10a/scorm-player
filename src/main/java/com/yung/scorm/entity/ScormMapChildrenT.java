package com.yung.scorm.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;

import com.yung.jpa.BasicEntity;
import com.yung.scorm.entity.dao.ScormMapChildrenTDAO;

@Entity(name="SCORM_MAP_CHILDREN_T")
@EntityListeners(value={ScormMapChildrenTDAO.class})
public class ScormMapChildrenT extends BasicEntity implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Column(name="ELEMENT_ID")
    @Id
    private BigDecimal elementId;

    @Column(name="CHILD_ID")
    private BigDecimal childId;

    @Column(name="CHILD_BINDING")
    @Id
    private String childBinding;

    public void setElementId(BigDecimal elementId) {
        this.elementId = elementId;
    }

    public BigDecimal getElementId() {
        return elementId;
    }

    public void setChildId(BigDecimal childId) {
        this.childId = childId;
    }

    public BigDecimal getChildId() {
        return childId;
    }

    public void setChildBinding(String childBinding) {
        this.childBinding = childBinding;
    }

    public String getChildBinding() {
        return childBinding;
    }
}
