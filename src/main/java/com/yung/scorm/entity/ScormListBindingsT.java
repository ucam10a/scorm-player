package com.yung.scorm.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;

import com.yung.jpa.BasicEntity;
import com.yung.scorm.entity.dao.ScormListBindingsTDAO;

@Entity(name="SCORM_LIST_BINDINGS_T")
@EntityListeners(value={ScormListBindingsTDAO.class})
public class ScormListBindingsT extends BasicEntity implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Column(name="ELEMENT_ID")
    @Id
    private BigDecimal elementId;

    @Column(name="BINDING")
    private String binding;

    @Column(name="SORT_ORDER")
    @Id
    private BigDecimal sortOrder;

    public void setElementId(BigDecimal elementId) {
        this.elementId = elementId;
    }

    public BigDecimal getElementId() {
        return elementId;
    }

    public void setBinding(String binding) {
        this.binding = binding;
    }

    public String getBinding() {
        return binding;
    }

    public void setSortOrder(BigDecimal sortOrder) {
        this.sortOrder = sortOrder;
    }

    public BigDecimal getSortOrder() {
        return sortOrder;
    }
}
