package com.yung.scorm.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;

import com.yung.jpa.BasicEntity;
import com.yung.scorm.entity.dao.ScormDatamanagerTDAO;

@Entity(name="SCORM_DATAMANAGER_T")
@EntityListeners(value={ScormDatamanagerTDAO.class})
public class ScormDatamanagerT extends BasicEntity implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Column(name="DATAMANAGER_ID")
    @Id
    private BigDecimal datamanagerId;

    @Column(name="CONTENT_PACKAGE_ID")
    private BigDecimal contentPackageId;

    @Column(name="COURSE_ID")
    private String courseId;

    @Column(name="SCO_ID")
    private String scoId;

    @Column(name="ACTIVITY_ID")
    private String activityId;

    @Column(name="USER_ID")
    private String userId;

    @Column(name="TITLE")
    private String title;

    @Column(name="ATTEMPT_NUMBER")
    private BigDecimal attemptNumber;

    @Column(name="BEGIN_DATE")
    private Timestamp beginDate;

    @Column(name="LAST_MODIFIED_DATE")
    private Timestamp lastModifiedDate;

    public void setDatamanagerId(BigDecimal datamanagerId) {
        this.datamanagerId = datamanagerId;
    }

    public BigDecimal getDatamanagerId() {
        return datamanagerId;
    }

    public void setContentPackageId(BigDecimal contentPackageId) {
        this.contentPackageId = contentPackageId;
    }

    public BigDecimal getContentPackageId() {
        return contentPackageId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    public String getCourseId() {
        return courseId;
    }

    public void setScoId(String scoId) {
        this.scoId = scoId;
    }

    public String getScoId() {
        return scoId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public String getActivityId() {
        return activityId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setAttemptNumber(BigDecimal attemptNumber) {
        this.attemptNumber = attemptNumber;
    }

    public BigDecimal getAttemptNumber() {
        return attemptNumber;
    }

    public void setBeginDate(Timestamp beginDate) {
        this.beginDate = beginDate;
    }

    public Timestamp getBeginDate() {
        return beginDate;
    }

    public void setLastModifiedDate(Timestamp lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Timestamp getLastModifiedDate() {
        return lastModifiedDate;
    }
}
