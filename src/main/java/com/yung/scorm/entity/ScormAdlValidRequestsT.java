package com.yung.scorm.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;

import com.yung.jpa.BasicEntity;
import com.yung.scorm.entity.dao.ScormAdlValidRequestsTDAO;

@Entity(name="SCORM_ADL_VALID_REQUESTS_T")
@EntityListeners(value={ScormAdlValidRequestsTDAO.class})
public class ScormAdlValidRequestsT extends BasicEntity implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Column(name="VALID_REQUESTS_ID")
    @Id
    private BigDecimal validRequestsId;

    @Column(name="IS_START_ENABLED")
    private BigDecimal isStartEnabled;

    @Column(name="IS_RESUME_ALL_ENABLED")
    private BigDecimal isResumeAllEnabled;

    @Column(name="IS_CONTINUE_ENABLED")
    private BigDecimal isContinueEnabled;

    @Column(name="IS_CONTINUE_EXIT_ENABLED")
    private BigDecimal isContinueExitEnabled;

    @Column(name="IS_PREVIOUS_ENABLED")
    private BigDecimal isPreviousEnabled;

    @Column(name="IS_SUSPEND_VALID")
    private BigDecimal isSuspendValid;

    public void setValidRequestsId(BigDecimal validRequestsId) {
        this.validRequestsId = validRequestsId;
    }

    public BigDecimal getValidRequestsId() {
        return validRequestsId;
    }

    public void setIsStartEnabled(BigDecimal isStartEnabled) {
        this.isStartEnabled = isStartEnabled;
    }

    public BigDecimal getIsStartEnabled() {
        return isStartEnabled;
    }

    public void setIsResumeAllEnabled(BigDecimal isResumeAllEnabled) {
        this.isResumeAllEnabled = isResumeAllEnabled;
    }

    public BigDecimal getIsResumeAllEnabled() {
        return isResumeAllEnabled;
    }

    public void setIsContinueEnabled(BigDecimal isContinueEnabled) {
        this.isContinueEnabled = isContinueEnabled;
    }

    public BigDecimal getIsContinueEnabled() {
        return isContinueEnabled;
    }

    public void setIsContinueExitEnabled(BigDecimal isContinueExitEnabled) {
        this.isContinueExitEnabled = isContinueExitEnabled;
    }

    public BigDecimal getIsContinueExitEnabled() {
        return isContinueExitEnabled;
    }

    public void setIsPreviousEnabled(BigDecimal isPreviousEnabled) {
        this.isPreviousEnabled = isPreviousEnabled;
    }

    public BigDecimal getIsPreviousEnabled() {
        return isPreviousEnabled;
    }

    public void setIsSuspendValid(BigDecimal isSuspendValid) {
        this.isSuspendValid = isSuspendValid;
    }

    public BigDecimal getIsSuspendValid() {
        return isSuspendValid;
    }
}