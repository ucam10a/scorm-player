package com.yung.scorm.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;

import com.yung.jpa.BasicEntity;
import com.yung.scorm.entity.dao.ScormUrlDAO;

@Entity(name="SCORM_URL")
@EntityListeners(value={ScormUrlDAO.class})
public class ScormUrl extends BasicEntity implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Column(name="URL_ID")
    @Id
    private BigDecimal urlId;

    @Column(name="PROTOCOL")
    private String protocol;

    @Column(name="HOST")
    private String host;

    @Column(name="PORT")
    private BigDecimal port;

    @Column(name="FILE_NAME")
    private String fileName;

    @Column(name="AUTHORITY")
    private String authority;

    @Column(name="REF")
    private String ref;

    public void setUrlId(BigDecimal urlId) {
        this.urlId = urlId;
    }

    public BigDecimal getUrlId() {
        return urlId;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getHost() {
        return host;
    }

    public void setPort(BigDecimal port) {
        this.port = port;
    }

    public BigDecimal getPort() {
        return port;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    public String getAuthority() {
        return authority;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public String getRef() {
        return ref;
    }
}
