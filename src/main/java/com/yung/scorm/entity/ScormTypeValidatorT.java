package com.yung.scorm.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;

import com.yung.jpa.BasicEntity;
import com.yung.scorm.entity.dao.ScormTypeValidatorTDAO;

@Entity(name="SCORM_TYPE_VALIDATOR_T")
@EntityListeners(value={ScormTypeValidatorTDAO.class})
public class ScormTypeValidatorT extends BasicEntity implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Column(name="ID")
    @Id
    private BigDecimal id;

    @Column(name="VALIDATOR_TYPE")
    private String validatorType;

    @Column(name="TYPE")
    private String type;

    @Column(name="INCLUDE_SUB_SECS")
    private BigDecimal includeSubSecs;

    @Column(name="INTER_ALLOW_EMPTY")
    private BigDecimal interAllowEmpty;

    @Column(name="ELEMENT")
    private String element;

    @Column(name="INTERACTION_TYPE")
    private BigDecimal interactionType;

    @Column(name="INT_MAX")
    private BigDecimal intMax;

    @Column(name="INT_MIN")
    private BigDecimal intMin;

    @Column(name="LANG_ALLOW_EMPTY")
    private BigDecimal langAllowEmpty;

    @Column(name="REAL_MAX")
    private Double realMax;

    @Column(name="REAL_MIN")
    private Double realMin;

    @Column(name="RESULT_VOCAB_LIST")
    private String resultVocabList;

    @Column(name="SPM")
    private BigDecimal spm;

    @Column(name="URI_SPM")
    private BigDecimal uriSpm;

    @Column(name="VOCAB_LIST")
    private String vocabList;

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setValidatorType(String validatorType) {
        this.validatorType = validatorType;
    }

    public String getValidatorType() {
        return validatorType;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setIncludeSubSecs(BigDecimal includeSubSecs) {
        this.includeSubSecs = includeSubSecs;
    }

    public BigDecimal getIncludeSubSecs() {
        return includeSubSecs;
    }

    public void setInterAllowEmpty(BigDecimal interAllowEmpty) {
        this.interAllowEmpty = interAllowEmpty;
    }

    public BigDecimal getInterAllowEmpty() {
        return interAllowEmpty;
    }

    public void setElement(String element) {
        this.element = element;
    }

    public String getElement() {
        return element;
    }

    public void setInteractionType(BigDecimal interactionType) {
        this.interactionType = interactionType;
    }

    public BigDecimal getInteractionType() {
        return interactionType;
    }

    public void setIntMax(BigDecimal intMax) {
        this.intMax = intMax;
    }

    public BigDecimal getIntMax() {
        return intMax;
    }

    public void setIntMin(BigDecimal intMin) {
        this.intMin = intMin;
    }

    public BigDecimal getIntMin() {
        return intMin;
    }

    public void setLangAllowEmpty(BigDecimal langAllowEmpty) {
        this.langAllowEmpty = langAllowEmpty;
    }

    public BigDecimal getLangAllowEmpty() {
        return langAllowEmpty;
    }

    public void setRealMax(Double realMax) {
        this.realMax = realMax;
    }

    public Double getRealMax() {
        return realMax;
    }

    public void setRealMin(Double realMin) {
        this.realMin = realMin;
    }

    public Double getRealMin() {
        return realMin;
    }

    public void setResultVocabList(String resultVocabList) {
        this.resultVocabList = resultVocabList;
    }

    public String getResultVocabList() {
        return resultVocabList;
    }

    public void setSpm(BigDecimal spm) {
        this.spm = spm;
    }

    public BigDecimal getSpm() {
        return spm;
    }

    public void setUriSpm(BigDecimal uriSpm) {
        this.uriSpm = uriSpm;
    }

    public BigDecimal getUriSpm() {
        return uriSpm;
    }

    public void setVocabList(String vocabList) {
        this.vocabList = vocabList;
    }

    public String getVocabList() {
        return vocabList;
    }
}
