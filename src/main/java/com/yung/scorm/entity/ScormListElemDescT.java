package com.yung.scorm.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;

import com.yung.jpa.BasicEntity;
import com.yung.scorm.entity.dao.ScormListElemDescTDAO;

@Entity(name="SCORM_LIST_ELEM_DESC_T")
@EntityListeners(value={ScormListElemDescTDAO.class})
public class ScormListElemDescT extends BasicEntity implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Column(name="ELEM_DESC_ID")
    @Id
    private BigDecimal elemDescId;

    @Column(name="CHILD_ID")
    private BigDecimal childId;

    @Column(name="SORT_ORDER")
    @Id
    private BigDecimal sortOrder;

    public void setElemDescId(BigDecimal elemDescId) {
        this.elemDescId = elemDescId;
    }

    public BigDecimal getElemDescId() {
        return elemDescId;
    }

    public void setChildId(BigDecimal childId) {
        this.childId = childId;
    }

    public BigDecimal getChildId() {
        return childId;
    }

    public void setSortOrder(BigDecimal sortOrder) {
        this.sortOrder = sortOrder;
    }

    public BigDecimal getSortOrder() {
        return sortOrder;
    }
}
