package com.yung.scorm.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;

import com.yung.jpa.BasicEntity;
import com.yung.scorm.entity.dao.ScormListElementsTDAO;

@Entity(name="SCORM_LIST_ELEMENTS_T")
@EntityListeners(value={ScormListElementsTDAO.class})
public class ScormListElementsT extends BasicEntity implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Column(name="DATAMODEL_ID")
    @Id
    private BigDecimal datamodelId;

    @Column(name="ELEMENT_ID")
    private BigDecimal elementId;

    @Column(name="SORT_ORDER")
    @Id
    private BigDecimal sortOrder;

    public void setDatamodelId(BigDecimal datamodelId) {
        this.datamodelId = datamodelId;
    }

    public BigDecimal getDatamodelId() {
        return datamodelId;
    }

    public void setElementId(BigDecimal elementId) {
        this.elementId = elementId;
    }

    public BigDecimal getElementId() {
        return elementId;
    }

    public void setSortOrder(BigDecimal sortOrder) {
        this.sortOrder = sortOrder;
    }

    public BigDecimal getSortOrder() {
        return sortOrder;
    }
}