package com.yung.scorm.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Clob;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;

import com.yung.jpa.BasicEntity;
import com.yung.scorm.entity.dao.ScormElementTDAO;

@Entity(name="SCORM_ELEMENT_T")
@EntityListeners(value={ScormElementTDAO.class})
public class ScormElementT extends BasicEntity implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Column(name="ELEMENT_ID")
    @Id
    private BigDecimal elementId;

    @Column(name="CLASS_TYPE")
    private String classType;

    @Column(name="DESCRIPTION")
    private BigDecimal description;

    @Column(name="PARENT")
    private BigDecimal parent;

    @Column(name="VALUE")
    private Clob value;

    @Column(name="IS_INITIALIZED")
    private BigDecimal isInitialized;

    @Column(name="TRUNC_SPM")
    private BigDecimal truncSpm;

    @Column(name="ELEMENT_DM")
    private BigDecimal elementDm;

    @Column(name="NAVIGATION_DM")
    private BigDecimal navigationDm;

    @Column(name="BINDING")
    private String binding;

    @Column(name="IS_RANDOMIZED")
    private BigDecimal isRandomized;

    @Column(name="DM_COUNT")
    private BigDecimal dmCount;

    public void setElementId(BigDecimal elementId) {
        this.elementId = elementId;
    }

    public BigDecimal getElementId() {
        return elementId;
    }

    public void setClassType(String classType) {
        this.classType = classType;
    }

    public String getClassType() {
        return classType;
    }

    public void setDescription(BigDecimal description) {
        this.description = description;
    }

    public BigDecimal getDescription() {
        return description;
    }

    public void setParent(BigDecimal parent) {
        this.parent = parent;
    }

    public BigDecimal getParent() {
        return parent;
    }

    public void setValue(Clob value) {
        this.value = value;
    }

    public Clob getValue() {
        return value;
    }

    public void setIsInitialized(BigDecimal isInitialized) {
        this.isInitialized = isInitialized;
    }

    public BigDecimal getIsInitialized() {
        return isInitialized;
    }

    public void setTruncSpm(BigDecimal truncSpm) {
        this.truncSpm = truncSpm;
    }

    public BigDecimal getTruncSpm() {
        return truncSpm;
    }

    public void setElementDm(BigDecimal elementDm) {
        this.elementDm = elementDm;
    }

    public BigDecimal getElementDm() {
        return elementDm;
    }

    public void setNavigationDm(BigDecimal navigationDm) {
        this.navigationDm = navigationDm;
    }

    public BigDecimal getNavigationDm() {
        return navigationDm;
    }

    public void setBinding(String binding) {
        this.binding = binding;
    }

    public String getBinding() {
        return binding;
    }

    public void setIsRandomized(BigDecimal isRandomized) {
        this.isRandomized = isRandomized;
    }

    public BigDecimal getIsRandomized() {
        return isRandomized;
    }

    public void setDmCount(BigDecimal dmCount) {
        this.dmCount = dmCount;
    }

    public BigDecimal getDmCount() {
        return dmCount;
    }
}