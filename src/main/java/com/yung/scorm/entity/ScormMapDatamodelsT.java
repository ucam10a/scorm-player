package com.yung.scorm.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;

import com.yung.jpa.BasicEntity;
import com.yung.scorm.entity.dao.ScormMapDatamodelsTDAO;

@Entity(name="SCORM_MAP_DATAMODELS_T")
@EntityListeners(value={ScormMapDatamodelsTDAO.class})
public class ScormMapDatamodelsT extends BasicEntity implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Column(name="DATAMANAGER_ID")
    @Id
    private BigDecimal datamanagerId;

    @Column(name="DATAMODEL_ID")
    private BigDecimal datamodelId;

    @Column(name="DM_BINDING")
    @Id
    private String dmBinding;

    public void setDatamanagerId(BigDecimal datamanagerId) {
        this.datamanagerId = datamanagerId;
    }

    public BigDecimal getDatamanagerId() {
        return datamanagerId;
    }

    public void setDatamodelId(BigDecimal datamodelId) {
        this.datamodelId = datamodelId;
    }

    public BigDecimal getDatamodelId() {
        return datamodelId;
    }

    public void setDmBinding(String dmBinding) {
        this.dmBinding = dmBinding;
    }

    public String getDmBinding() {
        return dmBinding;
    }
}
