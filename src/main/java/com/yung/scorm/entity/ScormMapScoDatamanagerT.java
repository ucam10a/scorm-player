package com.yung.scorm.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;

import com.yung.jpa.BasicEntity;
import com.yung.scorm.entity.dao.ScormMapScoDatamanagerTDAO;

@Entity(name="SCORM_MAP_SCO_DATAMANAGER_T")
@EntityListeners(value={ScormMapScoDatamanagerTDAO.class})
public class ScormMapScoDatamanagerT extends BasicEntity implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Column(name="ATTEMPT_ID")
    @Id
    private BigDecimal attemptId;

    @Column(name="DATAMANAGER_ID")
    private BigDecimal datamanagerId;

    @Column(name="SCO_ID")
    @Id
    private String scoId;

    public void setAttemptId(BigDecimal attemptId) {
        this.attemptId = attemptId;
    }

    public BigDecimal getAttemptId() {
        return attemptId;
    }

    public void setDatamanagerId(BigDecimal datamanagerId) {
        this.datamanagerId = datamanagerId;
    }

    public BigDecimal getDatamanagerId() {
        return datamanagerId;
    }

    public void setScoId(String scoId) {
        this.scoId = scoId;
    }

    public String getScoId() {
        return scoId;
    }
}
