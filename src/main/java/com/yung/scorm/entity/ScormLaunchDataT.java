package com.yung.scorm.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;

import com.yung.jpa.BasicEntity;
import com.yung.scorm.entity.dao.ScormLaunchDataTDAO;

@Entity(name="SCORM_LAUNCH_DATA_T")
@EntityListeners(value={ScormLaunchDataTDAO.class})
public class ScormLaunchDataT extends BasicEntity implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Column(name="LAUNCH_DATA_ID")
    @Id
    private BigDecimal launchDataId;

    @Column(name="ORG_IDENTIFIER")
    private String orgIdentifier;

    @Column(name="ITEM_IDENTIFIER")
    private String itemIdentifier;

    @Column(name="RESOURCE_IDENTIFIER")
    private String resourceIdentifier;

    @Column(name="MANIFEST_XML_BASE")
    private String manifestXmlBase;

    @Column(name="RESOURCES_XML_BASE")
    private String resourcesXmlBase;

    @Column(name="RESOURCE_XML_BASE")
    private String resourceXmlBase;

    @Column(name="PARAMETERS")
    private String parameters;

    @Column(name="PERSIST_STATE")
    private String persistState;

    @Column(name="LOCATION")
    private String location;

    @Column(name="SCORM_TYPE")
    private String scormType;

    @Column(name="ITEM_TITLE")
    private String itemTitle;

    @Column(name="DATA_FROM_LMS")
    private String dataFromLms;

    @Column(name="TIME_LIMIT_ACTION")
    private String timeLimitAction;

    @Column(name="MIN_NORMALIZED_MEASURE")
    private String minNormalizedMeasure;

    @Column(name="ATTEMPT_ABS_DUR_LIMIT")
    private String attemptAbsDurLimit;

    @Column(name="COMPLETION_THRESHOLD")
    private String completionThreshold;

    @Column(name="OBJECTIVES_LIST")
    private String objectivesList;

    @Column(name="IS_PREVIOUS")
    private BigDecimal isPrevious;

    @Column(name="IS_CONTINUE")
    private BigDecimal isContinue;

    @Column(name="IS_EXIT")
    private BigDecimal isExit;

    @Column(name="IS_EXIT_ALL")
    private BigDecimal isExitAll;

    @Column(name="IS_ABANDON")
    private BigDecimal isAbandon;

    @Column(name="IS_SUSPEND_ALL")
    private BigDecimal isSuspendAll;

    public void setLaunchDataId(BigDecimal launchDataId) {
        this.launchDataId = launchDataId;
    }

    public BigDecimal getLaunchDataId() {
        return launchDataId;
    }

    public void setOrgIdentifier(String orgIdentifier) {
        this.orgIdentifier = orgIdentifier;
    }

    public String getOrgIdentifier() {
        return orgIdentifier;
    }

    public void setItemIdentifier(String itemIdentifier) {
        this.itemIdentifier = itemIdentifier;
    }

    public String getItemIdentifier() {
        return itemIdentifier;
    }

    public void setResourceIdentifier(String resourceIdentifier) {
        this.resourceIdentifier = resourceIdentifier;
    }

    public String getResourceIdentifier() {
        return resourceIdentifier;
    }

    public void setManifestXmlBase(String manifestXmlBase) {
        this.manifestXmlBase = manifestXmlBase;
    }

    public String getManifestXmlBase() {
        return manifestXmlBase;
    }

    public void setResourcesXmlBase(String resourcesXmlBase) {
        this.resourcesXmlBase = resourcesXmlBase;
    }

    public String getResourcesXmlBase() {
        return resourcesXmlBase;
    }

    public void setResourceXmlBase(String resourceXmlBase) {
        this.resourceXmlBase = resourceXmlBase;
    }

    public String getResourceXmlBase() {
        return resourceXmlBase;
    }

    public void setParameters(String parameters) {
        this.parameters = parameters;
    }

    public String getParameters() {
        return parameters;
    }

    public void setPersistState(String persistState) {
        this.persistState = persistState;
    }

    public String getPersistState() {
        return persistState;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocation() {
        return location;
    }

    public void setScormType(String scormType) {
        this.scormType = scormType;
    }

    public String getScormType() {
        return scormType;
    }

    public void setItemTitle(String itemTitle) {
        this.itemTitle = itemTitle;
    }

    public String getItemTitle() {
        return itemTitle;
    }

    public void setDataFromLms(String dataFromLms) {
        this.dataFromLms = dataFromLms;
    }

    public String getDataFromLms() {
        return dataFromLms;
    }

    public void setTimeLimitAction(String timeLimitAction) {
        this.timeLimitAction = timeLimitAction;
    }

    public String getTimeLimitAction() {
        return timeLimitAction;
    }

    public void setMinNormalizedMeasure(String minNormalizedMeasure) {
        this.minNormalizedMeasure = minNormalizedMeasure;
    }

    public String getMinNormalizedMeasure() {
        return minNormalizedMeasure;
    }

    public void setAttemptAbsDurLimit(String attemptAbsDurLimit) {
        this.attemptAbsDurLimit = attemptAbsDurLimit;
    }

    public String getAttemptAbsDurLimit() {
        return attemptAbsDurLimit;
    }

    public void setCompletionThreshold(String completionThreshold) {
        this.completionThreshold = completionThreshold;
    }

    public String getCompletionThreshold() {
        return completionThreshold;
    }

    public void setObjectivesList(String objectivesList) {
        this.objectivesList = objectivesList;
    }

    public String getObjectivesList() {
        return objectivesList;
    }

    public void setIsPrevious(BigDecimal isPrevious) {
        this.isPrevious = isPrevious;
    }

    public BigDecimal getIsPrevious() {
        return isPrevious;
    }

    public void setIsContinue(BigDecimal isContinue) {
        this.isContinue = isContinue;
    }

    public BigDecimal getIsContinue() {
        return isContinue;
    }

    public void setIsExit(BigDecimal isExit) {
        this.isExit = isExit;
    }

    public BigDecimal getIsExit() {
        return isExit;
    }

    public void setIsExitAll(BigDecimal isExitAll) {
        this.isExitAll = isExitAll;
    }

    public BigDecimal getIsExitAll() {
        return isExitAll;
    }

    public void setIsAbandon(BigDecimal isAbandon) {
        this.isAbandon = isAbandon;
    }

    public BigDecimal getIsAbandon() {
        return isAbandon;
    }

    public void setIsSuspendAll(BigDecimal isSuspendAll) {
        this.isSuspendAll = isSuspendAll;
    }

    public BigDecimal getIsSuspendAll() {
        return isSuspendAll;
    }
}
