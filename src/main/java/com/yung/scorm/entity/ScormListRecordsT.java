package com.yung.scorm.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;

import com.yung.jpa.BasicEntity;
import com.yung.scorm.entity.dao.ScormListRecordsTDAO;

@Entity(name="SCORM_LIST_RECORDS_T")
@EntityListeners(value={ScormListRecordsTDAO.class})
public class ScormListRecordsT extends BasicEntity implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Column(name="ELEMENT_ID")
    @Id
    private BigDecimal elementId;

    @Column(name="RECORD_ID")
    private BigDecimal recordId;

    @Column(name="SORT_ORDER")
    @Id
    private BigDecimal sortOrder;

    public void setElementId(BigDecimal elementId) {
        this.elementId = elementId;
    }

    public BigDecimal getElementId() {
        return elementId;
    }

    public void setRecordId(BigDecimal recordId) {
        this.recordId = recordId;
    }

    public BigDecimal getRecordId() {
        return recordId;
    }

    public void setSortOrder(BigDecimal sortOrder) {
        this.sortOrder = sortOrder;
    }

    public BigDecimal getSortOrder() {
        return sortOrder;
    }
}