package com.yung.scorm.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;

import com.yung.jpa.BasicEntity;
import com.yung.scorm.entity.dao.ScormElementDescTDAO;

@Entity(name="SCORM_ELEMENT_DESC_T")
@EntityListeners(value={ScormElementDescTDAO.class})
public class ScormElementDescT extends BasicEntity implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Column(name="ELEM_DESC_ID")
    @Id
    private BigDecimal elemDescId;

    @Column(name="ED_BINDING")
    private String edBinding;

    @Column(name="IS_READABLE")
    private BigDecimal isReadable;

    @Column(name="IS_WRITABLE")
    private BigDecimal isWritable;

    @Column(name="INITIAL_VALUE")
    private String initialValue;

    @Column(name="IS_UNIQUE")
    private BigDecimal isUnique;

    @Column(name="IS_WRITE_ONCE")
    private BigDecimal isWriteOnce;

    @Column(name="VALUE_SPM")
    private BigDecimal valueSpm;

    @Column(name="SPM")
    private BigDecimal spm;

    @Column(name="OLD_SPM")
    private BigDecimal oldSpm;

    @Column(name="IS_MAXIMUM")
    private BigDecimal isMaximum;

    @Column(name="IS_SHOW_CHILDREN")
    private BigDecimal isShowChildren;

    public void setElemDescId(BigDecimal elemDescId) {
        this.elemDescId = elemDescId;
    }

    public BigDecimal getElemDescId() {
        return elemDescId;
    }

    public void setEdBinding(String edBinding) {
        this.edBinding = edBinding;
    }

    public String getEdBinding() {
        return edBinding;
    }

    public void setIsReadable(BigDecimal isReadable) {
        this.isReadable = isReadable;
    }

    public BigDecimal getIsReadable() {
        return isReadable;
    }

    public void setIsWritable(BigDecimal isWritable) {
        this.isWritable = isWritable;
    }

    public BigDecimal getIsWritable() {
        return isWritable;
    }

    public void setInitialValue(String initialValue) {
        this.initialValue = initialValue;
    }

    public String getInitialValue() {
        return initialValue;
    }

    public void setIsUnique(BigDecimal isUnique) {
        this.isUnique = isUnique;
    }

    public BigDecimal getIsUnique() {
        return isUnique;
    }

    public void setIsWriteOnce(BigDecimal isWriteOnce) {
        this.isWriteOnce = isWriteOnce;
    }

    public BigDecimal getIsWriteOnce() {
        return isWriteOnce;
    }

    public void setValueSpm(BigDecimal valueSpm) {
        this.valueSpm = valueSpm;
    }

    public BigDecimal getValueSpm() {
        return valueSpm;
    }

    public void setSpm(BigDecimal spm) {
        this.spm = spm;
    }

    public BigDecimal getSpm() {
        return spm;
    }

    public void setOldSpm(BigDecimal oldSpm) {
        this.oldSpm = oldSpm;
    }

    public BigDecimal getOldSpm() {
        return oldSpm;
    }

    public void setIsMaximum(BigDecimal isMaximum) {
        this.isMaximum = isMaximum;
    }

    public BigDecimal getIsMaximum() {
        return isMaximum;
    }

    public void setIsShowChildren(BigDecimal isShowChildren) {
        this.isShowChildren = isShowChildren;
    }

    public BigDecimal getIsShowChildren() {
        return isShowChildren;
    }
}
