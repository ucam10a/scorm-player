package com.yung.scorm.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;

import com.yung.jpa.BasicEntity;
import com.yung.scorm.entity.dao.ScormContentPackageTDAO;

@Entity(name="SCORM_CONTENT_PACKAGE_T")
@EntityListeners(value={ScormContentPackageTDAO.class})
public class ScormContentPackageT extends BasicEntity implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Column(name="PACKAGE_ID")
    @Id
    private BigDecimal packageId;

    @Column(name="TITLE")
    private String title;

    @Column(name="RESOURCE_ID")
    private String resourceId;

    @Column(name="MANIFEST_ID")
    private String manifestId;

    @Column(name="MANIFEST_RESOURCE_ID")
    private String manifestResourceId;

    @Column(name="CONTEXT")
    private String context;

    @Column(name="URL")
    private String url;

    @Column(name="RELEASE_ON")
    private Timestamp releaseOn;

    @Column(name="DUE_ON")
    private Timestamp dueOn;

    @Column(name="ACCEPT_UNTIL")
    private Timestamp acceptUntil;

    @Column(name="CREATED_ON")
    private Timestamp createdOn;

    @Column(name="CREATED_BY")
    private String createdBy;

    @Column(name="MODIFIED_ON")
    private Timestamp modifiedOn;

    @Column(name="MODIFIED_BY")
    private String modifiedBy;

    @Column(name="NUMBER_OF_TRIES")
    private BigDecimal numberOfTries;

    @Column(name="IS_DELETED")
    private BigDecimal isDeleted;

    public void setPackageId(BigDecimal packageId) {
        this.packageId = packageId;
    }

    public BigDecimal getPackageId() {
        return packageId;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setResourceId(String resourceId) {
        this.resourceId = resourceId;
    }

    public String getResourceId() {
        return resourceId;
    }

    public void setManifestId(String manifestId) {
        this.manifestId = manifestId;
    }

    public String getManifestId() {
        return manifestId;
    }

    public void setManifestResourceId(String manifestResourceId) {
        this.manifestResourceId = manifestResourceId;
    }

    public String getManifestResourceId() {
        return manifestResourceId;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public String getContext() {
        return context;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setReleaseOn(Timestamp releaseOn) {
        this.releaseOn = releaseOn;
    }

    public Timestamp getReleaseOn() {
        return releaseOn;
    }

    public void setDueOn(Timestamp dueOn) {
        this.dueOn = dueOn;
    }

    public Timestamp getDueOn() {
        return dueOn;
    }

    public void setAcceptUntil(Timestamp acceptUntil) {
        this.acceptUntil = acceptUntil;
    }

    public Timestamp getAcceptUntil() {
        return acceptUntil;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setModifiedOn(Timestamp modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public Timestamp getModifiedOn() {
        return modifiedOn;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setNumberOfTries(BigDecimal numberOfTries) {
        this.numberOfTries = numberOfTries;
    }

    public BigDecimal getNumberOfTries() {
        return numberOfTries;
    }

    public void setIsDeleted(BigDecimal isDeleted) {
        this.isDeleted = isDeleted;
    }

    public BigDecimal getIsDeleted() {
        return isDeleted;
    }
}