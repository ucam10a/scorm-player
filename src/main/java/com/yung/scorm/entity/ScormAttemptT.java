package com.yung.scorm.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.security.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;

import com.yung.jpa.BasicEntity;
import com.yung.scorm.entity.dao.ScormAttemptTDAO;

@Entity(name="SCORM_ATTEMPT_T")
@EntityListeners(value={ScormAttemptTDAO.class})
public class ScormAttemptT extends BasicEntity implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Column(name="ATTEMPT_ID")
    @Id
    private BigDecimal attemptId;

    @Column(name="CONTENT_PACKAGE_ID")
    private BigDecimal contentPackageId;

    @Column(name="COURSE_ID")
    private String courseId;

    @Column(name="LEARNER_ID")
    private String learnerId;

    @Column(name="LEARNER_NAME")
    private String learnerName;

    @Column(name="ATTEMPT_NUMBER")
    private BigDecimal attemptNumber;

    @Column(name="CREATED_ON")
    private Timestamp createdOn;

    @Column(name="MODIFIED_ON")
    private Timestamp modifiedOn;

    @Column(name="IS_SUSPENDED")
    private BigDecimal isSuspended;

    @Column(name="IS_NOT_EXITED")
    private BigDecimal isNotExited;

    public void setAttemptId(BigDecimal attemptId) {
        this.attemptId = attemptId;
    }

    public BigDecimal getAttemptId() {
        return attemptId;
    }

    public void setContentPackageId(BigDecimal contentPackageId) {
        this.contentPackageId = contentPackageId;
    }

    public BigDecimal getContentPackageId() {
        return contentPackageId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    public String getCourseId() {
        return courseId;
    }

    public void setLearnerId(String learnerId) {
        this.learnerId = learnerId;
    }

    public String getLearnerId() {
        return learnerId;
    }

    public void setLearnerName(String learnerName) {
        this.learnerName = learnerName;
    }

    public String getLearnerName() {
        return learnerName;
    }

    public void setAttemptNumber(BigDecimal attemptNumber) {
        this.attemptNumber = attemptNumber;
    }

    public BigDecimal getAttemptNumber() {
        return attemptNumber;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setModifiedOn(Timestamp modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public Timestamp getModifiedOn() {
        return modifiedOn;
    }

    public void setIsSuspended(BigDecimal isSuspended) {
        this.isSuspended = isSuspended;
    }

    public BigDecimal getIsSuspended() {
        return isSuspended;
    }

    public void setIsNotExited(BigDecimal isNotExited) {
        this.isNotExited = isNotExited;
    }

    public BigDecimal getIsNotExited() {
        return isNotExited;
    }
}
