package com.yung.scorm.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;

import com.yung.jpa.BasicEntity;
import com.yung.scorm.entity.dao.ScormDelimitDescTDAO;

@Entity(name="SCORM_DELIMIT_DESC_T")
@EntityListeners(value={ScormDelimitDescTDAO.class})
public class ScormDelimitDescT extends BasicEntity implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Column(name="DELIM_DESC_ID")
    @Id
    private BigDecimal delimDescId;

    @Column(name="DESC_NAME")
    private String descName;

    @Column(name="DEFAULT_VALUE")
    private String defaultValue;

    @Column(name="VALUE_SPM")
    private BigDecimal valueSpm;

    @Column(name="VALIDATOR")
    private BigDecimal validator;

    public void setDelimDescId(BigDecimal delimDescId) {
        this.delimDescId = delimDescId;
    }

    public BigDecimal getDelimDescId() {
        return delimDescId;
    }

    public void setDescName(String descName) {
        this.descName = descName;
    }

    public String getDescName() {
        return descName;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setValueSpm(BigDecimal valueSpm) {
        this.valueSpm = valueSpm;
    }

    public BigDecimal getValueSpm() {
        return valueSpm;
    }

    public void setValidator(BigDecimal validator) {
        this.validator = validator;
    }

    public BigDecimal getValidator() {
        return validator;
    }
}
