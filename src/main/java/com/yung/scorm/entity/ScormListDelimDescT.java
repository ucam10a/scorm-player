package com.yung.scorm.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;

import com.yung.jpa.BasicEntity;
import com.yung.scorm.entity.dao.ScormListDelimDescTDAO;

@Entity(name="SCORM_LIST_DELIM_DESC_T")
@EntityListeners(value={ScormListDelimDescTDAO.class})
public class ScormListDelimDescT extends BasicEntity implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Column(name="ELEM_DESC_ID")
    @Id
    private BigDecimal elemDescId;

    @Column(name="DELIM_DESC_ID")
    private BigDecimal delimDescId;

    @Column(name="SORT_ORDER")
    @Id
    private BigDecimal sortOrder;

    public void setElemDescId(BigDecimal elemDescId) {
        this.elemDescId = elemDescId;
    }

    public BigDecimal getElemDescId() {
        return elemDescId;
    }

    public void setDelimDescId(BigDecimal delimDescId) {
        this.delimDescId = delimDescId;
    }

    public BigDecimal getDelimDescId() {
        return delimDescId;
    }

    public void setSortOrder(BigDecimal sortOrder) {
        this.sortOrder = sortOrder;
    }

    public BigDecimal getSortOrder() {
        return sortOrder;
    }
}
