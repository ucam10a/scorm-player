package com.yung.scorm.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;

import com.yung.jpa.BasicEntity;
import com.yung.scorm.entity.dao.ScormListLaunchDataTDAO;

@Entity(name="SCORM_LIST_LAUNCH_DATA_T")
@EntityListeners(value={ScormListLaunchDataTDAO.class})
public class ScormListLaunchDataT extends BasicEntity implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Column(name="MANIFEST_ID")
    @Id
    private BigDecimal manifestId;

    @Column(name="LAUNCH_DATA_ID")
    private BigDecimal launchDataId;

    @Column(name="SORT_ORDER")
    @Id
    private BigDecimal sortOrder;

    public void setManifestId(BigDecimal manifestId) {
        this.manifestId = manifestId;
    }

    public BigDecimal getManifestId() {
        return manifestId;
    }

    public void setLaunchDataId(BigDecimal launchDataId) {
        this.launchDataId = launchDataId;
    }

    public BigDecimal getLaunchDataId() {
        return launchDataId;
    }

    public void setSortOrder(BigDecimal sortOrder) {
        this.sortOrder = sortOrder;
    }

    public BigDecimal getSortOrder() {
        return sortOrder;
    }
}
