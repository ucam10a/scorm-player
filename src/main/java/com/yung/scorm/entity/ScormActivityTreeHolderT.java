package com.yung.scorm.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Blob;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;

import com.yung.jpa.BasicEntity;
import com.yung.scorm.entity.dao.ScormActivityTreeHolderTDAO;

@Entity(name="SCORM_ACTIVITY_TREE_HOLDER_T")
@EntityListeners(value={ScormActivityTreeHolderTDAO.class})
public class ScormActivityTreeHolderT extends BasicEntity implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Column(name="HOLDER_ID")
    @Id
    private BigDecimal holderId;

    @Column(name="CONTENT_PACKAGE_ID")
    private BigDecimal contentPackageId;

    @Column(name="LEARNER_ID")
    private String learnerId;

    @Column(name="ACT_TREE")
    private Blob actTree;

    public void setHolderId(BigDecimal holderId) {
        this.holderId = holderId;
    }

    public BigDecimal getHolderId() {
        return holderId;
    }

    public void setContentPackageId(BigDecimal contentPackageId) {
        this.contentPackageId = contentPackageId;
    }

    public BigDecimal getContentPackageId() {
        return contentPackageId;
    }

    public void setLearnerId(String learnerId) {
        this.learnerId = learnerId;
    }

    public String getLearnerId() {
        return learnerId;
    }

    public void setActTree(Blob actTree) {
        this.actTree = actTree;
    }

    public Blob getActTree() {
        return actTree;
    }
}
