package com.yung.scorm.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;

import com.yung.jpa.BasicEntity;
import com.yung.scorm.entity.dao.ScormListDelimitersTDAO;

@Entity(name="SCORM_LIST_DELIMITERS_T")
@EntityListeners(value={ScormListDelimitersTDAO.class})
public class ScormListDelimitersT extends BasicEntity implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Column(name="ELEMENT_ID")
    @Id
    private BigDecimal elementId;

    @Column(name="DELIM_ID")
    private BigDecimal delimId;

    @Column(name="SORT_ORDER")
    @Id
    private BigDecimal sortOrder;

    public void setElementId(BigDecimal elementId) {
        this.elementId = elementId;
    }

    public BigDecimal getElementId() {
        return elementId;
    }

    public void setDelimId(BigDecimal delimId) {
        this.delimId = delimId;
    }

    public BigDecimal getDelimId() {
        return delimId;
    }

    public void setSortOrder(BigDecimal sortOrder) {
        this.sortOrder = sortOrder;
    }

    public BigDecimal getSortOrder() {
        return sortOrder;
    }
}