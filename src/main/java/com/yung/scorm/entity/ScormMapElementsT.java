package com.yung.scorm.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;

import com.yung.jpa.BasicEntity;
import com.yung.scorm.entity.dao.ScormMapElementsTDAO;

@Entity(name="SCORM_MAP_ELEMENTS_T")
@EntityListeners(value={ScormMapElementsTDAO.class})
public class ScormMapElementsT extends BasicEntity implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Column(name="DATAMODEL_ID")
    @Id
    private BigDecimal datamodelId;

    @Column(name="ELEMENT_ID")
    private BigDecimal elementId;

    @Column(name="ELEMENT_BINDING")
    @Id
    private String elementBinding;

    public void setDatamodelId(BigDecimal datamodelId) {
        this.datamodelId = datamodelId;
    }

    public BigDecimal getDatamodelId() {
        return datamodelId;
    }

    public void setElementId(BigDecimal elementId) {
        this.elementId = elementId;
    }

    public BigDecimal getElementId() {
        return elementId;
    }

    public void setElementBinding(String elementBinding) {
        this.elementBinding = elementBinding;
    }

    public String getElementBinding() {
        return elementBinding;
    }
}

