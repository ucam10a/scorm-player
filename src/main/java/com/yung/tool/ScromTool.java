package com.yung.tool;

import com.yung.scorm.model.Time;
import com.yung.servlet.GetScormContentServlet;

public class ScromTool {
	
	public static Time parseTime(String input) {
		Time ret = new Time();
		if (input == null || !input.startsWith("PT")) {
			return ret;
		}
		int idx = -1;
		input = input.substring(2);
		idx = input.indexOf("H");
		if (idx > 0) {
			String hour = input.substring(0, idx);
			ret.setHour(Double.valueOf(hour));
			input = input.substring(idx + 1);
		}
		idx = input.indexOf("M");
		if (idx > 0) {
			String min = input.substring(0, idx);
			ret.setMinute(Double.valueOf(min));
			input = input.substring(idx + 1);
		}
		idx = input.indexOf("S");
		if (idx > 0) {
			String sec = input.substring(0, idx);
			ret.setSecond(Double.valueOf(sec));
			input = input.substring(idx + 1);
		}
		return ret;
	}
	
	public static void main(String[] args) throws Exception {
		
		JavaLogDebugPrinter printer = new JavaLogDebugPrinter();
		Time tsp = parseTime("PT1M9.27S");
		
		printer.printObjectParam("tsp", tsp);
		
		String mime = GetScormContentServlet.guessMimeType(FileUtil.getFile("E:/test/sample.xlsx"));
		printer.printObjectParam("mime", mime);
		
	}
	
}
