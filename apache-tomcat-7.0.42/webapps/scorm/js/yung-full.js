/**
 * global variable object
 */
var yung_global_var = {
    _global_ajaxTimeout : null,
    YungClassProperty : {}
};

/**
 * Base component, like jQuery '$'
 * 
 */
window["$Y"] = function () {};

/**
 * register class to package
 * 
 * @param {$Class} type - class type
 */
window["$Y"]["reg"] = function (type) {
	if (type != null) {
		var className = type.classProp.name;
		window["$Y"]["register"](className, type);
	}
}

/**
 * register class to package
 * 
 * @param {string} className - class name
 * @param {$Class} type - class type
 */
window["$Y"]["register"] = function (className, type) {
    var clsArray = className.split(".");
    if (clsArray.length == 1) {
        return;
    }
    var idx = className.lastIndexOf(".");
    var packageName = className.substring(0, idx);
    var pack = _packageCheck(packageName);
    var clsName = className.substring(idx + 1, className.length);
    if (type != null) {
        if (pack[clsName] == null) {
            if (typeof type == "function") {
                pack[clsName] = type;
                return type;
            } else if (typeof type == "object") {
                pack[clsName] = type;
                return type;
            } else {
                throw "argument type is not function";
            }
        } else {
            throw className + " is already registered";
        }
    }
}

/**
 * import class to base component
 * 
 * @param {string} className - class name
 */
window["$Y"]["import"] = function (className) {
    if (typeof className != "string") {
        return;
    }
    var clsArray = className.split(".");
    if (clsArray.length == 1) {
        return;
    }
    var idx = className.lastIndexOf(".");
    var packageName = className.substring(0, idx);
    var pack = _packageCheck(packageName);
    var clsName = className.substring(idx + 1, className.length);
    if (clsName == '*') {
        for (var key in pack) {
            if (typeof pack[key] == 'function') {
                $Y[key] = pack[key];
            } else if (typeof pack[key] == 'object') {
                // for static import
                $Y[key] = pack[key];
            }
        }
    } else {
        if (typeof pack[clsName] == 'function') {
            $Y[clsName] = pack[clsName];
        } else if (typeof pack[clsName] == 'object') {
            // for static import
            $Y[clsName] = pack[clsName];
        } else if (pack[clsName] == null) {
            $Y.register(className);
            $Y[clsName] = pack[clsName];
        }
    }
}

/**
 * equal to import function
 * 
 */
window["$Y"]["def"] = window["$Y"]["import"];

/**
 * convenient to detact scroll end
 * 
 */
jQuery.fn.scrollEnd = function(callback, timeout) {          
    jQuery(this).scroll(function(){
    var $this = jQuery(this);
    if ($this.data('scrollTimeout')) {
      clearTimeout($this.data('scrollTimeout'));
    }
    $this.data('scrollTimeout', setTimeout(callback,timeout));
  });
};
/*
jQuery(window).scrollEnd(function(){
    alert('stopped scrolling');
}, 1000);
 */

var _base;
function _packageCheck(packages) {
    if (typeof packages == "string") {
        packages = packages.split(".");
    }
    _base = window;
    if (packages != null && packages.length > 0) {
        for (var i = 0; i < packages.length; i++) {
            if (_base[packages[i]] == null) {
                _base[packages[i]] = {};
            }
            _base = _base[packages[i]];
        }
    }
    return _base;
}

function _cacheClassProperty (clsName, key, value) {
    if (yung_global_var["YungClassProperty"] == null) {
        yung_global_var["YungClassProperty"] = {};
    }
    if (yung_global_var["YungClassProperty"][clsName] == null) {
        yung_global_var["YungClassProperty"][clsName] = {};
    }
    if (value != null) {
        if (jQuery.isArray(value)) {
            if (value.length != 0) {
                throw clsName + " initial property["+ key +"] Array should be empty!";
            }
            yung_global_var["YungClassProperty"][clsName][key] = new Array();
        } else if (typeof value == 'object') {
            if (jQuery.isPlainObject(value) == false) {
                throw clsName + " initial property["+ key +"] Object should be empty!";
            }
            yung_global_var["YungClassProperty"][clsName][key] = new Object();
        } else if (typeof value != 'string' && typeof value != 'number' && typeof value != 'boolean') {
            throw clsName + " initial property[" + key + "] type should be string, number, boolean, empty array or empty object";
        } else {
            yung_global_var["YungClassProperty"][clsName][key] = value;
        }
    } else {
        yung_global_var["YungClassProperty"][clsName][key] = null;
    }
}

function _getSuperClass (clsName) {
    return yung_global_var["YungClassProperty"][clsName]['superClass'];
}

function _getClassHierarchy(clsName) {
    var currentCls = clsName;
    var temp = [];
    temp.push(currentCls);
    while (true) {
        var currentCls = _getSuperClass(currentCls);
        if (currentCls != null) {
            temp.push(currentCls);
        } else {
            break;
        }
    }
    var ret = [];
    for (var i = temp.length - 1; i >= 0; i--){
        ret.push(temp[i]);
    }
    return ret;
}

function _replaceAll (targetStr, strFind, strReplace) {
    var index = 0;
    while (targetStr.indexOf(strFind, index) != -1) {
        targetStr = targetStr.replace(strFind, strReplace);
        index = targetStr.indexOf(strFind, index);
    }
    return targetStr;
}

/* Simple JavaScript Inheritance
 * By John Resig http://ejohn.org/
 * MIT Licensed.
 */
// Inspired by base2 and Prototype
(function(){
  var initializing = false, fnTest = /xyz/.test(function(){xyz;}) ? /\b_super\b/ : /.*/;
 
  // The base AbstractClass implementation (does nothing)
  this.AbstractClass = function(){};
 
  // Create a new AbstractClass that inherits from this class
  AbstractClass.extend = function(prop) {
    var _super = this.prototype;
    
    var unimplemented = [];
    if (typeof _super.classProp != 'undefined') {
        if (_super.classProp.name != '$Class') {
            var originalName = prop.classProp.name;
            var originalSuperName = _super.classProp.name;
            _cacheClassProperty(originalName, 'superClass', originalSuperName);
        }
        if (_super["classProp"]["unimplemented"] != null && _super["classProp"]["unimplemented"].length > 0) {
            if (jQuery.isArray(_super["classProp"]["unimplemented"]) == false) {
                throw "classProp.unimplemented is not Array";
            }
            for (var i = 0; i < _super["classProp"]["unimplemented"].length; i++) {
                unimplemented.push(_super["classProp"]["unimplemented"][i]);
            }
        }
    }
    
    // Instantiate a base class (but only create the instance,
    // don't run the init constructor)
    initializing = true;
    var prototype = new this();
    initializing = false;
    var cls = null;
    var pack = null;
    
    if (typeof prop["classProp"] == 'undefined') {
    	throw "Please define classProp in Class";
    }
    
    // Copy the properties over onto the new prototype
    for (var name in prop) {
      if (name === 'classProp') {
          try {
              Object.freeze(prop[name]);
          } catch (err) {
              // for IE quirk mode
          }
          var className = prop[name].name;
          if (yung_global_var["YungClassProperty"][className] == null) {
              yung_global_var["YungClassProperty"][className] = {};
          }
          var packages = className.split(".");
          if (packages.length > 1) {
            cls = packages[packages.length - 1];
            packages.splice(packages.length - 1, 1);
            pack = _packageCheck(packages);
          } else {
            cls = null;
            pack = null;
          }
      } else {
          if (typeof prop[name] != "function") {
              var originalName = prop.classProp.name;
              _cacheClassProperty(originalName, name, prop[name]);
          } else {
              var index = jQuery.inArray(name, unimplemented);
              if (index >= 0) {
                  unimplemented.splice(index, 1);
              }
          }
      }
      // Check if we're overwriting an existing function
      prototype[name] = typeof prop[name] == "function" &&
        typeof _super[name] == "function" && fnTest.test(prop[name]) ?
        (function(name, fn){
          return function() {
            var tmp = this._super;
           
            // Add a new ._super() method that is the same method
            // but on the super-class
            this._super = _super[name];
           
            // The method only need to be bound temporarily, so we
            // remove it when we're done executing
            var ret = fn.apply(this, arguments);        
            this._super = tmp;
           
            return ret;
          };
        })(name, prop[name]) :
        prop[name];
    }
    
    if (unimplemented != null && unimplemented.length > 0) {
        var todo = prop["classProp"]["unimplemented"];
        if (todo != null && todo.length > 0) {
            for (var i = 0; i < todo.length; i++) {
                var index = jQuery.inArray(todo[i], unimplemented);
                if (index >= 0) {
                    unimplemented.splice(index, 1);
                }
            }
        }
        if (unimplemented.length > 0) {
            throw "Class{" + prop["classProp"]["name"] + "} please implement method:" + JSON.stringify(unimplemented);    
        }
    }
    
    // The dummy class constructor
    function AbstractClass() {
      // block abstract or interface to create intance
      if (!initializing) {
          if (this["classProp"] != null && this["classProp"]["unimplemented"] != null && this["classProp"]["unimplemented"].length > 0) {
              throw "Class{" + this["classProp"]["name"] + "} is abstract or interface, can not create instance!";
          }   
      }
      // All construction is actually done in the init method
      if ( !initializing && this.init ) {
          if (typeof this.inheritProp == 'function') {
              this.inheritProp();
          }
          this.init.apply(this, arguments);
      }
    }
   
    // Populate our constructed prototype object
    AbstractClass.prototype = prototype;
   
    // Enforce the constructor to be what we expect
    AbstractClass.prototype.constructor = $Class;
 
    // And make this class extendable
    AbstractClass.extend = arguments.callee;
    
    if (pack != null && cls != null) {
        pack[cls] = AbstractClass;
    }
    return AbstractClass;
  };
})();

/*
 * JavaScript MD5
 * https://github.com/blueimp/JavaScript-MD5
 *
 * Copyright 2011, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * https://opensource.org/licenses/MIT
 *
 * Based on
 * A JavaScript implementation of the RSA Data Security, Inc. MD5 Message
 * Digest Algorithm, as defined in RFC 1321.
 * Version 2.2 Copyright (C) Paul Johnston 1999 - 2009
 * Other contributors: Greg Holt, Andrew Kepert, Ydnar, Lostinet
 * Distributed under the BSD License
 * See http://pajhome.org.uk/crypt/md5 for more info.
 */

/* global define */

;(function ($) {
  'use strict'

  /*
  * Add integers, wrapping at 2^32. This uses 16-bit operations internally
  * to work around bugs in some JS interpreters.
  */
  function safeAdd (x, y) {
    var lsw = (x & 0xffff) + (y & 0xffff)
    var msw = (x >> 16) + (y >> 16) + (lsw >> 16)
    return (msw << 16) | (lsw & 0xffff)
  }

  /*
  * Bitwise rotate a 32-bit number to the left.
  */
  function bitRotateLeft (num, cnt) {
    return (num << cnt) | (num >>> (32 - cnt))
  }

  /*
  * These functions implement the four basic operations the algorithm uses.
  */
  function md5cmn (q, a, b, x, s, t) {
    return safeAdd(bitRotateLeft(safeAdd(safeAdd(a, q), safeAdd(x, t)), s), b)
  }
  function md5ff (a, b, c, d, x, s, t) {
    return md5cmn((b & c) | (~b & d), a, b, x, s, t)
  }
  function md5gg (a, b, c, d, x, s, t) {
    return md5cmn((b & d) | (c & ~d), a, b, x, s, t)
  }
  function md5hh (a, b, c, d, x, s, t) {
    return md5cmn(b ^ c ^ d, a, b, x, s, t)
  }
  function md5ii (a, b, c, d, x, s, t) {
    return md5cmn(c ^ (b | ~d), a, b, x, s, t)
  }

  /*
  * Calculate the MD5 of an array of little-endian words, and a bit length.
  */
  function binlMD5 (x, len) {
    /* append padding */
    x[len >> 5] |= 0x80 << (len % 32)
    x[((len + 64) >>> 9 << 4) + 14] = len

    var i
    var olda
    var oldb
    var oldc
    var oldd
    var a = 1732584193
    var b = -271733879
    var c = -1732584194
    var d = 271733878

    for (i = 0; i < x.length; i += 16) {
      olda = a
      oldb = b
      oldc = c
      oldd = d

      a = md5ff(a, b, c, d, x[i], 7, -680876936)
      d = md5ff(d, a, b, c, x[i + 1], 12, -389564586)
      c = md5ff(c, d, a, b, x[i + 2], 17, 606105819)
      b = md5ff(b, c, d, a, x[i + 3], 22, -1044525330)
      a = md5ff(a, b, c, d, x[i + 4], 7, -176418897)
      d = md5ff(d, a, b, c, x[i + 5], 12, 1200080426)
      c = md5ff(c, d, a, b, x[i + 6], 17, -1473231341)
      b = md5ff(b, c, d, a, x[i + 7], 22, -45705983)
      a = md5ff(a, b, c, d, x[i + 8], 7, 1770035416)
      d = md5ff(d, a, b, c, x[i + 9], 12, -1958414417)
      c = md5ff(c, d, a, b, x[i + 10], 17, -42063)
      b = md5ff(b, c, d, a, x[i + 11], 22, -1990404162)
      a = md5ff(a, b, c, d, x[i + 12], 7, 1804603682)
      d = md5ff(d, a, b, c, x[i + 13], 12, -40341101)
      c = md5ff(c, d, a, b, x[i + 14], 17, -1502002290)
      b = md5ff(b, c, d, a, x[i + 15], 22, 1236535329)

      a = md5gg(a, b, c, d, x[i + 1], 5, -165796510)
      d = md5gg(d, a, b, c, x[i + 6], 9, -1069501632)
      c = md5gg(c, d, a, b, x[i + 11], 14, 643717713)
      b = md5gg(b, c, d, a, x[i], 20, -373897302)
      a = md5gg(a, b, c, d, x[i + 5], 5, -701558691)
      d = md5gg(d, a, b, c, x[i + 10], 9, 38016083)
      c = md5gg(c, d, a, b, x[i + 15], 14, -660478335)
      b = md5gg(b, c, d, a, x[i + 4], 20, -405537848)
      a = md5gg(a, b, c, d, x[i + 9], 5, 568446438)
      d = md5gg(d, a, b, c, x[i + 14], 9, -1019803690)
      c = md5gg(c, d, a, b, x[i + 3], 14, -187363961)
      b = md5gg(b, c, d, a, x[i + 8], 20, 1163531501)
      a = md5gg(a, b, c, d, x[i + 13], 5, -1444681467)
      d = md5gg(d, a, b, c, x[i + 2], 9, -51403784)
      c = md5gg(c, d, a, b, x[i + 7], 14, 1735328473)
      b = md5gg(b, c, d, a, x[i + 12], 20, -1926607734)

      a = md5hh(a, b, c, d, x[i + 5], 4, -378558)
      d = md5hh(d, a, b, c, x[i + 8], 11, -2022574463)
      c = md5hh(c, d, a, b, x[i + 11], 16, 1839030562)
      b = md5hh(b, c, d, a, x[i + 14], 23, -35309556)
      a = md5hh(a, b, c, d, x[i + 1], 4, -1530992060)
      d = md5hh(d, a, b, c, x[i + 4], 11, 1272893353)
      c = md5hh(c, d, a, b, x[i + 7], 16, -155497632)
      b = md5hh(b, c, d, a, x[i + 10], 23, -1094730640)
      a = md5hh(a, b, c, d, x[i + 13], 4, 681279174)
      d = md5hh(d, a, b, c, x[i], 11, -358537222)
      c = md5hh(c, d, a, b, x[i + 3], 16, -722521979)
      b = md5hh(b, c, d, a, x[i + 6], 23, 76029189)
      a = md5hh(a, b, c, d, x[i + 9], 4, -640364487)
      d = md5hh(d, a, b, c, x[i + 12], 11, -421815835)
      c = md5hh(c, d, a, b, x[i + 15], 16, 530742520)
      b = md5hh(b, c, d, a, x[i + 2], 23, -995338651)

      a = md5ii(a, b, c, d, x[i], 6, -198630844)
      d = md5ii(d, a, b, c, x[i + 7], 10, 1126891415)
      c = md5ii(c, d, a, b, x[i + 14], 15, -1416354905)
      b = md5ii(b, c, d, a, x[i + 5], 21, -57434055)
      a = md5ii(a, b, c, d, x[i + 12], 6, 1700485571)
      d = md5ii(d, a, b, c, x[i + 3], 10, -1894986606)
      c = md5ii(c, d, a, b, x[i + 10], 15, -1051523)
      b = md5ii(b, c, d, a, x[i + 1], 21, -2054922799)
      a = md5ii(a, b, c, d, x[i + 8], 6, 1873313359)
      d = md5ii(d, a, b, c, x[i + 15], 10, -30611744)
      c = md5ii(c, d, a, b, x[i + 6], 15, -1560198380)
      b = md5ii(b, c, d, a, x[i + 13], 21, 1309151649)
      a = md5ii(a, b, c, d, x[i + 4], 6, -145523070)
      d = md5ii(d, a, b, c, x[i + 11], 10, -1120210379)
      c = md5ii(c, d, a, b, x[i + 2], 15, 718787259)
      b = md5ii(b, c, d, a, x[i + 9], 21, -343485551)

      a = safeAdd(a, olda)
      b = safeAdd(b, oldb)
      c = safeAdd(c, oldc)
      d = safeAdd(d, oldd)
    }
    return [a, b, c, d]
  }

  /*
  * Convert an array of little-endian words to a string
  */
  function binl2rstr (input) {
    var i
    var output = ''
    var length32 = input.length * 32
    for (i = 0; i < length32; i += 8) {
      output += String.fromCharCode((input[i >> 5] >>> (i % 32)) & 0xff)
    }
    return output
  }

  /*
  * Convert a raw string to an array of little-endian words
  * Characters >255 have their high-byte silently ignored.
  */
  function rstr2binl (input) {
    var i
    var output = []
    output[(input.length >> 2) - 1] = undefined
    for (i = 0; i < output.length; i += 1) {
      output[i] = 0
    }
    var length8 = input.length * 8
    for (i = 0; i < length8; i += 8) {
      output[i >> 5] |= (input.charCodeAt(i / 8) & 0xff) << (i % 32)
    }
    return output
  }

  /*
  * Calculate the MD5 of a raw string
  */
  function rstrMD5 (s) {
    return binl2rstr(binlMD5(rstr2binl(s), s.length * 8))
  }

  /*
  * Calculate the HMAC-MD5, of a key and some data (raw strings)
  */
  function rstrHMACMD5 (key, data) {
    var i
    var bkey = rstr2binl(key)
    var ipad = []
    var opad = []
    var hash
    ipad[15] = opad[15] = undefined
    if (bkey.length > 16) {
      bkey = binlMD5(bkey, key.length * 8)
    }
    for (i = 0; i < 16; i += 1) {
      ipad[i] = bkey[i] ^ 0x36363636
      opad[i] = bkey[i] ^ 0x5c5c5c5c
    }
    hash = binlMD5(ipad.concat(rstr2binl(data)), 512 + data.length * 8)
    return binl2rstr(binlMD5(opad.concat(hash), 512 + 128))
  }

  /*
  * Convert a raw string to a hex string
  */
  function rstr2hex (input) {
    var hexTab = '0123456789abcdef'
    var output = ''
    var x
    var i
    for (i = 0; i < input.length; i += 1) {
      x = input.charCodeAt(i)
      output += hexTab.charAt((x >>> 4) & 0x0f) + hexTab.charAt(x & 0x0f)
    }
    return output
  }

  /*
  * Encode a string as utf-8
  */
  function str2rstrUTF8 (input) {
    return unescape(encodeURIComponent(input))
  }

  /*
  * Take string arguments and return either raw or hex encoded strings
  */
  function rawMD5 (s) {
    return rstrMD5(str2rstrUTF8(s))
  }
  function hexMD5 (s) {
    return rstr2hex(rawMD5(s))
  }
  function rawHMACMD5 (k, d) {
    return rstrHMACMD5(str2rstrUTF8(k), str2rstrUTF8(d))
  }
  function hexHMACMD5 (k, d) {
    return rstr2hex(rawHMACMD5(k, d))
  }

  function md5 (string, key, raw) {
    if (!key) {
      if (!raw) {
        return hexMD5(string)
      }
      return rawMD5(string)
    }
    if (!raw) {
      return hexHMACMD5(key, string)
    }
    return rawHMACMD5(key, string)
  }

  if (typeof define === 'function' && define.amd) {
    define(function () {
      return md5
    })
  } else if (typeof module === 'object' && module.exports) {
    module.exports = md5
  } else {
    $.md5 = md5
  }
})(this)

/**
 * check current browser properties
 * 
 * @return {string} browser properties
 */
com_yung_util_getbrowser = function () {
    var ret = yung_global_var["com_yung_util_getbrowser.result"];
    if (ret != null) {
        return ret;
    }
    var agt = navigator.userAgent.toLowerCase();
    if (agt.indexOf('msie') != -1) {
        this.browser = "msie";
        yung_global_var["com_yung_util_getbrowser.result"] = "msie";
        return "msie";
    }
    if (agt.indexOf('firefox') != -1) {
        this.browser = "firefox";
        yung_global_var["com_yung_util_getbrowser.result"] = "firefox";
        return "firefox";
    }
    if (agt.indexOf('safari') != -1) {
        this.browser = "safari";
        yung_global_var["com_yung_util_getbrowser.result"] = "safari";
        return "safari";
    }
    if (agt.indexOf('seamonkey') != -1) {
        this.browser = "seamonkey";
        yung_global_var["com_yung_util_getbrowser.result"] = "seamonkey";
        return "seamonkey";
    }
    if (agt.indexOf('netscape') != -1) {
        this.browser = "netscape";
        yung_global_var["com_yung_util_getbrowser.result"] = "netscape";
        return "netscape";
    }
    if (agt.indexOf('opera') != -1) {
        this.browser = "opera";
        yung_global_var["com_yung_util_getbrowser.result"] = "opera";
        return "opera";
    }
    if (agt.indexOf('windows') != -1 && agt.indexOf('gecko') != -1) {
        this.browser = "edge";
        yung_global_var["com_yung_util_getbrowser.result"] = "edge";
        return "edge";
    }
    yung_global_var["com_yung_util_getbrowser.result"] = "unknown";
    return "unknown";
}

/**
 * Basic Class, like JAVA Object
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var $Class = AbstractClass.extend({
	// for reflection
	classProp : { 
	    name : "$Class",
	    unimplemented : []
    },
    
    init: function(){
        throw "$Class can not be initialized!";
    },
    
    /** 
     * hash this instance to md5 string
     * 
     * @instance
     * @memberof $Class
     * @return {string} hash string
     */
    hashCode : function () {
        var json = JSON.stringify(this);
        if (this.classProp != null && this.classProp.name != null) {
            json = this.classProp.name + json;
        }
        return MD5Util.calc(json);
    },
    
    /** 
     * print to string
     * 
     * @instance
     * @memberof $Class
     * @return {string} result string
     */
    toString : function () {
        if (this.classProp != null && this.classProp.name != null) {
            return this.classProp.name + JSON.stringify(this);
        } else {
            return JSON.stringify(this);
        }
    },
    
    /** 
     * inherit property of from super class to current class
     * 
     * @private
     * @instance
     * @memberof $Class
     */
    inheritProp : function () {
        if (this.classProp != null && this.classProp.name != null) {
            var originClassName = this.classProp.name;
            var classArray = _getClassHierarchy(originClassName);
            for (var i = 0; i < classArray.length; i++) {
                var cls = classArray[i];
                var prop = yung_global_var["YungClassProperty"][cls];
                for (var key in prop) {
                    if (key != 'superClass') {
                        var value = prop[key];
                        if (value != null) {
                            if (jQuery.isArray(value)) {
                                this[key] = new Array();
                            } else if (typeof value == 'object') {
                                this[key] = new Object();
                            } else {
                                this[key] = value;
                            }
                        } else {
                            this[key] = null;
                        }
                    }
                }
            }
        }
    },
    
    /** 
     * replace string function
     * 
     * @instance
     * @memberof $Class
     * @param  {string} targetStr - target string
     * @param  {string} strFind - string to find
     * @param  {string} strReplace - string to replace
     * @return {string} result string
     */
    replaceAll : function(targetStr, strFind, strReplace) {
        return _replaceAll(targetStr, strFind, strReplace);
    }
});

function _validType (eleType, element) {
    if (typeof eleType == 'string') {
		var elementType = typeof element;
		if (eleType == 'string' && eleType == elementType) {
            return true;
        }
		if (eleType == 'number' && eleType == elementType) {
        	return true;
        }
		if (eleType == 'boolean' && eleType == elementType) {
        	return true;
        }
		if (eleType == 'function' && eleType == elementType) {
        	return true;
        }
	}
	if (typeof eleType == 'function' && element instanceof eleType) {
    	return true;
    }
	if (typeof eleType == 'function') {
        if (element["classProp"] != null && element["classProp"]["name"] != null) {
        	if (element["classProp"]["name"] == $Class.getClassName(eleType)) {
        		return true;
        	}
        }
	}
    if (typeof eleType == 'string') {
    	var elementType = typeof element;
    	if (eleType == 'object' && elementType == 'object') {
            return true;
        }
    }
    return false;
}

/**
 * Md5 hash utility
 *
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var MD5Util = {
	
	/**
	 * calculate string to md5 hash string
	 * 
	 * @memberof MD5Util
	 * @param {string} str - input string
	 * @return {string} hash string
	 */	
    calc : function (str) {
		return md5(str);
	}
};

/**
 * check current user locale
 * 
 * @return {string} user locale
 */
com_yung_util_getLang = function () {
    var ret = yung_global_var["com_yung_util_getLang.result"];
    if (ret != null) {
        return ret;
    }
    if (navigator.languages != undefined) {
        yung_global_var["com_yung_util_getLang.result"] = navigator.languages[0];
        return navigator.languages[0]; 
    } else {
        yung_global_var["com_yung_util_getLang.result"] = navigator.language;
        return navigator.language;
    }
}

function _isValidMd5(input)
{
    if (_validType('string', input) == false) {
        return false;
    }
    if (input == null || input == '') {
        return false;
    }
    if (/^[a-fA-F0-9]{32}$/.test(input)) {
        return true;
    }
    return false;
}
$Class.getInstance = function (arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10) {
    if (yung_global_var["instance"] == null) {
        yung_global_var["instance"] = {};
    }
    if (_validType('string', arg1) == false) {
        throw arg1 + " type is not string";
    }
    var isMd5 = _isValidMd5(arg1);
    if (isMd5) {
        return yung_global_var["instance"][arg1];
    } else {
        var className = arg1;
        var tokens = className.split(".");
        var type = null;
        var i = 0;
        while (i < tokens.length) {
            if (i == 0) {
                type = window[tokens[i]];
            } else {
                type = type[tokens[i]];
            }
            i++;
        }
        if (typeof type != 'function') {
            throw className + " is not constructor";
        }
        var id = arg2;
        var key = arg1 + "-" + arg2;
        var hashId = MD5Util.calc(key);
        if (yung_global_var["instance"][hashId] != null) {
            if (typeof yung_global_var["instance"][hashId] == 'string') {
                throw "cache dirty: " + yung_global_var["instance"][hashId];
            }
            return yung_global_var["instance"][hashId];
        }
        yung_global_var["instance"][hashId] = 'ready';
        var instance = new type(arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10);
        yung_global_var["instance"][hashId] = instance;
        return instance;
    }
}
$Class.validate = function (className, id) {
    if (className == null) {
        throw className + " is null";
    }
    if (id == null) {
        throw className + " method[init] first argument id is null";
    }
    if (_validType('string', className) == false) {
        throw className + " is not string";
    }
    if (_validType('string', id) == false) {
        throw className + " method[init] first argument id is not string";
    }
    var hashId = MD5Util.calc(className + "-" + id);
    if ($Class.getInstance(hashId) != 'ready') {
        throw "Please use $Class.getInstance('" + className + "', '" + id + "', arguments) to create instance!";
    }
}
$Class.getClassType = function (className) {
    var tokens = className.split(".");
    var type = null;
    var i = 0;
    while (i < tokens.length) {
        if (i == 0) {
            type = window[tokens[i]];
        } else {
            type = type[tokens[i]];
        }
        i++;
    }
    return type;
}
$Class.instanceOf = function (className, obj) {
    if (obj == null) {
        return false;
    }
    var clsType = $Class.getClassType(className);
    if (obj instanceof clsType) {
        return true;
    }
    return false;
}
$Class.getClassName = function (type) {
    if (type["classProp"] == null) {
        var superType = type.prototype;
        while (superType != null) {
            if (superType["classProp"] != null) {
                return superType["classProp"]["name"];
            }
            if (superType == type.prototype) {
                break;
            }
            superType = type.prototype;
        }
    } else {
        if (type["classProp"]["name"] != null) {
            return type["classProp"]["name"];
        }
    }
    if (type["name"] != null) {
        return type["name"];
    }
    return type + "";
}
$Class.checkFunction = function (type, funct) {
    if (typeof type[funct] != 'function') {
        var superType = type.prototype;
        while (superType != null) {
            if (typeof superType[funct] == 'function') {
                return true;
            }
            if (superType == type.prototype) {
                break;
            }
            superType = type.prototype;
        }
    }
    return false;
}


function fireInstanceMethod (hashId, method, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10) {
    if (_validType('string', hashId) == false) {
        throw "hashId is not string";
    }
    var isMd5 = _isValidMd5(hashId);
    if (isMd5 == false) {
        throw "hashId is not md5";
    }
    var instance = $Class.getInstance(hashId);
    if (instance == null) {
        throw "instance not exist";
    }
    if (typeof instance[method] == 'function') {
        instance[method](arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10);    
    }
}

var _searchIdx;
function binarySearchArray (arr, x, start, end, reverse, exact) {
	if (arr.length == 0) {
		return 0;
	}
	// Base Condtion
	if (start > end) {
		if (exact == true) {
			return -1;
		}
		return _searchIdx;
	}
	// Find the middle index
	var mid = Math.floor((start + end) / 2);
	_searchIdx = mid;
	// Compare mid with given key x
	if (typeof x.compareTo == 'function') {
		if (x.compareTo(arr[mid]) == 0) {
			if (exact) {
				return mid;
			} else {
				return -1;
			}
		}
	} else {
		if (arr[mid] === x){
			if (exact) {
				return mid;
			} else {
				return -1;
			}
		}
	}
	var bigger = null;
	var val = arr[mid];
	if (typeof x == 'number' || typeof x == 'string') {
		if (val > x) {
			bigger = true;
		} else {
			bigger = false;
		}
	} else if (typeof x.compareTo == 'function') {
		if (val.compareTo(x) > 0) {
			bigger = true;
		} else {
			bigger = false;
		}
	} else {
		return -1;
	}
	if (reverse == true) {
		if (bigger == true) {
			bigger = false;
		} else {
			bigger = true;
		}
	}
	if (bigger) {
		// If element at mid is greater than x,
		// search in the left half of mid
		return binarySearchArray(arr, x, start, mid - 1, reverse, exact);
	} else {
		// If element at mid is smaller than x,
		// search in the right half of mid
		return binarySearchArray(arr, x, mid + 1, end, reverse, exact);
	}
}

function expandSearchArray (arr, x, start) {
	if (arr.length == 0) {
		return -1;
	}
	if (arr[start] === x) {
		return start;
	}
	var leftIdx = start - 1;
	var rightIdx = start + 1;
	while (true) {
		if (leftIdx < 0 && rightIdx > arr.length) {
			break;
		}
		if (leftIdx >= 0 && arr[leftIdx] === x) {
			return leftIdx;
		}
		if (rightIdx < arr.length && arr[rightIdx] === x) {
			return rightIdx;
		}
		leftIdx--;
		rightIdx++;
	}
	return -1;
}
// global setting
yung_global_var.instance = {};
yung_global_var.YungClassProperty = {};

yung_global_var.widthOffset = {};
yung_global_var.widthOffset["msie"] = 1;
yung_global_var.widthOffset["firefox"] = 1;
yung_global_var.widthOffset["safari"] = 3;
yung_global_var.widthOffset["seamonkey"] = 2;
yung_global_var.widthOffset["netscape"] = 2;
yung_global_var.widthOffset["opera"] = 2;
yung_global_var.widthOffset["edge"] = 1;
yung_global_var.widthOffset["unknown"] = 2;

yung_global_var.heightOffset = {};
yung_global_var.heightOffset["msie"] = 1;
yung_global_var.heightOffset["firefox"] = 0;
yung_global_var.heightOffset["safari"] = 2;
yung_global_var.heightOffset["seamonkey"] = 0;
yung_global_var.heightOffset["netscape"] = 0;
yung_global_var.heightOffset["opera"] = 0;
yung_global_var.heightOffset["edge"] = 1;
yung_global_var.heightOffset["unknown"] = 0;

yung_global_var.leftOffset = {};
yung_global_var.leftOffset["msie"] = -2;
yung_global_var.leftOffset["firefox"] = 1;
yung_global_var.leftOffset["safari"] = 1;
yung_global_var.leftOffset["seamonkey"] = 0;
yung_global_var.leftOffset["netscape"] = 0;
yung_global_var.leftOffset["opera"] = 0;
yung_global_var.leftOffset["edge"] = 1;
yung_global_var.leftOffset["unknown"] = 0;

yung_global_var.topOffset = {};
yung_global_var.topOffset["msie"] = -1;
yung_global_var.topOffset["firefox"] = 1;
yung_global_var.topOffset["safari"] = 1;
yung_global_var.topOffset["seamonkey"] = 3;
yung_global_var.topOffset["netscape"] = 3;
yung_global_var.topOffset["opera"] = 3;
yung_global_var.topOffset["edge"] = 1;
yung_global_var.topOffset["unknown"] = 3;
// modified by: Yung Long Li
/**
 * Base64 utility
 *
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_Base64 = {
    classProp : { name : "com.yung.util.Base64"},
    _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
    
    /**
     * convert source string to base64 code
     *
     * @memberof com_yung_util_Base64
     * @param {String} input - source string
     * @return {String} base64 string
     */
    encode: function(input) {
        var output = "";
        var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
        var i = 0;

        input = this._utf8_encode(input);

        while (i < input.length) {

            chr1 = input.charCodeAt(i++);
            chr2 = input.charCodeAt(i++);
            chr3 = input.charCodeAt(i++);

            enc1 = chr1 >> 2;
            enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
            enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
            enc4 = chr3 & 63;

            if (isNaN(chr2)) {
                enc3 = enc4 = 64;
            } else if (isNaN(chr3)) {
                enc4 = 64;
            }

            output = output + this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) + this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);

        }

        return output;
    },
    
    /**
     * convert base64 code to source string
     *
     * @memberof com_yung_util_Base64
     * @param {String} input - base64 code
     * @return {String} source string
     */
    decode: function(input) {
        var output = "";
        var chr1, chr2, chr3;
        var enc1, enc2, enc3, enc4;
        var i = 0;

        input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

        while (i < input.length) {

            enc1 = this._keyStr.indexOf(input.charAt(i++));
            enc2 = this._keyStr.indexOf(input.charAt(i++));
            enc3 = this._keyStr.indexOf(input.charAt(i++));
            enc4 = this._keyStr.indexOf(input.charAt(i++));

            chr1 = (enc1 << 2) | (enc2 >> 4);
            chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
            chr3 = ((enc3 & 3) << 6) | enc4;

            output = output + String.fromCharCode(chr1);

            if (enc3 != 64) {
                output = output + String.fromCharCode(chr2);
            }
            if (enc4 != 64) {
                output = output + String.fromCharCode(chr3);
            }

        }

        output = this._utf8_decode(output);

        return output;

    },
    _utf8_encode: function(string) {
        string = string.replace(/\r\n/g, "\n");
        var utftext = "";

        for (var n = 0; n < string.length; n++) {

            var c = string.charCodeAt(n);

            if (c < 128) {
                utftext += String.fromCharCode(c);
            }
            else if ((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            }
            else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }

        }

        return utftext;
    },
    _utf8_decode: function(utftext) {
        var string = "";
        var i = 0;
        var c = c1 = c2 = 0;

        while (i < utftext.length) {

            c = utftext.charCodeAt(i);

            if (c < 128) {
                string += String.fromCharCode(c);
                i++;
            }
            else if ((c > 191) && (c < 224)) {
                c2 = utftext.charCodeAt(i + 1);
                string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                i += 2;
            }
            else {
                c2 = utftext.charCodeAt(i + 1);
                c3 = utftext.charCodeAt(i + 2);
                string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                i += 3;
            }

        }

        return string;
    }
};
$Y.reg(com_yung_util_Base64);
// Copyright (c) 2013 Pieroxy <pieroxy@pieroxy.net>
// This work is free. You can redistribute it and/or modify it
// under the terms of the WTFPL, Version 2
// For more information see LICENSE.txt or http://www.wtfpl.net/
//
// For more information, the home page:
// http://pieroxy.net/blog/pages/lz-string/testing.html
//
// LZ-based compression algorithm, version 1.4.4
// modified by: Yung Long Li

yung_global_var['com_yung_util_LZString.f'] = String.fromCharCode;
yung_global_var['com_yung_util_LZString.keyStrUriSafe'] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789*-$";
yung_global_var['com_yung_util_LZString.baseReverseDic'] = {};
/**
 * LZString utility
 *
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_LZString = {

	classProp : {
		name : "com.yung.util.LZString"
	},

	getBaseValue : function(alphabet, character) {
		if (!yung_global_var['com_yung_util_LZString.baseReverseDic'][alphabet]) {
			yung_global_var['com_yung_util_LZString.baseReverseDic'][alphabet] = {};
			for (var i = 0; i < alphabet.length; i++) {
				yung_global_var['com_yung_util_LZString.baseReverseDic'][alphabet][alphabet
						.charAt(i)] = i;
			}
		}
		return yung_global_var['com_yung_util_LZString.baseReverseDic'][alphabet][character];
	},

	// compress into a string that is already URI encoded
	compressToEncodedURIComponent : function(input) {
		if (input == null)
			return "";
		if (input == "")
			return "";
		return this._compress(input, 6, function(a) {
			return yung_global_var['com_yung_util_LZString.keyStrUriSafe']
					.charAt(a);
		});
	},

	/**
     * convert source string to LZString code
     *
     * @memberof com_yung_util_LZString
     * @param {String} input - source string
     * @return {String} LZString code
     */
	encode : function(input) {
		return this.compressToEncodedURIComponent(input);
	},

	// decompress from an output of compressToEncodedURIComponent
	decompressFromEncodedURIComponent : function(input) {
		if (input == null)
			return "";
		if (input == "")
			return "";
		input = input.replace(/ /g, "+");
		var _getBaseValue = this.getBaseValue;
		return this._decompress(input.length, 32, function(index) {
			return _getBaseValue(
					yung_global_var['com_yung_util_LZString.keyStrUriSafe'],
					input.charAt(index));
		});
	},

	/**
     * convert LZString code to source string
     *
     * @memberof com_yung_util_LZString
     * @param {String} input - LZString code
     * @return {String} source string
     */
	decode : function(input) {
		return this.decompressFromEncodedURIComponent(input);
	},

	compress : function(uncompressed) {
		return this._compress(uncompressed, 16, function(a) {
			return yung_global_var['com_yung_util_LZString.f'](a);
		});
	},
	_compress : function(uncompressed, bitsPerChar, getCharFromInt) {
		if (uncompressed == null)
			return "";
		var i, value, context_dictionary = {}, context_dictionaryToCreate = {}, context_c = "", context_wc = "", context_w = "", context_enlargeIn = 2, // Compensate for the first entry which should not count
		context_dictSize = 3, context_numBits = 2, context_data = [], context_data_val = 0, context_data_position = 0, ii;

		for (ii = 0; ii < uncompressed.length; ii += 1) {
			context_c = uncompressed.charAt(ii);
			if (!Object.prototype.hasOwnProperty.call(context_dictionary,
					context_c)) {
				context_dictionary[context_c] = context_dictSize++;
				context_dictionaryToCreate[context_c] = true;
			}

			context_wc = context_w + context_c;
			if (Object.prototype.hasOwnProperty.call(context_dictionary,
					context_wc)) {
				context_w = context_wc;
			} else {
				if (Object.prototype.hasOwnProperty.call(
						context_dictionaryToCreate, context_w)) {
					if (context_w.charCodeAt(0) < 256) {
						for (i = 0; i < context_numBits; i++) {
							context_data_val = (context_data_val << 1);
							if (context_data_position == bitsPerChar - 1) {
								context_data_position = 0;
								context_data
										.push(getCharFromInt(context_data_val));
								context_data_val = 0;
							} else {
								context_data_position++;
							}
						}
						value = context_w.charCodeAt(0);
						for (i = 0; i < 8; i++) {
							context_data_val = (context_data_val << 1)
									| (value & 1);
							if (context_data_position == bitsPerChar - 1) {
								context_data_position = 0;
								context_data
										.push(getCharFromInt(context_data_val));
								context_data_val = 0;
							} else {
								context_data_position++;
							}
							value = value >> 1;
						}
					} else {
						value = 1;
						for (i = 0; i < context_numBits; i++) {
							context_data_val = (context_data_val << 1) | value;
							if (context_data_position == bitsPerChar - 1) {
								context_data_position = 0;
								context_data
										.push(getCharFromInt(context_data_val));
								context_data_val = 0;
							} else {
								context_data_position++;
							}
							value = 0;
						}
						value = context_w.charCodeAt(0);
						for (i = 0; i < 16; i++) {
							context_data_val = (context_data_val << 1)
									| (value & 1);
							if (context_data_position == bitsPerChar - 1) {
								context_data_position = 0;
								context_data
										.push(getCharFromInt(context_data_val));
								context_data_val = 0;
							} else {
								context_data_position++;
							}
							value = value >> 1;
						}
					}
					context_enlargeIn--;
					if (context_enlargeIn == 0) {
						context_enlargeIn = Math.pow(2, context_numBits);
						context_numBits++;
					}
					delete context_dictionaryToCreate[context_w];
				} else {
					value = context_dictionary[context_w];
					for (i = 0; i < context_numBits; i++) {
						context_data_val = (context_data_val << 1)
								| (value & 1);
						if (context_data_position == bitsPerChar - 1) {
							context_data_position = 0;
							context_data.push(getCharFromInt(context_data_val));
							context_data_val = 0;
						} else {
							context_data_position++;
						}
						value = value >> 1;
					}

				}
				context_enlargeIn--;
				if (context_enlargeIn == 0) {
					context_enlargeIn = Math.pow(2, context_numBits);
					context_numBits++;
				}
				// Add wc to the dictionary.
				context_dictionary[context_wc] = context_dictSize++;
				context_w = String(context_c);
			}
		}

		// Output the code for w.
		if (context_w !== "") {
			if (Object.prototype.hasOwnProperty.call(
					context_dictionaryToCreate, context_w)) {
				if (context_w.charCodeAt(0) < 256) {
					for (i = 0; i < context_numBits; i++) {
						context_data_val = (context_data_val << 1);
						if (context_data_position == bitsPerChar - 1) {
							context_data_position = 0;
							context_data.push(getCharFromInt(context_data_val));
							context_data_val = 0;
						} else {
							context_data_position++;
						}
					}
					value = context_w.charCodeAt(0);
					for (i = 0; i < 8; i++) {
						context_data_val = (context_data_val << 1)
								| (value & 1);
						if (context_data_position == bitsPerChar - 1) {
							context_data_position = 0;
							context_data.push(getCharFromInt(context_data_val));
							context_data_val = 0;
						} else {
							context_data_position++;
						}
						value = value >> 1;
					}
				} else {
					value = 1;
					for (i = 0; i < context_numBits; i++) {
						context_data_val = (context_data_val << 1) | value;
						if (context_data_position == bitsPerChar - 1) {
							context_data_position = 0;
							context_data.push(getCharFromInt(context_data_val));
							context_data_val = 0;
						} else {
							context_data_position++;
						}
						value = 0;
					}
					value = context_w.charCodeAt(0);
					for (i = 0; i < 16; i++) {
						context_data_val = (context_data_val << 1)
								| (value & 1);
						if (context_data_position == bitsPerChar - 1) {
							context_data_position = 0;
							context_data.push(getCharFromInt(context_data_val));
							context_data_val = 0;
						} else {
							context_data_position++;
						}
						value = value >> 1;
					}
				}
				context_enlargeIn--;
				if (context_enlargeIn == 0) {
					context_enlargeIn = Math.pow(2, context_numBits);
					context_numBits++;
				}
				delete context_dictionaryToCreate[context_w];
			} else {
				value = context_dictionary[context_w];
				for (i = 0; i < context_numBits; i++) {
					context_data_val = (context_data_val << 1) | (value & 1);
					if (context_data_position == bitsPerChar - 1) {
						context_data_position = 0;
						context_data.push(getCharFromInt(context_data_val));
						context_data_val = 0;
					} else {
						context_data_position++;
					}
					value = value >> 1;
				}

			}
			context_enlargeIn--;
			if (context_enlargeIn == 0) {
				context_enlargeIn = Math.pow(2, context_numBits);
				context_numBits++;
			}
		}

		// Mark the end of the stream
		value = 2;
		for (i = 0; i < context_numBits; i++) {
			context_data_val = (context_data_val << 1) | (value & 1);
			if (context_data_position == bitsPerChar - 1) {
				context_data_position = 0;
				context_data.push(getCharFromInt(context_data_val));
				context_data_val = 0;
			} else {
				context_data_position++;
			}
			value = value >> 1;
		}

		// Flush the last char
		while (true) {
			context_data_val = (context_data_val << 1);
			if (context_data_position == bitsPerChar - 1) {
				context_data.push(getCharFromInt(context_data_val));
				break;
			} else
				context_data_position++;
		}
		return context_data.join('');
	},

	decompress : function(compressed) {
		if (compressed == null)
			return "";
		if (compressed == "")
			return null;
		return LZString._decompress(compressed.length, 32768, function(index) {
			return compressed.charCodeAt(index);
		});
	},

	_decompress : function(length, resetValue, getNextValue) {
		var dictionary = [], next, enlargeIn = 4, dictSize = 4, numBits = 3, entry = "", result = [], i, w, bits, resb, maxpower, power, c, data = {
			val : getNextValue(0),
			position : resetValue,
			index : 1
		};

		for (i = 0; i < 3; i += 1) {
			dictionary[i] = i;
		}

		bits = 0;
		maxpower = Math.pow(2, 2);
		power = 1;
		while (power != maxpower) {
			resb = data.val & data.position;
			data.position >>= 1;
			if (data.position == 0) {
				data.position = resetValue;
				data.val = getNextValue(data.index++);
			}
			bits |= (resb > 0 ? 1 : 0) * power;
			power <<= 1;
		}

		switch (next = bits) {
		case 0:
			bits = 0;
			maxpower = Math.pow(2, 8);
			power = 1;
			while (power != maxpower) {
				resb = data.val & data.position;
				data.position >>= 1;
				if (data.position == 0) {
					data.position = resetValue;
					data.val = getNextValue(data.index++);
				}
				bits |= (resb > 0 ? 1 : 0) * power;
				power <<= 1;
			}
			c = yung_global_var['com_yung_util_LZString.f'](bits);
			break;
		case 1:
			bits = 0;
			maxpower = Math.pow(2, 16);
			power = 1;
			while (power != maxpower) {
				resb = data.val & data.position;
				data.position >>= 1;
				if (data.position == 0) {
					data.position = resetValue;
					data.val = getNextValue(data.index++);
				}
				bits |= (resb > 0 ? 1 : 0) * power;
				power <<= 1;
			}
			c = yung_global_var['com_yung_util_LZString.f'](bits);
			break;
		case 2:
			return "";
		}
		dictionary[3] = c;
		w = c;
		result.push(c);
		while (true) {
			if (data.index > length) {
				return "";
			}

			bits = 0;
			maxpower = Math.pow(2, numBits);
			power = 1;
			while (power != maxpower) {
				resb = data.val & data.position;
				data.position >>= 1;
				if (data.position == 0) {
					data.position = resetValue;
					data.val = getNextValue(data.index++);
				}
				bits |= (resb > 0 ? 1 : 0) * power;
				power <<= 1;
			}

			switch (c = bits) {
			case 0:
				bits = 0;
				maxpower = Math.pow(2, 8);
				power = 1;
				while (power != maxpower) {
					resb = data.val & data.position;
					data.position >>= 1;
					if (data.position == 0) {
						data.position = resetValue;
						data.val = getNextValue(data.index++);
					}
					bits |= (resb > 0 ? 1 : 0) * power;
					power <<= 1;
				}

				dictionary[dictSize++] = yung_global_var['com_yung_util_LZString.f']
						(bits);
				c = dictSize - 1;
				enlargeIn--;
				break;
			case 1:
				bits = 0;
				maxpower = Math.pow(2, 16);
				power = 1;
				while (power != maxpower) {
					resb = data.val & data.position;
					data.position >>= 1;
					if (data.position == 0) {
						data.position = resetValue;
						data.val = getNextValue(data.index++);
					}
					bits |= (resb > 0 ? 1 : 0) * power;
					power <<= 1;
				}
				dictionary[dictSize++] = yung_global_var['com_yung_util_LZString.f'](bits);
				c = dictSize - 1;
				enlargeIn--;
				break;
			case 2:
				return result.join('');
			}

			if (enlargeIn == 0) {
				enlargeIn = Math.pow(2, numBits);
				numBits++;
			}

			if (dictionary[c]) {
				entry = dictionary[c];
			} else {
				if (c === dictSize) {
					entry = w + w.charAt(0);
				} else {
					return null;
				}
			}
			result.push(entry);

			// Add w+entry[0] to the dictionary.
			dictionary[dictSize++] = w + entry.charAt(0);
			enlargeIn--;

			w = entry;

			if (enlargeIn == 0) {
				enlargeIn = Math.pow(2, numBits);
				numBits++;
			}

		}
	}
};
$Y.reg(com_yung_util_LZString);
/**
 * AJAX tool, only support data type json and post action
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @property {string}  classProp.type - AJAX action type
 * @property {string}  classProp.dataType - AJAX data type
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_AjaxHttp = $Class.extend({
	
    classProp : { 
    	name : "com.yung.util.AjaxHttp",
    	type : 'POST',
    	dataType : 'json',
        loadGif : "data:image/png;base64,R0lGODlhSwBLAPcAAERERP7+/v39/fz8/Pn5+f////Dw8Pf39+bm5uvr6/b29tjY2H9/f5aWloKCgvv7+6CgoOrq6vHx8Y6Ojnx8fIWFheDg4Ofn59TU1OPj47GxsfPz85mZmaSkpLe3t9vb26ysrLW1tampqaGhoc3Nzby8vLq6urOzs6amptHR0YGBgX5+fpCQkI2NjcvLy5OTk3t7e+Hh4aenp9/f38XFxa6urpycnMLCwvr6+p6enoSEhIiIiO3t7d3d3cfHx4eHh/j4+MPDw/X19ZiYmOjo6M/Pz4qKiouLi7CwsMrKyra2tuLi4tbW1pSUlNnZ2eTk5MTExNra2u7u7sHBwb6+vu/v79PT05ubm97e3vT09Li4uMnJyfLy8uzs7NDQ0NLS0oaGhoyMjMzMzK+vr6KioqioqMbGxqqqqpGRkdzc3LS0tLm5uaOjo8jIyLu7u729vdfX13l5eZeXl3h4eJqamp+fn7Kysr+/v+Xl5c7OzpKSktXV1aWlpZ2dnenp6YODg62trYCAgI+Pj4mJiaurq319fcDAwJWVlXd3d3p6enZ2dkVFRUZGRkhISEtLS05OTk1NTXR0dHNzc29vb0pKSkdHR0xMTG5ubnJycnFxcXV1dUlJSVZWVlBQUFNTU1RUVGxsbFVVVWFhYWRkZHBwcG1tbWhoaGVlZWtra1JSUl9fX2BgYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH/C05FVFNDQVBFMi4wAwEAAAAh/wtYTVAgRGF0YVhNUDw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDIxIDc5LjE1NDkxMSwgMjAxMy8xMC8yOS0xMTo0NzoxNiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDpmODFlMTVmMC1jY2FlLTRkNjEtODhjMy1jMGE1MGRjZmE1YWMiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MTYzOThDRkVBODRGMTFFMzkyMzhFOEZGRjdFQ0E0OUUiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MTYzOThDRkRBODRGMTFFMzkyMzhFOEZGRjdFQ0E0OUUiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChNYWNpbnRvc2gpIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6ZjgxZTE1ZjAtY2NhZS00ZDYxLTg4YzMtYzBhNTBkY2ZhNWFjIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOmY4MWUxNWYwLWNjYWUtNGQ2MS04OGMzLWMwYTUwZGNmYTVhYyIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PgH//v38+/r5+Pf29fTz8vHw7+7t7Ovq6ejn5uXk4+Lh4N/e3dzb2tnY19bV1NPS0dDPzs3My8rJyMfGxcTDwsHAv769vLu6ubi3trW0s7KxsK+urayrqqmop6alpKOioaCfnp2cm5qZmJeWlZSTkpGQj46NjIuKiYiHhoWEg4KBgH9+fXx7enl4d3Z1dHNycXBvbm1sa2ppaGdmZWRjYmFgX15dXFtaWVhXVlVUU1JRUE9OTUxLSklIR0ZFRENCQUA/Pj08Ozo5ODc2NTQzMjEwLy4tLCsqKSgnJiUkIyIhIB8eHRwbGhkYFxYVFBMSERAPDg0MCwoJCAcGBQQDAgEAACH5BAUEAEUAIf4jUmVzaXplZCBvbiBodHRwczovL2V6Z2lmLmNvbS9yZXNpemUALAAAAABLAEsAAAjYAIsIHEiwoMGDCBMqXMiwocOHECNKnEixosWLGDNq3Mixo8ePIEOKHEmypMmTKFOqXMmypcuXMGPKnEmzps2bOHPq3Mmzp8+fQIMKHUq0qFGWcm66aRDG4RVPADqhKWmEQgWGZDYBAGCKYY6tYJuKDAN2yMJUYAHMWcgpLYBHIyuARaQQhtsXCiG43VpI5BGwSRV2ShtnIdS0cEfuiCOIoQ2tAEY5djt15gkbHBy+ePR2x9EiMD6LHk26tOnTqFOrXs26tevXsGPLnk27tu3buHPr3s27d5GAACH5BAUEAAEALBEAIQAqAAkAAAiCAAMIHEiwoMGDCAO0MFjjCoiEBD2gAQOR4JBOASCFGdgEgMcdFfs0EiiqYoArBXUEUANgIIAaEDEOxFQxVEFHAeq0FAhATsJLBVlARGnwkoadAQAAMjlQUkWZA3Eq9AigQsUmIwOoMtmg4EKBJ4aMYVqGThOmCiEFcKQCrVuIQAcGBAAh+QQFBAANACwQACEAKwAJAAAIjAAbCGxgiMnAgwgTKlTYJCGdVAAssVh4UEuTQRQRPmzQSdDAPgAGAsBIMcfAUxkF9kFI0hPCRhgopjqoKCUnhJYayAl5EICbhTUP6snIR6EiLJAQUkoh86CmlDMP5tTJswEANBnpDFyVssEQhB4FsnAEoBHWlBquyOkqUNCjBpB0IBQzZQ/buwiDCgwIACH5BAkEAA8ALBAAIQArAAkAAAiFAB8IJBSHj8CDCBMqXJjwisI5AB4AMOWD4UE3QwRZTNjnk0A9B+MkHLWxwyaBpjYKrJPwyIMxERECIGPRE8JEKhU+erBiYcqFDHIiHAOUUMyDACAIfYBzo02EOx9ISihqY52TD0guPQjyASBSEQGIuqNSzQMbSw8JfGQkoQ1TNLfKdYAwIAAh+QQJBABMACwQACEAKwAJAAAIjwCZCGwD5Y7AgwgTKlyYUI7CMJsAbGLAEGGJBmEqJrziCUAnNAcnABg58odGMhEBnNIosA/JkUeYJElJctOUiqleJmKp6iWAR0yU+Bx5giEMnyAryhgKAMYWmiM3vcH5EhHLji+BMgHj849GDilHsWQiB+nBQBE3ORhrxwaHsQLldPppEmESEzfh6kVIAWFAACH5BAkEABgALBAAIQArAAkAAAiMADEIxIBloMGDCBMqbIBwwiUAowQpNDiGg56JB6+kAtBpwkBBmwCIBHAF44uQAEoZwoghx0iRRwROeglAFcZOL1uw5EQTEoYMNEWWUAiF5g6MKIICmIMBFU1ON3Oy3PjSJ4ZBNJtgnIAS1BuWDWiyGKhnVM1DLDHYsUEnrcAmjwBAqoCwjdu7CBMZDAgAIfkECQQAEAAsEQAhACoACQAACJQAIQgcSLAgQR9FDCoceKQgmziiVMhYOPCMKAAA/jChOJBDJwCQGkLg8xFjIw0U7TTCiDERRwhyWGKsAIGCTABzKBq5CQAQx1A3KUEwdTMURaI3R1DswxPAJBg3FVEcxFMEx5IsLUFAshJjJUIUAUV9eehmC4FnVoRCVOPlGVUYYZB4CWGCJQCUAtGlSCPJ3oGlCAYEACH5BAkEAAcALBEAIQAqAAkAAAibAA8IHEiwoEA3LRTpYGGwoUA9BgM5iGPCocAzjgBoBADK4kAOqQA8mjDw0sZRFRu28bRx4xGPNloC2HHgh8w/DnPI1OgIjkVOMh8dANoSYkMdOzVycIgiqSQHMh04HJKUEhOLIVtaOuDD1EZRWhySyChTh8dDMhkeMKSoyR81HslsapmJhMcDhywBsGT2bkMtglAheuF3oCKCAQEAIfkECQQACAAsEQAgACoACwAACJgAEQgcSLAgQSYGEypcuFBQKQCQVNRgSLCJQTaHclAMBKBjx04dKHJIBaATi4E7PMJYeMWjS1AMW7o0goCPSwAhE/656TFnQk43ISHIcZNmQjA8O85RKCKpIjY32ShckRTAiIWebloSqMNjnIWHkppiOORmi4F0WgyhiOgmJRkUWTyCqIOiwiuqADSKhMSuQER+GY75EjhhQAAh+QQJBAAKACwRACEAKgAJAAAImAAVCBxIsKBAJSpGaWJhsKFADgbf7FjjcCCHTQAyArjkpqJAG54ApGIocAjGTRAdQqGkUWMgj31aAgijYArGjJsAOZwgEyeUipxkPlKgR6Ymh6V6ZlTikJDSQlRuAth0xmELpZu0VAwlVCCdk00qlpCqEYzHKzJfDNQCo4ZHBUbInnrz9lAnAI92vHV4hgEnU39+7lWwgmBAACH5BAkEABYALBEAIQAqAAkAAAiOAC0IHEiwoEBDFY7AgGCwoUAjDWE4JGhDFYCLABJNHKinE4BHLAZ2sBjKzkZTGDEO2fgiJYAWAldh9DRxiMuLozZ6TLlpSyCXQRw2uXkRgUM9RG9YsHgxVU2iqnS63KQ0BycAntRsPHXz0EYWLv8QPLFRIIurGB2UtfACEoBNYNY2XDNHEAOGci2o2TIwIAAh+QQJBAAHACwRACEAKwAJAAAIjwAPCBxIsODAOoIEaTDIUMMOTRP4GEzA0KApABgZoak4UBDGj00GQjHFyNRCjhQ+ZhzCUY3KjxAElvooiuOZlxhVcDyCE4COA1koqexRsUbPUxwT9RwlkybHED0ZcATT04HAIBdFaeF4QBHOIxyN4iRDMAPXgao+Mjp0VsdLI2c5NlnBwkbcA2wGmapAZ2BAACH5BAkEAA0ALBIAIAAqAAsAAAieABsIHEiwoMGDBsXYkDOkDcKHA93A6ANxYB9LADJuklPxYKBGGUWNgQgho8mMegRSIIWmIouTADrVQCgGEsyMjbSsMBkGYqebFBCOuGnyhSKTox5eIcoJYR2iGQsVMnnE580dCHNABWCjwQ5ULCqCgQnpDEIvjog2GtlxoCKQADiVgUiHaIW2Bc+gGtSWDlwAjcDgHXzwhhwHTewMDAgAIfkECQQACQAsEgAgACoACwAACJQAEwgcSLCgwYMGbagQRCEHwocE1TiEKJABgIsXM1E8yIHTxTkmIAbCSPKSwCuqGlAc0Yikqoc5SMrs0wfjC4ikZAKogxCTTow7RGEMBNGjzCsIG/y8WMgGRhYQC+nsgzDn0goJ+qg6RLFOS4yoHtZcqnLjQA6pLoKaAlHRT1FmDSrRYlaSTFE04uo12EFFoB9IBQYEACH5BAkEABkALBIAIAAqAAsAAAiWADMIHEiwoMGDBqG0+NFADcKHBC9AJDhhE4CLm8BMPDgi0yZOGiEKukjyogqBdOawmCiDU0kKD31YLIlxxJWZfSAaoQkgBEI0PEmiCUQSFcQjPOkgZBH04ik6M69AHMRTCcKRTSdkGKIpJ0QynUrCeHhnJk8+GwmeQQXgUciHLYL+SWuQCV0jZgFoosv34BgjoPScGRgQACH5BAkEADsALBIAIAAqAAsAAAiSAHcIHEiwoMGDB89k6oCwoUOHbE4BmKhqwkOELS7paPKww6aJIDcNEcghDouLo0ACOORQosqJn3bY+Aigj8MGL1M1JPNSJRoHIC85hNGzBUKiPSfKmDnRhsMmLzs1PJN04p8dQxTZfFhKZR2HopJ2InGR4JsXpyhweCiHpsorZeMehMAJZKe1cvMWbKJnUJ6BAQEAIfkECQQAMQAsEgAgACoACwAACJUAYwgcSLCgwYMH7dzgg7ChQ4dnCm0CACCTjIcIr6iCUeMhIVUUQ3IiI5BDnCYYI4WkBMghg5AwTcW4MhEABIcQYAIA1VBLTZ0ANAQKeckhKp2PuCCkATSkCJoUbTisozNOQyVNKYaIMURRDoyFQm4S4RBRU00YDeoJ86PMQw2cdFLSkLbuQSSTQqI6Y7evQQ1jlBAMCAAh+QQJBAAJACwSACAAKgALAAAIjgATCBxIsKDBgwjhIFzIsGGCH5wAcKLgEOGZPocqggLAkeOqgXIQvXCooxFHU3cYDurIskUCDiYB2GA4BRLLHwxVsexoKQGDjqAYeti5YuGTnSzJwORIh2GRmBz1MIyItGeCQ5GuOFwKYJQJhj+QAjBSseAIDhNIOBy1c5SHsnAN/ugEANKRNXHzFhxTMCAAIfkECQQADAAsEgAhACoACQAACIAAGQgcSLCgwYMG+awYBYMOwocDXRwqA3GgAwAYMUoaSCdRg4oM+nDC+KeijYwo9YTchLFPRVQom0C8iBLjHAYqMqKCaKbmzoemamLkxMAGSwAuIcLM+AJiTqGKBHJQVAekHU8kK9YRCiAMSINQjJD5mqhmoK9oH9JZwSlRi4EBAQAh+QQJBABGACwSACEAKQAJAAAIgQCNCBxIsKDBgwaR/JGzIgfChwNPeIA4cMgmABg3CRLIIU4TikZQnMIIg6KdixgzqrGBsg/FUikBjID4I2ZKFYFSXoLYxeYgiIpsYhx0BeUViqZi2oBYQSiAFUYOzVlKkY0qjJgoykCZchMgkAZBnAE7iOsml2DTHoQw6IiKMQIDAgAh+QQJBAAbACwSACEAKQAJAAAIgwA3CBxIsKDBgwaZ9NEzxAXChwNP2IE4kI4oABhVoRA4ZM4Eihv4jMI4B+QpjCg5TbmyCaMNiqZQAuAA8YVMlEcCoSwFkcfNChCH3MSYiE5LAFdgyhwCUdBQAA02DNH0kuIIVRgxUWSCVeanOyANgjgTNkxXAJ48hF2LMAkaB0amCAwIACH5BAkEAAsALBIAIQApAAkAAAh7ABcIHEiwoMGDBzWoIISwIUE1IRwShAGgIqY7AukkOiRxgYxTFSl0pFCxpKkFEDZVrCMRVUkAVxyeeVnShoOSkxxKobnDYQuaFU3ZUAmATsuXNhyeAApAzgIOiEZ0zLGqoqKOc2ie6ngQ0BmuCySVHNUGrNmDHS7FFBgQACH5BAkEADUALBIAIQApAAkAAAh9AGsIHEiwoMGDCEO0QciQ4Ak1DQnqeQRgk46BHOK8iFgDxSkAAGBw3AGyZKEadTaBvBIRVUkANiJSfLnJB4OSlxp2eQngR8MKPEFqyaESAJ2Ipl5yiNiJ56Y7NYYoysGRjSqQkTi24AmG48ExIrzWGKRy0w6xaBFOMeRhYEAAIfkECQQAHwAsEgAhACkACQAACHkAPwgcSLCgwYMIiSBcWPDECYYED00CgOrQwCFzXkD8wGcUAAAUNjbZ9PEjmg9XSAKoA9FUSQAQIGZ6CUDUh0AlSzFMQPMHwyw0P/qgo9JGy5dXIE58qUqgHE0xIfZR9RHTRhYqPxrdaBCECK4fjrhUlRSsWYR7CAYEACH5BAUoADcALBIAIQApAAkAAAh4AG8IHEiwoMGDCBMqJHhCzcKBHSalijNiIIc4eh6iOAUAAAyNmzoC2HTlBoeQAGwsRCUSQJ2FmFoCkHQjkMhLCiXI/LGQk0wABq6gLKnQVMscMGViEjhEUZ+HbFR1jPSQDsqRDR4eBHFG6w0UiDZFGuK1bEIhBAMCADs="
    },
    
    /**
     * action url
     * @member {string}
     * @instance
     * @memberof com_yung_util_AjaxHttp
     */
    actionUrl : null,
    
    /**
     * form id for sending parameter
     * @member {string}
     * @instance
     * @memberof com_yung_util_AjaxHttp
     */
    formId : null,
    
    
    /**
     * Callback for AJAX reponse
     *
     * @callback AJAXCallback
     * @param {object} response - response data object
     * @param {com_yung_util_AjaxHttp} self - this com_yung_util_AjaxHttp
     * @param {object} caller - caller which pass by send method
     */
    /**
     * AJAX callback function
     * @member {AJAXCallback}
     * @instance
     * @memberof com_yung_util_AjaxHttp
     */
    callback : null,
    
    /**
     * show loading image, default true
     * @member {boolean}
     * @instance
     * @memberof com_yung_util_AjaxHttp
     */
    loading : true,
    
    /**
     * show error message when AJAX error occurs, default true
     * @member {boolean}
     * @instance
     * @memberof com_yung_util_AjaxHttp
     */
    showError : true,
    
    /**
     * show AJAX loading function
     * @member {function}
     * @instance
     * @memberof com_yung_util_AjaxHttp
     */
    ajaxLoadingShow : null,
    
    /**
     * hide AJAX loading function
     * @member {function}
     * @instance
     * @memberof com_yung_util_AjaxHttp
     */
    ajaxLoadingHide : null,
    
    /**
	 * constructor
     * @memberof com_yung_util_AjaxHttp
     * @param  {string} actionUrl - action url
     * @param  {string} formId - from id
     * @param  {AJAXCallback} callback - ajax callback
     * @param  {function} ajaxLoadingShow - function to show loading image
     * @param  {function} ajaxLoadingHide - function to hide loading image
     */
    init: function(actionUrl, formId, callback, ajaxLoadingShow, ajaxLoadingHide){
        this.actionUrl = actionUrl;
        if (formId) this.formId = formId;
        if (callback) this.callback = callback;
        this.loading = true;
        this.showError = true;
        if (typeof ajaxLoadingShow == 'function' && typeof ajaxLoadingHide == 'function') {
            // customized loading and function
            this.ajaxLoadingShow = ajaxLoadingShow;
            this.ajaxLoadingHide = ajaxLoadingHide;
        } else {
            // use standard loading
            com_yung_util_AjaxHttp.createLoadingDiv();
            var browser = com_yung_util_getbrowser();
            if (browser == 'msie') {
                com_yung_util_AjaxHttp.setImage(this.classProp.loadGif, this);
            }
            this.ajaxLoadingShow = function () {
            	var browser = com_yung_util_getbrowser();
            	var windowHeight = (window.innerHeight || document.documentElement.scrollHeight || 0);
                if (windowHeight < jQuery(document).height()) {
                    // use maximum height
                    windowHeight = jQuery(document).height();
                }
                if (browser == 'msie') {
            		windowHeight = windowHeight - 3;
            	}
                var windowWidth = (window.innerWidth || document.documentElement.scrollWidth || 0);
                if (windowWidth < jQuery(document).width()) {
                    // use maximum width
                    if (browser != 'msie') {
                        windowWidth = jQuery(document).width();
                    }
                }
                if (browser == 'msie') {
                	windowWidth = windowWidth - 3;
            	}
                if (jQuery("#loadingCSS-imgDiv")[0] != null) {
                    jQuery("#loadingCSS").css("width", windowWidth + "px");
                    jQuery("#loadingCSS").css("height", windowHeight + "px");
                    jQuery("#loadingCSS").css("left", "0px");
                } else {
                    jQuery("#yung-backgroundDiv").css("width", windowWidth + "px");
                    jQuery("#yung-backgroundDiv").css("height", windowHeight + "px");
                }
                jQuery("#loadingCSS").css("display", "block");
            };
            this.ajaxLoadingHide = function () {
                jQuery("#loadingCSS").css("display", "none");
                var blinkTxt = jQuery("#text-blink")[0];
                if (blinkTxt != null) {
                    clearInterval(yung_global_var["_global_text_blink"]);
                }
            };
        }
        return this;
    },
    
    /** 
     * before send
     *
     * @private
     * @instance
     * @memberof com_yung_util_AjaxHttp
     */
    beforeSend : function() {
        if (this.loading) {
            if (yung_global_var["_global_ajaxTimeout"] == null) {
                yung_global_var["_global_ajaxTimeout"] = setTimeout(this.ajaxLoadingShow, 500);
            } else {
                clearTimeout(yung_global_var["_global_ajaxTimeout"]);
                delete yung_global_var["_global_ajaxTimeout"];
                yung_global_var["_global_ajaxTimeout"] = setTimeout(this.ajaxLoadingShow, 500);
            }
        }
    },
    
    /** 
     * after send
     *
     * @private
     * @instance
     * @memberof com_yung_util_AjaxHttp
     */
    sendComplete : function () {
        clearTimeout(yung_global_var["_global_ajaxTimeout"]);
        delete yung_global_var["_global_ajaxTimeout"];
        this.ajaxLoadingHide();
    },
    
    fixImageWhenScroll : function () {
        var imgDiv = jQuery("#loadingCSS-imgDiv");
        var imgWidth = imgDiv.width();
        var imgHeight = imgDiv.height();
        var position = new com.yung.util.Position(imgDiv);
        var pos = position.getCenterPosition(imgWidth, imgHeight);
        imgDiv.css("position", "absolute");
        imgDiv.css("top", pos.top + "px");
        imgDiv.css("left", pos.left + "px");
    },
    
    /** 
     * set show loading flag
     *
     * @instance
     * @memberof com_yung_util_AjaxHttp
     * @param  {boolean} loading - show loading flag
     */
    setLoading : function(loading){
        this.loading = loading;
    },
    
    /** 
     * set show error flag
     *
     * @instance
     * @memberof com_yung_util_AjaxHttp
     * @param  {boolean} loading - show error flag
     */
    setShowError : function(showErr){
        this.showError = showErr;
    },
    
    /** 
     * send request
     *
     * @instance
     * @memberof com_yung_util_AjaxHttp
     * @param  {object} caller - caller for callback use
     */
    send : function(caller){
        if (this.actionUrl == null) {
            alert("please define actionUrl!");
            return;
        }
        // show load.gif if more than half second
        this.beforeSend();
        
        var params = "";
        // param serialize
        if (this.formId != null) {
            if (jQuery('#' + this.formId)[0] != null) {
                params = jQuery('#' + this.formId).serialize();
            } else if (jQuery('form[name="' + this.formId + '"]')[0] != null) {
                params = jQuery('form[name="' + this.formId + '"]').serialize();
            }
        }
        //alert("params ok");
        
        var callback = this.callback;
        var self = this;
        
        jQuery.ajax({
            type: self.classProp.type,
            dataType: self.classProp.dataType,
            error: function(x, status, error) {
                self.sendComplete();
                if (self.showError) alert("An ajax error occurred: " + status + ", Error: " + error + ", actionUrl: " + self.actionUrl);
            },
            url: this.actionUrl,
            data: params,
            jsonpCallback: callback,
            success:function(result){
                self.sendComplete();
                if (typeof result == 'string') {
                    result = JSON.parse(result);
                }
                this.jsonpCallback(result, self, caller);
            }
        });
    }
});
com_yung_util_AjaxHttp.createLoadingDiv = function () {
    var loading = jQuery("#loadingCSS");
    if (loading[0] == null) {
        var windowHeight = (window.innerHeight || document.documentElement.scrollHeight || 0);
        if (windowHeight < jQuery(document).height()) {
            // use maximum height
            windowHeight = jQuery(document).height();
        }
        var windowWidth = (window.innerWidth || document.documentElement.scrollWidth || 0);
        if (windowWidth < jQuery(document).width()) {
            // use maximum width
            windowWidth = jQuery(document).width();
        }
        var template = new com.yung.util.BasicTemplate();
        var data = {};
        data["windowHeight"] = windowHeight;
        data["windowWidth"] = windowWidth;
        template.add('<div id="loadingCSS" style="display:none" tabindex="1" >');
        template.add('    <div id="yung-backgroundDiv" style="width: {{windowWidth}}px; height: {{windowHeight}}px; position: absolute; text-align: left: 0px; top: 0px; vertical-align: middle; z-index: 15000; -moz-opacity: .85; opacity: .85; filter: alpha(opacity=85); background: none 0px 0px repeat scroll rgb(255, 255, 255);" >');
        template.add('        <table class="loading_gif_center" >');
        template.add('            <tr><td>');
        template.add('                <div id="loadingCSS-content" class="loader">Loading...</div>');
        template.add('            </td></tr>');
        template.add('        </table>');
        template.add('    </div>');
        template.add('</div>');
        jQuery("body").append(template.toHtml(data));
    }
};
com_yung_util_AjaxHttp.setLoadingText = function (text, self) {
    if (jQuery("#loadingCSS-imgDiv")[0] == null) {
        var windowHeight = (window.innerHeight || document.documentElement.scrollHeight || 0);
        if (windowHeight > jQuery(document).height()) {
            windowHeight = jQuery(document).height();
        }
        var windowWidth = (window.innerWidth || document.documentElement.scrollWidth || 0);
        if (windowWidth > jQuery(document).width()) {
            windowWidth = jQuery(document).width();
        }
        jQuery("#loadingCSS").attr("style", "width: " + windowWidth + "px; height: " + windowHeight + "px; display: none; position: absolute; text-align: left: 0px; top: 0px; vertical-align: middle; z-index: 15000; -moz-opacity: .85; opacity: .85; filter: alpha(opacity=85); background: none 0px 0px repeat scroll rgb(255, 255, 255);");
        var template = new com.yung.util.BasicTemplate();
        var data = {};
        data["text"] = text;
        template.add('<table id="text-blink-table" style="position: absolute; width: 100%;" align="center" >');
        template.add('    <tr><td align="center" >');
        template.add('        <div id="loadingCSS-imgDiv" style="">');
        template.add('            <span id="text-blink">{{text}}</span>');
        template.add('        </div>');
        template.add('    </td></tr>');
        template.add('</table>');
        jQuery("#loadingCSS").html(template.toHtml(data));
        if (com_yung_util_getbrowser() == 'msie') {
            self.fixImageWhenScroll();
            jQuery(window).scrollEnd(self.fixImageWhenScroll, 150);
        }
    }
};
com_yung_util_AjaxHttp.setImage = function (imgSrc, self) {
    if (jQuery("#loadingCSS-imgDiv")[0] == null) {
        var windowHeight = (window.innerHeight || document.documentElement.scrollHeight || 0);
        if (windowHeight > jQuery(document).height()) {
            windowHeight = jQuery(document).height();
        }
        var windowWidth = (window.innerWidth || document.documentElement.scrollWidth || 0);
        if (windowWidth > jQuery(document).width()) {
            windowWidth = jQuery(document).width();
        }
        jQuery("#loadingCSS").attr("style", "width: " + windowWidth + "px; height: " + windowHeight + "px; display: none; position: absolute; text-align: left: 0px; top: 0px; vertical-align: middle; z-index: 15000; -moz-opacity: .85; opacity: .85; filter: alpha(opacity=85); background: none 0px 0px repeat scroll rgb(255, 255, 255);");
        var template = new com.yung.util.BasicTemplate();
        var data = {};
        data["imgSrc"] = imgSrc;
        template.add('<table class="loading_gif_center" style="width: 100%; height: 100%;" >');
        template.add('    <tr><td align="center" >');
        template.add('        <div id="loadingCSS-imgDiv" style="">');
        template.add('            <img id="pageLoading" src="{{imgSrc}}" />');
        template.add('        </div>');
        template.add('    </td></tr>');
        template.add('</table>');
        jQuery("#loadingCSS").html(template.toHtml(data));
        if (com_yung_util_getbrowser() == 'msie') {
            self.fixImageWhenScroll();
            jQuery(window).scrollEnd(self.fixImageWhenScroll, 150);
        }
    }
};

/**
 * AJAX upload tool
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_AjaxHttp
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_AjaxUpload = com_yung_util_AjaxHttp.extend({
    
	classProp : { 
		name : "com.yung.util.AjaxUpload"
	},
	
	/**
	 * constructor
     * @memberof com_yung_util_AjaxUpload
     * @param  {string} actionUrl - action url
     * @param  {string} formId - from id
     * @param  {AJAXCallback} callback - ajax callback
     * @param  {function} ajaxLoadingShow - function to show loading image
     * @param  {function} ajaxLoadingHide - function to hide loading image
     */
    init : function (actionUrl, formId, callback, ajaxLoadingShow, ajaxLoadingHide) {
        this._super(actionUrl, formId, callback, ajaxLoadingShow, ajaxLoadingHide);
        return this;
    },
    
    /** 
     * send request
     *
     * @instance
     * @memberof com_yung_util_AjaxUpload
     * @param  {object} caller - caller for callback use
     */
    send : function (caller) {
        if (this.actionUrl == null) {
            alert("please define action!");
            return;
        }
        // show load.gif if more than half second
        this.beforeSend();
        
        var formData = new FormData();
        jQuery("#" + this.formId + " :input").each(function(key, input){
            var name = jQuery(this).attr("name");
            var type = jQuery(this).attr("type");
            if ("file" == type) {
                var files = input.files;
                var i = 0;
                var file = files[i];
                while (file != null) {
                    formData.append(name, file);
                    i++;
                    file = files[i];
                }
            } else if ("button" != type && "submit" != type && name != null && name != '') {
                formData.append(name, input.value);
            }
        });
        
        var callback = this.callback;
        var self = this;
        
        jQuery.ajax({
            type:'POST',
            dataType: 'json',
            data: formData,
            processData: false,
            contentType: false,
            error: function(x, status, error) {
                self.sendComplete();
                if (self.showError) alert("An ajax error occurred: " + status + ", Error: " + error + ", actionUrl: " + self.actionUrl);
            },
            url: this.actionUrl,
            jsonpCallback: callback,
            success:function(result){
                self.sendComplete();
                if (typeof result == 'string') {
                    result = JSON.parse(result);
                }
                this.jsonpCallback(result, self, caller);
            }
        });
    }
});

/**
 * HTML Audio tool
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_AudioRecorder = $Class.extend({
    
    classProp : { 
        name : "com.yung.util.AudioRecorder" 
    },
    
    /**
     * Media recorder object
     * @member {object}
     * @instance
     * @memberof com_yung_util_AudioRecorder
     */
    mediaRecorder : null,
    
    /**
     * Media record data bytes
     * @member {Array}
     * @instance
     * @memberof com_yung_util_AudioRecorder
     */
    recordedChunks : null,
    
    /**
     * Div id to put HTML player bar
     * @member {string}
     * @instance
     * @memberof com_yung_util_AudioRecorder
     */
    playerDivId : null,
    
    /**
     * Media recorder controller
     * @member {object}
     * @instance
     * @memberof com_yung_util_AudioRecorder
     */
    mediaControl : null,
    
    /**
     * constructor
     * @memberof com_yung_util_AudioRecorder
     * @param  {string} playerDivId - div id to put player bar
     */
    init : function(playerDivId) {
        if (!navigator.getUserMedia && !navigator.webkitGetUserMedia && !navigator.mozGetUserMedia && !navigator.msGetUserMedia) {
            alert('User Media API not supported.');
            return null;
        }
        var constraints = {};
        constraints['audio'] = true;
        constraints['_instance'] = this;
        this.getUserMedia(constraints, function(stream) {
            constraints["_instance"].mediaRecorder = new MediaRecorder(stream, constraints);
            constraints["_instance"].mediaRecorder.ondataavailable = function (event) {
                if (event.data.size > 0) {
                    constraints["_instance"].recordedChunks.push(event.data);
                } else {
                    alert("audio is empty!");
                }
            };
        }, function(err) {
            alert('Error: ' + err.name + ", " + err.message);
        });
        this.recordedChunks = [];
        this.playerDivId = playerDivId;
        this.mediaControl = null;
        if (this.playerDivId != null) {
            var mediaCtrl = jQuery("#" + this.playerDivId + " audio")[0];
            if (mediaCtrl == null) {
                jQuery("#" + this.playerDivId).html('<audio controls><source src="" type="audio/mpeg"/></audio>');
            }
            this.mediaControl = jQuery("#" + this.playerDivId + " audio")[0];
        }
        return this;
    },
    
    getUserMedia : function (options, successCallback, failureCallback) {
        var api = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;
        if (api) {
            return api.bind(navigator)(options, successCallback, failureCallback);
        }
    },
    
    /** 
     * start record action
     * 
     * @instance
     * @memberof com_yung_util_AudioRecorder
     */
    startRecord : function () {
        this.recordedChunks = [];
        this.mediaRecorder.start();
    },
    
    /** 
     * stop record action
     * 
     * @instance
     * @memberof com_yung_util_AudioRecorder
     */
    stopRecord : function () {
        this.mediaRecorder.stop();
        if (this.playerDivId != null) {
            var self = this;
            setTimeout(function () {
                var superBuffer = new Blob(self.recordedChunks);
                self.mediaControl.src = window.URL.createObjectURL(superBuffer);
                jQuery("#" + self.playerDivId).css("display", "block");
            }, 100);
        }
    },
    
    /** 
     * play record action
     * 
     * @instance
     * @memberof com_yung_util_AudioRecorder
     */
    playRecord : function () {
        if (this.playerDivId == null) {
            alert("please define player div id!");
            return;
        }
        jQuery("#" + this.playerDivId).css("display", "block");
    },
    
    /** 
     * download media file
     * 
     * @instance
     * @memberof com_yung_util_AudioRecorder
     */
    downloadRecord : function () {
        var blob = new Blob(this.recordedChunks, {
			type : 'audio'
		});
		var url = URL.createObjectURL(blob);
		var a = document.createElement('a');
		document.body.appendChild(a);
		a.style = 'display: none';
		a.href = url;
		a.download = 'audio.mp3';
		a.click();
		window.URL.revokeObjectURL(url);
    },
    
    
    /**
     * Callback for after upload response 
     *
     * @callback audioAfterSend
     * @param {object} response - response data
     * @param {com_yung_util_AjaxHttp} self - this com_yung_util_AjaxHttp
     * @param {object} caller - caller which pass by send method
     */
    /** 
     * upload media file
     * 
     * @instance
     * @memberof com_yung_util_AudioRecorder
     * @param  {string} formId - upload form id
     * @param  {string} base64DataId - input id to store base64 data
     * @param  {string} nameId - input id for upload file name
     * @param  {actionUrl} nameId - form action url
     * @param  {audioAfterSend} afterSend - a callback after upload
     */
    uploadRecord : function (formId, base64DataId, nameId, name, actionUrl, afterSend) {
        var blob = new Blob(this.recordedChunks, {
			type : 'audio'
		});
		var reader = new FileReader();
        reader.readAsDataURL(blob); 
        reader.onloadend = function() {
        	var dataUrl = reader.result;
            var base64data = dataUrl.split(',')[1];
            jQuery("#" + base64DataId).val(base64data);
            jQuery("#" + nameId).val(name);
            var ajaxhttp = new com_yung_util_AjaxHttp(actionUrl, formId, function callback(response, self, caller)
            {
                var errormsg = response.errormsg;
                if(errormsg != ''){
                    alert(errormsg);
                    return;
                } else {
                	if (typeof afterSend == 'function') {
                		afterSend(response, self, caller);
                	} else {
                		alert("Upload successfully!");
                	}
                }
            });
            ajaxhttp.send();
       }
    }
});

/**
 * Abstract HTML template
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_RawTemplate = $Class.extend({
	
	classProp : { 
		name : "com.yung.util.RawTemplate",
		unimplemented : ['setData', 'getData', 'toHtml']
	},
	
	/**
     * template list to store HTML
     * @member {com_yung_util_BasicList}
     * @instance
     * @memberof com_yung_util_RawTemplate
     */
	template : null,
	
	/**
	 * constructor
     * @memberof com_yung_util_RawTemplate
     */
	init : function () {
        this.template = new com.yung.util.BasicList('string');
        return this;
    },
    
    /** 
     * add HTML to template
     * 
     * @instance
     * @memberof com_yung_util_RawTemplate
     * @param  {string} html - HTML code
     */
    add : function (html) {
        if (typeof html != 'string') {
            throw "argument type only allows string";
        }
        this.template.add(html);
    },
    
    /** 
     * search HTML code and find string pattern then return an array
     * 
     * @instance
     * @memberof com_yung_util_RawTemplate
     * @param  {string} html - source HTML code
     * @return  {Array} string pattern array
     */
    searchPattern : function (html) {
        var ret = [];
        var index = 0;
        while (html.indexOf('{{', index) != -1) {
            var startIdx = html.indexOf('{{', index);
            var endIdx = html.indexOf('}}', index);
            var pattern = html.substring(startIdx + 2, endIdx);
            ret.push(pattern);
            index = endIdx + 2;
        }
        return ret;
    },
    
    /** 
     * remove string pattern in HTML code
     * 
     * @instance
     * @memberof com_yung_util_RawTemplate
     * @param  {string} html - source HTML code
     * @return  {string} html without string pattern
     */
    removePattern : function (html) {
    	var result = html;
    	var patternArray = this.searchPattern(html);
    	for (var i = 0; i < patternArray.length; i++) {
    		result = this.replaceAll(result, "{{" + patternArray[i] + "}}", '');
    	}
    	return result;
    }
});

/**
 * simple HTML template, only one line.
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_RawTemplate
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_SimpleTemplate = com_yung_util_RawTemplate.extend({
    
	classProp : { 
		name : "com.yung.util.SimpleTemplate" 
	},
    
	/**
     * data object to render HTML
     * @member {object}
     * @instance
     * @memberof com_yung_util_SimpleTemplate
     */
	data : null,
    
	/**
     * template list to store HTML
     * @member {com_yung_util_BasicList}
     * @instance
     * @memberof com_yung_util_SimpleTemplate
     */
	template : null,
	
	/**
	 * constructor
     * @memberof com_yung_util_RawTemplate
     * @param  {string} tmpStr - one line template string
     * @param  {string} key - data key
     * @param  {string} value - data value
     */
	init : function (tmpStr, key, value) {
		var data = {};
		data[key] = value;
        this.setData(data);
        this.template = new com.yung.util.BasicList('string');
        this.template.add(tmpStr);
        return this;
    },
    
    /** 
     * set data object to render HTML code
     * 
     * @instance
     * @memberof com_yung_util_SimpleTemplate
     * @param  {object} data - data object
     */
    setData : function (data) {
        if (data == null) {
            this.data = {};
        } else {
            if (typeof data != 'object') {
                throw "data is not object";
            } else {
                this.data = data;
            }
        }
    },
    
    /** 
     * get data object
     * 
     * @instance
     * @memberof com_yung_util_SimpleTemplate
     * @return  {object} data object
     */
    getData : function () {
    	return this.data;
    },
    
    /** 
     * render HTML code
     * 
     * @instance
     * @memberof com_yung_util_SimpleTemplate
     * @param  {object} data - data object optional
     * @return  {string} html replaced by render key/value
     */
    toHtml : function (data) {
        if (data != null) {
            this.setData(data);
        }
        var html = '';
        for (var i = 0; i < this.template.size(); i++) {
            html = html + this.template.get(i);
        }
        var keyArray = this.searchPattern(html);
        for (var i = 0; i < keyArray.length; i++) {
        	var key = keyArray[i];
        	var value = this.data[key];
        	if (typeof value == 'string' || typeof value == 'number') {
                html = this.replaceAll(html, "{{" + key + "}}", value + '');
            }
        }
        return html;
    }
});

/**
 * Basic HTML template
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_RawTemplate
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_BasicTemplate = com_yung_util_RawTemplate.extend({
    
	classProp : { 
		name : "com.yung.util.BasicTemplate" 
	},
    
	/**
     * data object to render HTML
     * @member {object}
     * @instance
     * @memberof com_yung_util_BasicTemplate
     */
	data : null,
    
	/**
     * template list to store HTML
     * @member {com_yung_util_BasicList}
     * @instance
     * @memberof com_yung_util_BasicTemplate
     */
	template : null,
	
	/**
     * function map for render HTML
     * @member {com_yung_util_Map}
     * @instance
     * @memberof com_yung_util_BasicTemplate
     */
	conditionMap : null,
    
	/**
	 * constructor
     * @memberof com_yung_util_RawTemplate
     * @param  {object} data - data object to render HTML
     */
	init : function (data) {
        this.setData(data);
        this.template = new com.yung.util.BasicList('string');
        this.conditionMap = new com.yung.util.Map('string', 'function');
        return this;
    },
    
    /** 
     * set data object to render HTML code
     * 
     * @instance
     * @memberof com_yung_util_BasicTemplate
     * @param  {object} data - data object
     */
    setData : function (data) {
        if (data == null) {
            this.data = {};
        } else {
            if (typeof data != 'object') {
                throw "data is not object";
            } else {
                this.data = data;
            }
        }
    },
    
    /** 
     * get data array object
     * 
     * @instance
     * @memberof com_yung_util_BasicTemplate
     * @return  {object} data object
     */
    getData : function () {
        return this.data;
    },
    
    /**
     * Callback to override data value for render HTML
     *
     * @callback overrideRenderValue
     * @param {object} data - data object
     * @param {string} key - render key
     * @param {string} originValue - original render value
     */
    /** 
     * set data object to render HTML code
     * 
     * @instance
     * @memberof com_yung_util_BasicTemplate
     * @param  {string} key - render key
     * @param  {overrideRenderValue} condition - a callback to run
     */
    setCondition : function (key, condition) {
        if (condition == null) {
            return;
        } else {
            if (typeof condition == 'function') {
                this.conditionMap.put(key, condition);
            } else {
                throw "condition is not function";
            }
        }
    },
    
    /** 
     * render HTML code
     * 
     * @instance
     * @memberof com_yung_util_BasicTemplate
     * @param  {object} data - data object optional
     * @return  {string} html replaced by render key/value
     */
    toHtml : function (data) {
        if (data != null) {
            this.setData(data);
        }
        var html = '';
        for (var i = 0; i < this.template.size(); i++) {
            html = html + this.template.get(i);
        }
        var keyArray = this.searchPattern(html);
        for (var i = 0; i < keyArray.length; i++) {
        	var key = keyArray[i];
        	var value = this.data[key];
        	if (this.conditionMap.get(key) != null) {
                var customVal = this.conditionMap.get(key)(data, key, value);
                value = customVal;
            }
        	if (typeof value == 'string' || typeof value == 'number') {
                html = this.replaceAll(html, "{{" + key + "}}", value + '');
            }
        }
        return html;
    }
});

/**
 * Array HTML template, to render basic template multiple times 
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_RawTemplate
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_ArrayTemplate = com_yung_util_RawTemplate.extend({
    
	classProp : { 
		name : "com.yung.util.ArrayTemplate" 
	},
	
	/**
     * basic template to render HTML
     * @member {com_yung_util_BasicTemplate}
     * @instance
     * @memberof com_yung_util_ArrayTemplate
     */
    basicTmp : null,
    
    /**
     * data array to render HTML
     * @member {Array}
     * @instance
     * @memberof com_yung_util_ArrayTemplate
     */
    dataArray : null,
    
    /**
	 * constructor
     * @memberof com_yung_util_RawTemplate
     * @param  {Array} dataArray - data array to render HTML
     * @param  {com_yung_util_BasicTemplate} basicTmp - basic template to render HTML
     */
    init : function (dataArray, basicTmp) {
        this.setDataArray(dataArray);
        this.setBasicTmp(basicTmp);
        this.template = new com.yung.util.BasicList('string');
        return this;
    },
    
    /** 
     * set data array to render HTML code
     * 
     * @instance
     * @memberof com_yung_util_ArrayTemplate
     * @param  {Array} dataArray - data array
     */
    setDataArray : function (dataArray) {
        if (dataArray == null) {
            this.dataArray = [];
        } else {
            if (jQuery.isArray(dataArray) == false) {
                throw "dataArray is not array";
            } else {
                this.dataArray = dataArray;
            }
        }
    },
    setData : function (dataArray) {
        this.setDataArray(dataArray);
    },
    
    /** 
     * get data array object
     * 
     * @instance
     * @memberof com_yung_util_ArrayTemplate
     * @return  {object} data object
     */
    getDataArray : function () {
        return this.dataArray;
    },
    getData : function () {
        return this.dataArray;
    },
    
    /** 
     * set basic template to render HTML code
     * 
     * @instance
     * @memberof com_yung_util_ArrayTemplate
     * @param  {com_yung_util_BasicTemplate} basicTmp - basic template
     */
    setBasicTmp : function (basicTmp) {
        if (basicTmp == null) {
            this.basicTmp = null;
        } else {
            var valid = basicTmp instanceof com_yung_util_BasicTemplate;
            if (valid == false) {
                throw "basicTmp is not instance of com.yung.util.BasicTemplate";
            } else {
                this.basicTmp = basicTmp;
            }
        }
    },
    
    /** 
     * render HTML code
     * 
     * @instance
     * @memberof com_yung_util_ArrayTemplate
     * @param  {Array} dataArray - data array optional
     * @return  {string} html replaced by render key/value
     */
    toHtml : function (dataArray) {
        if (dataArray != null) {
            this.setDataArray(dataArray);
        }
        var html = "";
        if (this.basicTmp == null && this.template.size() > 0) {
        	// use template to create basicTmp
        	this.basicTmp = new com.yung.util.BasicTemplate();
        	for (var i = 0; i < this.template.size(); i++) {
        		this.basicTmp.add(this.template.get(i));
        	}
        }
        if (this.basicTmp != null) {
            for (var i = 0; i < this.dataArray.length; i++) {
                this.basicTmp.setData(this.dataArray[i]);
                html = html + this.basicTmp.toHtml();
            }
        }
        return html;
    }
});

/**
 * Complex template data
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_TemplateData = $Class.extend({
    
	classProp : { 
		name : "com.yung.util.TemplateData"
	},
    
	/**
     * basic template map
     * @member {com_yung_util_Map}
     * @instance
     * @memberof com_yung_util_TemplateData
     */
    tmpMap : null,
    
    /**
     * data map
     * @member {com_yung_util_Map}
     * @instance
     * @memberof com_yung_util_TemplateData
     */
    dataMap : null,
    
	/**
	 * constructor
     * @memberof com_yung_util_TemplateData
     */
	init : function() {
		this.tmpMap = new com_yung_util_Map('string', 'object');
		this.dataMap = new com_yung_util_Map('string', 'object');
    },
    
    /** 
     * add template and data
     * 
     * @instance
     * @memberof com_yung_util_TemplateData
     * @param  {string} key - pattern key [template key] 
     * @param  {com_yung_util_RawTemplate} template - template
     * @param  {object | Array | com_yung_util_TemplateData} data - data object
     */
    addTemplate : function (key, template, data) {
    	if (key == null) {
            return;
        } else {
        	if (template == null) {
        		return ;
        	} else if (template instanceof com_yung_util_ArrayTemplate) {
            	if (data == null) {
                	return;
                } else if (jQuery.isArray(data) == true) {
                	this.tmpMap.put(key, template);
                	this.dataMap.put(key, data);
                } else {
                	throw "data is not Array";
                }
            } else if (template instanceof com_yung_util_ComplexTemplate) {
            	if (data == null) {
                	return;
                } else if (data instanceof com_yung_util_TemplateData) {
                	this.tmpMap.put(key, template);
                	this.dataMap.put(key, data);
                } else {
                	throw "data is not com.yung.util.TemplateData";
                }
            } else if (template instanceof com_yung_util_RawTemplate) {
                if (data == null) {
                    return;
                } else if (typeof data == 'object') {
                    this.tmpMap.put(key, template);
                    this.dataMap.put(key, data);
                } else {
                    throw "data is not object";
                }
            } else {
                throw "template is not instance of com.yung.util.RawTemplate";
                
            }
        }
    },
    
    /** 
     * get basic template map to render HTML code
     * 
     * @instance
     * @memberof com_yung_util_TemplateData
     * @return  {com_yung_util_Map} basic template map
     */
    getTmpMap : function () {
    	return this.tmpMap;
    },
    
    /** 
     * get data map to render HTML code
     * 
     * @instance
     * @memberof com_yung_util_TemplateData
     * @return {com_yung_util_Map} data map
     */
    getDataMap : function () {
    	return this.dataMap;
    }
});    


/**
 * Complex HTML template, to render two types of template
 * basic template and array template
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_RawTemplate
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_ComplexTemplate = com_yung_util_RawTemplate.extend({
	
    classProp : { 
    	name : "com.yung.util.ComplexTemplate" 
    },
    
    /**
     * complex template data
     * @member {com_yung_util_TemplateData}
     * @instance
     * @memberof com_yung_util_ComplexTemplate
     */
    data : null,
    
    /**
     * template list to store HTML
     * @member {com_yung_util_BasicList}
     * @instance
     * @memberof com_yung_util_ComplexTemplate
     */
    template : null,
    
    /**
	 * constructor
     * @memberof com_yung_util_ComplexTemplate
     * @param  {com_yung_util_Map} tmpMap - basic template map
     * @param  {com_yung_util_Map} dataMap - data map
     */
    init : function () {
        this.data = new com_yung_util_TemplateData();
        this.template = new com.yung.util.BasicList('string');
        return this;
    },
    
    /** 
     * add template and data
     * 
     * @instance
     * @memberof com_yung_util_ComplexTemplate
     * @param  {string} key - pattern key [template key] 
     * @param  {com_yung_util_RawTemplate} template - template
     * @param  {object | Array | com_yung_util_TemplateData} data - data object
     */
    addTemplate : function (key, template, data) {
        this.data.addTemplate(key, template, data);
    },
    
    /** 
     * set complex template data to render HTML code
     * 
     * @instance
     * @memberof com_yung_util_ComplexTemplate
     * @param  {com_yung_util_Map} dataMap - data map
     */
    setData : function (data) {
        if (data == null) {
            this.dataMap = new com_yung_util_TemplateData();
        } else {
        	var valid = _validType(com_yung_util_TemplateData, data);
        	if (valid == false) {
        		throw "data type is not com.yung.util.TemplateData";
        	}
            this.data = data;
        }
    },
    
    /** 
     * get data object
     * 
     * @instance
     * @memberof com_yung_util_ComplexTemplate
     * @return  {object} data object
     */
    getData : function () {
        return this.data;
    },
    
    /** 
     * render HTML code
     * 
     * @instance
     * @memberof com_yung_util_ComplexTemplate
     * @param  {com_yung_util_TemplateData} data - complex tempalte data [optional]
     * @return  {string} html replaced by render key/value
     */
    toHtml : function (data) {
        if (data != null) {
            this.setData(data);
        }
        var html = '';
        for (var i = 0; i < this.template.size(); i++) {
            html = html + this.template.get(i);
        }
        var tmpMap = this.data.getTmpMap();
        var dataMap = this.data.getDataMap();
        var keyArray = tmpMap.getKeyArray();
        for (var i = 0; i < keyArray.length; i++) {
            var key = keyArray[i];
            var tmp = tmpMap.get(key);
            var dataObj = dataMap.get(key);
            html = this.replaceAll(html, "{{" + key + "}}", tmp.toHtml(dataObj) + '');
        }
        return html;
    }
});

/**
 * Calendar tool, like JAVA calendar
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_Calendar = $Class.extend({
    
	classProp : {
    	name : "com.yung.util.Calendar"
    },
    
    /**
     * Date object
     * @member {jsDate}
     * @instance
     * @memberof com_yung_util_Calendar
     */
    d : null,
    
    /**
	 * constructor
     * @memberof com_yung_util_Calendar
     * @param  {number} arg1 - year
     * @param  {number} arg2 - month
     * @param  {number} arg3 - day of month
     * @param  {number} arg4 - hour(24hr)
     * @param  {number} arg5 - minute
     * @param  {number} arg6 - second
     * @param  {number} arg7 - millisecond
     */
    init : function(arg1, arg2, arg3, arg4, arg5, arg6, arg7) {
        if (arg1 instanceof Date) {
            this.d = arg1;
            return this;
        }
        if (arg1 == null) arg1 = 0;
        if (arg2 == null) arg2 = 0;
        if (arg3 == null) arg3 = 0;
        if (arg4 == null) arg4 = 0;
        if (arg5 == null) arg5 = 0;
        if (arg6 == null) arg6 = 0;
        if (arg7 == null) arg7 = 0;
        if (typeof arg1 != "number") {
            throw "Year should be integer";
        }
        if (typeof arg2 != "number") {
            throw "Month should be integer";
        }
        if (typeof arg3 != "number") {
            throw "Day should be integer";
        }
        if (typeof arg4 != "number") {
            throw "Hours should be integer";
        }
        if (typeof arg6 != "number") {
            throw "Minutes should be integer";
        }
        if (typeof arg7 != "number") {
            throw "Milliseconds should be integer";
        }
        var year, month, day, hour, min, sec, milli;
        year = parseInt(arg1);
        month = parseInt(arg2) - 1;
        if (month < 0) month = 0;
        day = parseInt(arg3);
        hour = parseInt(arg4);
        min = parseInt(arg5);
        sec = parseInt(arg6);
        milli = parseInt(arg7);
        var d = new Date(year, month, day, hour, min, sec, milli);
        this.d = d;
        return this;
    },
    
    /** 
     * get year
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @return {number} year number
     */
    getYear : function () {
        return this.d.getFullYear();
    },
    
    /** 
     * get month
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @return {number} month number
     */
    getMonth : function () {
        return this.d.getMonth();
    },
    
    /** 
     * get day of month
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @return {number} day of month number
     */
    getDate : function () {
        return this.d.getDate();
    },
    
    /** 
     * get hour(24hr)
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @return {number} hour(24hr) number
     */
    getHours : function () {
        return this.d.getHours();
    },
    
    /** 
     * get minute
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @return {number} minute number
     */
    getMinutes : function () {
        return this.d.getMinutes();
    },
    
    /** 
     * get second
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @return {number} second number
     */
    getSeconds : function () {
        return this.d.getSeconds();
    },
    
    /** 
     * get millisecond
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @return {number} millisecond number
     */
    getMilliseconds : function () {
        return this.d.getMilliseconds();
    },
    
    /** 
     * get day of week
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @return {number} day of week number
     */
    getDay : function () {
        return this.d.getDay();
    },
    
    /** 
     * set year
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @param {number} year - year number
     */
    setYear : function (year) {
        this.d.setFullYear(year);
    },
    
    /** 
     * set month
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @param {number} month - month number
     */
    setMonth : function (month) {
        return this.d.setMonth(month);
    },
    
    /** 
     * set day of month
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @param {number} day - day of month number
     */
    setDate : function (day) {
        return this.d.setDate(day);
    },
    
    /** 
     * set hours
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @param {number} hours - hours number
     */
    setHours : function (hours) {
        return this.d.setHours(hours);
    },
    
    /** 
     * set minute
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @param {number} min - minute number
     */
    setMinutes : function (min) {
        return this.d.setMinutes(min);
    },
    
    /** 
     * set second
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @param {number} sec - second number
     */
    setSeconds : function (sec) {
        return this.d.setSeconds(sec);
    },
    
    /** 
     * set millisecond
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @param {number} milli - millisecond number
     */
    setMilliseconds : function (milli) {
        return this.d.setMilliseconds(milli);
    },
    
    /** 
     * add year to calendar
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @param {number} year - year number
     */
    addYear : function (year) {
        this.d.setFullYear(year + this.getYear());
    },
    
    /** 
     * add month to calendar
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @param {number} month - month number
     */
    addMonth : function (month) {
        return this.d.setMonth(month + this.getMonth());
    },
    
    /** 
     * add day to calendar
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @param {number} day - day number
     */
    addDay : function (day) {
        return this.d.setDate(day + this.getDate());
    },
    
    /** 
     * add hour to calendar
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @param {number} hours - hours number
     */
    addHours : function (hours) {
        return this.d.setHours(hours + this.getHours());
    },
    
    /** 
     * add minute to calendar
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @param {number} min - minute number
     */
    addMinutes : function (min) {
        return this.d.setMinutes(min + this.getMinutes());
    },
    
    /** 
     * add second to calendar
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @param {number} sec - second number
     */
    addSeconds : function (sec) {
        return this.d.setSeconds(sec + this.getSeconds());
    },
    
    /** 
     * add millisecond to calendar
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @param {number} milli - millisecond number
     */
    addMilliseconds : function (milli) {
        return this.d.setMilliseconds(milli + this.getMilliseconds());
    },
    
    /** 
     * get unix time
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @return {number} unit time number
     */
    getTime : function () {
        return this.d.getTime();
    },
    
    /** 
     * set unix time
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @param {number} time - unit time number
     */
    setTime : function (time) {
        if (typeof time != 'number') {
            throw "argument time should be integer";
        }
        time = parseInt(time);
        this.d = new Date(time);
    },
    
    /** 
     * convert to jsDate
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @return {Date} jsDate
     */
    toDate : function () {
        return new Date(this.d.getTime());
    },
    
    /** 
     * to string
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     */
    toString : function () {
        return this.d.toString();
    }
});

/**
 * Date format tool, like JAVA SimpleDateFormat
 * @property {object}  classProp - class property
 * @property {string}  classProp.year - year pattern
 * @property {string}  classProp.month - month pattern
 * @property {string}  classProp.date - day pattern
 * @property {string}  classProp.hour - hour pattern
 * @property {string}  classProp.min - minute pattern
 * @property {string}  classProp.sec - second pattern
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_SimpleDateFormat = $Class.extend({
    
	classProp : { 
    	name : "com.yung.util.SimpleDateFormat", 
    	year : "yyyy", 
    	month : "MM", 
    	date : "dd", 
    	hour : "HH", 
    	min : "mm", 
    	sec : "ss",
    	milli : "SSS"
    },
    
    /**
     * date pattern
     * @member {string}
     * @instance
     * @memberof com_yung_util_SimpleDateFormat
     */
    dateFormat : null,
    
    /**
	 * constructor
	 * 
     * @memberof com_yung_util_SimpleDateFormat
     * @param  {string} arg1 - date format pattern
     */
    init : function(arg1) {
        if (typeof arg1 != 'string') {
            throw "argument type is not string";
        }
        this.dateFormat = arg1;
    },
    
    /**
	 * use date pattern to parse string to get com_yung_util_Calendar instance
 	 * 
	 * @instance
     * @memberof com_yung_util_SimpleDateFormat
     * @param  {string} dateString - date string
     * @return {com_yung_util_Calendar} calendar
     */
    parse : function (dateString) {
        var year = 0;
        var month = 0;
        var date = 0;
        var hour = 0;
        var min = 0;
        var sec = 0;
        var milli = 0;
        for (var key in this.classProp) {
            var match = this.classProp[key];
            if (key == 'year') {
                var start = this.dateFormat.indexOf(match);
                if (start >= 0) {
                    var end = start + match.length;
                    var yearStr = dateString.substring(start, end);
                    if (jQuery.isNumeric(yearStr) == false) {
                        throw dateString + " format error!";
                    }
                    year = parseInt(yearStr * 1.0);
                }
            } else if (key == 'month') {
                var start = this.dateFormat.indexOf(match);
                if (start >= 0) {
                    var end = start + match.length;
                    var monthStr = dateString.substring(start, end);
                    if (jQuery.isNumeric(monthStr) == false) {
                        throw dateString + " format error!";
                    }
                    month = parseInt(monthStr * 1.0);
                }
            } else if (key == 'date') {
                var start = this.dateFormat.indexOf(match);
                if (start >= 0) {
                    var end = start + match.length;
                    var dateStr = dateString.substring(start, end);
                    if (jQuery.isNumeric(dateStr) == false) {
                        throw dateString + " format error!";
                    }
                    date = parseInt(dateStr * 1.0);
                }
            } else if (key == 'hour') {
                var start = this.dateFormat.indexOf(match);
                if (start >= 0) {
                    var end = start + match.length;
                    var hourStr = dateString.substring(start, end);
                    if (jQuery.isNumeric(hourStr) == false) {
                        throw dateString + " format error!";
                    }
                    hour = parseInt(hourStr * 1.0);
                }
            } else if (key == 'min') {
                var start = this.dateFormat.indexOf(match);
                if (start >= 0) {
                    var end = start + match.length;
                    var minStr = dateString.substring(start, end);
                    if (jQuery.isNumeric(minStr) == false) {
                        throw dateString + " format error!";
                    }
                    min = parseInt(minStr * 1.0);
                }
            } else if (key == 'sec') {
                var start = this.dateFormat.indexOf(match);
                if (start >= 0) {
                    var end = start + match.length;
                    var secStr = dateString.substring(start, end);
                    if (jQuery.isNumeric(secStr) == false) {
                        throw dateString + " format error!";
                    }
                    sec = parseInt(secStr * 1.0);
                }
            } else if (key == 'milli') {
                var start = this.dateFormat.indexOf(match);
                if (start >= 0) {
                    var end = start + match.length;
                    var milliStr = dateString.substring(start, end);
                    if (jQuery.isNumeric(milliStr) == false) {
                        throw dateString + " format error!";
                    }
                    milli = parseInt(milliStr * 1.0);
                }
            }
        }
        var cal = new com.yung.util.Calendar(year, month, date, hour, min, sec, milli);
        return cal;
    },
    
    /**
	 * padding zero to string
	 * 
	 * @private
     * @memberof com_yung_util_SimpleDateFormat
     * @param  {string} str - target string
     * @param  {number} total - total length of result string
     * @return {string} result string
     */
    paddingZero : function (str, total) {
        var diff = total - str.length;
        if (diff > 0) {
            var ret = "";
            for (var i = 0; i < diff; i++) {
                ret = ret + "0";
            }
            ret = ret + str;
            return ret;
        } else {
            return str;
        }
    },
    
    /**
	 * use date pattern to format calendar to date string
	 * 
	 * @instance
	 * @memberof com_yung_util_SimpleDateFormat
     * @param  {com_yung_util_Calendar | jsDate} date - calendar
     * @return {string} date string
     */
    format : function (date) {
        var cal = null;
        if (date instanceof Date) {
            cal = new com.yung.util.Calendar(date);
        } else if (date instanceof com.yung.util.Calendar) {
            cal = date;
        } else {
            throw "argument date is not a Date or com.yung.util.Calendar";
        }
        var ret = "" + this.dateFormat;
        for (var key in this.classProp) {
            if (key == 'year') {
                ret = this.replaceAll(ret, this.classProp[key], this.paddingZero(cal.getYear() + "", 4));
            } else if (key == 'month') {
                ret = this.replaceAll(ret, this.classProp[key], this.paddingZero((cal.getMonth() + 1) + "", 2));
            } else if (key == 'date') {
                ret = this.replaceAll(ret, this.classProp[key], this.paddingZero(cal.getDate() + "", 2));
            } else if (key == 'hour') {
                ret = this.replaceAll(ret, this.classProp[key], this.paddingZero(cal.getHours() + "", 2));
            } else if (key == 'min') {
                ret = this.replaceAll(ret, this.classProp[key], this.paddingZero(cal.getMinutes() + "", 2));
            } else if (key == 'sec') {
                ret = this.replaceAll(ret, this.classProp[key], this.paddingZero(cal.getSeconds() + "", 2));
            } else if (key == 'milli') {
                ret = this.replaceAll(ret, this.classProp[key], this.paddingZero(cal.getMilliseconds() + "", 2));
            }
        }
        return ret;
    },
    
    /**
	 * use date pattern to check date string if valid
	 * 
	 * @instance
	 * @memberof com_yung_util_SimpleDateFormat
     * @param  {string} dateString - date string
     * @return {boolean} valid or not
     */
    validate : function (dateString) {
        try {
            this.parse(dateString);
            return true;
        } catch (err) {
            return false;
        }
    }
});
/**
 * Collection interface. Define implement methods.
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @property {Array}  classProp.unimplemented - unimplemented method
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_CollectionInterface = $Class.extend({

    classProp : { 
		name : "com.yung.util.CollectionInterface",
		unimplemented : ['toArray', 'size', 'getFirstElement', 'add', 'remove', 'contains']
    }
    
});

/**
 * Abstract Class for collection
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @property {Array}  classProp.unimplemented - unimplemented method
 * @augments com_yung_util_CollectionInterface
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_Collection = com_yung_util_CollectionInterface.extend({
    
	classProp : { 
		name : "com.yung.util.Collection",
		unimplemented : ['add', 'remove']
	},
    
	/**
     * element type
     * @member {all}
     * @instance
     * @memberof com_yung_util_Collection
     */
	type : null,
    
	/**
     * element array
     * @member {string | number | boolean | object}
     * @instance
     * @memberof com_yung_util_Collection
     */
	array : [],
	
    /** 
     * clear all elements
     * 
     * @instance
     * @memberof com_yung_util_Collection
     */
    clear : function () {
        this.array = [];
        if (this.obj != null) {
            this.obj = {};
        }
        if (this.tsoArray != null) {
            this.tsoArray = [];
        }
        this.tsoIdx = 0;
    },
    
    /**
	 * add elements by array
	 * 
	 * @instance
     * @memberof com_yung_util_Collection
     * @param  {Array} elementArray - element array
     * @return {boolean} success or not
     */
    addByArray : function (elementArray) {
        if (elementArray != null) {
            if (jQuery.isArray(elementArray)) {
                for (var i = 0; i < elementArray.length; i++) {
                    this.add(elementArray[i]);
                }
                return true;
            } else {
                throw "argument type is not array";
            }
        }
        return true;
    },
    
    /** 
     * check collection contains element 
     * 
     * @instance
     * @memberof com_yung_util_Collection
     * @param  {validType} element - element to check if exist
     * @return {boolean} exist or not
     */
    contains : function (element) {
        if (element == null) {
            return false;
        }
        if (this.validType(element) == false) {
            return false;
        }
        if (this.type == 'string' || this.type == 'number') {
        	if (this.obj != null) {
        		if (this.obj[element + ""] == null) {
                    return false;
                } else {
                    return true;
                }
        	} else {
        		if (jQuery.inArray(element, this.array) < 0) {
                    return false;
                } else {
                    return true;
                }
        	}
        } else if (typeof this.type == 'function') {
        	var hash = null;
        	if (this.valueHash == true) {
                var json = JSON.stringify(element);
                hash = MD5Util.calc(json);
            } else {
                if (typeof element["hashCode"] == 'function') {
                    hash = element["hashCode"]();
                } 
            }
        	if (this.obj != null && hash != null) {
        		if (this.obj[hash] == null) {
                    return false;
                } else {
                    return true;
                }
        	}
        } else if (this.type == 'object') {
            var hash = null;
            if (this.valueHash == true) {
                var json = JSON.stringify(element);
                hash = MD5Util.calc(json);
            }
        	if (this.obj != null && hash != null) {
        		if (this.obj[hash] == null) {
                    return false;
                } else {
                    return true;
                }
        	}
        }
        if (jQuery.inArray(element, this.array) < 0) {
            return false;
        } else {
            return true;
        }
    },
    
    /** 
     * check collection is empty 
     * 
     * @instance
     * @memberof com_yung_util_Collection
     * @return {boolean} empty or not
     */
    isEmpty : function () {
        if (this.array.length == 0) {
            return true;
        } else {
            return false;
        }
    },
    
    /** 
     * get collection size 
     * 
     * @instance
     * @memberof com_yung_util_Collection
     * @return {number} collection size
     */
    size : function () {
        return this.array.length;
    },
    
    /** 
     * return element array
     * 
     * @instance
     * @memberof com_yung_util_Collection
     * @return {Array} element array
     */
    toArray : function () {
        return this.array;
    },
    
    /** 
     * return a copy of element array
     * 
     * @instance
     * @memberof com_yung_util_Collection
     * @return {Array} element array
     */
    cloneArray : function () {
        var arr = [];
        for (var i = 0; i < this.array.length; i++) {
            arr.push(this.array[i]);
        }
        return arr;
    },
    
    /** 
     * to string
     * 
     * @instance
     * @memberof com_yung_util_Collection
     * @return {string} message string
     */
    toString : function () {
        var ret = "";
        if (this.array.length > 0 && (this.type == 'string' || this.type == 'number')) {
            ret = ret + "[";
        }
        for (var i = 0; i < this.array.length; i++) {
            
            if (this.type == 'string') {
                ret = ret + "'" + this.array[i] + "', ";
            } else if (this.type == 'number') {
                ret = ret + this.array[i] + ", ";
            } else {
                if (i == 0) ret = ret + "\n";
                if (jQuery.isArray(this.array[i])) {
                    ret = ret + "    " + JSON.stringify(this.array[i]) + ",\n"; 
                } else if (typeof this.array[i].toString == "function") {
                    ret = ret + "    " + this.array[i].toString() + ",\n"; 
                } else {
                    ret = ret + "    " + JSON.stringify(this.array[i]) + ",\n"; 
                }
            }
        }
        if (ret.length > 0) {
            ret = ret.substring(0, ret.length - 2);
        }
        if (this.array.length > 0 && (this.type == 'string' || this.type == 'number')) {
            ret = ret + "]";
        }
        return this.classProp.name + ret;
    },
    
    /** 
     * sort collection elements
     * 
     * @instance
     * @param  {boolean} reverse - is reverse sort
     * @memberof com_yung_util_Collection
     */
    sort : function (reverse) {
        if (reverse == null || reverse == false) {
            reverse = false;
        } else {
            reverse = true;
        }
        if (reverse == false) {
            if (this.type == 'string') {
                this.array.sort();
            } else if (this.type == 'number') {
                this.array.sort(function(a, b){return a - b});
            } else if (typeof this.type == 'function') {
                var instance = new this.type();
                if (typeof instance.compareTo == 'function') {
                    this.array.sort(function(a, b){return a.compareTo(b);});
                } else {
                	throw "instance not implement compareTo";
                }
            }
        } else {
            if (this.type == 'string') {
                this.array.sort();
                this.array.reverse();
            } else if (this.type == 'number') {
                this.array.sort(function(a, b){return b - a});
            } else if (typeof this.type == 'function') {
                var instance = new this.type();
                if (typeof instance.compareTo == 'function') {
                    this.array.sort(function(a, b){return b.compareTo(a);});
                } else {
                	throw "instance not implement compareTo";
                }
            }
        }
    },
    
    /** 
     * check object is collection or not
     * 
     * @memberof com_yung_util_Collection
     * @param  {object} obj - object to check
     * @return {boolean} is collection
     */
    isCollection : function (obj) {
    	return obj instanceof com.yung.util.CollectionInterface;
    },
    
    /** 
     * check element type is valid
     * 
     * @instance
     * @memberof com_yung_util_Collection
     * @param  element - element to check
     * @return {boolean} element type valid or not
     */
    validType : function (element) {
    	return _validType(this.type, element);
    },
    
    getFirstElement :  function () {
        if (this.size() == 0) {
            return null;
        }
        return this.array[0];
    },
    
    binarySearchArray : function(arr, x, start, end, reverse, exact) {
    	return binarySearchArray(arr, x, start, end, reverse, exact);
	}
});

/**
 * Basic set, only allow type string ,number and boolean
 * 
 * @property {object} classProp - class property
 * @property {string} classProp.name - class name
 * @augments com_yung_util_Collection
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_BasicSet = com_yung_util_Collection.extend({
    
	classProp : { 
		name : "com.yung.util.BasicSet"
	},
	
	/**
     * element array
     * @member {string | numbe | boolean}
     * @instance
     * @memberof com_yung_util_Collection
     */
	array : [],
	obj : {},
	tsoArray : [],
	tsoIdx : 0,
	
	/**
	 * constructor
     * @memberof com_yung_util_BasicSet
     * @param  {string} type - element type
     */
    init : function(type){
        if (type == null) {
            throw "com.yung.util.BasicSet type can not be null!";
        }
        if (typeof type == "string") {
            type = type.toLowerCase();
        }
        if (type != 'string' && type != 'number' && type != 'boolean') {
            throw "com.yung.util.BasicSet type can only be string, number or boolean!";
        }
        this.type = type;
        this.array = [];
        this.obj = {};
        this.tsoArray = [];
        return this;
    },
    
    /**
     * Adds the specified element to this set 
     * 
     * @memberof com_yung_util_BasicSet
     * @param {validType} element - element to add
     * @instance
     * @return {boolean} success or not
     */
    add : function (element) {
        if (element == null) {
            return false;
        }
        if (this.validType(element) == false) {
            throw "argument type is not " + this.type;
        }
        if (!this.contains(element)) {
            this.obj[element + ""] = this.tsoIdx;
            this.array.push(element);
            this.tsoArray.push(this.tsoIdx);
            this.tsoIdx++;
            return true;
        }
        return false;
    },
    
    /**
     * Adds the all elements of collection to this set 
     * 
     * @memberof com_yung_util_BasicSet
     * @param  {com_yung_util_CollectionInterface} collection - collection to add
     * @instance
     * @return {boolean} success or not
     */
    addAll : function (collection) {
    	if (collection == null) {
            throw "argument can not be empty";
        }
        if (!collection instanceof com_yung_util_CollectionInterface) {
            throw "collection is not com.yung.util.CollectionInterface";
        }
        var array = collection.toArray();
        for (var i = 0; i < array.length; i++) {
            this.add(array[i]);
        }
        return true;
    },
    
    /** 
     * check collection if contains all elements in another collection 
     * 
     * @instance
     * @memberof com_yung_util_BasicSet
     * @param  {com_yung_util_CollectionInterface} collection - collection to check
     * @return {boolean} exist or not
     */
    containsAll : function (collection) {
        if (collection == null) {
            throw "argument can not be empty";
        }
        if (this.isCollection(collection) == false) {
            return false;
        }
        if (this.size() != collection.size()) {
            return false;
        }
        var array = collection.toArray();
        for (var i = 0; i < array.length; i++) {
            if (this.contains(array[i]) == false) {
                return false;
            }
        }
        return true;
    },
    
    /**
     * Removes the specified element to this set 
     * 
     * @memberof com_yung_util_BasicSet
     * @param {validType} element - element to remove
     * @instance
     * @return {boolean} success or not
     */
    remove : function (element) {
        if (element == null) {
            return false;
        }
        if (this.validType(element) == false) {
        	return false;
        }
        if (this.obj[element + ""] == null) {
        	return false;
        }
        var index = this.binarySearchArray(this.tsoArray, this.obj[element + ""], 0, this.tsoArray.length - 1, false, true);
        if (index < 0) {
            return false;
        } else {
        	this.array.splice(index, 1);
        	this.tsoArray.splice(index, 1);
        	delete this.obj[element + ""];
            return true;
        }
    },
    
    /**
     * Removes the all elements of collection to this set 
     * 
     * @memberof com_yung_util_BasicSet
     * @param  {com_yung_util_CollectionInterface} collection - collection to remove
     * @instance
     * @return {boolean} success or not
     */
    removeAll : function (collection) {
        if (collection == null) {
            return false;
        }
        if (this.isCollection(collection) == false) {
        	throw "argument is not com.yung.util.Collection";
        }
        var array = collection.toArray();
        for (var i = 0; i < array.length; i++) {
            this.remove(array[i]);
        }
        return true;
    }
    
});

/**
 * raw object set, not good in performance
 *
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_BasicSet
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_RawObjectSet = com_yung_util_BasicSet.extend({
    
	classProp : { 
		name : "com.yung.util.RawObjectSet"
	},
	
    /**
     * element array
     * @member {Array}
     * @instance
     * @memberof com_yung_util_RawObjectSet
     */
    array : null,
    
    /**
	 * constructor
     * @memberof com_yung_util_RawObjectSet
     */
    init : function(){
        this.type = 'object';
        this.array = [];
        return this;
    },
    
    /**
     * Adds the specified element to this set 
     * 
     * @memberof com_yung_util_RawObjectSet
     * @param {validType} element - element to add
     * @instance
     * @return {boolean} success or not
     */
    add : function (element) {
        if (element == null) {
            return false;
        }
        if (this.validType(element) == false) {
            throw "argument type must be object";
        }
        if (jQuery.inArray(element, this.array) < 0) {
        	this.array.push(element);
            return true;
        }
        return false;
    },
    
    /**
     * Adds the all elements of collection to this set 
     * 
     * @memberof com_yung_util_RawObjectSet
     * @param  {com_yung_util_CollectionInterface} collection - collection to add
     * @instance
     * @return {boolean} success or not
     */
    addAll : function (collection) {
    	if (collection == null) {
            throw "argument can not be empty";
        }
        if (!collection instanceof com_yung_util_CollectionInterface) {
            throw "collection is not com.yung.util.CollectionInterface";
        }
        var array = collection.toArray();
        for (var i = 0; i < array.length; i++) {
            this.add(array[i]);
        }
        return true;
    },
    
    /** 
     * check collection contains element 
     * 
     * @instance
     * @memberof com_yung_util_RawObjectSet
     * @param  {validType} element - element to check if exist
     * @return {boolean} exist or not
     */
    contains : function (element) {
        if (element == null) {
            return false;
        }
        if (this.validType(element) == false) {
            return false;
        }
        if (jQuery.inArray(element, this.array) < 0) {
            return false;
        } else {
            return true;
        }
    },
    
    /**
     * Removes the specified element to this set 
     * 
     * @memberof com_yung_util_RawObjectSet
     * @param {validType} element - element to remove
     * @instance
     * @return {boolean} success or not
     */
    remove : function (element) {
        if (element == null) {
            return false;
        }
        if (this.validType(element) == false) {
            return false;
        }
        var index = jQuery.inArray(element, this.array);
        if (index < 0) {
            return false;
        } else {
            this.array.splice(index, 1);
            return true;                
        }
    }
    
});

/**
 * Raw object set, only allow type object
 * object element will use JSON.stringify to check exist 
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_BasicSet
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_ValueHashSet = com_yung_util_BasicSet.extend({
    
	classProp : { 
	    name : "com.yung.util.ValueHashSet"
    },
    
    /**
     * element array
     * @member {object}
     * @instance
     * @memberof com_yung_util_ValueHashSet
     */
    array : null,
    obj : null,
    tsoArray : [],
    tsoIdx : 0,
    valueHash : true,
    
    /**
	 * constructor
     * @memberof com_yung_util_ValueHashSet
     */
    init : function(){
        this.array = [];
        this.obj = {};
        this.type = 'object';
        return this;
    },
    
    /**
     * Adds the specified element to this set 
     * 
     * @memberof com_yung_util_ValueHashSet
     * @param {validType} element - element to add
     * @instance
     * @return {boolean} success or not
     */
    add : function (element) {
        if (element == null) {
            return false;
        }
        if (this.validType(element) == false) {
            throw "argument type is not " + this.type;
        }
        var json = JSON.stringify(element);
        var hash = MD5Util.calc(json);
        if (this.obj[hash] == null) {
            this.obj[hash] = this.tsoIdx;
            this.array.push(element);
            this.tsoArray.push(this.tsoIdx);
            this.tsoIdx++;
            return true;
        }
        return false;
    },
    
    /**
     * Adds the all elements of collection to this set 
     * 
     * @memberof com_yung_util_ValueHashSet
     * @param  {com_yung_util_CollectionInterface} collection - collection to add
     * @instance
     * @return {boolean} success or not
     */
    addAll : function (collection) {
    	if (collection == null) {
            throw "argument can not be empty";
        }
        if (!collection instanceof com_yung_util_CollectionInterface) {
            throw "collection is not com.yung.util.CollectionInterface";
        }
        var array = collection.toArray();
        for (var i = 0; i < array.length; i++) {
            this.add(array[i]);
        }
        return true;
    },
    
    /** 
     * check collection contains element 
     * 
     * @instance
     * @memberof com_yung_util_ValueHashSet
     * @param  {validType} element - element to check if exist
     * @return {boolean} exist or not
     */
    contains : function (element) {
        if (element == null) {
            return false;
        }
        if (this.validType(element) == false) {
            return false;
        }
        var json = JSON.stringify(element);
        var hashCode = MD5Util.calc(json);
        if (this.obj[hashCode] == null) {
            return false;
        } else {
            return true;
        }
    },
    
    /**
     * Removes the specified element to this set 
     * 
     * @memberof com_yung_util_ValueHashSet
     * @param {validType} element - element to remove
     * @instance
     * @return {boolean} success or not
     */
    remove : function (element) {
        if (element == null) {
            return false;
        }
        if (typeof element != this.type) {
            return false;
        }
        var json = JSON.stringify(element);
        var hashCode = MD5Util.calc(json);
        if (this.obj[hashCode] == null) {
        	return false;
        }
        var index = this.binarySearchArray(this.tsoArray, this.obj[hashCode], 0, this.tsoArray.length - 1, false, true);
        if (index < 0) {
            return false;
        } else {
            this.array.splice(index, 1);
            this.tsoArray.splice(index, 1);
            delete this.obj[hashCode];
            return true;                
        }
    }
    
});

/**
 * sorted set, only allow type string and number
 * 
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_BasicSet
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_SortedSet = com_yung_util_BasicSet.extend({
	
    classProp : { 
    	name : "com.yung.util.SortedSet"
    },
    
    /**
     * reverse falg
     * @member {boolean}
     * @instance
     * @memberof com_yung_util_SortedSet
     */
    reverse : null,
    
    /**
     * element array
     * @member {string | number}
     * @instance
     * @memberof com_yung_util_SortedSet
     */
    array : null,
    obj : null,
    
    /**
	 * constructor
     * @memberof com_yung_util_SortedSet
     * @param  {string} type - element type
     * @param  {boolean} reverse - reverse flag
     */
    init : function(type, reverse){
        if (type == null) {
            throw "com.yung.util.SortedSet type can not be null!";
        }
        if (typeof type == "string") {
            type = type.toLowerCase();
        }
        if (type != 'string' && type != 'number') {
            throw "com.yung.util.SortedSet type can only be string or number!";
        }
        if (reverse === true) {
            this.reverse = true;
        } else {
            this.reverse = false;
        }
        this.type = type;
        this.array = [];
        this.obj = {};
        return this;
    },
    
    /**
     * Adds the specified element to this set 
     * 
     * @memberof com_yung_util_SortedSet
     * @param {validType} element - element to add
     * @instance
     * @return {boolean} success or not
     */
    add : function (element) {
        if (element == null) {
            return false;
        }
        if (this.validType(element) == false) {
            throw "argument type is not " + this.type;
        }
        if (this.obj[element + ""] == null) {
        	var index = this.binarySearchArray(this.array, element, 0, this.array.length - 1, this.reverse);
        	if (this.reverse) {
        		if (element < this.array[index]) {
        			index = index + 1;
        		}
        	} else {
        		if (element > this.array[index]) {
        			index = index + 1;
        		}
        	}
        	this.obj[element + ""] = this.tsoIdx;
        	this.array.splice(index, 0, element);
        }
        return true;
    },
    
    /**
     * Removes the specified element to this set 
     * 
     * @memberof com_yung_util_SortedSet
     * @param {validType} element - element to remove
     * @instance
     * @return {boolean} success or not
     */
    remove : function (element) {
        if (element == null) {
            return false;
        }
        if (this.validType(element) == false) {
        	return false;
        }
        if (this.obj[element + ""] == null) {
        	return false;
        }
        var index = this.binarySearchArray(this.array, element, 0, this.array.length - 1, this.reverse, true);
        if (index < 0) {
            return false;
        } else {
        	this.array.splice(index, 1);
        	delete this.obj[element + ""];
            return true;
        }
    }
});

/**
 * object hash set, only allow element which implements hashCode function
 * 
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_BasicSet
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_ObjectHashSet = com_yung_util_BasicSet.extend({
    
	classProp : { 
		name : "com.yung.util.ObjectHashSet"
	},
    
	/**
     * element type name
     * @member {string}
     * @instance
     * @memberof com_yung_util_ObjectHashSet
     */
	typeName : null,
    
	/**
     * element array
     * @member {object}
     * @instance
     * @memberof com_yung_util_ObjectHashSet
     */
	array : null,
	obj : null,
	tsoArray : [],
	tsoIdx : 0,
	
	/**
	 * constructor
     * @memberof com_yung_util_ObjectHashSet
     * @param  {function} type - element function
     */
	init : function(type){
        if (type == null) {
            throw "com.yung.util.ObjectHashSet type can not be null!";
        }
        if (typeof type != 'function') {
            throw "com.yung.util.ObjectHashSet type can only be function!";
        }
        if ($Class.checkFunction(type, 'hashCode') == false) {
            throw "Please implement hashCode function in " + $Class.getClassName(type);
        }
        this.type = type;
        this.typeName = $Class.getClassName(type);
        this.array = [];
        this.obj = {};
        return this;
    },
    
    /**
     * Adds the specified element to this set 
     * 
     * @memberof com_yung_util_ObjectHashSet
     * @param {validType} element - element to add
     * @instance
     * @return {boolean} success or not
     */
    add : function (element) {
        if (element == null) {
            return false;
        }
        if (this.validType(element) == false) {
            throw "argument type is not " + this.typeName;
        }
        var hashCode = element.hashCode();
        if (this.obj[hashCode] == null) {
            this.obj[hashCode] = this.tsoIdx;
            this.array.push(element);
            this.tsoArray.push(this.tsoIdx);
            this.tsoIdx++;
            return true;
        }
        return false;
    },
    
    /**
     * Adds the all elements of collection to this set 
     * 
     * @memberof com_yung_util_ObjectHashSet
     * @param  {com_yung_util_CollectionInterface} collection - collection to add
     * @instance
     * @return {boolean} success or not
     */
    addAll : function (collection) {
    	if (collection == null) {
            throw "argument can not be empty";
        }
        if (!collection instanceof com_yung_util_CollectionInterface) {
            throw "collection is not com.yung.util.CollectionInterface";
        }
        var array = collection.toArray();
        for (var i = 0; i < array.length; i++) {
            this.add(array[i]);
        }
        return true;
    },
    
    /** 
     * check collection contains element 
     * 
     * @instance
     * @memberof com_yung_util_ObjectHashSet
     * @param  {validType} element - element to check if exist
     * @return {boolean} exist or not
     */
    contains : function (element) {
        if (element == null) {
            return false;
        }
        if (this.validType(element) == false) {
            return false;
        }
        var hashCode = element.hashCode();
        if (this.obj[hashCode] == null) {
            return false;
        } else {
            return true;
        }
    },
    
    /**
     * Removes the specified element to this set 
     * 
     * @memberof com_yung_util_ObjectHashSet
     * @param {validType} element - element to remove
     * @instance
     * @return {boolean} success or not
     */
    remove : function (element) {
        if (element == null) {
            return false;
        }
        if (this.validType(element) == false) {
            return false;
        }
        var hashCode = element.hashCode();
        if (this.obj[hashCode] == null) {
        	return false;
        }
        var index = this.binarySearchArray(this.tsoArray, this.obj[hashCode], 0, this.tsoArray.length - 1, false, true);
        if (index < 0) {
            return false;
        } else {
            this.array.splice(index, 1);
            this.tsoArray.splice(index, 1);
            delete this.obj[hashCode];
            return true;
        }
    }
    
});

/**
 * tree object hash set, only allow element which implements hashCode and compareTo function
 * element will be sorted
 * 
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_ObjectHashSet
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_SortedHashSet = com_yung_util_ObjectHashSet.extend({
    
	classProp : { 
		name : "com.yung.util.SortedHashSet"
	},
    
	/**
     * reverse falg
     * @member {boolean}
     * @instance
     * @memberof com_yung_util_SortedHashSet
     */
	reverse : null,
    
	/**
     * element type name
     * @member {string}
     * @instance
     * @memberof com_yung_util_SortedHashSet
     */
	typeName : null,
    
	/**
     * element array
     * @member {object}
     * @instance
     * @memberof com_yung_util_SortedHashSet
     */
	array : null,
	obj : null,
	
	/**
	 * constructor
     * @memberof com_yung_util_SortedHashSet
     * @param  {function} type - element function
     * @param  {boolean} reverse - reverse flag
     */
	init : function(type, reverse){
        if (type == null) {
            throw "com.yung.util.SortedHashSet type can not be null!";
        }
        if (typeof type != 'function') {
            throw "com.yung.util.ObjectHashSet type can only be function!";
        }
        var typeName = $Class.getClassName(type);
        if ($Class.checkFunction(type, 'hashCode') == false) {
            throw "Please implement hashCode function in " + typeName;
        }
        if ($Class.checkFunction(type, 'compareTo') == false) {
            throw "Please implement compareTo function in " + typeName;
        }
        this.type = type;
        this.typeName = typeName;
        if (reverse === true) {
            this.reverse = true;
        } else {
            this.reverse = false;
        }
        this.array = [];
        this.obj = {};
        return this;
    },
    
    /**
     * Adds the specified element to this set 
     * 
     * @memberof com_yung_util_SortedHashSet
     * @param {validType} element - element to add
     * @instance
     * @return {boolean} success or not
     */
    add : function (element) {
        if (element == null) {
            return false;
        }
        if (this.validType(element) == false) {
            throw "argument type is not " + this.typeName;
        }
        var hashCode = element.hashCode();
        if (this.obj[hashCode] == null) {
        	var index = this.binarySearchArray(this.array, element, 0, this.array.length - 1, this.reverse);
        	if (this.reverse) {
        		if (this.array[index] != null && element.compareTo(this.array[index]) < 0) {
        			index = index + 1;
        		}
        	} else {
        		if (this.array[index] != null && element.compareTo(this.array[index]) > 0) {
        			index = index + 1;
        		}
        	}
        	this.array.splice(index, 0, element);
        	this.obj[hashCode] = this.tsoIdx;
        }
        return true;
    },
    
    /**
     * Removes the specified element to this set 
     * 
     * @memberof com_yung_util_SortedHashSet
     * @param {validType} element - element to remove
     * @instance
     * @return {boolean} success or not
     */
    remove : function (element) {
        if (element == null) {
            return false;
        }
        if (this.validType(element) == false) {
        	return false;
        }
        var hashCode = element.hashCode();
        if (this.obj[hashCode] == null) {
        	return false;
        }
        var index = this.binarySearchArray(this.array, element, 0, this.array.length - 1, this.reverse, true);
        if (index < 0) {
            return false;
        } else {
        	this.array.splice(index, 1);
        	delete this.obj[hashCode];
            return true;
        }
    }
});

/**
 * Basic list, only allow type string and number
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_Collection
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_AbstractQueue = com_yung_util_Collection.extend({
    
    classProp : { 
        name : "com.yung.util.AbstractQueue",
        unimplemented : ['add', 'remove']
    },
    
    /**
     * Retrieves, but does not remove, the head of this queue,
     * or returns null if this queue is empty.
     * 
     * @memberof com_yung_util_AbstractQueue
     * @instance
     * @return {element} retrieve element
     */
    peek : function () {
        if (this.array.length == 0) {
            return null;
        } else {
            return this.array[0];
        }
    },
    
    /**
     * Retrieves and removes the head of this queue,
     * or returns null if this queue is empty.
     * 
     * @memberof com_yung_util_AbstractQueue
     * @instance
     * @return {element} retrieve element
     */
    poll : function () {
        if (this.array.length == 0) {
            return null;
        } else {
            return this.array.shift();
        }
    }
    
});

/**
 * Basic list, only allow type string and number
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_AbstractQueue
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_BasicList = com_yung_util_AbstractQueue.extend({
    
	classProp : { 
		name : "com.yung.util.BasicList"
    },
    
    /**
	 * constructor
     * @memberof com_yung_util_BasicList
     * @param  {string} type - element type
     */
    init : function(type){
        if (type == null) {
            throw "com.yung.util.List type can not be null!";
        }
        if (typeof type == "string") {
            type = type.toLowerCase();
        }
        if (type != 'string' && type != 'number') {
            throw "com.yung.util.List type can only be string or number!";
        }
        this.type = type;
        this.array = [];
        return this;
    },
    
    /**
     * Adds the specified element to this list by index 
     * index is optional
     * 
     * @memberof com_yung_util_BasicList
     * @param  {validType} element - element to add
     * @param  {number} index - index to add
     * @instance
     * @return {boolean} success or not
     */
    add : function (element, index) {
    	if (this.validType(element) == false) {
            throw "argument type is not " + this.type;
        }
    	var ret = null;
        if (index == null) {
            ret = this.addElement(element);
        } else {
            ret = this.addByIndex(element, index);
        }
        return ret;
    },
    
    /**
     * Adds the specified element to this list 
     * 
     * @memberof com_yung_util_BasicList
     * @param  {validType} element - element to add
     * @instance
     * @return {boolean} success or not
     */
    addElement : function (element) {
        if (element == null) {
            return false;
        }
        if (this.validType(element) == false) {
            throw "argument type is not " + this.type;
        }
        this.array.push(element);
        return true;
    },
    
    /**
     * Adds the specified element to this list by index
     * 
     * @memberof com_yung_util_BasicList
     * @param  {validType} element - element to add
     * @param  {number} index - index to add
     * @instance
     * @return {boolean} success or not
     */
    addByIndex : function (element, index) {
        if (element == null) {
            return false;
        }
        if (this.validType(element) == false) {
            throw "argument type is not " + this.type;
        }
        this.array.splice(index, 0, element);
        return true;
    },
    
    /**
     * Adds the elements of collection to this list 
     * 
     * @memberof com_yung_util_BasicList
     * @param  {com_yung_util_CollectionInterface} collection - collection to add
     * @param  {number} index - index to add, if null then add to list tail
     * @instance
     * @return {boolean} success or not
     */
    addAll : function (collection, index) {
    	var ret = null;
        if (index == null) {
            ret = this.addAllElements(collection);
        } else {
            ret = this.addAllByIndex(collection, index);
        }
        return ret;
    },
    
    /**
     * Adds the elements of collection to this list tail 
     * 
     * @memberof com_yung_util_BasicList
     * @param  {com_yung_util_CollectionInterface} collection - collection to add
     * @instance
     * @return {boolean} success or not
     */
    addAllElements : function (collection) {
    	if (collection == null) {
            throw "argument can not be empty";
        }
        if (!collection instanceof com_yung_util_CollectionInterface) {
            throw "collection is not com.yung.util.CollectionInterface";
        }
        var array = collection.toArray();
        for (var i = 0; i < array.length; i++) {
            this.addElement(array[i]);
        }
        return true;
    },
    
    /**
     * Adds the elements of collection to this list 
     * 
     * @memberof com_yung_util_BasicList
     * @param  {com_yung_util_CollectionInterface} collection - collection to add
     * @param  {number} index - index to add, if null then add to list tail
     * @instance
     * @return {boolean} success or not
     */
    addAllByIndex : function (collection, index) {
    	if (collection == null) {
            throw "argument can not be empty";
        }
        if (!collection instanceof com_yung_util_CollectionInterface) {
            throw "collection is not com.yung.util.CollectionInterface";
        }
        var array = collection.toArray();
        var idx = index;
        for (var i = 0; i < array.length; i++) {
        	this.addByIndex(array[i], idx);
            idx++;
        }
        return true;
    },
    
    /**
     * get element by index
     * 
     * @memberof com_yung_util_BasicList
     * @param  {number} index - index of element
     * @instance
     * @return {element} element
     */
    get : function (index) {
        return this.array[index];
    },
    
    /**
     * Returns the index of the first occurrence of the specified element in this list
     * 
     * @memberof com_yung_util_BasicList
     * @param  {validType} element - element
     * @instance
     * @return {number} index
     */
    indexOf : function (element) {
        if (element == null) {
            return -1;
        }
        if (this.validType(element) == false) {
            return -1;
        }
        for (var i = 0; i < this.array.length; i++) {
            if (element === this.array[i]) {
                return i;
            }
        }
        return -1;
    },
    
    /**
     * Returns the index of the last occurrence of the specified element in this list
     * 
     * @memberof com_yung_util_BasicList
     * @param  {validType} element - element
     * @instance
     * @return {number} index
     */
    lastIndexOf : function (element) {
        if (element == null) {
            return -1;
        }
        if (this.validType(element) == false) {
            return -1;
        }
        for (var i = this.array.length - 1; i >= 0; i--) {
            if (element === this.array[i]) {
                return i;
            }
        }
        return -1;
    },
    
    /**
     * Removes the element at the specified position in this list
     * 
     * @memberof com_yung_util_BasicList
     * @param  {number} index - index
     * @instance
     * @return {validType} removed element
     */
    removeByIndex : function (index) {
        if (index < 0 || index >= this.array.length) {
            return null;
        }
        var ret = this.array[index];
        this.array.splice(index, 1);
        return ret;
    },
    
    /**
     * Removes the first occurrence of the specified element from this list
     * 
     * @memberof com_yung_util_BasicList
     * @param  {validType} element - element
     * @instance
     * @return {boolean} success or not
     */
    removeElement : function (element) {
        if (element == null) {
            return false;
        }
        if (this.validType(element) == false) {
            return false;
        }
        var index = jQuery.inArray(element, this.array);
        if (index < 0) {
            return false;
        } else {
            if (index !== -1) {
                this.array.splice(index, 1);
                return true;
            } else {
                return false;
            }
        }
    },
    remove : function (element) {
        return this.removeElement(element);
    },
    
    /**
     * Removes from this list all of its elements that are contained in the specified collection
     * 
     * @memberof com_yung_util_BasicList
     * @param  {com_yung_util_CollectionInterface} collection - collection to remove
     * @instance
     * @return {boolean} success or not
     */
    removeAll : function (collection) {
        if (collection == null) {
            return false;
        }
        if (this.isCollection(collection) == false) {
            return false;
        }
        var array = collection.toArray();
        for (var i = 0; i < array.length; i++) {
            this.removeElement(array[i]);
        }
        return true;
    },
    
    /**
     * set element by specified index
     * 
     * @memberof com_yung_util_BasicList
     * @param  {number} index - index to set
     * @param  {validType} element - element to set
     * @instance
     * @return {validType} element to set
     */
    set : function (index, element) {
    	if (this.validType(element) == false) {
    		throw "argument type is not " + this.type;
        }
    	this.array[index] = element;
        return element;
    },
    
    /**
     * Returns a view of the portion of this list between the specified fromIndex, inclusive, and toIndex, exclusive.
     * 
     * @memberof com_yung_util_BasicList
     * @param  {number} fromIndex - from index
     * @param  {number} toIndex - to index
     * @instance
     * @return {com_yung_util_BasicList} result list
     */
    subList : function (fromIndex, toIndex) {
        var ret = new com.yung.util.List(this.type);
        for (var i = 0; i < this.array.length; i++) {
            if (i >= fromIndex && i <= toIndex) {
                ret.add(this.array[i]);
            }
        }
        return ret;
    }
});

/**
 * raw object list, only allow type object
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_BasicList
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_RawObjectList = com_yung_util_BasicList.extend({
    
	classProp : { 
		name : "com.yung.util.RawObjectList"
    },
    
    /**
	 * constructor
     * @memberof com_yung_util_BasicList
     */
    init : function(){
        this.type = 'object';
        this.array = [];
        return this;
    }
});

/**
 * function object list, only allow type function object
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_BasicList
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_ObjectList = com_yung_util_BasicList.extend({
    
	classProp : { 
		name : "com.yung.util.ObjectList"
	},
    
	/**
     * element type name
     * @member {string}
     * @instance
     * @memberof com_yung_util_ObjectList
     */
	typeName : null,
    
	/**
	 * constructor
     * @memberof com_yung_util_BasicList
     * @param  {function} type - element function
     */
	init : function(type){
        if (type == null) {
            throw "com.yung.util.ObjectList type can not be null!";
        }
        if (typeof type != 'function') {
            throw "com.yung.util.ObjectList type can only be function!";
        }
        this.type = type;
        this.typeName = $Class.getClassName(type);
        this.array = [];
        return this;
    }
});

/**
 * Link node
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_LinkNode = $Class.extend({
    
    classProp : { 
        name : "com.yung.util.LinkNode"
    },
    
    /**
     * element type
     * @member {all}
     * @instance
     * @memberof com_yung_util_LinkNode
     */
    type : null,
    
    /**
     * before link node
     * @member {com_yung_util_LinkNode}
     * @instance
     * @memberof com_yung_util_LinkNode
     */
    before : null,
    
    /**
     * after link node
     * @member {com_yung_util_LinkNode}
     * @instance
     * @memberof com_yung_util_LinkNode
     */
    after : null,
    
    /**
     * element
     * @member {all}
     * @instance
     * @memberof com_yung_util_LinkNode
     */
    value : null,
    
    /**
     * constructor
     * @memberof com_yung_util_LinkNode
     * @param  {all} type - element type
     */
    init : function(type){
        if (type == null) {
            throw "com.yung.util.LinkNode type can not be null!";
        }
        this.type = type;
        return this;
    },
    
    /**
     * set before link node
     * 
     * @instance
     * @memberof com_yung_util_LinkNode
     * @param  {com_yung_util_LinkNode} beforeNode - before link node
     */
    setBefore : function (beforeNode) {
        this.before = beforeNode;
    },
    
    /**
     * set after link node
     * 
     * @instance
     * @memberof com_yung_util_LinkNode
     * @param  {com_yung_util_LinkNode} afterNode - after link node
     */
    setAfter : function (afterNode) {
        this.after = afterNode;
    },
    
    /**
     * next link node
     * 
     * @instance
     * @memberof com_yung_util_LinkNode
     * @return {com_yung_util_LinkNode} next link node
     */
    next : function () {
        return this.after;
    },
    
    /**
     * set link node value
     * 
     * @instance
     * @memberof com_yung_util_LinkNode
     * @param  {com_yung_util_LinkNode} value - link node value
     */
    setValue : function (value) {
        if (this.validType(value) == false) {
            throw value + " is not " + this.type;
        }
        this.value = value;
    },
    
    /**
     * get link node value
     * 
     * @instance
     * @memberof com_yung_util_LinkNode
     * @return {all} link node value
     */
    getValue : function () {
        return this.value;
    },
    
    /** 
     * check element type is valid
     * 
     * @instance
     * @memberof com_yung_util_LinkNode
     * @param  element - element to check
     * @return {boolean} element type valid or not
     */
    validType : function (element) {
        return _validType(this.type, element);
    }
    
});

/**
 * Abstract Class for link collection
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @property {Array}  classProp.unimplemented - unimplemented method
 * @augments com_yung_util_CollectionInterface
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_LinkCollection = com_yung_util_CollectionInterface.extend({
    
    classProp : { 
        name : "com.yung.util.LinkCollection",
        unimplemented : ['add', 'remove']
    },
    
    /**
     * element type
     * @member {all}
     * @instance
     * @memberof com_yung_util_LinkCollection
     */
    type : null,
    
    /**
     * object to store link node
     * @member {object}
     * @private
     * @memberof com_yung_util_LinkCollection
     */
    obj : null,
    
    /**
     * collection length
     * @member {number}
     * @instance
     * @memberof com_yung_util_LinkCollection
     */
    length : 0,
    
    /**
     * first linked node
     * @member {com_yung_util_LinkNode}
     * @instance
     * @memberof com_yung_util_LinkCollection
     */
    first : null,
    
    /**
     * last linked node
     * @member {com_yung_util_LinkNode}
     * @instance
     * @memberof com_yung_util_LinkCollection
     */
    last : null,
    
    /**
     * constructor
     * @memberof com_yung_util_LinkCollection
     * @param  {string} type - element type
     */
    init : function(type){
        if (type == null) {
            throw "com.yung.util.LinkCollection type can not be null!";
        }
        this.type = type;
        this.obj = {};
        return this;
    },
    
    /** 
     * clear all elements
     * 
     * @instance
     * @memberof com_yung_util_LinkCollection
     */
    clear : function () {
        this.obj = {};
        this.length = 0;
    },
    
    /**
     * add elements by array
     * 
     * @instance
     * @memberof com_yung_util_LinkCollection
     * @param  {Array} elementArray - element array
     * @return {boolean} success or not
     */
    addByArray : function (elementArray) {
        if (elementArray != null) {
            if (jQuery.isArray(elementArray)) {
                for (var i = 0; i < elementArray.length; i++) {
                    this.add(elementArray[i]);
                }
                return true;
            } else {
                throw "argument type is not array";
            }
        }
        return true;
    },
    
    /** 
     * check collection contains element 
     * 
     * @instance
     * @memberof com_yung_util_LinkCollection
     * @param  {validType} element - element to check if exist
     * @return {boolean} exist or not
     */
    contains : function (element) {
        if (element == null) {
            return false;
        }
        if (this.validType(element) == false) {
            return false;
        }
        if (this.type == 'string' || this.type == 'number') {
            if (this.obj[element + ""] == null) {
                return false;
            } else {
                return true;
            }
        } else if (typeof this.type == 'function') {
            var hash = null;
            if (this.valueHash == true) {
                var json = JSON.stringify(element);
                hash = MD5Util.calc(json);
            } else {
                if (typeof element["hashCode"] == 'function') {
                    hash = element["hashCode"]();
                } 
            }
            if (hash != null) {
                if (this.obj[hash] == null) {
                    return false;
                } else {
                    return true;
                }
            }
        } else if (this.type == 'object') {
            var hash = null;
            if (this.valueHash == true) {
                var json = JSON.stringify(element);
                hash = MD5Util.calc(json);
            }
            if (hash != null) {
                if (this.obj[hash] == null) {
                    return false;
                } else {
                    return true;
                }
            }
        }
        throw "unknow condition";
    },
    
    /** 
     * check collection is empty 
     * 
     * @instance
     * @memberof com_yung_util_LinkCollection
     * @return {boolean} empty or not
     */
    isEmpty : function () {
        if (this.size() == 0) {
            return true;
        } else {
            return false;
        }
    },
    
    /** 
     * get collection size 
     * 
     * @instance
     * @memberof com_yung_util_LinkCollection
     * @return {number} collection size
     */
    size : function () {
        return this.length;
    },
    
    /** 
     * return element array
     * 
     * @instance
     * @memberof com_yung_util_LinkCollection
     * @return {Array} element array
     */
    toArray : function () {
        var ret = [];
        ret.push(this.first.getValue());
        var linkNode = this.first;
        while (linkNode.next() != null) {
            linkNode = linkNode.next();
            ret.push(linkNode.getValue());
        }
        return ret;
    },
    
    /** 
     * to string
     * 
     * @instance
     * @memberof com_yung_util_LinkCollection
     * @return {string} message string
     */
    toString : function () {
        var array = this.toArray();
        return this.classProp.name + JSON.stringify(array);
    },
    
    /** 
     * check object is link collection or not
     * 
     * @memberof com_yung_util_LinkCollection
     * @param  {object} obj - object to check
     * @return {boolean} is link collection
     */
    isCollection : function (obj) {
    	var ret = obj instanceof com.yung.util.CollectionInterface;
        return ret;
    },
    
    /**
     * Adds the all elements of collection to this set 
     * 
     * @memberof com_yung_util_LinkCollection
     * @param  {com_yung_util_CollectionInterface} collection - collection to add
     * @instance
     * @return {boolean} success or not
     */
    addAll : function (collection) {
        if (collection == null) {
            throw "argument can not be empty";
        }
        if (!collection instanceof com_yung_util_CollectionInterface) {
            throw "collection is not com.yung.util.CollectionInterface";
        }
        var array = collection.toArray();
        for (var i = 0; i < array.length; i++) {
            this.add(array[i]);
        }
        return true;
    },
    
    getFirstElement :  function () {
        if (this.size() == 0) {
            return null;
        }
        return this.first.getValue();
    },
    
    /** 
     * check element type is valid
     * 
     * @instance
     * @memberof com_yung_util_LinkCollection
     * @param  element - element to check
     * @return {boolean} element type valid or not
     */
    validType : function (element) {
        return _validType(this.type, element);
    }
});

/**
 * basic linked set, only allow type string and number.
 * better for large scale, ex more than 100000
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_LinkCollection
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_LinkedSet = com_yung_util_LinkCollection.extend({
    
    classProp : { 
        name : "com.yung.util.LinkedSet"
    },
    
    /**
     * constructor
     * @memberof com_yung_util_LinkCollection
     * @param  {string} type - element type
     */
    init : function(type){
        if (type == null) {
            throw "com.yung.util.LinkedSet type can not be null!";
        }
        if (typeof type != 'string') {
            throw "com.yung.util.LinkedSet type can only be string or number";
        }
        if (type != 'string' && type != 'number') {
            throw "com.yung.util.LinkedSet type can only be string or number";
        }
        this.type = type;
        this.obj = {};
        return this;
    },
    
    /**
     * add element
     * 
     * @instance
     * @memberof com_yung_util_LinkCollection
     * @param  {all} element - element to add
     * @return {boolean} success or not
     */
    add : function (element) {
        if (element == null) {
            return false;
        }
        if (this.validType(element) == false) {
            return false;
        }
        var linkNode = new com_yung_util_LinkNode(this.type);
        linkNode.setValue(element);
        if (this.length == 0) {
            this.first = linkNode;
        } else {
            if (this.obj[element + ""] != null) {
                return false;
            }
            if (this.last == null) {
                linkNode.setBefore(this.first);
                this.first.setAfter(linkNode);
                this.last = linkNode;
            } else {
                linkNode.setBefore(this.last);
                this.last.setAfter(linkNode);
                this.last = linkNode;
            }
        }
        this.length++;
        this.obj[element + ""] = linkNode;
    },
    
    /**
     * remove element
     * 
     * @instance
     * @memberof com_yung_util_LinkCollection
     * @param  {all} element - element to remove
     * @return {all} removed element
     */
    remove : function (element) {
        if (element == null) {
            return null;
        }
        if (this.validType(element) == false) {
            return null;
        }
        var linkNode = this.obj[element + ""];
        if (linkNode == null) {
            return null;
        }
        if (linkNode == this.first) {
            var newFirst = this.first.after;
            this.first = newFirst;
        } else if (linkNode == this.last) {
            var newLast = this.last.before;
            this.last = newLast;
        } else {
            var prev = linkNode.before;
            var next = linkNode.after;
            prev.after = next;
            next.before = prev;
        }
        delete this.obj[element + ""];
        this.length--;
        return element;
    }
    
});

/**
 * JS utility Set by restrict type, best performance
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_CollectionInterface
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var system_util_Set = com_yung_util_CollectionInterface.extend({
    
    classProp : { 
        name : "system.util.Set"
    },
    
    /**
     * element type
     * @member {all}
     * @instance
     * @memberof system_util_Set
     */
    type : null,
    
    /**
     * JS set to store value
     * @member {Set}
     * @private
     * @memberof system_util_Set
     */
    obj : null,
    
    /**
     * constructor
     * @memberof system_util_Set
     * @param  {string} type - element type
     */
    init : function(type){
        if (type == null) {
            throw "system.util.Set type can not be null!";
        }
        this.type = type;
        this.obj = new Set();
        return this;
    },
    
    /** 
     * clear all elements
     * 
     * @instance
     * @memberof system_util_Set
     */
    clear : function () {
        this.obj.clear();
    },
    
    /**
     * add elements by array
     * 
     * @instance
     * @memberof system_util_Set
     * @param  {Array} elementArray - element array
     * @return {boolean} success or not
     */
    addByArray : function (elementArray) {
        if (elementArray != null) {
            if (jQuery.isArray(elementArray)) {
                for (var i = 0; i < elementArray.length; i++) {
                    this.add(elementArray[i]);
                }
                return true;
            } else {
                throw "argument type is not array";
            }
        }
        return true;
    },
    
    /** 
     * check collection contains element 
     * 
     * @instance
     * @memberof system_util_Set
     * @param  {validType} element - element to check if exist
     * @return {boolean} exist or not
     */
    contains : function (element) {
        if (this.validType(element) == false) {
            return false;
        }
        return this.obj.has(element);
    },
    
    /** 
     * check collection is empty 
     * 
     * @instance
     * @memberof system_util_Set
     * @return {boolean} empty or not
     */
    isEmpty : function () {
        if (this.obj.size == 0) {
            return true;
        } else {
            return false;
        }
    },
    
    /** 
     * get collection size 
     * 
     * @instance
     * @memberof system_util_Set
     * @return {number} collection size
     */
    size : function () {
        return this.obj.size;
    },
    
    /** 
     * return element array
     * 
     * @instance
     * @memberof system_util_Set
     * @return {Array} element array
     */
    toArray : function () {
        var ret = [];
        this.obj.forEach(function (index, value, set) {
            ret.push(value);
        });
        return ret;
    },
    
    /** 
     * to string
     * 
     * @instance
     * @memberof system_util_Set
     * @return {string} message string
     */
    toString : function () {
        var array = this.toArray();
        return this.classProp.name + JSON.stringify(array);
    },
    
    /** 
     * check element type is valid
     * 
     * @instance
     * @memberof system_util_Set
     * @param  element - element to check
     * @return {boolean} element type valid or not
     */
    validType : function (element) {
        return _validType(this.type, element);
    },
    
    /**
     * add element
     * 
     * @instance
     * @memberof system_util_Set
     * @param  {all} element - element to add
     * @return {boolean} success or not
     */
    add : function (element) {
        if (element == null) {
            return false;
        }
        if (this.validType(element) == false) {
            return false;
        }
        return this.obj.add(element);
    },
    
    /**
     * Adds the all elements of collection to this set 
     * 
     * @memberof system_util_Set
     * @param  {com_yung_util_CollectionInterface} collection - collection to add
     * @instance
     * @return {boolean} success or not
     */
    addAll : function (collection) {
        if (collection == null) {
            throw "argument can not be empty";
        }
        if (!collection instanceof com_yung_util_CollectionInterface) {
            throw "collection is not com.yung.util.CollectionInterface";
        }
        var array = collection.toArray();
        for (var i = 0; i < array.length; i++) {
            this.add(array[i]);
        }
        return true;
    },
    
    /**
     * remove element
     * 
     * @instance
     * @memberof system_util_Set
     * @param  {all} element - element to remove
     * @return {all} removed element
     */
    remove : function (element) {
        if (element == null) {
            return null;
        }
        if (this.validType(element) == false) {
            return null;
        }
        return this.obj["delete"](element);
    },
    
    isCollection : function (obj) {
    	var ret = obj instanceof com.yung.util.CollectionInterface;
        return ret;
    },
    
    getFirstElement :  function () {
        if (this.size() == 0) {
            return null;
        }
        var ret = null;
        try {
            this.obj.forEach(function (index, value, set) {
                ret = value;
                throw "done";
            });
        } catch (e) {
            if ("done" != (e + "")) {
                throw e;
            }
        }
        return ret;
    }
    
});

/**
 * Tree set collection, use TreeMap as base
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_CollectionInterface
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_TreeSet = com_yung_util_CollectionInterface.extend({
    
    classProp : { 
        name : "com.yung.util.TreeSet"
    },

    /**
     * element type
     * @member {all}
     * @instance
     * @memberof com_yung_util_TreeSet
     */
    type : null,
    reverse : null,
    map : null,
    validType : function (element) {
        return _validType(this.type, element);
    },
    
    /**
     * constructor
     * @memberof com_yung_util_TreeSet
     * @param  {function} type - element function
     * @param  {boolean} reverse - reverse flag
     */
    init : function(type, reverse){
        if (type == null) {
            throw "com.yung.util.TreeSet type can not be null!";
        }
        if (type != 'string' && type != 'number') {
            throw "com.yung.util.TreeSet type can only be string or number!";
        }
        this.type = type;
        if (reverse == true) {
            this.reverse = true;
        } else {
            this.reverse = false;
        }
        this.map = new com.yung.util.TreeMap(type, type, reverse);
        return this;
    },
    
    /**
     * Adds the specified element to this set 
     * 
     * @memberof com_yung_util_TreeSet
     * @param {validType} element - element to add
     * @instance
     * @return {boolean} success or not
     */
    add : function (element) {
        if (element == null) {
            return false;
        }
        if (this.validType(element) == false) {
            throw "argument type is not " + this.type;
        }
        var val = null;
        if (this.type == 'string') {
            val = "";
        } else if (this.type == 'number') {
            val = 0;
        }
        return this.map.put(element, val);
    },
    
    /**
     * Adds the all elements of collection to this set 
     * 
     * @memberof com_yung_util_TreeSet
     * @param  {com_yung_util_CollectionInterface} collection - collection to add
     * @instance
     * @return {boolean} success or not
     */
    addAll : function (collection) {
        if (collection == null) {
            throw "argument can not be empty";
        }
        if (!collection instanceof com_yung_util_CollectionInterface) {
            throw "collection is not com.yung.util.CollectionInterface";
        }
        var array = collection.toArray();
        for (var i = 0; i < array.length; i++) {
            this.add(array[i]);
        }
        return true;
    },
    
    /**
     * Removes the specified element to this set 
     * 
     * @memberof com_yung_util_TreeSet
     * @param {validType} element - element to remove
     * @instance
     * @return {boolean} success or not
     */
    remove : function (element) {
        if (element == null) {
            return false;
        }
        if (this.validType(element) == false) {
            return false;
        }
        return this.map.remove(element);
    },
    
    /** 
     * clear all elements
     * 
     * @instance
     * @memberof com_yung_util_TreeSet
     */
    clear : function () {
        this.map.clear();
    },
    
    /**
     * add elements by array
     * 
     * @instance
     * @memberof com_yung_util_TreeSet
     * @param  {Array} elementArray - element array
     * @return {boolean} success or not
     */
    addByArray : function (elementArray) {
        if (elementArray != null) {
            if (jQuery.isArray(elementArray)) {
                for (var i = 0; i < elementArray.length; i++) {
                    this.add(elementArray[i]);
                }
                return true;
            } else {
                throw "argument type is not array";
            }
        }
        return true;
    },
    
    /** 
     * check collection contains element 
     * 
     * @instance
     * @memberof com_yung_util_TreeSet
     * @param  {validType} element - element to check if exist
     * @return {boolean} exist or not
     */
    contains : function (element) {
        return this.map.containsKey(element);
    },
    
    /** 
     * check collection is empty 
     * 
     * @instance
     * @memberof com_yung_util_TreeSet
     * @return {boolean} empty or not
     */
    isEmpty : function () {
        if (this.map.size() == 0) {
            return true;
        } else {
            return false;
        }
    },
    
    /** 
     * get collection size 
     * 
     * @instance
     * @memberof com_yung_util_TreeSet
     * @return {number} collection size
     */
    size : function () {
        return this.map.size();
    },
    
    /** 
     * return element array
     * 
     * @instance
     * @memberof com_yung_util_TreeSet
     * @return {Array} element array
     */
    toArray : function () {
        return this.map.getKeyArray();
    },
    
    getFirstElement : function () {
        return this.map.firstKey();
    },
    
    /** 
     * to string
     * 
     * @instance
     * @memberof com_yung_util_TreeSet
     * @return {string} message string
     */
    toString : function () {
        var ret = "";
        var array = this.toArray();
        if (array.length > 0 && (this.type == 'string' || this.type == 'number')) {
            ret = ret + "[";
        }
        for (var i = 0; i < array.length; i++) {
            if (this.type == 'string') {
                ret = ret + "'" + array[i] + "', ";
            } else if (this.type == 'number') {
                ret = ret + array[i] + ", ";
            } else {
                if (i == 0) ret = ret + "\n";
                if (jQuery.isArray(array[i])) {
                    ret = ret + "    " + JSON.stringify(array[i]) + ",\n"; 
                } else if (typeof array[i].toString == "function") {
                    ret = ret + "    " + array[i].toString() + ",\n"; 
                } else {
                    ret = ret + "    " + JSON.stringify(array[i]) + ",\n"; 
                }
            }
        }
        if (ret.length > 0) {
            ret = ret.substring(0, ret.length - 2);
        }
        if (array.length > 0 && (this.type == 'string' || this.type == 'number')) {
            ret = ret + "]";
        }
        return this.classProp.name + ret;
    }
    
});

var com_yung_util_Set = function (type, valueHash) {
    if (typeof type == "string") {
        type = type.toLowerCase();
    }
    if (type == 'string' || type == 'number') {
        if (typeof Set != 'undefined') {
            return new system_util_Set(type);
        }
        return new com.yung.util.BasicSet(type);
    }
    if (typeof type == 'function') {
        if (valueHash == true) {
            return new com.yung.util.ValueHashSet(type);
        } else {
            if ($Class.checkFunction(type, 'hashCode') == true) {
                return new com.yung.util.ObjectHashSet(type);
            }
        }
    }
    if (type == 'object') {
        if (valueHash == true) {
            return new com.yung.util.ValueHashSet(type);
        } else {
            if (typeof Set != 'undefined') {
                return new system_util_Set(type);
            }
            return new com.yung.util.RawObjectSet(type);
        }
    }
    throw "argument type not match any type";
}
$Y.register("com.yung.util.Set", com_yung_util_Set);

var com_yung_util_List = function (type) {
    if (typeof type == "string") {
        type = type.toLowerCase();
    }
    if (type == 'string' || type == 'number') {
        return new com.yung.util.BasicList(type);
    }
    if (type == 'object') {
        return new com.yung.util.RawObjectList(type);
    }
    if (typeof type == 'function') {
        return new com.yung.util.ObjectList(type);
    } else {
        throw "argument type not match any type";
    }
}
$Y.register("com.yung.util.List", com_yung_util_List);

/**
 * Combo box, like autocomplete
 * 
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_Combo = $Class.extend({

    classProp : { 
        name : "com.yung.util.Combo" 
    },
    
    hashId : '',
    
    /**
     * minimum length for filter
     * @member {number}
     * @instance
     * @memberof com_yung_util_Combo
     */
    minLength : 2,
    
    /**
     * maximum items to display
     * @member {number}
     * @instance
     * @memberof com_yung_util_Combo
     */
    maxItems : 8,
    
    /**
     * element id
     * @member {string}
     * @instance
     * @memberof com_yung_util_Combo
     */
    eleId : null,
    
    /**
     * float div to place word list
     * @member {com_yung_util_FloatDiv}
     * @instance
     * @memberof com_yung_util_Combo
     */
    floatDiv : null,
    displayHeight : 250,
    
    /**
     * select value array
     * @member {com_yung_util_Set}
     * @instance
     * @memberof com_yung_util_Combo
     */
    selectValues : null,
    
    
    selectIndex : null,
    
    
    chosedValue : "",
    
    /**
     * Callback for customized filter for selectValues
     *
     * @callback ComboCustomFilter
     * @param {string} reqCode - input code
     * @param {com_yung_util_Combo} combo - com_yung_util_Combo instance
     * @return {Array} customized select value array
     */
    /**
     * customized filter for selectValues
     * @member {ComboCustomFilter}
     * @instance
     * @memberof com_yung_util_Combo
     */
    filter : null,
    
    /**
     * Callback after combo choose value
     *
     * @callback AfterComboChoose
     * @param {string} eleId - element id
     * @param {string} inputVal - select value
     */
    /**
     * callback after combo choose value
     * @member {AfterComboChoose}
     * @instance
     * @memberof com_yung_util_Combo
     */
    afterChoose : null,
    
    
    /**
     * constructor
     * @memberof com_yung_util_Combo
     * @param  {string} eleId - element id
     * @param  {Array} selectValues - select value array
     */
    init : function (eleId, selectValues) {
        $Class.validate(this.classProp.name, eleId);
        if (eleId == null || eleId == '') {
            throw "Please specify eleId";
        }
        this.eleId = eleId;
        this.hashId = MD5Util.calc(this.classProp.name + "-" + eleId);
        this.selectValues = new com_yung_util_Set('string');
        this.setSelectValues(selectValues);
        jQuery("#" + eleId).attr("autocomplete", "off");
        this.createCombo();
        return this;
    },
    
    /** 
     * set select values
     * 
     * @instance
     * @memberof com_yung_util_Combo
     * @param {Array | com_yung_util_CollectionInterface} selectValues - select values
     */
    setSelectValues : function (selectValues) {
        if (selectValues == null) {
            return;
        }
        if (_validType(Array, selectValues) == true) {
            this.selectValues.clear();
            this.selectValues.addByArray(selectValues);
        } else if (_validType(com_yung_util_CollectionInterface, selectValues) == true) {
            this.selectValues.clear();
            this.selectValues.addAll(selectValues);
        } else {
            throw "selectValues can only be Array or com.yung.util.CollectionInterface type";
        }
    },
    
    /** 
     * add select values
     * 
     * @instance
     * @memberof com_yung_util_Combo
     * @param {string} selectValue - select value
     */
    addSelectValues : function (selectValue) {
        if (selectValue == null) {
            return;
        }
        if (_validType('string', selectValue) == false) {
            throw "selectValue can only be string";
        }
        this.selectValues.add(selectValue);
    },
    
    /** 
     * set minimum length for filter
     * 
     * @instance
     * @memberof com_yung_util_Combo
     * @param {number} minLength - minimum length for filter
     */
    setMinLength : function (minLength) {
        if (minLength == null) {
            return;
        }
        if (_validType('number', minLength) == false) {
            throw "minLength can only be number";
        }
        this.minLength = minLength;
    },
    
    /** 
     * set maximum items to display
     * 
     * @instance
     * @memberof com_yung_util_Combo
     * @param {number} maxItems - maximum items to display
     */
    setMaxItems : function (maxItems) {
        if (maxItems == null) {
            return;
        }
        if (_validType('number', maxItems) == false) {
            throw "maxItems can only be number";
        }
        this.maxItems = maxItems;
    },
    
    /** 
     * set customized filter
     * 
     * @instance
     * @memberof com_yung_util_Combo
     * @param {ComboCustomFilter} filter - customized filter for selectValues
     */
    setFilter : function (filter) {
        if (typeof filter != 'function') {
            throw "filter is not function";
        }
        this.filter = filter;
    },
    
    /** 
     * set after choose callback
     * 
     * @instance
     * @memberof com_yung_util_Combo
     * @param {AfterComboChoose} afterChoose - after choose callback
     */
    setAfterChoose : function (afterChoose) {
        if (typeof afterChoose != 'function') {
            throw "afterChoose is not function";
        }
        this.afterChoose = afterChoose;
    },
    
    /** 
     * set input value
     * 
     * @instance
     * @memberof com_yung_util_Combo
     * @param {string} inputVal - string put into input
     */
    setInputValue : function (inputVal) {
        if (inputVal == null) {
            inputVal = this.chosedValue;
        }
        jQuery("#" + this.eleId).val(inputVal);
        if (typeof this.afterChoose == 'function') {
            this.afterChoose(this.eleId, inputVal);
        }
        this.floatDiv.closeFloatDiv();
        this.selectIndex = null;
        this.chosedValue = '';
    },
    
    /** 
     * standard filter
     * 
     * @instance
     * @memberof com_yung_util_Combo
     * @param {string} reqCode - request code from input
     * @return {Array} filtered value array
     */
    standardFilter : function (reqCode) {
        var displayValues = [];
        var array = this.selectValues.toArray();
        for (var i = 0; i < array.length; i++) {
            var val = array[i];
            if (val.indexOf(reqCode) >= 0) {
                displayValues.push(val);
            }
        }
        return displayValues;
    },
    
    /** 
     * create combo
     * 
     * @private
     * @instance
     * @memberof com_yung_util_Combo
     */
    createCombo : function () {
        this.floatDiv = $Class.getInstance("com.yung.util.FloatDiv", "combo-" + this.eleId, true, "overflow-x: hidden; overflow-y: auto;");
        this.bindFunction();
    },
    bindFunction : function () {
        var bindId = this.eleId;
        var minLen = this.minLength;
        var self = this;
        jQuery("#" + bindId).keyup(function(event){
            var length = jQuery("#yung-Combo-table-" + bindId + " td").length;
            var scrollExist = false;
            var floatDivElement = self.floatDiv.getFloatDivElement();
            if (jQuery("#yung-Combo-table-" + bindId).height() > self.displayHeight) {
                scrollExist = true;
            }
            // press up
            if (event.keyCode == 38) {
                event.preventDefault();
                if (self.selectIndex == 0) {
                    // already on top
                    return;
                }
                if (self.selectIndex == null) {
                    return;
                } else {
                    var inputbox = self;
                    var setIdx = null;
                    var setVal = '';
                    jQuery("#yung-Combo-table-" + bindId + " td").each(function( index ) {
                        if (inputbox.selectIndex == (index + 1)) {
                            setIdx = index;
                            setVal = jQuery( this ).text();
                            jQuery( this ).css("background", "#A4BED4");
                            if (scrollExist) {
                                var position = new com.yung.util.Position(jQuery(this));
                                var botPos = position.getBotLeftPosition(null, null, floatDivElement);
                                var topPos = position.getTopLeftPosition(null, null, floatDivElement);
                                var currentScrollTop = floatDivElement.scrollTop();
                                var tdHeight = jQuery(this).height();
                                if (botPos.top > (currentScrollTop + self.displayHeight)){
                                    // outside bottom
                                    var offsetY = 3;
                                    if (com_yung_util_getbrowser() == 'msie') {
                                        offsetY = offsetY + 3;
                                    }
                                    floatDivElement.scrollTop(botPos.top - self.displayHeight + offsetY);
                                }
                                if (currentScrollTop > topPos.top) {
                                    // outside top
                                    floatDivElement.scrollTop(topPos.top - 3);
                                }
                            }
                            return;
                        }
                        jQuery( this ).css("background", "");
                    });
                    if (self.selectIndex != setIdx) {
                        self.selectIndex = setIdx;
                        self.chosedValue = setVal;
                    }
                }
                return;
            }
            // press down
            if (event.keyCode == 40) {
                event.preventDefault();
                if (self.selectIndex == (length - 1)) {
                    // already on bottom
                    return;
                }
                var inputbox = self;
                var setIdx = null;
                var setVal = '';
                jQuery("#yung-Combo-table-" + bindId + " td").each(function( index ) {
                    if (inputbox.selectIndex == null) {
                        if (index == 0) {
                            setIdx = index;
                            setVal = jQuery( this ).text();
                            jQuery( this ).css("background", "#A4BED4");
                            return;
                        }
                    }
                    if (inputbox.selectIndex == (index - 1)) {
                        setIdx = index;
                        setVal = jQuery( this ).text();
                        jQuery( this ).css("background", "#A4BED4");
                        if (scrollExist) {
                            var position = new com.yung.util.Position(jQuery(this));
                            var botPos = position.getBotLeftPosition(null, null, floatDivElement);
                            var topPos = position.getTopLeftPosition(null, null, floatDivElement);
                            var currentScrollTop = floatDivElement.scrollTop();
                            var tdHeight = jQuery(this).height();
                            if (botPos.top > (currentScrollTop + self.displayHeight)){
                                // outside bottom
                                var offsetY = 3;
                                if (com_yung_util_getbrowser() == 'msie') {
                                    offsetY = offsetY + 3;
                                }
                                floatDivElement.scrollTop(botPos.top - self.displayHeight + offsetY);
                            }
                            if (currentScrollTop > topPos.top) {
                                // outside top
                                floatDivElement.scrollTop(topPos.top - 3);
                            }
                        }
                        return;
                    }
                    jQuery( this ).css("background", "");
                });
                if (self.selectIndex != setIdx) {
                    self.selectIndex = setIdx;
                    self.chosedValue = setVal;
                }
                return;
            }
            // press enter
            if (event.keyCode == 13) {
                event.preventDefault();
                if (self.selectIndex == null) {
                    return;
                } else {
                    self.setInputValue(self.chosedValue);
                }
                return;
            }
            self.selectIndex = null;
            self.chosedValue = '';
            var reqCode = jQuery("#" + bindId).val();
            if (reqCode.length < minLen) {
                return;
            }
            if (typeof self.filter == 'function') {
                var array = self.filter(reqCode, self);
                if (array != null) {
                    if (_validType(Array, array) == true) {
                        self.showDropList(array, bindId);
                    }
                }
            } else {
                // standard filter
                var displayValues = self.standardFilter(reqCode);
                self.showDropList(displayValues, bindId);
            }
        });
    },
    
    /** 
     * show drop list
     * 
     * @memberof com_yung_util_Combo
     * @param {Array} displayValues - list values to display
     * @param {string} bindId - binding element id
     */
    showDropList : function (displayValues, bindId) {
        if (bindId == null) {
            bindId = this.eleId;
        }
        if (displayValues == null || displayValues.length == 0) {
            this.floatDiv.closeFloatDiv();
            return;
        }
        var _displayValues = [];
        for (var i = 0; i < displayValues.length; i++) {
            _displayValues.push(displayValues[i]);
            if (_displayValues.length > this.maxItems) {
                break;
            }
        }
        displayValues = _displayValues;
        // display floatDiv
        var display = this.floatDiv.isDisplay();
        var html = "<table id='yung-Combo-table-" + bindId + "' style='width: 100%;'>";
        var basicTmp = new com.yung.util.BasicTemplate();
        var template = new com.yung.util.ArrayTemplate();
        basicTmp.add("<tr>");
        basicTmp.add("    <td class='yung-combo' style='' align='left' onclick='fireInstanceMethod(\"" + this.hashId + "\", \"setInputValue\", \"{{val}}\");'>{{val}}</td>");
        basicTmp.add("</tr>");
        template.setBasicTmp(basicTmp);
        var data = [];
        for (var i = 0; i < displayValues.length; i++) {
            var obj = {};
            obj["val"] = displayValues[i];
            data.push(obj);
        }
        var html = html + template.toHtml(data) + "</table>";
        if (display == false) {
            var browser = com_yung_util_getbrowser();
            var ele = jQuery("#" + bindId);
            var position = new com_yung_util_Position(ele);
            var offsetY = 6;
            var typeName = jQuery("#" + this.eleId).prop('nodeName');
            typeName = typeName.toUpperCase();
            if ("TEXTAREA" == typeName) {
            	offsetY = offsetY - 3;
            }
            if (browser == 'msie') {
                offsetY = 2;
            }
            var pos = position.getBotLeftPosition(0, offsetY);
            var width = jQuery("#" + bindId).width();
            this.floatDiv.openFloatDiv(html, pos, width, this.displayHeight);
        } else {
            this.floatDiv.setFloatContent(html);
        }
    }
    
});

/** 
 * get com_yung_util_Combo global instance,
 * 
 * @param  {string} eleId - element id
 * @param  {Array} selectValues - select value array
 * @return  {com_yung_util_Combo} com_yung_util_Combo instance
 */
com_yung_util_Combo.instance = function(eleId, selectValues) {
    return $Class.getInstance("com.yung.util.Combo", eleId, selectValues);
}

/**
 * select Combo box, like autocomplete, but show drop list when focus
 * 
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_Combo
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_SelectCombo = com_yung_util_Combo.extend({

    classProp : { 
        name : "com.yung.util.SelectCombo" 
    },
    
    /**
     * minimum length for filter
     * @member {number}
     * @instance
     * @memberof com_yung_util_SelectCombo
     */
    minLength : 1,
    
    /**
     * constructor
     * @memberof com_yung_util_SelectCombo
     * @param  {string} eleId - element id
     * @param  {Array} selectValues - select value array
     */
    init : function (eleId, selectValues) {
        return this._super(eleId, selectValues);
    },
    bindFunction : function () {
        var bindId = this.eleId;
        var self = this;
        // show when onFoucus
        jQuery("#" + bindId).focus(function() {
            var reqCode = jQuery("#" + bindId).val();
            if (reqCode == '') {
                self.showDropList(self.selectValues.toArray(), bindId);
            } else {
                var displayValues = [];
                var array = self.selectValues.toArray();
                for (var i = 0; i < array.length; i++) {
                    var val = array[i];
                    if (val.indexOf(reqCode) >= 0) {
                        displayValues.push(val);
                    }
                }
                self.showDropList(displayValues, bindId);
            }
        });
        this._super();
    }
});

/** 
 * get com_yung_util_SelectCombo global instance,
 * 
 * @param  {string} eleId - element id
 * @param  {Array} selectValues - select value array
 * @return  {com_yung_util_SelectCombo} com_yung_util_SelectCombo instance
 */
com_yung_util_SelectCombo.instance = function(eleId, selectValues) {
    return $Class.getInstance("com.yung.util.SelectCombo", eleId, selectValues);
}

/**
 * select Combo box, like autocomplete. 
 * allow to append search word.
 * 
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_Combo
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_AppendCombo = com_yung_util_Combo.extend({
    
    classProp : { 
        name : "com.yung.util.AppendCombo" 
    },
    
    /**
     * constructor
     * @memberof com_yung_util_AppendCombo
     * @param  {string} eleId - element id
     * @param  {Array} selectValues - select value array
     */
    init : function (eleId, selectValues) {
        return this._super(eleId, selectValues);
    },
    
    /** 
     * standard filter
     * 
     * @instance
     * @memberof com_yung_util_AppendCombo
     * @param {string} reqCode - request code from input
     * @return {Array} filtered value array
     */
    standardFilter : function (reqCode) {
        var searchCode = reqCode;
        var prefixCode = '';
        var idx = reqCode.lastIndexOf(' ');
        if (idx > 0) {
            searchCode = searchCode.substring(idx + 1, searchCode.length);
            prefixCode = reqCode.substring(0, idx + 1);
        }
        if (searchCode.length < this.minLength) {
            return [];
        }
        var displayValues = [];
        var array = this.selectValues.toArray();
        for (var i = 0; i < array.length; i++) {
            var val = array[i];
            if (val.indexOf(searchCode) >= 0) {
                displayValues.push(prefixCode + val);
            }
        }
        return displayValues;
    }
    
});

/** 
 * get com_yung_util_AppendCombo global instance,
 * 
 * @param  {string} eleId - element id
 * @param  {Array} selectValues - select value array
 * @return  {com_yung_util_AppendCombo} com_yung_util_AppendCombo instance
 */
com_yung_util_AppendCombo.instance = function(eleId, selectValues) {
    return $Class.getInstance("com.yung.util.AppendCombo", eleId, selectValues);
}

/**
 * Date picker
 * 
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_DatePicker = $Class.extend({

    classProp : { 
        name : "com.yung.util.DatePicker" 
    },
    
    hashId : '',
    
    /**
     * element id
     * @member {string}
     * @instance
     * @memberof com_yung_util_DatePicker
     */
    eleId : null,
    
    /**
     * float div to place word list
     * @member {com_yung_util_FloatDiv}
     * @instance
     * @memberof com_yung_util_DatePicker
     */
    floatDiv : null,
    displayHeight : 136,
    displayWidth : 220,
    blockFire : false,
    delayFire : 200,
    adjustX : 0,
    adjustY : 0,
    
    
    /**
     * select date
     * @member {com_yung_util_Calendar}
     * @instance
     * @memberof com_yung_util_DatePicker
     */
    selectDate : null,
    
    /**
     * Callback after date choose value
     *
     * @callback AfterDateChoose
     * @param {string} eleId - element id
     * @param {string} inputVal - select date
     */
    /**
     * callback after date choose value
     * @member {AfterDateChoose}
     * @instance
     * @memberof com_yung_util_DatePicker
     */
    afterChoose : null,
    
    
    /**
     * constructor
     * @memberof com_yung_util_DatePicker
     * @param  {string} eleId - element id
     * @param  {string} format - date format, ex: yyyy/MM/dd, See JAVA SimpleDateFormat
     */
    init : function (eleId, format) {
        $Class.validate(this.classProp.name, eleId);
        if (eleId == null || eleId == '') {
            throw "Please specify eleId";
        }
        this.eleId = eleId;
        this.hashId = MD5Util.calc(this.classProp.name + "-" + eleId);
        if (format == null) {
            throw "format is empty!";
        }
        if (_validType('string', format) == false) {
            throw "format is not string";
        }
        this.format = format;
        this.selectDate = new com.yung.util.Calendar(new Date());
        jQuery("#" + eleId).attr("autocomplete", "off");
        this.createPicker();
        return this;
    },
    
    /** 
     * set after choose callback
     * 
     * @instance
     * @memberof com_yung_util_DatePicker
     * @param {AfterDateChoose} afterChoose - after choose callback
     */
    setAfterChoose : function (afterChoose) {
        if (typeof afterChoose != 'function') {
            throw "afterChoose is not function";
        }
        this.afterChoose = afterChoose;
    },
    
    /** 
     * set input value
     * 
     * @instance
     * @memberof com_yung_util_DatePicker
     * @param {string} inputVal - date put into input
     */
    setInputValue : function (inputVal) {
        jQuery("#" + this.eleId).val(inputVal);
        if (typeof this.afterChoose == 'function') {
            this.afterChoose(this.eleId, inputVal);
        }
        this.floatDiv.closeFloatDiv();
        var sdf = new com.yung.util.SimpleDateFormat(this.format);
        this.selectDate = sdf.parse(inputVal);
    },
    
    /** 
     * create date picker
     * 
     * @private
     * @instance
     * @memberof com_yung_util_DatePicker
     */
    createPicker : function () {
        this.floatDiv = $Class.getInstance("com.yung.util.FloatDiv", "datePicker-" + this.eleId);
        this.floatDiv.showCloseButton(false);
        this.bindFunction();
    },
    bindFunction : function () {
        var bindId = this.eleId;
        var self = this;
        // show when onFoucus
        jQuery("#" + bindId).focus(function() {
            var display = self.floatDiv.isDisplay();
            if (display == false) {
                self.showPicker(bindId);
            }
        });
        // hide when blur
        jQuery("#" + bindId).blur(function() {
            setTimeout(function(){ 
                if (self.blockFire == true) {
                    self.blockFire = false;
                    jQuery("#" + bindId).focus();
                    return;
                }
                self.floatDiv.closeFloatDiv();
            }, self.delayFire);
        });
    },
    
    /** 
     * show date picker
     * 
     * @memberof com_yung_util_DatePicker
     * @param {string} bindId - binding element id
     */
    showPicker : function (bindId) {
        if (bindId == null) {
            bindId = this.eleId;
        }
        // display floatDiv
        var display = this.floatDiv.isDisplay();
        var val = jQuery("#" + bindId).val();
        if (display == false) {
            var sdf = new com.yung.util.SimpleDateFormat(this.format);
            if (sdf.validate(val) == true) {
                this.selectDate = sdf.parse(val);
            }
        }
        var html = this.createMonthTable(val);
        if (display == false) {
            var browser = com_yung_util_getbrowser();
            var ele = jQuery("#" + bindId);
            var position = new com_yung_util_Position(ele);
            var offsetY = 7;
            var offsetX = 0;
            var typeName = jQuery("#" + this.eleId).prop('nodeName');
            typeName = typeName.toUpperCase();
            if ("TEXTAREA" == typeName) {
            	offsetY = offsetY - 3;
            }
            if (browser == 'msie') {
                offsetY = offsetY - 4;
                offsetX = offsetX - 2;
            }
            if (browser == 'safari') {
                offsetX = offsetX - 1;
            }
            if (browser == 'firefox') {
                offsetY = offsetY - 2;
                offsetX = offsetX - 1;
            }
            if (browser == 'edge') {
                offsetY = offsetY - 2;
                offsetX = offsetX - 1;
            }
            offsetX = offsetX + this.adjustX;
            offsetY = offsetY + this.adjustY;
            var pos = position.getBotLeftPosition(offsetX, offsetY);
            var self = this;
            this.floatDiv.openFloatDiv(html, pos, this.displayWidth, this.displayHeight, function () {
                self.adjustSize();
            });
        } else {
            this.floatDiv.setFloatContent(html);
            this.adjustSize();
        }
    },
    adjustSize : function () {
        var tableWidth = jQuery("#yung-datePicker-" + this.eleId).width() + 2;
        var tableHeight = jQuery("#yung-datePicker-" + this.eleId).height() + 1;
        var browser = com_yung_util_getbrowser();
        if (browser == 'msie') {
            tableHeight = tableHeight + 3;
        } else if (browser == 'safari') {
            tableHeight = tableHeight + 1;
        }
        jQuery("#yung-FloatDiv-datePicker-" + this.eleId).css("height", tableWidth + "px");
        jQuery("#yung-FloatDiv-datePicker-" + this.eleId).css("height", tableHeight + "px");
    },
    createMonthTable : function (val) {
        var sdf = new com.yung.util.SimpleDateFormat(this.format);
        var inputCal = null;
        var now = new com.yung.util.Calendar(new Date());
        if (val != null && val != '') {
            if (sdf.validate(val)) {
                inputCal = sdf.parse(val);
            }
        }
        var cal = null;
        if (this.selectDate != null) {
            cal = this.selectDate;
        } else {
            cal = new com.yung.util.Calendar(new Date());
        }
        var year = cal.getYear();
        var month = cal.getMonth();
        var totalDays = this.getMonthDays(year, month + 1);
        var weekIdx = 0;
        var weekDayIdx = 0;
        var weekTmpArray = [];
        
        var weekTmp = null;
        var weekDayArrayTmp = null;
        var weekDayArrayData = [];
        var thisMonthStartDay = new com.yung.util.Calendar(year, month + 1, 1);
        var wDay = thisMonthStartDay.getDay();
        for (var i = 0; i < totalDays; i++) {
            if (weekDayIdx == 0) {
                weekTmp = new com.yung.util.ComplexTemplate();
                weekTmp.add("<tr>");
                weekTmp.add("    {{weekDayArrayTmp}}");
                weekTmp.add("</tr>");
                weekDayArrayTmp = new com.yung.util.ArrayTemplate();
                weekDayArrayTmp.add("<td align='center' style='width: 30px; height: 22px; border: 1px solid; border-collapse: collapse; {{background}}'>");
                weekDayArrayTmp.add("    <a href='javascript:void(0);' style='{{style}}' onclick='fireInstanceMethod(\"" + this.hashId + "\", \"setInputValue\", \"{{val}}\");' >{{day}}</a>");
                weekDayArrayTmp.add("</td>");
                weekDayArrayData = [];
                if (weekIdx == 0 && wDay > 0) {
                    for (var j = 0; j < wDay; j++) {
                        var data = {};
                        data["day"] = "";
                        data["val"] = "";
                        data["background"] = "";
                        weekDayIdx++;
                        weekDayArrayData.push(data);
                    }
                }
            }
            var data = {};
            data["day"] = "" + (i + 1);
            data["val"] = this.getDateString(year, month + 1, i + 1);
            if (inputCal != null && (i + 1) == inputCal.getDate() && month == inputCal.getMonth() && year == inputCal.getYear()) {
                data["background"] = "background: yellow;";
            } else if (inputCal == null && (i + 1) == now.getDate() && month == now.getMonth() && year == now.getYear()) {
                data["background"] = "background: yellow;";
            } else {
                data["background"] = "";
            }
            weekDayIdx++;
            weekDayArrayData.push(data);
            if (weekDayIdx == 7) {
                weekTmp.addTemplate("weekDayArrayTmp", weekDayArrayTmp, weekDayArrayData);
                weekTmpArray.push(weekTmp);
                weekDayIdx = 0;
                weekIdx++;
            }
        }
        if (weekDayArrayData.length > 0 && weekDayArrayData.length < 7) {
            if (weekDayArrayData.length < 7) {
                var diff = 7 - weekDayArrayData.length;
                for (var i = 0; i < diff; i++) {
                    var data = {};
                    data["day"] = "";
                    data["val"] = "";
                    data["background"] = "";
                    weekDayArrayData.push(data);
                }
            }
            weekTmp.addTemplate("weekDayArrayTmp", weekDayArrayTmp, weekDayArrayData);
            weekTmpArray.push(weekTmp);
        }
        var tableTmp = new com.yung.util.ComplexTemplate();
        tableTmp.add("<table id='yung-datePicker-" + this.eleId + "' align='center' style='background: white; table-layout:fixed; word-break:break-all; font-size: 13px; border: 2px solid black; border-collapse: collapse;' >");
        tableTmp.add("    <tr>");
        tableTmp.add("        <td align='center' style='width: 30px; border:1px solid; border-collapse:collapse; cursor: pointer; background: gray; color: white;' onclick='fireInstanceMethod(\"" + this.hashId + "\", \"prevYear\");'> &#10096; &#10096;</td>");
        tableTmp.add("        <td align='center' style='width: 30px; border:1px solid; border-collapse:collapse; cursor: pointer; background: gray; color: white;' onclick='fireInstanceMethod(\"" + this.hashId + "\", \"prevMonth\");'> &#10096; </td>");
        tableTmp.add("        <td align='center' colspan='3' style='border:1px solid; border-collapse:collapse;'>{{yearMonthTmp}}</td>");
        tableTmp.add("        <td align='center' style='width: 30px; border:1px solid; border-collapse:collapse; cursor: pointer; background: gray; color: white;' onclick='fireInstanceMethod(\"" + this.hashId + "\", \"nextMonth\");'> &#10097; </td>");
        tableTmp.add("        <td align='center' style='width: 30px; border:1px solid; border-collapse:collapse; cursor: pointer; background: gray; color: white;' onclick='fireInstanceMethod(\"" + this.hashId + "\", \"nextYear\");'> &#10097;  &#10097; </td>");
        tableTmp.add("    </tr>");
        tableTmp.add("    <tr style='background: lightgray;'>");
        tableTmp.add("        <td align='center' style='width: 30px; border:1px solid; border-collapse:collapse;'>Sun</td>");
        tableTmp.add("        <td align='center' style='width: 30px; border:1px solid; border-collapse:collapse;'>Mon</td>");
        tableTmp.add("        <td align='center' style='width: 30px; border:1px solid; border-collapse:collapse;'>Tue</td>");
        tableTmp.add("        <td align='center' style='width: 30px; border:1px solid; border-collapse:collapse;'>Wed</td>");
        tableTmp.add("        <td align='center' style='width: 30px; border:1px solid; border-collapse:collapse;'>Thr</td>");
        tableTmp.add("        <td align='center' style='width: 30px; border:1px solid; border-collapse:collapse;'>Fri</td>");
        tableTmp.add("        <td align='center' style='width: 30px; border:1px solid; border-collapse:collapse;'>Sat</td>");
        tableTmp.add("    </tr>");
        for (var i = 0; i < weekTmpArray.length; i++) {
            tableTmp.add("    <tr>{{weekTmpArray" + i + "}}</tr>");
            tableTmp.addTemplate("weekTmpArray" + i, weekTmpArray[i], weekTmpArray[i].getData());
        }
        tableTmp.add("</table>");
        var monthStr = this.getMonthString(year, month + 1);
        var yearMonthTmp = new com.yung.util.SimpleTemplate("{{val}}", "val", monthStr);
        tableTmp.addTemplate("yearMonthTmp", yearMonthTmp, yearMonthTmp.getData());
        return tableTmp.toHtml();
    },
    
    getMonthDays : function (year, month) {
        var thisMonthStartDay = new com.yung.util.Calendar(year, month, 1);
        var nextMonthStartDay = new com.yung.util.Calendar(year, month + 1, 1);
        var duration = nextMonthStartDay.getTime() - thisMonthStartDay.getTime();
        return (duration / 1000 / 3600 / 24);
    },
    
    getDateString : function (year, month, day) {
        var cal = new com.yung.util.Calendar(year, month, day);
        var sdf = new com.yung.util.SimpleDateFormat(this.format);
        return sdf.format(cal);
    },
    
    getMonthString : function (year, month) {
        var cal = new com.yung.util.Calendar(year, month, 1);
        var sdf = new com.yung.util.SimpleDateFormat("yyyy/MM");
        return sdf.format(cal);
    },
    
    prevYear : function () {
        this.blockFire = true;
        this.selectDate.addYear(-1);
        this.showPicker();
    },
    
    prevMonth : function () {
        this.blockFire = true;
        this.selectDate.addMonth(-1);
        this.showPicker();
    },
    
    nextYear : function () {
        this.blockFire = true;
        this.selectDate.addYear(1);
        this.showPicker();
    },
    
    nextMonth : function () {
        this.blockFire = true;
        this.selectDate.addMonth(1);
        this.showPicker();
    }
});

/** 
 * get com_yung_util_DatePicker global instance
 * 
 * @param  {string} eleId - element id
 * @param  {string} format - date format, ex: yyyy/MM/dd, See JAVA SimpleDateFormat
 * @return  {com_yung_util_DatePicker} com_yung_util_DatePicker instance
 */
com_yung_util_DatePicker.instance = function(eleId, format) {
    return $Class.getInstance("com.yung.util.DatePicker", eleId, format);
}

// specific table grid date picker
var com_yung_util_GridDatePicker = com_yung_util_DatePicker.extend({
    
    classProp : { 
        name : "com.yung.util.GridDatePicker" 
    },
    
    grid : null,
    delayFire : 100,
    
    init : function (eleId, format, grid) {
        this.grid = grid;
        return this._super(eleId, format);
    },
    
    bindFunction : function () {
        var bindId = this.eleId;
        var self = this;
        // show when onFoucus
        jQuery("#" + bindId).focus(function() {
            var display = self.floatDiv.isDisplay();
            if (display == false) {
                self.showPicker(bindId);
            }
        });
        // hide when blur
        jQuery("#" + bindId).blur(function() {
            setTimeout(function(){ 
                if (self.blockFire == true) {
                    self.blockFire = false;
                    jQuery("#" + bindId).focus();
                    return;
                }
                self.floatDiv.closeFloatDiv();
                jQuery("#" + self.grid.gridDivId + "-datePicker").css("display", "none");
                if (typeof self.afterChoose == 'function') {
                    var inputVal = jQuery("#" + self.grid.gridDivId + "-datePicker").val();
                    self.afterChoose(bindId, inputVal);
                }
            }, self.delayFire);
        });
    }
    
});

com_yung_util_GridDatePicker.instance = function(eleId, format, grid) {
    return $Class.getInstance("com.yung.util.GridDatePicker", eleId, format, grid);
}

/**
 * Convenient debug tool to print message on browser console
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @property {string}  LINE_SEPARATOR - line separator
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_DebugPrinter = $Class.extend({
	
	classProp : { 
		name : "com.yung.util.DebugPrinter" 
    },
	
    /**
     * maximum indent threshold
     * @member {number}
     * @instance
     * @memberof com_yung_util_DebugPrinter
     */
	MAX_INDENT : 40,
    
	LINE_SEPARATOR : "\n",
    
	/**
     * flag to show out of buffer
     * @member {boolean}
     * @instance
     * @memberof com_yung_util_DebugPrinter
     */
	showOutOfBuffer : true,
    
	/**
     * flag to display null value
     * @member {boolean}
     * @instance
     * @memberof com_yung_util_DebugPrinter
     */
	showNull : true,
    
	/**
	 * constructor
     * @memberof com_yung_util_DebugPrinter
     */
    init : function () {
        return this;
    },
    
    /** 
     * to check input if it is allowable
     * 
     * @private
     * @memberof com_yung_util_DebugPrinter
     * @param {Object} input - Object for check
     * @return {Boolean} allowable flag
     */
    allowType : function (input) {
        if (typeof input == 'undefined') {
            return true;
        } else if (typeof input == 'function') {
            return false;
        } else if (typeof input == 'string') {
            return true;
        } else if (typeof input == 'number') {
            return true;
        } else if (typeof input == 'boolean') {
            return true;
        } else if (typeof input == 'object') {
            return true;
        } else if (jQuery.isArray(input)) {
            return true;
        } else {
            throw "unknown type: " + typeof input;
        }
    },
    
    /**
     * to check input if it is printable
     * 
     * @private
     * @memberof com_yung_util_DebugPrinter
     * @param {String} input - object type
     * @param {Object} inputObj - Object for check
     * @return {Boolean} printable flag
     */
    printable : function (input, inputObj) {
        if (input == 'string') {
            return true;
        } else if (input == 'number') {
            return true;
        } else if (input == 'boolean') {
            return true;
        } else {
            if (Object.prototype.toString.call(inputObj) === '[object Date]'){
                return true;
            }
            return false;
        }
    },
    
    /**
     * print object to buffer
     * 
     * @private
     * @memberof com_yung_util_DebugPrinter
     * @param {String} bud - String buffer to hold message
     * @param {String} indent - String for indent
     * @param {String} objName - object name
     * @param {Object} obj - Object to print
     */
    printObjectParam : function (bud, indent, objName, obj) {
        if (indent.length > this.MAX_INDENT) {
            if (this.showOutOfBuffer) bud = bud + indent + "skip following objet ..." + this.LINE_SEPARATOR;
            return bud;
        }
        if (obj == null) {
            if (this.showNull) bud = bud + indent + "Object:" + objName + " is null";
            return bud;
        }
        if (jQuery.isArray(obj)) {
            bud = this.printArray(bud, indent, objName, obj);
        } else {
            if (this.allowType(obj) == false) {
                return bud;
            }
            var type = typeof obj;
            if (this.printable(type, obj) == true) {
                bud = bud + indent + objName + "[" + type + "] = " + obj + this.LINE_SEPARATOR;
            } else {
                bud = bud + indent + "object name:" + objName + this.LINE_SEPARATOR;
                // print field
                var start = true;
                if (start) {
                    indent = "    " + indent;
                    start = false;
                }
                for(var key in obj) {
                    var returnObj = obj[key];
                    if (returnObj == null) {
                        if (this.showNull) bud = bud + indent + key + " is null!" + this.LINE_SEPARATOR;
                        continue;
                    }
                    if (jQuery.isArray(returnObj)) {
                        bud = this.printArray(bud, indent, key, returnObj) + this.LINE_SEPARATOR;
                    } else {
                        var returnObjType = typeof returnObj;
                        if (this.printable(returnObjType, returnObj) == true) {
                            bud = bud + indent + key + "[" + returnObjType + "] = " + returnObj + this.LINE_SEPARATOR;
                        } else {
                            bud = this.printObjectParam(bud, indent, key, returnObj);
                        }
                    }
                }
            }
        }
        return bud;
    },
    
    /**
     * print Array to buffer
     *
     * @private
     * @memberof com_yung_util_DebugPrinter
     * @param {String} bud - String buffer to hold message
     * @param {String} indent - String for indent
     * @param {String} objName - array name
     * @param {Array} array - Array to print
     */
    printArray : function (bud, indent, objName, array) {
        bud = bud + indent + "Array : " + objName + this.LINE_SEPARATOR;
        for (var i = 0; i < array.length; i++) {
            bud = this.printObjectParam(bud, indent + "    ", objName + i, array[i]);
        }
        return bud;
    },
    
    /**
     * print object to console
     *
     * @instance
     * @memberof com_yung_util_DebugPrinter
     * @param {String} objName - object name
     * @param {Object} obj - Object to print
     */
    printObject : function (objName, obj) {
        if (obj == null) {
            this.printMessage(objName);
            return;
        }
        var bud = "";
        bud = this.printObjectParam(bud, "", objName, obj);
        this.printMessage(bud);
    },
    
    /**
     * print object to alert box
     *
     * @instance
     * @memberof com_yung_util_DebugPrinter
     * @param {String} objName - object name
     * @param {Object} obj - Object to print
     */
    alertObject : function (objName, obj) {
        if (obj == null) {
            this.printMessage(objName);
            return;
        }
        var bud = "";
        bud = this.printObjectParam(bud, "", objName, obj);
        alert(bud);
    },
    
    /**
     * print message to console
     *
     * @instance
     * @memberof com_yung_util_DebugPrinter
     * @param {String} message - message to print
     */
    printMessage : function (message) {
        console.log(message);
    }
});
     
/**
 * Float div element, usually used for display message[simple HTML], like tooltip or menu. 
 * Note: float div will not create mask to block screen. 
 * if mask is required, use com_yung_util_Popup
 * 
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_FloatDiv = $Class.extend({

    classProp : { 
        name : "com.yung.util.FloatDiv" 
    },
    
    hashId : '',
    
    /**
     * float div id
     * @member {string}
     * @instance
     * @memberof com_yung_util_FloatDiv
     */
    divId : null,
    
    /**
     * div overflow
     * @member {string}
     * @instance
     * @memberof com_yung_util_FloatDiv
     */
    overflow : '',
    
    /**
     * constructor
     * @memberof com_yung_util_FloatDiv
     * @param  {string} divId - float div id
     * @param  {boolean} onblurClose - a flag to close float div when onblur
     * @param  {string} overflow - float div overflow
     */
    init : function (divId, onblurClose, overflow) {
        $Class.validate(this.classProp.name, divId);
        this.divId = divId;
        this.hashId = MD5Util.calc(this.classProp.name + "-" + divId);
        if (overflow != null) {
            this.overflow = overflow;
        }
        this.createFloatDiv();
        if (onblurClose == true) {
            this.onblurClose();
        }
        return this;
    },
    
    /** 
     * close float div when onblur
     * 
     * @private
     * @instance
     * @memberof com_yung_util_FloatDiv
     */
    onblurClose : function () {
        var element = jQuery("body");
        var event = "click";
        var self = this;
        element.bind(event, function() {
            self.closeFloatDiv();
        });
        this.showCloseButton(false);
    },
    
    /** 
     * show close button in div
     * 
     * @instance
     * @memberof com_yung_util_FloatDiv
     * @param  {boolean} show - show or not
     */
    showCloseButton : function (show) {
        if (show == true) {
            jQuery("#yung-FloatDiv-closeBtn-" + this.divId).css('display', '');
        } else {
            jQuery("#yung-FloatDiv-closeBtn-" + this.divId).css('display', 'none');
        }
    },
    
    /** 
     * create div content
     * 
     * @private
     * @instance
     * @memberof com_yung_util_FloatDiv
     */
    createFloatDiv : function () {
        var floatDiv = jQuery("#" + this.divId)[0];
        if (floatDiv == null) {
            var obj = {};
            obj["divId"] = this.divId;
            obj["overflow"] = this.overflow;
            var template = new com.yung.util.BasicTemplate(obj);
            template.add('<div id="yung-FloatDiv-{{divId}}" class="float-border" style="position: absolute; left: 0px; top: 0px; margin: 0px; display: none; background: white; box-shadow: 5px 5px 5px #d7d7d7; width:300px; z-index: 5000; {{overflow}}">');
            template.add('    <div id="yung-FloatDiv-container-{{divId}}" style="display: block; width: 100%; height: 100%;">');
            template.add('        <div id="yung-FloatDiv-content-{{divId}}" style="position: absolute; margin: 0px; height: 100%; width: 100%;" ></div>');
            template.add('        <div id="yung-FloatDiv-closeBtn-{{divId}}" style="position: absolute; right: 2px; top: 0px; cursor: pointer;"><span style="color: red;" title="close" onclick="fireInstanceMethod(\'' + this.hashId + '\', \'showFloatDiv\', false);" style="cursor: pointer;" >&#10006;</span></div>');
            template.add('    </div>');
            template.add('</div>');
            var html = template.toHtml();
            jQuery("body").append(html);
        } else {
            throw "div id: " + this.divId + " element exist, please use another one.";
        }
    },
    
    /** 
     * set html content for float div
     * 
     * @private
     * @instance
     * @memberof com_yung_util_FloatDiv
     * @param  {string} html - HTML code 
     */
    setFloatContent : function (html) {
        jQuery("#yung-FloatDiv-content-" + this.divId).html(html);
    },
    
    /** 
     * show float content
     * 
     * @private
     * @instance
     * @memberof com_yung_util_FloatDiv
     */
    showFloatContent : function () {
        jQuery("#yung-FloatDiv-container-" + this.divId).css("display", "block");
    },
    
    /** 
     * show float div
     * 
     * @private
     * @instance
     * @memberof com_yung_util_FloatDiv
     * @param  {boolean} show - show flag
     * @param  {com_yung_util_FloatDiv} self - com_yung_util_FloatDiv itself, usually for callback use
     */
    showFloatDiv : function (show, self) {
        var divId = this.divId;
        if (self != null) {
            divId = self.divId;
        }
        if (show == false) {
            var display = jQuery("#yung-FloatDiv-" + divId).css("display");
            if (display != 'none') {
                jQuery("#yung-FloatDiv-" + divId).css("display", "none");
            }
            return; 
        }
        var display = jQuery("#yung-FloatDiv-" + divId).css("display");
        if (display != 'block') {
            jQuery("#yung-FloatDiv-" + divId).css("display", "block");
        }
    },
    
    /**
     * Callback when close float div 
     *
     * @callback floatDivCloseCallback
     */
    /** 
     * close and hide div
     * 
     * @instance
     * @memberof com_yung_util_FloatDiv
     * @param  {floatDivCloseCallback} callback - callback when close float div 
     */
    closeFloatDiv : function (callback) {
        this.showFloatDiv(false);
        if (typeof callback === "function") {
            callback();
        }   
    },
    
    /** 
     * open float div, width and height are optional.
     * 
     * @instance
     * @memberof com_yung_util_FloatDiv
     * @param  {string} html - HTML code for float div
     * @param  {object} pos - position object, include top and left number
     * @param  {number} width - float div width
     * @param  {number} height - float div height
     * @param  {function} callback - callback
     */
    openFloatDiv : function (html, pos, width, height, callback) {
        if (width == null) {
            width = 500;
        }
        if (height == null) {
            height = 300;
        }
        if (pos == null) {
            throw "please specify position!";
        }
        var windowWidth = jQuery(document).width();
        if (pos.left < 0) pos.left = 0; 
        if ((pos.left + width) > windowWidth) pos.left = windowWidth - width;
        var top = pos.top + "px";
        var left = pos.left + "px";
        jQuery("#yung-FloatDiv-" + this.divId).css("width", width);
        jQuery("#yung-FloatDiv-" + this.divId).css("height", height);
        jQuery("#yung-FloatDiv-" + this.divId).css("left", left);
        jQuery("#yung-FloatDiv-" + this.divId).css("top", top);
        jQuery("#yung-FloatDiv-content-" + this.divId).html(html);
        // use setTimeout to prevent body click event conflict
        var self = this;
        setTimeout(function(){ 
            self.showFloatDiv(true, self);
            if (typeof callback == 'function') {
                callback();
            }
        }, 300);
    },
    
    /**
     * return if div display
     * 
     * @instance
     * @memberof com_yung_util_FloatDiv
     * @return {boolean} display or not
     */
    isDisplay : function () {
        var display = jQuery("#yung-FloatDiv-" + this.divId).css("display");
        if (display == 'none') {
            return false;
        } else {
            return true;
        }
    },
    
    /**
     * return float div element
     * 
     * @instance
     * @memberof com_yung_util_FloatDiv
     * @return {HTMLElement} float div element
     */
    getFloatDivElement : function () {
        return jQuery("#yung-FloatDiv-" + this.divId);
    }
});

/** 
 * get com_yung_util_FloatDiv global instance,
 * 
 * @param  {string} divId - div id
 * @param  {boolean} onblurClose - a flag to close float div when onblur
 * @param  {string} overflow - float div overflow
 * @return  {com_yung_util_FloatDiv} com_yung_util_FloatDiv instance
 */
com_yung_util_FloatDiv.instance = function(divId, onblurClose, overflow) {
    return $Class.getInstance("com.yung.util.FloatDiv", divId, onblurClose, overflow);
}

/**
 * Float iframe element, usually used for display complex message, like report, table.
 * Note: float iframe will not create mask to block screen. 
 * if mask is required, use com_yung_util_Popup
 * 
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_FloatIframe = $Class.extend({
    
    classProp : { 
        name : "com.yung.util.FloatIframe" 
    },
    
    hashId : '',
    
    /**
     * float iframe div id
     * @member {string}
     * @instance
     * @memberof com_yung_util_FloatIframe
     */
    divId : null,
    
    /**
     * constructor
     * @memberof com_yung_util_FloatIframe
     * @param  {string} divId - float iframe div id
     * @param  {boolean} onblurClose - a flag to close float iframe when onblur
     */
    init : function (divId, onblurClose) {
        $Class.validate(this.classProp.name, divId);
        this.divId = divId;
        this.hashId = MD5Util.calc(this.classProp.name + "-" + divId);
        this.createFloatIFrame();
        if (onblurClose == true) {
            this.onblurClose();
        }
        return this;
    },
    
    /** 
     * close float iframe when onblur
     * 
     * @private
     * @instance
     * @memberof com_yung_util_FloatIframe
     */
    onblurClose : function () {
        var element = jQuery("body");
        var event = "click";
        var self = this;
        element.bind(event, function() {
            self.closeFloatIframe();
        });
        this.showCloseButton(false);
    },
    
    /** 
     * show close button in div
     * 
     * @instance
     * @memberof com_yung_util_FloatIframe
     * @param  {boolean} show - show or not
     */
    showCloseButton : function (show) {
        if (show == true) {
            jQuery("#yung-FloatIframe-closeBtn-" + this.divId).css('display', '');
        } else {
            jQuery("#yung-FloatIframe-closeBtn-" + this.divId).css('display', 'none');
        }
    },
    
    /** 
     * create iframe content
     * 
     * @private
     * @instance
     * @memberof com_yung_util_FloatIframe
     */
    createFloatIFrame : function () {
        var floatDiv = jQuery("#yung-FloatIframe-" + this.divId)[0];
        if (floatDiv == null) {
            var obj = {};
            obj["divId"] = this.divId;
            var template = new com.yung.util.BasicTemplate(obj);
            template.add('<div id="yung-FloatIframe-{{divId}}" class="float-border" style="position: absolute; left: 0px; top: 0px; margin: 3px; display: none; background: white; box-shadow: 5px 5px 5px #d7d7d7; width: 500px; width:300px; z-index: 5000;">');
            template.add('    <div id="yung-FloatIframe-loading-{{divId}}" style="width: 100%; height: 100%; display: block;">');
            template.add('        <table width="100%" height="100%">');
            template.add('            <tr>');
            template.add('                <td align="center"><div class="loader">Loading...</div></td>');
            template.add('            </tr>');
            template.add('        </table>');
            template.add('    </div>');
            template.add('    <div id="yung-FloatIframe-container-{{divId}}" style="display: none; width: 100%; height: 100%;">');
            template.add('        <div id="yung-FloatIframe-closeBtn-{{divId}}" style="position: absolute; right: 2px; top: 0px; cursor: pointer;"><span style="color: red; background-color: white;" title="close" onclick="fireInstanceMethod(\'' + this.hashId + '\', \'showFloatIframe\', false);" style="cursor: pointer;" >&#10006;</span></div>');
            template.add('        <iframe id="yung-FloatIframe-content-{{divId}}" src="" width="100%" frameborder="0" style="height: 100%; border-style: none transparent;" onload="fireInstanceMethod(\'' + this.hashId + '\', \'showFloatContent\');"></iframe>');
            template.add('    </div>');
            template.add('</div>');
            var html = template.toHtml();
            jQuery("body").append(html);
        } else {
            throw "div id: " + this.divId + " element exist, please use another one.";
        }
    },
    
    /** 
     * show float content
     * 
     * @private
     * @instance
     * @memberof com_yung_util_FloatIframe
     */
    showFloatContent : function () {
        jQuery("#yung-FloatIframe-loading-" + this.divId).css("display", "none");
        jQuery("#yung-FloatIframe-container-" + this.divId).css("display", "block");
    },
    
    /** 
     * show float iframe
     * 
     * @private
     * @instance
     * @memberof com_yung_util_FloatIframe
     * @param  {boolean} show - show flag
     */
    showFloatIframe : function (show) {
        var divId = this.divId;
        if (show == false) {
            var display = jQuery("#yung-FloatIframe-" + divId).css("display");
            if (display != 'none') {
                jQuery("#yung-FloatIframe-" + divId).css("display", "none");
            }
            jQuery("yung-FloatIframe-loading" + divId).css("display", "block");
            return; 
        }
        var display = jQuery("#yung-FloatIframe-" + divId).css("display");
        if (display != 'block') {
            jQuery("#yung-FloatIframe-" + divId).css("display", "block");
        }
    },
    
    /**
     * Callback when close float iframe 
     *
     * @callback floatIframeCloseCallback
     */
    /** 
     * close and hide iframe
     * 
     * @instance
     * @memberof com_yung_util_FloatIframe
     * @param  {floatIframeCloseCallback} callback - callback when close float iframe 
     */
    closeFloatIframe : function (callback) {
        this.showFloatIframe(false);
        if (typeof callback === "function") {
            callback();
        }   
    },
    
    /** 
     * open float iframe, width and height are optional.
     * 
     * @instance
     * @memberof com_yung_util_FloatDiv
     * @param  {string} url - url for float iframe
     * @param  {object} pos - position object, include top and left number
     * @param  {number} width - float iframe width
     * @param  {number} height - float iframe height
     */
    openFloatIframe : function (url, pos, width, height) {
        if (width == null) {
            width = 500;
        }
        if (height == null) {
            height = 300;
        }
        if (pos == null) {
            alert("please specify position!");
        }
        var top = pos.top + "px";
        var left = pos.left + "px";
        jQuery("#yung-FloatIframe-" + this.divId).css("width", width);
        jQuery("#yung-FloatIframe-" + this.divId).css("height", height);
        jQuery("#yung-FloatIframe-" + this.divId).css("left", left);
        jQuery("#yung-FloatIframe-" + this.divId).css("top", top);
        document.getElementById('yung-FloatIframe-content-' + this.divId).src = url;
        // use setTimeout to prevent body click event conflict
        var self = this;
        setTimeout(function(){ 
            self.showFloatIframe(true); 
        }, 300);
    }
});

/** 
 * get com_yung_util_FloatIframe global instance,
 * 
 * @param  {string} divId - div id
 * @param  {boolean} onblurClose - a flag to close float div when onblur
 * @return  {com_yung_util_FloatIframe} com_yung_util_FloatIframe instance
 */
com_yung_util_FloatIframe.instance = function(divId, onblurClose) {
    return $Class.getInstance("com.yung.util.FloatIframe", divId, onblurClose);
}

/**
 * Map interface. Define implement methods.
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @property {Array}  classProp.unimplemented - unimplemented method
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_MapInterface = $Class.extend({

    classProp : { 
		name : "com.yung.util.MapInterface",
		unimplemented : ['entryArray', 'size', 'getKeyArray', 'get', 'put', 'remove', 'containsKey']
    }
    
});

/**
 * Map entry
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_Entry = $Class.extend({
    
	classProp : { 
		name : "com.yung.util.Entry"
	},
    
	/**
     * key
     * @member {string | number}
     * @instance
     * @memberof com_yung_util_Entry
     */
	key : null,
    
	/**
     * value
     * @member {string | number | object}
     * @instance
     * @memberof com_yung_util_Entry
     */
	value : null,
    
	/**
	 * constructor
     * @memberof com_yung_util_Entry
     * @param  {string | number} key - key
     * @param  {string | number | boolean | object} value - value
     */
	init : function(key, value) {
        this.key = key;
        this.value = value;
    },
    
    /** 
     * get key
     * 
     * @instance
     * @memberof com_yung_util_Entry
     * @return  {string | number} key
     */
    getKey : function () {
        return this.key;
    },
    
    /** 
     * get value
     * 
     * @instance
     * @memberof com_yung_util_Entry
     * @return  {string | number | bollean | object} value
     */
    getValue : function () {
        return this.value;
    },
    
    /** 
     * set value
     * 
     * @instance
     * @memberof com_yung_util_Entry
     * @param  {string | number | bollean | object} value - value
     */
    setValue : function (value) {
        this.value = value;
    }
});    

/**
 * Basic Map, only allow key type string, number
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_MapInterface
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_BasicMap = com_yung_util_MapInterface.extend({
	
    classProp : { 
    	name : "com.yung.util.BasicMap"
    },
    
    /**
     * key type
     * @member {string | number}
     * @instance
     * @memberof com_yung_util_BasicMap
     */
    keyType : null,
    
    /**
     * value type
     * @member {all}
     * @instance
     * @memberof com_yung_util_BasicMap
     */
    valueType : null,
    
    /**
     * value type name
     * @member {all}
     * @instance
     * @memberof com_yung_util_BasicMap
     */
    valueTypeName : null,
    
    /**
     * set to store all keys 
     * @member {com_yung_util_BasicSet}
     * @instance
     * @memberof com_yung_util_BasicMap
     */
    keyCollection : null,
    
    /**
     * obj to store all key value pair 
     * @member {object}
     * @instance
     * @memberof com_yung_util_BasicMap
     */
    obj : null,
    
    /**
	 * constructor
     * @memberof com_yung_util_BasicMap
     * @param  {string} keyType - key type
     * @param  {string | function | object} valueType - value type
     */
    init : function(keyType, valueType){
        if (keyType == null || valueType == null) {
            throw "com.yung.util.BasicMap key type and value type can not be null!";
        }
        if (typeof keyType == "string") {
            keyType = keyType.toLowerCase();
        }
        if (keyType != 'string' && keyType != 'number') {
            throw "com.yung.util.BasicMap key type can only be string or number!";
        }
        this.keyType = keyType;
        this.valueType = valueType;
        this.valueTypeName = $Class.getClassName(valueType);
        this.obj = {};
        this.keyCollection = new com_yung_util_Set(keyType);
        return this;
    },
    
    /** 
     * clear all key value pairs
     * 
     * @instance
     * @memberof com_yung_util_BasicMap
     */
    clear : function () {
        this.obj = {};
        this.keyCollection.clear();
    },
    
    /** 
     * check map if contains key 
     * 
     * @instance
     * @memberof com_yung_util_BasicMap
     * @param  {keyType} key - key to check if exist
     * @return {boolean} exist or not
     */
    containsKey : function (key) {
        if (key == null) {
            return false;
        }
        if (this.validKeyType(key) == false) {
            return false;
        }
        if (this.obj[key + ""] == null) {
            return false;
        } else {
            return true;
        }
    },
    
    /** 
     * check map if contains value 
     * 
     * @instance
     * @memberof com_yung_util_BasicMap
     * @param  {valueType} value - value to check if exist
     * @return {boolean} exist or not
     */
    containsValue : function (value) {
        if (value == null) {
            return false;
        }
        if (this.validValueType(value) == false) {
            return false;
        }
        for (var key in this.obj) {
            if (this.obj[key + ""] === value) {
                return true;
            }
        }
        return false;
    },
    
    /** 
     * get value by key
     * 
     * @instance
     * @memberof com_yung_util_BasicMap
     * @param  {keyType} key - key to get value
     * @return {valueType} value
     */
    get : function (key) {
        if (key == null) {
            return null;
        }
        if (this.validKeyType(key) == false) {
            return null;
        }
        return this.obj[key + ""];
    },
    
    /** 
     * put key value to map 
     * 
     * @instance
     * @memberof com_yung_util_BasicMap
     * @param  {keyType} value - key to put
     * @param  {valueType} value - value to put
     */
    put : function (key, value) {
    	if (this.validKeyType(key) == false) {
            throw "key type is not " + this.keyType;
        }
        if (this.validValueType(value) == false) {
            if (typeof this.valueType == "string") {
                throw "value type is not string";
            } else if (typeof this.valueType == "number") {
                throw "value type is not number";
            } else if (typeof this.valueType == "boolean") {
                throw "value type is not boolean";
            } else if (typeof this.valueType == "object") {
                throw "value type is not object";
            } else {
                throw "value type is not " + this.valueTypeName;
            }
        }
        if (this.obj[key + ""] == null) {
            this.keyCollection.add(key);
            this.obj[key + ""] = value;
        } else {
            this.obj[key + ""] = value;
        }
    },
    
    /** 
     * put key value to map 
     * 
     * @instance
     * @memberof com_yung_util_BasicMap
     * @param  {com_yung_util_MapInterface} map - map to put
     */
    putAll : function (map) {
        if (map == null) {
            return;
        } else {
        	if (!map instanceof com_yung_util_MapInterface) {
        		throw "map is not com_yung_util_MapInterface";
        	}
        	if (map.size() == 0) {
        		return;
        	}
            var firstKey = map.getKeyArray()[0];
        	if (this.validKeyType(firstKey) == false) {
                throw "key type is not " + this.keyType;
            }
            if (this.validValueType(map.get(firstKey)) == false) {
                if (typeof this.valueType == "string") {
                    throw "value type is not string";
                } else if (typeof this.valueType == "number") {
                    throw "value type is not number";
                } else if (typeof this.valueType == "boolean") {
                    throw "value type is not boolean";
                } else if (typeof this.valueType == "object") {
                    throw "value type is not object";
                } else {
                    throw "value type is not " + this.valueTypeName;
                }
            }
        	var entryArray = map.entryArray()
            for (var i = 0; i < entryArray.length; i++) {
                var entry = entryArray[i];
                this.put(entry.getKey(), entry.getValue());
            }
        }
    },
    
    /** 
     * remove value by key
     * 
     * @instance
     * @memberof com_yung_util_BasicMap
     * @param  {keyType} key - key to get value
     * @return {valueType} removed value
     */
    remove : function (key) {
        if (key == null) {
            return null;
        }
        if (this.validKeyType(key) == false) {
            return null;
        }
        if (this.obj[key + ""] == null) {
        	return null;
        }
        var value = this.obj[key + ""]
        this.keyCollection.remove(key);
        delete this.obj[key + ""];
        return value;
    },
    
    /** 
     * map size
     * 
     * @instance
     * @memberof com_yung_util_BasicMap
     * @return {number} map size
     */
    size : function () {
        return this.keyCollection.size();
    },
    
    /** 
     * map is empty or not
     * 
     * @instance
     * @memberof com_yung_util_BasicMap
     * @return {boolean} is empty or not
     */
    isEmpty : function () {
        if (this.size() == 0) {
            return true;
        } else {
            return false;
        }
    },
    
    /** 
     * return map entry array
     * 
     * @instance
     * @memberof com_yung_util_BasicMap
     * @return {Array} map entry array
     */
    entryArray : function () {
        var array = [];
        var keyArray = this.keyCollection.toArray();
        for (var i = 0; i < keyArray.length; i++) {
            var entry = new com.yung.util.Entry(keyArray[i], this.obj[keyArray[i]]);
            array.push(entry);
        }
        return array;
    },
    
    /** 
     * get key array
     * 
     * @instance
     * @memberof com_yung_util_BasicMap
     * @return {Array} map key array
     */
    getKeyArray : function () {
        return this.keyCollection.toArray();
    },
    
    /** 
     * return a copy of key array
     * 
     * @instance
     * @memberof com_yung_util_BasicMap
     * @return {Array} a copy of key array
     */
    cloneKeyArray : function () {
        var array = [];
        var keyArray = this.keyCollection.toArray();
        for (var i = 0; i < keyArray.length; i++) {
            array.push(keyArray[i]);
        }
        return array;
    },
    
    /** 
     * return a copy of key set
     * 
     * @instance
     * @memberof com_yung_util_BasicMap
     * @return {com_yung_util_Set} a copy of key set
     */
    keySet : function () {
        return this.keyCollection;
    },
    
    /** 
     * return a copy of value list
     * 
     * @instance
     * @memberof com_yung_util_BasicMap
     * @return {com_yung_util_CollectionInterface} a copy of value list
     */
    values : function () {
        var list = new com.yung.util.List(this.valueType);
        var entryArray = this.entryArray();
        for (var i = 0; i < entryArray.length; i++) {
            var entry = entryArray[i];
            list.add(entry.getValue());
        }
        return list;
    },
    
    /** 
     * to string
     * 
     * @instance
     * @memberof com_yung_util_BasicMap
     * @return {string} string
     */
    toString : function () {
        var ret = this.classProp.name + "\n";
        var keyArray = this.keyCollection.toArray();
        for (var i = 0; i < keyArray.length; i++) {
            var val = this.obj[keyArray[i]];
            var valStr = "";
            if (typeof val.toString == 'function') {
                valStr = val.toString();
            } else {
                valStr = JSON.stringify(this.obj[keyArray[i]])
            }
            if (this.keyType == 'string') {
                ret = ret + "    '" + keyArray[i] + "': " + valStr + " \n";
            } else if (this.keyType == 'number') {
                ret = ret + "    " + keyArray[i] + ": " + valStr + " \n";
            }
        }
        return ret;
    },
    
    /** 
     * return a copy of map
     * 
     * @instance
     * @memberof com_yung_util_BasicMap
     * @return {com_yung_util_BasicMap} a copy of map
     */
    clone : function () {
        var ret = new com.yung.util.BasicMap(this.keyType, this.valueType);
        ret.putAll(this);
        return ret;
    },
    
    /** 
     * check key type
     * 
     * @private
     * @instance
     * @memberof com_yung_util_BasicMap
     * @return {all} value
     */
    validValueType : function (value) {
        return _validType(this.valueType, value);
    },
    
    /** 
     * check key type
     * 
     * @private
     * @instance
     * @memberof com_yung_util_BasicMap
     * @return {all} key
     */
    validKeyType : function (key) {
    	return _validType(this.keyType, key);
    }
    
});

/**
 * Sorted Map
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_BasicMap
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_SortedMap = com_yung_util_BasicMap.extend({
    
	classProp : { 
		name : "com.yung.util.SortedMap"
	},
    
	
	/**
     * key type
     * @member {string | number}
     * @instance
     * @memberof com_yung_util_SortedMap
     */
    keyType : null,
    
    /**
     * value type
     * @member {all}
     * @instance
     * @memberof com_yung_util_SortedMap
     */
    valueType : null,
    
    /**
     * value type name
     * @member {all}
     * @instance
     * @memberof com_yung_util_BasicMap
     */
    valueTypeName : null,
    
    /**
     * set to store all keys 
     * @member {com_yung_util_SortedSet}
     * @instance
     * @memberof com_yung_util_SortedMap
     */
    keyCollection : null,
    
    /**
     * obj to store all key value pair 
     * @member {object}
     * @instance
     * @memberof com_yung_util_SortedMap
     */
    obj : null,
    
    /**
     * flag to sort keys
     * @member {boolean}
     * @instance
     * @memberof com_yung_util_SortedMap
     */
	reverse : null,
    
	/**
	 * constructor
     * @memberof com_yung_util_SortedMap
     * @param  {string} keyType - key type
     * @param  {string | function | object} valueType - value type
     * @param  {boolean} reverse - flag to sort keys
     */
	init : function(keyType, valueType, reverse){
        if (keyType == null || valueType == null) {
            throw "com.yung.util.SortedMap key type and value type can not be null!";
        }
        if (typeof keyType == "string") {
            keyType = keyType.toLowerCase();
        }
        if (keyType != 'string' && keyType != 'number') {
            throw "com.yung.util.SortedMap key type can only be string or number!";
        }
        if (reverse === true) {
            this.reverse = true;
        } else {
            this.reverse = false;
        }
        this.keyType = keyType;
        this.valueType = valueType;
        this.valueTypeName = $Class.getClassName(valueType);
        this.obj = {};
        this.keyCollection = new com_yung_util_SortedSet(keyType, this.reverse);
        return this;
    },
    
    /** 
     * put key value to map 
     * 
     * @instance
     * @memberof com_yung_util_SortedMap
     * @param  {keyType} value - key to put
     * @param  {valueType} value - value to put
     */
    put : function (key, value) {
    	if (this.validKeyType(key) == false) {
            throw "key type is not " + this.keyType;
        }
        if (this.validValueType(value) == false) {
            if (typeof this.valueType == "string") {
                throw "value type is not string";
            } else if (typeof this.valueType == "number") {
                throw "value type is not number";
            } else if (typeof this.valueType == "boolean") {
                throw "value type is not boolean";
            } else if (typeof this.valueType == "object") {
                throw "value type is not object";
            } else {
                throw "value type is not " + this.valueTypeName;
            }
        }
        if (this.obj[key + ""] == null) {
            this.keyCollection.add(key);
            this.obj[key + ""] = value;
        } else {
            this.obj[key + ""] = value;
        }
    },
    
    /** 
     * return a copy of map
     * 
     * @instance
     * @memberof com_yung_util_SortedMap
     * @return {com_yung_util_SortedMap} a copy of map
     */
    clone : function () {
        var ret = new com.yung.util.SortedMap(this.keyType, this.valueType);
        ret.putAll(this);
        return ret;
    },
    
    binarySearchArray : function(arr, x, start, end, reverse, exact) {
    	return binarySearchArray(arr, x, start, end, reverse, exact);
	},
	
	/** 
     * remove value by key
     * 
     * @instance
     * @memberof com_yung_util_SortedMap
     * @param  {keyType} key - key to get value
     * @return {valueType} removed value
     */
    remove : function (key) {
        if (key == null) {
            return null;
        }
        if (this.validKeyType(key) == false) {
            return null;
        }
        if (this.obj[key + ""] == null) {
        	return null;
        }
        var value = this.obj[key + ""]
        this.keyCollection.remove(key);
        delete this.obj[key + ""];
        return value;
    }
});


/**
 * JS Map, only allow key type string, number. best performance
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_MapInterface
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var system_util_Map = com_yung_util_MapInterface.extend({
    
    classProp : { 
        name : "system.util.Map"
    },
    
    /**
     * key type
     * @member {string | number}
     * @instance
     * @memberof system_util_Map
     */
    keyType : null,
    
    /**
     * value type
     * @member {all}
     * @instance
     * @memberof system_util_Map
     */
    valueType : null,
    
    /**
     * value type name
     * @member {all}
     * @instance
     * @memberof system_util_Map
     */
    valueTypeName : null,
    
    /**
     * set to store all keys 
     * @member {system_util_Set}
     * @instance
     * @memberof system_util_Map
     */
    keyCollection : null,
    
    /**
     * map to store all key value pair 
     * @member {Map}
     * @instance
     * @memberof system_util_Map
     */
    obj : null,
    
    /**
     * constructor
     * @memberof system_util_Map
     * @param  {string} keyType - key type
     * @param  {string | function | object} valueType - value type
     */
    init : function(keyType, valueType){
        if (keyType == null || valueType == null) {
            throw "com.yung.util.BasicMap key type and value type can not be null!";
        }
        if (typeof keyType == "string") {
            keyType = keyType.toLowerCase();
        }
        if (keyType != 'string' && keyType != 'number') {
            throw "system.util.Map key type can only be string or number!";
        }
        this.keyType = keyType;
        this.valueType = valueType;
        this.valueTypeName = $Class.getClassName(valueType);
        this.obj = new Map();
        this.keyCollection = new system_util_Set(keyType);
        return this;
    },
    
    /** 
     * clear all key value pairs
     * 
     * @instance
     * @memberof system_util_Map
     */
    clear : function () {
        this.obj.clear();
        this.keyCollection.clear();
    },
    
    /** 
     * check map if contains key 
     * 
     * @instance
     * @memberof system_util_Map
     * @param  {keyType} key - key to check if exist
     * @return {boolean} exist or not
     */
    containsKey : function (key) {
        if (key == null) {
            return false;
        }
        if (this.validKeyType(key) == false) {
            return false;
        }
        if (this.obj.get(key) == null) {
            return false;
        } else {
            return true;
        }
    },
    
    /** 
     * check map if contains value 
     * 
     * @instance
     * @memberof system_util_Map
     * @param  {valueType} value - value to check if exist
     * @return {boolean} exist or not
     */
    containsValue : function (value) {
        if (value == null) {
            return false;
        }
        if (this.validValueType(value) == false) {
            return false;
        }
        try {
            this.obj.forEach(function (val, key, set) {
                if (val === value) {
                    throw "done";
                }
            });
            return false;
        } catch (e) {
            if ("done" != (e + "")) {
                throw e;
            }
            return true;
        }
    },
    
    /** 
     * get value by key
     * 
     * @instance
     * @memberof system_util_Map
     * @param  {keyType} key - key to get value
     * @return {valueType} value
     */
    get : function (key) {
        if (key == null) {
            return null;
        }
        if (this.validKeyType(key) == false) {
            return null;
        }
        return this.obj.get(key);
    },
    
    /** 
     * put key value to map 
     * 
     * @instance
     * @memberof system_util_Map
     * @param  {keyType} value - key to put
     * @param  {valueType} value - value to put
     */
    put : function (key, value) {
        if (this.validKeyType(key) == false) {
            throw "key type is not " + this.keyType;
        }
        if (this.validValueType(value) == false) {
            if (typeof this.valueType == "string") {
                throw "value type is not string";
            } else if (typeof this.valueType == "number") {
                throw "value type is not number";
            } else if (typeof this.valueType == "boolean") {
                throw "value type is not boolean";
            } else if (typeof this.valueType == "object") {
                throw "value type is not object";
            } else {
                throw "value type is not " + this.valueTypeName;
            }
        }
        if (this.obj.has(key) == false) {
            this.keyCollection.add(key);
            this.obj.set(key, value);
        } else {
            this.obj.set(key, value);
        }
    },
    
    /** 
     * put key value to map 
     * 
     * @instance
     * @memberof system_util_Map
     * @param  {com_yung_util_MapInterface} map - map to put
     */
    putAll : function (map) {
        if (map == null) {
            return;
        } else {
            if (!map instanceof com_yung_util_MapInterface) {
                throw "map is not com_yung_util_MapInterface";
            }
            if (map.size() == 0) {
                return;
            }
            var firstKey = map.getKeyArray()[0];
            if (this.validKeyType(firstKey) == false) {
                throw "key type is not " + this.keyType;
            }
            if (this.validValueType(map.get(firstKey)) == false) {
                if (typeof this.valueType == "string") {
                    throw "value type is not string";
                } else if (typeof this.valueType == "number") {
                    throw "value type is not number";
                } else if (typeof this.valueType == "boolean") {
                    throw "value type is not boolean";
                } else if (typeof this.valueType == "object") {
                    throw "value type is not object";
                } else {
                    throw "value type is not " + this.valueTypeName;
                }
            }
            var entryArray = map.entryArray()
            for (var i = 0; i < entryArray.length; i++) {
                var entry = entryArray[i];
                this.put(entry.getKey(), entry.getValue());
            }
        }
    },
    
    /** 
     * remove value by key
     * 
     * @instance
     * @memberof system_util_Map
     * @param  {keyType} key - key to get value
     * @return {valueType} removed value
     */
    remove : function (key) {
        if (key == null) {
            return null;
        }
        if (this.validKeyType(key) == false) {
            return null;
        }
        if (this.obj.has(key) == false) {
            return null;
        }
        var value = this.obj.get(key);
        this.keyCollection.remove(key);
        this.obj["delete"](key);
        return value;
    },
    
    /** 
     * map size
     * 
     * @instance
     * @memberof system_util_Map
     * @return {number} map size
     */
    size : function () {
        return this.keyCollection.size();
    },
    
    /** 
     * map is empty or not
     * 
     * @instance
     * @memberof system_util_Map
     * @return {boolean} is empty or not
     */
    isEmpty : function () {
        if (this.size() == 0) {
            return true;
        } else {
            return false;
        }
    },
    
    /** 
     * return map entry array
     * 
     * @instance
     * @memberof system_util_Map
     * @return {Array} map entry array
     */
    entryArray : function () {
        var array = [];
        var keyArray = this.keyCollection.toArray();
        for (var i = 0; i < keyArray.length; i++) {
            var entry = new com.yung.util.Entry(keyArray[i], this.obj.get(keyArray[i]));
            array.push(entry);
        }
        return array;
    },
    
    /** 
     * get key array
     * 
     * @instance
     * @memberof system_util_Map
     * @return {Array} map key array
     */
    getKeyArray : function () {
        return this.keyCollection.toArray();
    },
    
    /** 
     * return a copy of key set
     * 
     * @instance
     * @memberof system_util_Map
     * @return {com_yung_util_Set} a copy of key set
     */
    keySet : function () {
        return this.keyCollection;
    },
    
    /** 
     * return a copy of value list
     * 
     * @instance
     * @memberof system_util_Map
     * @return {com_yung_util_CollectionInterface} a copy of value list
     */
    values : function () {
        var list = new com.yung.util.List(this.valueType);
        this.obj.forEach(function (value, key, map) {
            list.add(value);
        });
        return list;
    },
    
    /** 
     * to string
     * 
     * @instance
     * @memberof system_util_Map
     * @return {string} string
     */
    toString : function () {
        var ret = this.classProp.name + "\n";
        var keyArray = this.keyCollection.toArray();
        for (var i = 0; i < keyArray.length; i++) {
            var val = this.obj.get(keyArray[i]);
            var valStr = "";
            if (typeof val.toString == 'function') {
                valStr = val.toString();
            } else {
                valStr = JSON.stringify(this.obj[keyArray[i]])
            }
            if (this.keyType == 'string') {
                ret = ret + "    '" + keyArray[i] + "': " + valStr + " \n";
            } else if (this.keyType == 'number') {
                ret = ret + "    " + keyArray[i] + ": " + valStr + " \n";
            }
        }
        return ret;
    },
    
    /** 
     * return a copy of map
     * 
     * @instance
     * @memberof system_util_Map
     * @return {system_util_Map} a copy of map
     */
    clone : function () {
        var ret = new system.util.Map(this.keyType, this.valueType);
        ret.putAll(this);
        return ret;
    },
    
    /** 
     * check key type
     * 
     * @private
     * @instance
     * @memberof system_util_Map
     * @return {all} value
     */
    validValueType : function (value) {
        return _validType(this.valueType, value);
    },
    
    /** 
     * check key type
     * 
     * @private
     * @instance
     * @memberof system_util_Map
     * @return {all} key
     */
    validKeyType : function (key) {
        return _validType(this.keyType, key);
    }
    
});

/**
 * Tree node.
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_TreeNode = $Class.extend({

    classProp : { 
        name : "com.yung.util.TreeNode"
    },
    
    /**
     * is leaf node
     * @member {boolean}
     * @instance
     * @memberof com_yung_util_TreeNode
     */
    isLeaf : null,
    
    /**
     * collection to store all keys 
     * @member {Array}
     * @instance
     * @memberof com_yung_util_TreeNode
     */
    keys : [],
    keyNumber : 0,
    leftMostKey : null,
    keyType : null,
    
    /**
     * object reference
     * @member {Array}
     * @instance
     * @memberof com_yung_util_TreeNode
     */
    nodeRef : [],
    prevLeaf : null,
    mergeInternal : null,
    
    /**
     * constructor
     * @memberof com_yung_util_TreeNode
     * @param  {boolean} isLeaf - whether leaf node
     * @param  {boolean} reverse - reverse order
     * @param  {all} keyType - key type
     */
    init : function(isLeaf, reverse, keyType){
        if (_validType('boolean', isLeaf) == false) {
            throw "isLeaf type is not boolean";
        }
        if (isLeaf == true) {
            this.isLeaf = true;
        } else {
            this.isLeaf = false;
        }
        if (reverse == true) {
            this.reverse = true;
        } else {
            this.reverse = false;
        }
        this.keyType = keyType;
        return this;
    },
    
    /**
     * copy node
     * @memberof com_yung_util_TreeNode
     * @param  {com_yung_util_TreeNode} node - the node to copy from
     * @param  {number} from - where in n to start copying from
     * @param  {number} num - the number of keys/refs to copy
     */
    copy : function (node, from, num) {
        this.keyNumber = num;
        for (var i = 0; i < num; i++) { 
            this.keys[i] = node.keys[from + i];
            this.nodeRef[i] = node.nodeRef[from + i];
        }
        this.nodeRef[num] = node.nodeRef[from + num];
    },
    
    /**
     * Find the "<=" match position in this node.
     * @memberof com_yung_util_TreeNode
     * @param  {key} key - the key to be matched.
     * @return  {number} the position of match within node, where nKeys indicates no match
     */
    find : function (key) {
        // TODO: use binary search
        if (this.keyType == 'string' || this.keyType == 'number') {
            for (var i = 0; i < this.keyNumber; i++) {
                if (this.reverse == true) {
                    if (key >= this.keys[i]) return i;
                } else {
                    if (key <= this.keys[i]) return i;
                }
            }
        } else {
            for (var i = 0; i < this.keyNumber; i++) {
                if (this.reverse == true) {
                    if (key.compareTo(this.keys[i]) >= 0) return i;
                } else {
                    if (key.compareTo(this.keys[i]) <= 0) return i;
                }
            }
        }
        return this.keyNumber;
    }
    
});
com_yung_util_TreeNode["notFound"] = function (){};

/**
 * Tree map. Best performance for sorted key, but only allow key type string or number
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_MapInterface
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_TreeMap = com_yung_util_MapInterface.extend({
    
    classProp : { 
        name : "com.yung.util.TreeMap"
    },
    
    ORDER : 29,  // odd number only
    MAX : null,
    MID : null,
    MIN_LEAF : null,
    MIN_NODE : null,
    reverse : false,
    
    /**
     * reverse order
     * @member {boolean}
     * @instance
     * @memberof com_yung_util_TreeMap
     */
    reverse : false,
    
    /**
     * key type
     * @member {string | number}
     * @instance
     * @memberof com_yung_util_TreeMap
     */
    keyType : null,
    
    /**
     * key type name
     * @member {all}
     * @instance
     * @memberof com_yung_util_TreeMap
     */
    keyTypeName : null,
    
    /**
     * value type
     * @member {all}
     * @instance
     * @memberof com_yung_util_TreeMap
     */
    valueType : null,
    
    /**
     * value type name
     * @member {all}
     * @instance
     * @memberof com_yung_util_TreeMap
     */
    valueTypeName : null,
    
    /**
     * root of tree
     * @member {com_yung_util_TreeNode}
     * @instance
     * @memberof com_yung_util_TreeMap
     */
    root : null,
    
    /**
     * first leaf of tree
     * @member {com_yung_util_TreeNode}
     * @instance
     * @memberof com_yung_util_TreeMap
     */
    firstLeaf : null,
    
    /**
     * key size
     * @member {number}
     * @instance
     * @memberof com_yung_util_TreeMap
     */
    keyCount : 0,
    
    /**
     * constructor
     * @memberof com_yung_util_TreeMap
     * @param  {string} keyType - key type
     * @param  {string | function | object} valueType - value type
     * @param  {boolean} reverse - reverse order
     */
    init : function(keyType, valueType, reverse){
        if (keyType == null || valueType == null) {
            throw "com.yung.util.TreeMap key type and value type can not be null!";
        }
        if (typeof keyType == "string") {
            keyType = keyType.toLowerCase();
        }
        if (keyType != 'string' && keyType != 'number') {
            throw "com.yung.util.TreeMap key type can only be string or number!";
        }
        if (reverse == true) {
            this.reverse = true;
        } else {
            this.reverse = false;
        }
        this.MAX = this.ORDER - 1;
        this.MID = (this.ORDER + 1) / 2;
        this.MIN_LEAF = Math.floor(this.ORDER / 2);
        this.MIN_NODE = Math.floor(this.MAX / 2);
        this.keyType = keyType;
        this.keyTypeName = keyType;
        this.valueType = valueType;
        this.valueTypeName = $Class.getClassName(valueType);
        this.root = new com.yung.util.TreeNode(true, this.reverse, this.keyType);
        this.firstLeaf = this.root;
        return this;
    },
    
    /** 
     * return map entry array
     * 
     * @instance
     * @memberof com_yung_util_TreeMap
     * @return {Array} map entry array
     */
    entryArray : function () {
        var entryArray = [];
        var nextLeaf = this.firstLeaf;
        while (nextLeaf != null) {
            for (var i = 0; i < nextLeaf.keys.length; i++) {
                var en = new com.yung.util.Entry(nextLeaf.keys[i], nextLeaf.nodeRef[i]);
                entryArray.push(en);
            }
            nextLeaf = nextLeaf.nodeRef[nextLeaf.nodeRef.length - 1];
        }
        return entryArray;
    },
    
    /** 
     * get key array
     * 
     * @instance
     * @memberof com_yung_util_TreeMap
     * @return {Array} map key array
     */
    getKeyArray : function () {
        var keyArray = [];
        var nextLeaf = this.firstLeaf;
        while (nextLeaf != null) {
            for (var i = 0; i < nextLeaf.keys.length; i++) {
                keyArray.push(nextLeaf.keys[i]);
            }
            nextLeaf = nextLeaf.nodeRef[nextLeaf.nodeRef.length - 1];
        }
        return keyArray;
    },
    
    /** 
     * get value by key
     * 
     * @instance
     * @memberof com_yung_util_TreeMap
     * @param  {keyType} key - key to get value
     * @return {valueType} value
     */
    get : function (key) {
        if (key == null) {
            return null;
        }
        if (this.validKeyType(key) == false) {
            return null;
        }
        var ret = this.find(key, this.root);
        if (ret === com_yung_util_TreeNode["notFound"]) {
            return null;
        }
        return ret;
    },
    find : function (key, node) {
        var nd = node;
        while (nd.isLeaf == false) {
            var i = this.findNextNode(key, nd);
            nd = nd.nodeRef[i];
        }
        var ret = nd.find(key);
        if (ret < nd.keyNumber) {
            return nd.nodeRef[ret];
        } else {
            return com_yung_util_TreeNode["notFound"];
        }
    },
    findNextNode : function (key, node) {
        if (this.keyType == 'string' || this.keyType == 'number') {
            for (var i = 0; i < node.keyNumber; i++) {
                if (node.reverse == true) {
                    if (key > node.keys[i]) return i;
                } else {
                    if (key < node.keys[i]) return i;
                }
            }
        } else {
            for (var i = 0; i < node.keyNumber; i++) {
                if (node.reverse == true) {
                    if (key.compareTo(node.keys[i]) > 0) return i;
                } else {
                    if (key.compareTo(node.keys[i]) < 0) return i;
                }
            }
        }
        return node.keyNumber;
    },
    
    /** 
     * remove value by key
     * 
     * @instance
     * @memberof com_yung_util_TreeMap
     * @param  {keyType} key - key to get value
     * @return {boolean} removed or not
     */
    remove : function (key) {
        if (key == null) {
            return false;
        }
        if (this.validKeyType(key) == false) {
            return false;
        }
        var originKeyCount = this.keyCount;
        this.delKey(key, this.root);
        if (originKeyCount == this.keyCount) {
            return false;
        } else {
            return true;
        }
    },
    delKey : function (key, node) {
        var handleNode = null;                                                      // holder for current handling node
        if (node.isLeaf == true) {                                                  // handle leaf node level
            var index = node.find(key);
            var ret = this.splice(key, node, index, true);                          // splice (key, value) pair at find position
            if (ret != false) {
                this.keyCount--;
            }
            if (node.keyNumber < this.MIN_LEAF) {
                return node;                                                        // node too small then go to next node to merge
            } else if (index == 0) {
                return node;                                                        // node left most key change 
            }
        } else {                                                                    // handle internal node level
            var i = this.findNextNode(key, node);                                   // find next node position
            handleNode = this.delKey(key, node.nodeRef[i]);                         // recursive call to delete    
            if (handleNode != null) {
                handleNode = this.deleteNode(key, handleNode, node);
            }
        }
        return handleNode; 
    },
    splice : function (key, node, index, isLeft) {
        if (this.keyType == 'string' || this.keyType == 'number') {
            if (index >= node.keyNumber || key != node.keys[index]) {
                // BpTreeMap.delete: attempt to delete not exist key
                return false;
            }
        } else {
            if (index >= node.keyNumber || key.compareTo(node.keys[index]) != 0) {
                // BpTreeMap.delete: attempt to delete not exist key
                return false;
            }
        }
        for (var j = index; j < node.keyNumber; j++) {
            node.keys[j] = node.keys[j + 1];                                     // make room: shift keys right
            if (isLeft || j > index + 1) {                                       // ???
                node.nodeRef[j] = node.nodeRef[j + 1];                           // make room: shift values right
            }
        }
        node.keys.pop();                                                         // remove last not used element
        node.nodeRef.pop();                                                      // remove last not used element
        node.keyNumber--;                                                        // decrement number of keys
        return true;
    },
    deleteNode : function (deleteKey, handleNode, node) {
        var nextNode = null;
        var leftMostKeyChange = false;
        var index = this.seekNodeIndex(node, handleNode);
        if (handleNode.isLeaf == true) {
            if (handleNode.keyNumber < this.MIN_LEAF) {
                if (index == 0) {                                                // left most leaf
                    if (node.nodeRef[index + 1] != null) {                       // right leaf if not null
                        if (node.nodeRef[index + 1].keyNumber > this.MIN_LEAF) { // borrow key from right leaf    
                            var borrowLeaf = node.nodeRef[index + 1];
                            var borrowKey = borrowLeaf.keys[0];
                            var borrowVal = borrowLeaf.nodeRef[0];
                            this.splice(borrowKey, borrowLeaf, 0, true);
                            this.wedge(borrowKey, borrowVal, handleNode, handleNode.find(borrowKey), true);
                            node.keys[index] = borrowLeaf.keys[0];
                        } else {                                                 // merge with right leaf
                            var rightLeaf = node.nodeRef[index + 1];
                            this.merge(handleNode, rightLeaf, node);
                            if (node.keyNumber < this.MIN_NODE) {
                                if (node === this.root) {
                                    if (node.nodeRef.length == 1) {
                                        this.root = handleNode;
                                        return null;
                                    }
                                } else {
                                    node.mergeInternal = true;
                                }
                            }
                        }
                    } else {
                        node.mergeInternal = true;                               // can not borrow or merge then return node for next level merge 
                    }
                } else {                                                         // not left most leaf
                    if (node.nodeRef[index - 1].keyNumber > this.MIN_LEAF) {     // borrow key from left leaf
                        var borrowLeaf = node.nodeRef[index - 1];
                        var borrowKey = borrowLeaf.keys[borrowLeaf.keyNumber - 1];
                        var borrowVal = borrowLeaf.nodeRef[borrowLeaf.keyNumber - 1];
                        this.splice(borrowKey, borrowLeaf, borrowLeaf.keyNumber - 1, true);
                        this.wedge(borrowKey, borrowVal, handleNode, handleNode.find(borrowKey), true);
                        node.keys[index - 1] = borrowKey;
                    } else if (node.nodeRef[index + 1] != null && node.nodeRef[index + 1].keyNumber > this.MIN_LEAF) { // borrow key from right leaf
                        var borrowLeaf = node.nodeRef[index + 1];
                        var borrowKey = borrowLeaf.keys[0];
                        var borrowVal = borrowLeaf.nodeRef[0];
                        this.splice(borrowKey, borrowLeaf, 0, true);
                        this.wedge(borrowKey, borrowVal, handleNode, handleNode.find(borrowKey), true);
                        node.keys[index] = borrowLeaf.keys[0];
                    } else {                                                     // merge with left leaf
                        var leftLeaf = node.nodeRef[index - 1];
                        this.merge(leftLeaf, handleNode, node);
                        if (node.keyNumber < this.MIN_NODE) {
                            if (node === this.root) {
                                if (node.nodeRef.length == 1) {
                                    this.root = leftLeaf;
                                    return null;
                                }
                            } else {
                                node.mergeInternal = true;
                            }
                        }
                    }
                }
            }
        } else {
            if (handleNode.mergeInternal == true) {
                if (index == 0) {                                                // left most node
                    if (node.nodeRef[index + 1] != null) {                       // right node if not null
                        if (node.nodeRef[index + 1].keyNumber > this.MIN_NODE) { // borrow key from right node    
                            var borrowNode = node.nodeRef[index + 1];
                            var borrowKey = borrowNode.keys[0];
                            var borrowRef = borrowNode.nodeRef[0];
                            this.splice(borrowKey, borrowNode, 0, true);
                            if (borrowNode.nodeRef[0].isLeaf == true) {
                                if (borrowNode.leftMostKey != borrowNode.nodeRef[0].keys[0]) {
                                    borrowNode.leftMostKey = borrowNode.nodeRef[0].keys[0];
                                }
                            } else {
                                if (borrowNode.leftMostKey != borrowNode.nodeRef[0].leftMostKey) {
                                    borrowNode.leftMostKey = borrowNode.nodeRef[0].leftMostKey;
                                }
                            }
                            node.keys[index] = borrowNode.leftMostKey;
                            if (this.keyType == 'string' || this.keyType == 'number') {
                                if (this.reverse == true) {
                                    if (borrowRef.keys[0] > borrowKey) {
                                        borrowKey = borrowRef.keys[0];
                                    }
                                } else {
                                    if (borrowRef.keys[0] < borrowKey) {
                                        borrowKey = borrowRef.keys[0];
                                    }
                                }
                            } else {
                                if (this.reverse == true) {
                                    if (borrowRef.keys[0].compareTo(borrowKey) > 0) {
                                        borrowKey = borrowRef.keys[0];
                                    }
                                } else {
                                    if (borrowRef.keys[0].compareTo(borrowKey) < 0) {
                                        borrowKey = borrowRef.keys[0];
                                    }
                                }
                            }
                            var idx = handleNode.find(borrowKey);
                            this.wedge(borrowKey, borrowRef, handleNode, idx, false);
                            if (handleNode.nodeRef[0].isLeaf == true) {
                                handleNode.keys[idx] = handleNode.nodeRef[idx + 1].keys[0];
                                if (idx > 0) {
                                    handleNode.keys[idx - 1] = handleNode.nodeRef[idx].keys[0];
                                }
                            } else {
                                handleNode.keys[idx] = handleNode.nodeRef[idx + 1].leftMostKey;
                                if (idx > 0) {
                                    handleNode.keys[idx - 1] = handleNode.nodeRef[idx].leftMostKey;
                                }
                            }
                            handleNode.mergeInternal = null;
                        } else {                                                 // merge with right node
                            var rightNode = node.nodeRef[index + 1];
                            this.mergeInternal(handleNode, rightNode, node);
                            if (handleNode.nodeRef[0].isLeaf == true) {
                                handleNode.leftMostKey = handleNode.nodeRef[0].keys[0];
                            } else {
                                handleNode.leftMostKey = handleNode.nodeRef[0].leftMostKey;
                            }
                            handleNode.mergeInternal = null;
                            if (node.keyNumber < this.MIN_NODE) {
                                if (node === this.root) {
                                    if (node.nodeRef.length == 1) {
                                        this.root = handleNode;
                                        return null;
                                    }
                                } else {
                                    node.mergeInternal = true;
                                }
                            }
                        }
                    } else {
                        handleNode.mergeInternal = null;
                        node.mergeInternal = true;                               // go to next top node to handle
                    }
                } else {                                                         // not left most node                         
                    if (node.nodeRef[index - 1].keyNumber > this.MIN_NODE) {     // borrow key from left node
                        var borrowNode = node.nodeRef[index - 1];
                        var borrowKey = borrowNode.keys[borrowNode.keyNumber - 1];
                        var borrowRef = borrowNode.nodeRef[borrowNode.keyNumber];
                        this.splice(borrowKey, borrowNode, borrowNode.keyNumber - 1, false);
                        var idx = handleNode.find(borrowKey);
                        this.wedge(borrowKey, borrowRef, handleNode, idx, true);
                        handleNode.keys[idx] = handleNode.nodeRef[idx + 1].keys[0];
                        if (handleNode.nodeRef[0].isLeaf == true) {
                            handleNode.leftMostKey = handleNode.nodeRef[0].keys[0];
                        } else {
                            handleNode.leftMostKey = handleNode.nodeRef[0].leftMostKey;
                        }
                        handleNode.mergeInternal = null;
                    } else if (node.nodeRef[index + 1] != null && node.nodeRef[index + 1].keyNumber > this.MIN_LEAF) { // borrow key from right node
                        var borrowNode = node.nodeRef[index + 1];
                        var borrowKey = borrowNode.keys[0];
                        var borrowRef = borrowNode.nodeRef[0];
                        this.splice(borrowKey, borrowNode, 0, true);
                        if (borrowNode.nodeRef[0].isLeaf == true) {
                            if (borrowNode.leftMostKey != borrowNode.nodeRef[0].keys[0]) {
                                borrowNode.leftMostKey = borrowNode.nodeRef[0].keys[0];
                            }
                        } else {
                            if (borrowNode.leftMostKey != borrowNode.nodeRef[0].leftMostKey) {
                                borrowNode.leftMostKey = borrowNode.nodeRef[0].leftMostKey;
                            }
                        }
                        node.keys[index] = borrowNode.leftMostKey;
                        if (this.keyType == 'string' || this.keyType == 'number') {
                            if (this.reverse == true) {
                                if (borrowRef.keys[0] > borrowKey) {
                                    borrowKey = borrowRef.keys[0];
                                }
                            } else {
                                if (borrowRef.keys[0] < borrowKey) {
                                    borrowKey = borrowRef.keys[0];
                                }
                            }
                        } else {
                            if (this.reverse == true) {
                                if (borrowRef.keys[0].compareTo(borrowKey) > 0) {
                                    borrowKey = borrowRef.keys[0];
                                }
                            } else {
                                if (borrowRef.keys[0].compareTo(borrowKey) < 0) {
                                    borrowKey = borrowRef.keys[0];
                                }
                            }
                        }
                        var idx = handleNode.find(borrowKey);
                        this.wedge(borrowKey, borrowRef, handleNode, idx, false);
                        if (handleNode.nodeRef[0].isLeaf == true) {
                            handleNode.keys[idx] = handleNode.nodeRef[idx + 1].keys[0];
                            if (idx > 0) {
                                handleNode.keys[idx - 1] = handleNode.nodeRef[idx].keys[0];
                            }
                        } else {
                            handleNode.keys[idx] = handleNode.nodeRef[idx + 1].leftMostKey;
                            if (idx > 0) {
                                handleNode.keys[idx - 1] = handleNode.nodeRef[idx].leftMostKey;
                            }
                        }
                        handleNode.mergeInternal = null;
                    } else {                                                     // merge with left node
                        var leftNode = node.nodeRef[index - 1];
                        this.mergeInternal(leftNode, handleNode, node);
                        handleNode.mergeInternal = null;
                        if (node.keyNumber < this.MIN_NODE) {
                            if (node === this.root) {
                                if (node.nodeRef.length == 1) {
                                    this.root = leftNode;
                                    return null;
                                }
                            } else {
                                node.mergeInternal = true;
                            }
                        }
                    }
                }
            }
        }
        var firstRef = node.nodeRef[0];
        if (firstRef.isLeaf == true) {
            if (node.leftMostKey != firstRef.keys[0]) {
                node.leftMostKey = firstRef.keys[0];
                leftMostKeyChange = true;
            }
            for (var i = 0; i < node.keyNumber; i++) {
                node.keys[i] = node.nodeRef[i + 1].keys[0];
            }
        } else {
            if (node.leftMostKey != firstRef.leftMostKey) {
                node.leftMostKey = firstRef.leftMostKey;
                leftMostKeyChange = true;
            }
            for (var i = 0; i < node.keyNumber; i++) {
                node.keys[i] = node.nodeRef[i + 1].leftMostKey;
            }
        }
        if (node.keyNumber != node.keys.length) {
            // FIXME temporary fix
            var length = node.keys.length;
            for (var i = node.keyNumber; i < length; i++) {
                node.keys.pop();
            }
        }
        if (leftMostKeyChange == true) {
            return node;
        }
        if (node.mergeInternal == true) {
            return node;
        }
        return null;
    },
    seekNodeIndex : function (parent, child) {
        if (parent == null || child == null) {
            throw "parent or child is null";
        }
        for (var i = 0; i < parent.nodeRef.length; i++) {
            if (parent.nodeRef[i] === child) {
                return i;
            }
        }
        throw "not found";
    },
    merge : function (leftNode, rightNode, topNode) {
        var leftNodeKeyLength = leftNode.keys.length;
        leftNode.keyNumber = leftNodeKeyLength;
        for (var i = 0; i < rightNode.keyNumber; i++) {                                       // copy right leaf to left leaf
            leftNode.keys[leftNodeKeyLength + i] = rightNode.keys[i];
            leftNode.nodeRef[leftNodeKeyLength + i] = rightNode.nodeRef[i];
            leftNode.keyNumber++;
        }
        leftNode.nodeRef[leftNode.keyNumber] = rightNode.nodeRef[rightNode.keyNumber];        // link left leaf to right leaf
        var index = this.seekNodeIndex(topNode, rightNode);
        topNode.keys.splice(index - 1, 1);                                                    // remove top node key
        topNode.nodeRef.splice(index, 1);                                                     // remove top node reference
        topNode.keyNumber--;
        if (topNode.nodeRef.length == 1) {                                                    // update top node with new key value
            topNode.keys[0] = topNode.nodeRef[0].keys[0];                                     // add temporary key for next merge
        } else {
            if ((index - 2) >= 0) {
                topNode.keys[index - 2] = topNode.nodeRef[index - 1].keys[0];
            }
        }
    },
    mergeInternal : function (leftNode, rightNode, topNode) {
        var leftNodeKeyLength = leftNode.keys.length;
        var leftNodeRefLength = leftNode.nodeRef.length;
        leftNode.keyNumber = leftNodeKeyLength;
        for (var i = 0; i < rightNode.nodeRef.length; i++) {                                  // copy right node to left node
            leftNode.nodeRef[leftNodeRefLength + i] = rightNode.nodeRef[i];                   // add left node with new reference
            leftNode.keys[leftNodeKeyLength + i] = rightNode.nodeRef[i].keys[0];              // add left node with new keys
            leftNode.keyNumber++;
        }
        for (var i = 0; i < leftNode.keyNumber; i++) {                                        // update key value
            var key = null;
            if (leftNode.nodeRef[0].isLeaf == true) {
                key = leftNode.nodeRef[i + 1].keys[0];
            } else {
                key = leftNode.nodeRef[i + 1].leftMostKey;
            }
            leftNode.keys[i] = key;
        }
        var index = this.seekNodeIndex(topNode, rightNode);
        topNode.keys.splice(index - 1, 1);                                                    // remove top node key
        topNode.nodeRef.splice(index, 1);                                                     // remove top node reference
        topNode.keyNumber--;
        if (topNode.nodeRef.length == 1) {                                                    // update top node with new key value
            topNode.keys[0] = topNode.nodeRef[0].keys[0];                                     // add temporary key for next merge
        } else {
            if ((index - 2) >= 0) {
                topNode.keys[index - 2] = topNode.nodeRef[index - 1].keys[0];
            }
        }
    },
    
    /** 
     * put key value to map 
     * 
     * @instance
     * @memberof com_yung_util_TreeMap
     * @param  {keyType} value - key to put
     * @param  {valueType} value - value to put
     */
    put : function (key, value) {
        if (key == null) {
            return false;
        }
        if (this.validKeyType(key) == false) {
            throw "key: '" + key + "' type is not " + this.keyTypeName;
        }
        if (this.validValueType(value) == false) {
            throw "value: '" + value + "' type is not " + this.valueTypeName;
        }
        this.insert(key, value, this.root);
    },
    insert : function (key, value, node) {
        var newNode = null;                                                         // holder for right sibling node
        if (node.isLeaf == true) {                                                  // handle leaf node level
            var ret = this.wedge(key, value, node, node.find(key), true);           // wedge (key, value) pair at find position
            if (ret != false) {
                this.keyCount++;
            }
            if (node.keyNumber > this.MAX) {                                        // current node is full
                newNode = this.split(node);                                         // split current node, return right sibling
                node.nodeRef[node.keyNumber] = newNode;                             // link left leaf n to right leaf r
                newNode.prevLeaf = node;                                            // link right leaf n to left leaf r
                if (node == this.root) {
                    this.root = this.makeRoot(node, newNode.keys[0], newNode);      // at root => make a new root
                }
            }
        } else {                                                                    // handle internal node level
            var i = this.findNextNode(key, node);                                   // find next node position
            newNode = this.insert(key, value, node.nodeRef[i]);                     // recursive call to insert    
            if (newNode != null) {
                newNode = this.insertNode(newNode.keys[0], newNode, node);
            }
        }
        return newNode;   
    },
    wedge : function (key, value, node, index, isLeft) {
        if (this.keyType == 'string' || this.keyType == 'number') {
            if (index < node.keyNumber && key == node.keys[index]) {
                // BpTreeMap.insert: attempt to insert duplicate key
                node.nodeRef[index] = value;
                return false;
            }
        } else {
            if (index < node.keyNumber && key.compareTo(node.keys[index]) == 0) {
                // BpTreeMap.insert: attempt to insert duplicate key
                node.nodeRef[index] = value;
                return false;
            }
        }
        node.nodeRef[node.keyNumber + 1] = node.nodeRef[node.keyNumber];         // preserving the last value, last value is next sibling
        for (var j = node.keyNumber; j > index; j--) {
            node.keys[j] = node.keys[j - 1];                                     // make room: shift keys right
            if (isLeft || j > index + 1) {                                       // ???
                node.nodeRef[j] = node.nodeRef[j - 1];                           // make room: shift values right
            }
        }
        node.keys[index] = key;                                                  // place new key
        if (isLeft == true) {
            node.nodeRef[index] = value;                                         // place new value
        } else {
            node.nodeRef[index + 1] = value;                                     // ???
        }
        node.keyNumber++;                                                        // increment number of keys
        if (node.isLeaf == false) {
            var val = node.nodeRef[0];
            if (val.isLeaf == true) {
                node.leftMostKey = val.keys[0];
            } else {
                node.leftMostKey = val.leftMostKey;
            }
        }
        return true;
    },
    insertNode : function (key, value, node) {
        var newNode = null;
        var index = node.find(key);
        if (this.keyType == 'string' || this.keyType == 'number') {
            if (this.reverse == true) {
                if (value.isLeaf == false && key < value.leftMostKey) {
                    key = value.leftMostKey;
                }
            } else {
                if (value.isLeaf == false && key > value.leftMostKey) {
                    key = value.leftMostKey;
                }    
            }
        } else {
            if (this.reverse == true) {
                if (value.isLeaf == false && key.compareTo(value.leftMostKey) < 0) {
                    key = value.leftMostKey;
                }
            } else {
                if (value.isLeaf == false && key.compareTo(value.leftMostKey) > 0) {
                    key = value.leftMostKey;
                }    
            }
        }
        this.wedge(key, value, node, index, false);
        if (node.keyNumber > this.MAX) {                                         // current node is full
            newNode = this.splitInternal(node);                                  // split current node, return right sibling
            if (node == this.root) {
                var newKey = newNode.keys.shift();
                newNode.nodeRef.shift();
                newNode.keyNumber--;
                var val = newNode.nodeRef[0];
                if (val.isLeaf == true) {
                    newNode.leftMostKey = val.keys[0];
                } else {
                    newNode.leftMostKey = val.leftMostKey;
                }
                if (this.keyType == 'string' || this.keyType == 'number') {
                    if (this.reverse == true) {
                        if (newKey < newNode.leftMostKey) {
                            newKey = newNode.leftMostKey;
                        }
                    } else {
                        if (newKey > newNode.leftMostKey) {
                            newKey = newNode.leftMostKey;
                        }
                    }
                } else {
                    if (this.reverse == true) {
                        if (newKey.compareTo(newNode.leftMostKey) < 0) {
                            newKey = newNode.leftMostKey;
                        }
                    } else {
                        if (newKey.compareTo(newNode.leftMostKey) > 0) {
                            newKey = newNode.leftMostKey;
                        }
                    }    
                }
                this.root = this.makeRoot(node, newKey, newNode);                // at root => make a new root
                this.root.leftMostKey = node.leftMostKey;
            } else {
                newNode.keys.shift();
                newNode.nodeRef.shift();
                newNode.keyNumber--;
                var val = newNode.nodeRef[0];
                if (val.isLeaf == true) {
                    newNode.leftMostKey = val.keys[0];
                } else {
                    newNode.leftMostKey = val.leftMostKey;
                }
            }
        }
        return newNode;
    },
    split : function (node) {
        var r = new com.yung.util.TreeNode(true, this.reverse, this.keyType);    // make a right sibling node (r)
        r.copy(node, this.MID, this.ORDER - this.MID);                           // copy second half to node r
        for (var i = this.MID; i < node.keyNumber; i++) {                        // delete node key/value
            node.keys.pop();
            node.nodeRef.pop();
        }
        node.nodeRef.pop();                                                      // delete last node ref
        node.keyNumber = this.MID;                                               // reset the number of keys in node n
        var originalR = r.nodeRef[r.keyNumber];
        if (originalR != null) {
            originalR.prevLeaf = r;                                              // link original right leaf to new split leaf
        }
        return r;                                                                // return right sibling
    },
    splitInternal : function (node) {
        var r = new com.yung.util.TreeNode(false, this.reverse, this.keyType);   // make a internal right sibling node (r)
        r.copy(node, this.MID - 1, this.ORDER - this.MID + 1);                   // copy second half to node r
        for (var i = this.MID - 1; i < node.keyNumber; i++) {                    // delete node key/value
            node.keys.pop();
            node.nodeRef.pop();
        }
        node.keyNumber = this.MID - 1;                                           // reset the number of keys in node n
        return r;                                                                // return right sibling
    },
    makeRoot : function (leftChildNode, key, rightChildNode) {
        var nr = new com.yung.util.TreeNode(false, this.reverse, this.keyType);     // make a node to become the new root
        nr.keyNumber = 1;                                                           // key number set to 1
        nr.nodeRef[0] = leftChildNode;                                              // reference to left node
        nr.keys[0] = key;                                                           // divider key - largest left
        nr.nodeRef[1] = rightChildNode;                                             // reference to right node
        nr.leftMostKey = leftChildNode.keys[0];
        return nr;
    },
    
    /** 
     * return the first key of map 
     * 
     * @instance
     * @memberof com_yung_util_TreeMap
     * @return {keyType}the first key of map
     */
    firstKey : function () {
        return this.firstLeaf.keys[0];
    },
    
    /** 
     * return the last key of map 
     * 
     * @instance
     * @memberof com_yung_util_TreeMap
     * @return {keyType} the last key of map
     */
    lastKey : function () {
        var node = this.root;
        while(node.isLeaf != true) {
            node = node.nodeRef[node.nodeRef.length - 1];
        }
        return node.keys[node.keys.length - 1];
    },
    
    /** 
     * map size
     * 
     * @instance
     * @memberof com_yung_util_TreeMap
     * @return {number} map size
     */
    size : function () {
        return this.keyCount;
    },
    
    /** 
     * clear all key value pairs
     * 
     * @instance
     * @memberof com_yung_util_TreeMap
     */
    clear : function () {
        this.root = new com.yung.util.TreeNode(true, this.reverse, this.keyType);
        this.firstLeaf = this.root;
    },
    
    /** 
     * check map if contains key 
     * 
     * @instance
     * @memberof com_yung_util_TreeMap
     * @param  {keyType} key - key to check if exist
     * @return {boolean} exist or not
     */
    containsKey : function (key) {
        if (key == null) {
            return null;
        }
        if (this.validKeyType(key) == false) {
            return null;
        }
        var ret = this.find(key, this.root);
        if (ret === com_yung_util_TreeNode["notFound"]) {
            return false;
        }
        return true;
    },
    
    /** 
     * check map if contains value 
     * 
     * @instance
     * @memberof com_yung_util_TreeMap
     * @param  {valueType} value - value to check if exist
     * @return {boolean} exist or not
     */
    containsValue : function (value) {
        var nextLeaf = this.firstLeaf;
        while (nextLeaf != null) {
            for (var i = 0; i < nextLeaf.keys.length; i++) {
                var val = nextLeaf.nodeRef[i];
                if (val === value) {
                    return true;
                }
            }
            nextLeaf = nextLeaf.nodeRef[nextLeaf.nodeRef.length - 1];
        }
        return false;
    },
    
    /** 
     * put key value to map 
     * 
     * @instance
     * @memberof com_yung_util_TreeMap
     * @param  {com_yung_util_MapInterface} map - map to put
     */
    putAll : function (map) {
        if (map == null) {
            return;
        } else {
            if (!map instanceof com_yung_util_MapInterface) {
                throw "map is not com_yung_util_MapInterface";
            }
            if (map.size() == 0) {
                return;
            }
            var firstKey = map.getKeyArray()[0];
            if (this.validKeyType(firstKey) == false) {
                throw "key type is not " + this.keyType;
            }
            if (this.validValueType(map.get(firstKey)) == false) {
                if (typeof this.valueType == "string") {
                    throw "value type is not string";
                } else if (typeof this.valueType == "number") {
                    throw "value type is not number";
                } else if (typeof this.valueType == "boolean") {
                    throw "value type is not boolean";
                } else if (typeof this.valueType == "object") {
                    throw "value type is not object";
                } else {
                    throw "value type is not " + this.valueTypeName;
                }
            }
            var entryArray = map.entryArray()
            for (var i = 0; i < entryArray.length; i++) {
                var entry = entryArray[i];
                this.put(entry.getKey(), entry.getValue());
            }
        }
    },
    
    /** 
     * map is empty or not
     * 
     * @instance
     * @memberof com_yung_util_TreeMap
     * @return {boolean} is empty or not
     */
    isEmpty : function () {
        if (this.size() == 0) {
            return true;
        } else {
            return false;
        }
    },
    
    /** 
     * check key type
     * 
     * @private
     * @instance
     * @memberof com_yung_util_TreeMap
     * @return {all} value
     */
    validValueType : function (value) {
        return _validType(this.valueType, value);
    },
    
    /** 
     * check key type
     * 
     * @private
     * @instance
     * @memberof com_yung_util_TreeMap
     * @return {all} key
     */
    validKeyType : function (key) {
        return _validType(this.keyType, key);
    },
    
    toString : function () {
        var ret = this.classProp.name + "\n";
        var entryArray = this.entryArray();
        for (var i = 0; i < entryArray.length; i++) {
            var key = entryArray[i].getKey();
            var val = entryArray[i].getValue();
            var valStr = "";
            if (typeof val.toString == 'function') {
                valStr = val.toString();
            } else {
                valStr = JSON.stringify(val);
            }
            if (this.keyType == 'string') {
                ret = ret + "    '" + key + "': " + valStr + " \n";
            } else if (this.keyType == 'number') {
                ret = ret + "    " + key + ": " + valStr + " \n";
            }
        }
        return ret;
    },
    
    debugNode : function (node, level, bud) {
        if (node == null) bud = bud + "print: unexpected null node\n";

        if (node == this.root) bud = bud + "BpTreeMap Node\n";
        bud = bud + "-------------------------------------------\n";

        for (var j = 0; j < level; j++) bud = bud + "\t";
        bud = bud + "[ . ";
        for (var i = 0; i < node.keyNumber; i++) {
            bud = bud + node.keys[i] + " . ";
        }
        bud = bud + "]\n";
        if (node.isLeaf == false) {
            for (var i = 0; i <= node.keyNumber; i++) {
                bud = this.debugNode(node.nodeRef[i], level + 1, bud);
            }
        }
        if (node == this.root) {
            bud = bud + "-------------------------------------------\n";
        }
        return bud;
    },
    
    debugLeaf : function (leaf, bud) {
        if (leaf == null) bud = bud + "print: unexpected null node\n";
        bud = bud + "BpTreeMap Leaf\n";
        bud = bud + "-------------------------------------------\n";
        var printLeaf = leaf;
        while (printLeaf != null) {
            bud = bud + "[";
            for (var i = 0; i < printLeaf.nodeRef.length - 1; i++) {
                if (i == 0) {
                    bud = bud + printLeaf.nodeRef[i];
                } else {
                    bud = bud + ", " + printLeaf.nodeRef[i];
                }
            }
            bud = bud + "] ";
            printLeaf = printLeaf.nodeRef[printLeaf.nodeRef.length - 1];
        }
        bud = bud + "\n";
        bud = bud + "-------------------------------------------\n";
        return bud;
    }
    
});

/**
 * Tree hash map, for key type is function
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_TreeMap
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_TreeHashMap = com_yung_util_TreeMap.extend({
    
    classProp : { 
        name : "com.yung.util.TreeHashMap"
    },

    /**
     * constructor
     * @memberof com_yung_util_TreeHashMap
     * @param  {function} keyType - key type
     * @param  {all} valueType - value type
     * @param  {boolean} reverse - reverse order
     */
    init : function(keyType, valueType, reverse){
        if (keyType == null || valueType == null) {
            throw "com.yung.util.TreeHashMap key type and value type can not be null!";
        }
        if (typeof keyType == "function") {
            if ($Class.checkFunction(keyType, 'compareTo') == false) {
                throw "key type: " + $Class.getClassName(keyType) + " should implement compareTo function!";
            }
        } else {
            throw "com.yung.util.TreeHashMap key type can only be function!";
        }
        if (reverse == true) {
            this.reverse = true;
        } else {
            this.reverse = false;
        }
        this.MAX = this.ORDER - 1;
        this.MID = (this.ORDER + 1) / 2;
        this.MIN_LEAF = Math.floor(this.ORDER / 2);
        this.MIN_NODE = Math.floor(this.MAX / 2);
        this.keyType = keyType;
        this.keyTypeName = $Class.getClassName(keyType);
        this.valueType = valueType;
        this.valueTypeName = $Class.getClassName(valueType);
        this.root = new com.yung.util.TreeNode(true, this.reverse, this.keyType);
        this.firstLeaf = this.root;
        return this;
    },
    
    toString : function () {
        var ret = this.classProp.name + "\n";
        var entryArray = this.entryArray();
        for (var i = 0; i < entryArray.length; i++) {
            var key = entryArray[i].getKey();
            var val = entryArray[i].getValue();
            var valStr = "";
            if (typeof val.toString == 'function') {
                valStr = val.toString();
            } else {
                valStr = JSON.stringify(val);
            }
            if (typeof key.toString == 'function') {
                ret = ret + "    " + key + " : " + valStr + " \n";
            } else {
                ret = ret + "    " + JSON.stringify(key) + " : " + valStr + " \n";
            }
        }
        return ret;
    }
    
});

var com_yung_util_Map = function (keyType, valueType) {
    if (typeof keyType == "string") {
        keyType = keyType.toLowerCase();
    }
    if (keyType == 'string' || keyType == 'number') {
        if (typeof Map != 'undefined') {
            return new system_util_Map(keyType, valueType);
        }
        return new com.yung.util.BasicMap(keyType, valueType);
    }
    throw "argument keyType not match any type";
}
$Y.register("com.yung.util.Map", com_yung_util_Map);

/**
 * An easy tool to implement movable
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_Movable = {
    
	classProp : { 
		name : "com.yung.util.Movable" 
	},
    
	/**
     * Callback when drag
     *
     * @callback MovableDragCallback
     * @param  {number} startX - element start left position
     * @param  {number} startY - element start top position
     * @param  {number} endX - element end left position
     * @param  {number} endY - element end top position
     */
	/**
	 * enable draggable to element
	 * 
     * @memberof com_yung_util_Movable
     * @param  {string} eleId - element id to enable movable
     * @param  {boolean} disableX - element disable movable in X-axis
     * @param  {boolean} disableY - element disable movable in Y-axis
     * @param  {object} bound - element move boundary ex. {maxX: 100, minX: 0, maxY: 100, minY: 0}
     * @param  {MovableDragCallback} callback - callback when drag
     */
	enable: function(eleId, disableX, disableY, bound, callback){
	    if (disableX == true) {
	        disableX = true;
	    } else {
	        disableX = false;
	    }
	    if (disableY == true) {
	        disableY = true;
        } else {
            disableY = false;
        }
	    if (com_yung_util_getbrowser() == 'msie') {
	        com_yung_util_Movable.draggable(document.getElementById(eleId), disableX, disableY, bound, callback);
	    } else {
	        _dragElement(document.getElementById(eleId), disableX, disableY, bound, callback);
	    }
	},
    
    /**
     * Callback for mouse event listener
     *
     * @callback draggableEventCallback
     * @param {event} e - mouse event
     */
    /** 
     * add event listener to specified element
     * 
     * @private
     * @memberof com_yung_util_Movable
     * @param  {HTMLElement} element - element to enable draggable
     * @param  {string} type - event type, ex. mousedown, mouseon
     * @param  {draggableEventCallback} callback - listener callback
     * @param  {boolean} capture - capture flag
     */
    addListener : function (element, type, callback, capture) {
        if (element.addEventListener) {
            element.addEventListener(type, callback, capture);
        } else {
            element.attachEvent("on" + type, callback);
        }
    },
    
    /** 
     * enable draggble to specified element
     * 
     * @private
     * @memberof com_yung_util_Movable
     * @param  {HTMLElement} element - element to enable draggable
     * @param  {boolean} disableX - element disable movable in X-axis
     * @param  {boolean} disableY - element disable movable in Y-axis
     * @param  {object} bound - element move boundary ex. {maxX: 100, minX: 0, maxY: 100, minY: 0}
     */
    draggable : function (element, disableX, disableY, bound, callback) {
        var dragging = null;
        if (bound == null) {
            bound = {};
        }
        com_yung_util_Movable.addListener(element, "mousedown", function (e) {
            var evt = window.event || e;
            dragging = {
                mouseX: evt.clientX,
                mouseY: evt.clientY,
                startX: parseInt(element.style.left),
                startY: parseInt(element.style.top)
            };
            bound["startX"] = evt.clientX;
            bound["startY"] = evt.clientY;
            if (element.setCapture) element.setCapture();
        });
        com_yung_util_Movable.addListener(element, "losecapture", function () {
            dragging = null;
        });
        com_yung_util_Movable.addListener(document, "mouseup", function () {
            if (document.releaseCapture) document.releaseCapture();
            if (typeof callback == 'function') {
            	if (bound["startX"] != null && bound["startY"] != null && bound["endX"] != null && bound["endY"] != null) {
            		var move = false;
            		if (disableX == false && bound["startX"] != bound["endX"]) {
            		    move = true;
            		}
            		if (disableY == false && bound["startY"] != bound["endY"]) {
                        move = true;
                    }
            		if (move == true) {
            	        // after drag
                        callback(bound["startX"], bound["startY"], bound["endX"], bound["endY"]);   
                    }
            	}
            }
            dragging = null;
            delete bound["startX"];
            delete bound["startY"];
            delete bound["endX"];
            delete bound["endY"];
        }, true);
        var dragTarget = element.setCapture ? element : document;
        com_yung_util_Movable.addListener(dragTarget, "mousemove", function (e) {
            if (!dragging) return;
            var evt = window.event || e;
            var top = dragging.startY + (evt.clientY - dragging.mouseY);
            var left = dragging.startX + (evt.clientX - dragging.mouseX);
            var posX = 0;
            var posY = 0;
            if (disableY == false) {
                posY = Math.max(0, top);
                if (bound != null) {
                    if (bound["maxY"] != null && posY > bound["maxY"]) {
                        posY = bound["maxY"];
                    }
                    if (bound["minY"] != null && posY < bound["minY"]) {
                        posY = bound["minY"];
                    }
                }
                element.style.top = posY + "px";
            }
            if (disableX == false) {
                posX = Math.max(0, left);
                if (bound != null) {
                    if (bound["maxX"] != null && posX > bound["maxX"]) {
                        posX = bound["maxX"];
                    }
                    if (bound["minX"] != null && posX < bound["minX"]) {
                        posX = bound["minX"];
                    }
                }
                element.style.left = posX + "px";
            }
            bound["endX"] = evt.clientX;
            bound["endY"] = evt.clientY;
        }, true);
    }
};

function _dragElement(elmnt, disableX, disableY, bound, callback) {
    if (disableX != true) {
        disableX = false;
    }
    if (disableY != true) {
        disableY = false;
    }
    var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
    var beginX = 0, beginY = 0, finishX = 0, finishY = 0
    //if (document.getElementById(elmnt.id + "header")) {
    //    /* if present, the header is where you move the DIV from: */
    //    document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
    //} else {
        /* otherwise, move the DIV from anywhere inside the DIV: */
        elmnt.onmousedown = dragMouseDown;
    //}

    function dragMouseDown(e) {
        e = e || window.event;
        e.preventDefault();
        // get the mouse cursor position at startup:
        pos3 = e.clientX;
        beginX = e.clientX;
        pos4 = e.clientY;
        beginY = e.clientY;
        document.onmouseup = closeDragElement;
        // call a function whenever the cursor moves:
        document.onmousemove = elementDrag;
    }

    function elementDrag(e) {
        e = e || window.event;
        e.preventDefault();
        // calculate the new cursor position:
        pos1 = pos3 - e.clientX;
        pos2 = pos4 - e.clientY;
        pos3 = e.clientX;
        pos4 = e.clientY;
        // set the element's new position:
        var posX = 0;
        var posY = 0;
        if (disableY == false) {
            posY = elmnt.offsetTop - pos2;
            if (bound != null) {
                if (bound["maxY"] != null && posY > bound["maxY"]) {
                    posY = bound["maxY"];
                }
                if (bound["minY"] != null && posY < bound["minY"]) {
                    posY = bound["minY"];
                }
            }
            elmnt.style.top = posY + "px";
        }
        if (disableX == false) {
            posX = elmnt.offsetLeft - pos1;
            if (bound != null) {
                if (bound["maxX"] != null && posX > bound["maxX"]) {
                    posX = bound["maxX"];
                }
                if (bound["minX"] != null && posX < bound["minX"]) {
                    posX = bound["minX"];
                }
            }
            elmnt.style.left = posX + "px";
        }
        finishX = e.clientX;
        finishY = e.clientY;
    }

    function closeDragElement(e) {
        /* stop moving when mouse button is released: */
        document.onmouseup = null;
        document.onmousemove = null;
        if (typeof callback == 'function') {
            callback(beginX, beginY, finishX, finishY);
        }
    }
}

$Y.reg(com_yung_util_Movable);  
/**
 * An easy tool create popup iframe
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_Popup = $Class.extend({
	
	classProp : { 
    	name : "com.yung.util.Popup" 
    },
    
    color : "rgb(204, 216, 231)",
    
    /**
     * Index to identified iframe window
     * @member {number}
     * @instance
     * @memberof com_yung_util_Popup
     */
    currentPopupIndex : null,
    currentWidth : 0,
    currentHeight : 0,
    enableFixScorll : true,
    
    /**
     * a flag to decide whether can close iframe window
     * @member {boolean}
     * @instance
     * @memberof com_yung_util_Popup
     */
    canCloseWindow : null,
    
    /**
	 * constructor, private used only
	 * Note: do not use this constructor to create instance.
	 * Use com.yung.util.Popup.instance()
	 * 
	 * @private
     * @memberof com_yung_util_Popup
     */
    init : function () {
        if (yung_global_var["com_yung_util_Popup"] != null) {
            throw "Please use com.yung.util.Popup.instance() to create instance!";
        }
        this.currentPopupIndex = 0;
        this.canCloseWindow = true;
        com_yung_util_Popup.instance(this);
        // follow use only for old IE
        var browser = com_yung_util_getbrowser();
        if (browser == 'msie') {
            this.enableFixScorll = false;
            var self = this;
            jQuery(window).scroll(function(){
                if (self.enableFixScorll == false) {
                    return;
                }
                var holder = document.getElementById('popup-holder-' + self.currentPopupIndex);
                if (holder == null) {
                    return;
                }
                jQuery("#popup-holder-" + self.currentPopupIndex).css("display", "none");
            });
            jQuery(window).scrollEnd(function(){
                if (self.enableFixScorll == false) {
                    return;
                }
                var holder = document.getElementById('popup-holder-' + self.currentPopupIndex);
                if (holder == null) {
                    return;
                }
                var position = new com_yung_util_Position(document);
                var pos = position.getCenterPosition(self.currentWidth, self.currentHeight);
                var top = pos.top;
                var left = pos.left;
                document.getElementById('popup-holder-' + self.currentPopupIndex).style.top = top;
                document.getElementById('popup-holder-' + self.currentPopupIndex).style.left = left;
                jQuery("#popup-holder-" + self.currentPopupIndex).css("display", "");
            }, 150);    
        }
        return this;
    },
    
    /** 
     * set popup color
     * 
     * @instance
     * @memberof com_yung_util_Popup
     * @param  {string} color - popup window color
     */
    setColor : function (color) {
    	if (_validType('string', color) == false) {
    		throw "color is not string type";
    	}
    	if (color != null && color != '') {
    		this.color = color;
        }
    },
    
    /** 
     * create popup iframe
     * 
     * @private
     * @instance
     * @memberof com_yung_util_Popup
     * @param  {number} index - index to identified iframe
     */
    createPopoupFrame : function (index) {
        var popup = jQuery("#popup-background-" + index)[0];
        if (popup == null) {
            var obj = {};
            obj["index"] = index;
            var template = new com.yung.util.BasicTemplate(obj);
            template.add('<div id="popup-background-{{index}}" style="display: none; position: absolute; left: 0px; top: 0px; width: 100%; height: 2000px; z-index: 10{{index}}00; -moz-opacity: .85; opacity: .85; filter: alpha(opacity=85); background: none 0px 0px repeat scroll rgb(255, 255, 255);" ></div>');
            template.add('<div id="popup-holder-{{index}}" class="dialog-border" style="display: none; position: absolute; left: 0px; top: 0px; border-radius: 3px; background-color: ' + this.color + '; z-index: 10{{index}}01; width: 200px; height: 100px; box-shadow: 5px 5px 5px #d7d7d7;" >');
            template.add('    <div id="popup-titlebar-{{index}}" class="dialog-titlebar" style="cursor: move; background-color: ' + this.color + ';" >');
            template.add('        <div id="popup-title-{{index}}" style="float: left; padding-left: 18px; padding-top: 3px; "></div><div style="height: 18px;" class="dialog-closeButton" onclick="com_yung_util_Popup.instance().closePopup()"><div class="toolButton"><span style="color: red;" title="close">&#10006;</span></div></div>');
            template.add('    </div>');
            template.add('    <div id="iframe-holder-{{index}}" style="margin: 5px; display: none; background: white;" height="100%">');
            template.add('        <iframe id="popup-content-{{index}}" src="" width="99%" frameborder="0" style="height: 100%; border-style: none transparent;" onload="com_yung_util_Popup.instance().showIframe({{index}});"></iframe>');
            template.add('    </div>');
            template.add('</div>');
            var html = template.toHtml();
            jQuery("body").append(html);
            com.yung.util.Movable.enable("popup-holder-" + index);
	    }
    },
    
    /** 
     * set canClose flag
     * 
     * @instance
     * @memberof com_yung_util_Popup
     * @param  {number} canClose - a flag to decide whether can close iframe window
     */
    setCanCloseWindow : function (canClose) {
        this.canCloseWindow = canClose;
    },
    
    /** 
     * show popup iframe
     * 
     * @private
     * @instance
     * @memberof com_yung_util_Popup
     * @param  {number} index - index to identified iframe
     * @param  {boolean} show - show iframe or not
     */
    showIframe : function (index, show) {
        if (show == false) {
            jQuery("#iframe-holder-" + index).css("display", "none");
            return;    
        }
        jQuery("#iframe-holder-" + index).css("display", "block");
    },
    
    /**
     * Callback when close popup
     *
     * @callback closePopupCallback
     */
    /** 
     * close[hide] popup iframe
     * 
     * @instance
     * @memberof com_yung_util_Popup
     * @param  {number} index - index to identified iframe
     * @param  {closePopupCallback} callback - callback when close popup
     */
    closePopup : function (index, callback) {
        if (this.canCloseWindow) {
            if (index == null) {
                index = this.currentPopupIndex;
            }
            var win = this.getIframeWin();
            if (win != null && typeof win["closePopupBefore"] == "function") {
                var ret = win["closePopupBefore"]();
                if (ret == false) {
                    return;
                }
            }
            this.showIframe(index, false);
            var bg = document.getElementById('popup-background-' + index);
            if (bg != null) {
                document.getElementById('popup-background-' + index).style.display = 'none';
                document.getElementById('popup-holder-' + index).style.display = 'none';
                this.currentPopupIndex = this.currentPopupIndex - 1;
            }
            if (this.currentPopupIndex < 0) {
                this.currentPopupIndex = 0;
            }
            if (typeof callback === "function") {
                callback();
            }
        } else {
            alert("Can not close window now!");
        }
    },
    
    /** 
     * open popup iframe, position and index are optional.
     * if position is empty, use center position of window.
     * if index is empty, use currentPopupIndex plus one[next popup]
     * 
     * @instance
     * @memberof com_yung_util_Popup
     * @param  {string} title - popup window title
     * @param  {number} width - popup window width
     * @param  {number} height - popup window height
     * @param  {string} url - popup iframe url
     * @param  {object} pos - position for popup iframe [optional]
     * @param  {number} index - index of popup iframe [optional]
     */
    openPopup : function (title, width, height, url, pos, index) {
        var windowHeight = (window.innerHeight || document.documentElement.scrollHeight || 0);
        if (windowHeight > jQuery(document).height()) {
            windowHeight = jQuery(document).height();
        }
        var windowWidth = (window.innerWidth || document.documentElement.scrollWidth || 0);
        if (windowWidth > jQuery(document).width()) {
            windowWidth = jQuery(document).width();
        }
        var widthStr = width + "";
        var heightStr = height + "";
        if (widthStr.indexOf('%') > 0) {
            var idx = widthStr.indexOf('%');
            var popWidthPercent = widthStr.substring(0, idx) * 1.0;
            if (isNaN(popWidthPercent)) {
                alert(popWidthPercent + " is not number!");
            }
            width = windowWidth * popWidthPercent / 100;
        }
        if (heightStr.indexOf('%') > 0) {
            var idx = heightStr.indexOf('%');
            var popHeightPercent = heightStr.substring(0, idx) * 1.0;
            if (isNaN(popHeightPercent)) {
                alert(popHeightPercent + " is not number!");
            }
            height = windowHeight * popHeightPercent / 100;
        }
        this.currentWidth = width;
        this.currentHeight = height;
        var browser = com_yung_util_getbrowser();
        if (pos == null) {
            var position = new com_yung_util_Position(document);
            if (this.enableFixScorll == true && browser != 'msie') {
                pos = position.getScreenCenterPosition(width, height); 
            } else {
                pos = position.getCenterPosition(width, height);
            }
        }
        this.openPopupWithPos(title, width, height, url, pos, index);
    },
    
    /** 
     * open popup iframe, position and index are optional.
     * if position is empty, use center position of window.
     * if index is empty, use currentPopupIndex plus one[next popup]
     * 
     * @private
     * @instance
     * @memberof com_yung_util_Popup
     * @param  {string} title - popup window title
     * @param  {number} width - popup window width
     * @param  {number} height - popup window height
     * @param  {string} url - popup iframe url
     * @param  {object} pos - position for popup iframe [optional]
     * @param  {number} index - index of popup iframe [optional]
     */
    openPopupWithPos : function (title, width, height, url, pos, index) {
        if (index == null) {
            this.currentPopupIndex = this.currentPopupIndex + 1;
            index = this.currentPopupIndex;
        }
        this.createPopoupFrame(index);
        var windowHeight = (window.innerHeight || document.documentElement.scrollHeight || 0);
        var maskHeight = windowHeight;
        if (windowHeight > jQuery(document).height()) {
            windowHeight = jQuery(document).height();
        } else {
            maskHeight = jQuery(document).height();
        }
        var windowWidth = (window.innerWidth || document.documentElement.scrollWidth || 0);
        if (windowWidth > jQuery(document).width()) {
            windowWidth = jQuery(document).width();
        }
        if (pos == null) {
            var position = new com_yung_util_Position(document);
            pos = position.getCenterPosition(width, height);    
        }
        var top = pos.top;
        var left = pos.left;
        var browser = com_yung_util_getbrowser();
        if (this.enableFixScorll == true && browser != 'msie') {
            jQuery("#popup-holder-" + index).css("position", "fixed");
        } else {
            jQuery("#popup-holder-" + index).css("position", "absolute");
        }
        document.getElementById('popup-holder-' + index).style.top = top;
        document.getElementById('popup-holder-' + index).style.left = left;
        document.getElementById('popup-title-' + index).innerHTML = title;
        document.getElementById('popup-holder-' + index).style.height = height;
        document.getElementById('popup-holder-' + index).style.width = width;
        document.getElementById('popup-content-' + index).style.height = height - 50;
        document.getElementById('popup-content-' + index).src = url;
        document.getElementById('popup-background-' + index).style.display = 'block';
        document.getElementById('popup-background-' + index).style.height = maskHeight + 'px';
        document.getElementById('popup-background-' + index).style.width = windowWidth + 'px';
        document.getElementById('popup-holder-' + index).style.display = 'block';
    },
    
    /** 
     * get iframe window by index, if index is empty, use currentPopupIndex
     * 
     * @instance
     * @memberof com_yung_util_Popup
     * @param  {number} index - index to identified iframe
     * @return  {window} iframe window
     */
    getIframeWin : function (index) {
        if (index == null) {
            index = this.currentPopupIndex;
        }
        var ele = document.getElementById("popup-content-" + index);
        if (ele == null) {
            return null;
        }
        var win = document.getElementById("popup-content-" + index).contentWindow;
        return win;
    },
    
    /** 
     * get parent iframe window
     * 
     * @instance
     * @memberof com_yung_util_Popup
     * @return  {window} iframe window
     */
    getParentWin : function () {
        var win = this.getIframeWin(this.currentPopupIndex - 1);
        if (win == null) {
            return window;
        } else {
            return win;
        }
    },
    
    /** 
     * search parent iframe window iteratively until find partial url string. 
     * if not found return null
     * 
     * @instance
     * @memberof com_yung_util_Popup
     * @param  {string} partialUrl - partial url
     * @return  {window} iframe window
     */
    getParentWinByUrl : function (partialUrl) {
    	var nextIdx = this.currentPopupIndex - 1;
    	while (true) {
    		if (nextIdx < 0) {
    			break;
    		}
    		var win = this.getIframeWin(nextIdx);
    		if (win == null) {
    			break;
    		}
    		if (win.location.href.indexOf(partialUrl) >= 0) {
    			return win;
    		}
    		nextIdx = nextIdx -1;
    	}
    	if (window.location.href.indexOf(partialUrl) >= 0) {
    		return window;
    	}
    	return null;
    },
    
    /** 
     * To Fix popup position when scroll window
     * 
     * @instance
     * @memberof com_yung_util_Popup
     * @param  {boolean} enableFixScorll - fix or not
     */
    setEnableFixScroll : function (enableFixScorll) {
        if (enableFixScorll == true) {
            this.enableFixScorll = true;
        } else {
            this.enableFixScorll = false;
        }
    }
    
});

/** 
 * get com_yung_util_Popup global instance,
 * com_yung_util_Popup instance is global unique.
 * 
 * @param  {com_yung_util_Popup} popup - com_yung_util_Popup instance, for private use only
 * @return  {com_yung_util_Popup} com_yung_util_Popup instance
 */
com_yung_util_Popup.instance = function(popup) {
    if (popup != null) {
        if (popup instanceof com_yung_util_Popup) {
            yung_global_var["com_yung_util_Popup"] = popup;
        }
    } else {
        try {
            if (window.parent.yung_global_var != null && window.parent.yung_global_var["com_yung_util_Popup"] != null) {
                return window.parent.yung_global_var["com_yung_util_Popup"];
            }    
        } catch (e) {
            // prevent permission denied
        }
        var instance = yung_global_var["com_yung_util_Popup"];
        if (instance == null) {
            instance = new com_yung_util_Popup();
        }
        return instance;
    }
}
/**
 * An easy tool to get position of element
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_Position = $Class.extend({
	
    classProp : { 
    	name : "com.yung.util.Position" 
    },
    
    /**
     * HTML element to get position
     * @member {HTMLElement}
     * @instance
     * @memberof com_yung_util_Position
     */
    element : null,
    
    /**
	 * constructor
     * @memberof com_yung_util_Position
     * @param  {HTMLElement} element - HTML element to get position
     */
    init : function (element) {
        if (element == null) {
            throw "element can not be null";
        }
        if (typeof element.get == 'function') {
            if (element[0] == null) {
                throw "element can not be null";
            }
            this.element = element;
        } else {
            this.element = jQuery(element);
        }
        return this;
    },
    
    /** 
     * check current window if in iframe
     * 
     * @instance
     * @memberof com_yung_util_Position
     * @return  {boolean} in iframe or not
     */
    inIframe : function () {
        try {
            return window.self !== window.top;
        } catch (e) {
            return true;
        }
    },
    
    
    
    getParentBaseTop : function () {
        var inIframe = this.inIframe();
        if (inIframe == true) {
            var doc = window.parent.document.documentElement;
            var top = (window.parent.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);
            if (top == 0) {
                // quirk mode will get 0, so check if browser is IE quirk
                var browser = com_yung_util_getbrowser();
                if (browser == 'msie') {
                    // IE quirk
                    top = window.parent.document.body.scrollTop - (doc.clientTop || 0);
                }
            }
            return top;
        } else {
            return 0;
        }
    },
    getParentBaseLeft : function () {
        var inIframe = this.inIframe();
        if (inIframe == true) {
            var doc = window.parent.document.documentElement;
            var left = (window.parent.pageXOffset || doc.scrollLeft) - (doc.clientLeft || 0);
            if (left == 0) {
                // quirk mode will get 0, so check if browser is IE quirk
                var browser = com_yung_util_getbrowser();
                if (browser == 'msie') {
                    // IE quirk
                    left = window.parent.document.body.scrollLeft - (doc.clientLeft || 0);
                }
            }
            return left;
        } else {
            return 0;
        }
    },
    
    
    
    
    /** 
     * get document element top position, use by scroll bar occurs
     * 
     * @instance
     * @memberof com_yung_util_Position
     * @return  {number} document element top position
     */
    getBaseTop : function () {
    	var doc = document.documentElement;
    	var top = (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);
    	if (top == 0) {
    	    // quirk mode will get 0, so check if browser is IE quirk
    	    var browser = com_yung_util_getbrowser();
    	    if (browser == 'msie') {
    	        // IE quirk
                top = document.body.scrollTop - (doc.clientTop || 0);
            }
    	}
    	return top;
    },
    
    /** 
     * get document element left position, use by scroll bar occurs
     * 
     * @instance
     * @memberof com_yung_util_Position
     * @return  {number} document element left position
     */
    getBaseLeft : function () {
    	var doc = document.documentElement;
    	var left = (window.pageXOffset || doc.scrollLeft) - (doc.clientLeft || 0);
    	if (left == 0) {
            // quirk mode will get 0, so check if browser is IE quirk
    	    var browser = com_yung_util_getbrowser();
            if (browser == 'msie') {
                // IE quirk
                left = document.body.scrollLeft - (doc.clientLeft || 0);
            }
        }
    	return left;
    },
    
    /** 
     * get specified element bottom left position
     * 
     * @instance
     * @memberof com_yung_util_Position
     * @param  {number} offsetX - offset left [default 0]
     * @param  {number} offsetY - offset top [default 0]
     * @param  {HTMLElement} base - base HTMLElement [optional]
     * @return  {object} specified element bottom left position
     */
    getBotLeftPosition : function (offsetX, offsetY, base) {
        if (offsetX == null) {
            offsetX = 0;
        }
        if (offsetY == null) {
            offsetY = 0;
        }
        var baseEle = null;
        if (base != null) {
            if (typeof base.get == 'function') {
                baseEle = base;
            } else {
                baseEle = jQuery(base);
            }
        }
        var scrollTop = 0;
        var scrollLeft = 0;
        if (base == null) {
            scrollTop = this.getBaseTop();
            scrollLeft = this.getBaseLeft();
        } else {
            scrollTop = baseEle.scrollTop();
            scrollLeft = baseEle.scrollLeft();
        }
        var rect = this.getBoundingClientRect(baseEle);
        var pos = {};
        pos.left = rect.left + scrollLeft + offsetX;
        pos.top = rect.top + scrollTop + this.element.height() + offsetY;
        return pos;
    },
    
    /** 
     * get specified element top left position
     * 
     * @instance
     * @memberof com_yung_util_Position
     * @param  {number} offsetX - offset left [default 0]
     * @param  {number} offsetY - offset top [default 0]
     * @param  {HTMLElement} base - base HTMLElement [optional]
     * @return  {object} specified element top left position
     */
    getTopLeftPosition : function (offsetX, offsetY, base) {
        if (offsetX == null) {
            offsetX = 0;
        }
        if (offsetY == null) {
            offsetY = 0;
        }
        var baseEle = null;
        if (base != null) {
            if (typeof base.get == 'function') {
                baseEle = base;
            } else {
                baseEle = jQuery(base);
            }
        }
        var scrollTop = 0;
        var scrollLeft = 0;
        if (base == null) {
            scrollTop = this.getBaseTop();
            scrollLeft = this.getBaseLeft();
        } else {
            scrollTop = baseEle.scrollTop();
            scrollLeft = baseEle.scrollLeft();
        }
        var rect = this.getBoundingClientRect(baseEle);
        var pos = {};
        pos.left = rect.left + scrollLeft + offsetX;
        pos.top = rect.top + scrollTop + offsetY;
        return pos;
    },
    
    /** 
     * get specified element top right position
     * 
     * @instance
     * @memberof com_yung_util_Position
     * @param  {number} offsetX - offset left [default 0]
     * @param  {number} offsetY - offset top [default 0]
     * @param  {HTMLElement} base - base HTMLElement [optional]
     * @return  {object} specified element top right position
     */
    getTopRightPosition : function (offsetX, offsetY, base) {
        if (offsetX == null) {
            offsetX = 0;
        }
        if (offsetY == null) {
            offsetY = 0;
        }
        var baseEle = null;
        if (base != null) {
            if (typeof base.get == 'function') {
                baseEle = base;
            } else {
                baseEle = jQuery(base);
            }
        }
        var scrollTop = 0;
        var scrollLeft = 0;
        if (base == null) {
            scrollTop = this.getBaseTop();
            scrollLeft = this.getBaseLeft();
        } else {
            scrollTop = baseEle.scrollTop();
            scrollLeft = baseEle.scrollLeft();
        }
        var rect = this.getBoundingClientRect(baseEle);
        var pos = {};
        pos.left = rect.left + scrollLeft + this.element.width() + offsetX;
        pos.top = rect.top + scrollTop + offsetY;
        return pos;
    },
    
    /** 
     * get specified element bottom right position
     * 
     * @instance
     * @memberof com_yung_util_Position
     * @param  {number} offsetX - offset left [default 0]
     * @param  {number} offsetY - offset top [default 0]
     * @param  {HTMLElement} base - base HTMLElement [optional]
     * @return  {object} specified element bottom right position
     */
    getBotRightPosition : function (offsetX, offsetY, base) {
        if (offsetX == null) {
            offsetX = 0;
        }
        if (offsetY == null) {
            offsetY = 0;
        }
        var baseEle = null;
        if (base != null) {
            if (typeof base.get == 'function') {
                baseEle = base;
            } else {
                baseEle = jQuery(base);
            }
        }
        var scrollTop = 0;
        var scrollLeft = 0;
        if (base == null) {
            scrollTop = this.getBaseTop();
            scrollLeft = this.getBaseLeft();
        } else {
            scrollTop = baseEle.scrollTop();
            scrollLeft = baseEle.scrollLeft();
        }
        var rect = this.getBoundingClientRect(baseEle);
        var pos = {};
        pos.left = rect.left + scrollLeft + this.element.width() + offsetX;
        pos.top = rect.top + scrollTop + this.element.height() + offsetY;
        return pos;
    },
    
    /** 
     * get center position of specified width and height
     * 
     * @memberof com_yung_util_Position
     * @param  {number} width - width [default 0]
     * @param  {number} height - height [default 0]
     * @return  {object} specified element bottom left position
     */
    getCenterPosition : function (width, height) {
        if (width == null) {
            width = 0;
        }
        if (height == null) {
            height = 0;
        }
        var windowHeight = (window.innerHeight || document.documentElement.scrollHeight || 0);
        if (windowHeight > jQuery(document).height()) {
            windowHeight = jQuery(document).height();
        }
        var windowWidth = (window.innerWidth || document.documentElement.scrollWidth || 0);
        if (windowWidth > jQuery(document).width()) {
            windowWidth = jQuery(document).width();
        }
        var pos = {};
        pos["left"] = this.getBaseLeft() + (windowWidth - width) / 2;
        pos["top"] = this.getBaseTop() + (windowHeight - height) / 2;
        if (pos["left"] < 0 || pos["top"] < 0) {
            var windowWidth = (window.innerWidth || document.documentElement.scrollWidth || 0);
            var windowHeight = (window.innerHeight || document.documentElement.scrollHeight || 0);
        	return this.getCenterPositionWithWinSize(width, height, windowWidth, windowHeight);
        }
        if (pos.top < 0) {
            pos.top = 0;
        }
        if (pos.left < 0) {
            pos.left = 0;
        }
        return pos;
    },
    
    /** 
     * get center position of specified width, height, windowWidth and windowHeight
     * 
     * @private
     * @instance
     * @memberof com_yung_util_Position
     * @param  {number} width - width [default 0]
     * @param  {number} height - height [default 0]
     * @param  {number} windowWidth - window width
     * @param  {number} windowHeight - window height
     * @return  {object} specified element bottom left position
     */
    getCenterPositionWithWinSize : function (width, height, windowWidth, windowHeight) {
        if (width == null) {
            width = 0;
        }
        if (height == null) {
            height = 0;
        }
        if (windowWidth == null) {
        	throw "windowWidth not defined";
        }
        if (windowHeight == null) {
            throw "windowHeight not defined";
        }
        var pos = {};
        pos["left"] = this.getBaseLeft() + (windowWidth - width) / 2;
        pos["top"] = this.getBaseTop() + (windowHeight - height) / 2;
        if (pos.top < 0) {
            pos.top = 0;
        }
        if (pos.left < 0) {
            pos.left = 0;
        }
        return pos;
    },
    
    /** 
     * get bounding client rect
     * 
     * @instance
     * @memberof com_yung_util_Position
     * @param  {HTMLElement} base - base element [optional]
     * @return  {object} bounding client rect
     */
    getBoundingClientRect : function (base) {
        var ret = {};
        var rect = this.element.get(0).getBoundingClientRect();
        if (base != null) {
            var baseEle = null;
            if (typeof base.get == 'function') {
                baseEle = base;
            } else {
                baseEle = jQuery(base);
            }
            var baseRect = baseEle.get(0).getBoundingClientRect();
            ret.top = rect.top - baseRect.top;
            ret.left = rect.left - baseRect.left;
        } else {
            ret.top = rect.top;
            ret.left = rect.left;
        }
        return ret;
    },
    
    /** 
     * get window screen size
     * 
     * @instance
     * @memberof com_yung_util_Position
     * @return  {object} specified element width and height
     */
    getScrennSize : function () {
        var ret = {};
        ret["height"] = jQuery(window).height();
        ret["width"] = jQuery(window).width();
        return ret;
    },
    
    /** 
     * get window screen center position
     * 
     * @instance
     * @memberof com_yung_util_Position
     * @param  {number} width - width [default 0]
     * @param  {number} height - height [default 0]
     * @return  {object} specified element bottom left position
     */
    getScreenCenterPosition : function (width, height) {
        if (width == null) {
            width = 0;
        }
        if (height == null) {
            height = 0;
        }
        var winSize = this.getScrennSize();
        var pos = {};
        pos["left"] = (winSize.width - width) / 2;
        pos["top"] = (winSize.height - height) / 2;
        if (pos.top < 0) {
            pos.top = 0;
        }
        if (pos.left < 0) {
            pos.left = 0;
        }
        return pos;
    }
    
});

/** 
 * Returns browser scroll bar width
 * 
 * @memberof com_yung_util_Position
 * @return {number} scroll bar width
 */
com_yung_util_Position.getScrollbarWidth = function () {
    var ret = yung_global_var["com_yung_util_Position.getScrollbarWidth.result"];
    if (ret != null) {
        return ret;
    }
    // Creating invisible container
    var outer = document.createElement('div');
    outer.style.visibility = 'hidden';
    outer.style.overflow = 'scroll'; // forcing scrollbar to appear
    outer.style.msOverflowStyle = 'scrollbar'; // needed for WinJS apps
    document.body.appendChild(outer);

    // Creating inner element and placing it in the container
    var inner = document.createElement('div');
    outer.appendChild(inner);

    // Calculating difference between container's full width and the child width
    var scrollbarWidth = (outer.offsetWidth - inner.offsetWidth);
    var scrollbarHeight = (outer.offsetHeight - inner.offsetHeight);
    if (scrollbarWidth == 0) {
        scrollbarWidth = 17;
    }
    if (scrollbarHeight == 0) {
        scrollbarHeight = scrollbarWidth;
    } else if (scrollbarHeight > 25) {
        scrollbarHeight = scrollbarWidth;
    }

    // Removing temporary elements from the DOM
    outer.parentNode.removeChild(outer);
    yung_global_var["com_yung_util_Position.getScrollbarWidth.result"] = scrollbarWidth;
    yung_global_var["com_yung_util_Position.getScrollbarHeight.result"] = scrollbarHeight;
    return scrollbarWidth;
}


/** 
 * Returns browser scroll bar height
 * 
 * @memberof com_yung_util_Position
 * @return {number} scroll bar height
 */
com_yung_util_Position.getScrollbarHeight = function () {
    var ret = yung_global_var["com_yung_util_Position.getScrollbarHeight.result"];
    if (ret != null) {
        return ret;
    }
    // Creating invisible container
    var outer = document.createElement('div');
    outer.style.visibility = 'hidden';
    outer.style.overflow = 'scroll'; // forcing scrollbar to appear
    outer.style.msOverflowStyle = 'scrollbar'; // needed for WinJS apps
    document.body.appendChild(outer);

    // Creating inner element and placing it in the container
    var inner = document.createElement('div');
    outer.appendChild(inner);

    // Calculating difference between container's full width and the child width
    var scrollbarWidth = (outer.offsetWidth - inner.offsetWidth);
    var scrollbarHeight = (outer.offsetHeight - inner.offsetHeight);
    if (scrollbarWidth == 0) {
        scrollbarWidth = 17;
    }
    if (scrollbarHeight == 0) {
        scrollbarHeight = scrollbarWidth;
    } else if (scrollbarHeight > 25) {
        scrollbarHeight = scrollbarWidth;
    }
    
    // Removing temporary elements from the DOM
    outer.parentNode.removeChild(outer);
    yung_global_var["com_yung_util_Position.getScrollbarWidth.result"] = scrollbarWidth;
    yung_global_var["com_yung_util_Position.getScrollbarHeight.result"] = scrollbarHeight;
    return scrollbarHeight;
}
/**
 * String utility, require {@link com_yung_util_Validator}
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_StringUtils = {
    
	classProp : { 
		name : "com.yung.util.StringUtils"
	},
	
	/** 
     * check string is empty or null
     * 
     * @memberof com_yung_util_StringUtils
     * @param {string} str - string to check
     * @return {boolean} true or false
     */
    isBlank : function (str) {
        if (str === null || str === '') {
            return true;
        } else {
            return false;
        }
    },
    
    /** 
     * Returns string, with leading and trailing whitespace omitted.
     * 
     * @memberof com_yung_util_StringUtils
     * @param {string} str - target string
     * @return {string} result string
     */
    trim : function (str) {
        return str.replace(/^\s*/, "").replace(/\s*$/, "");
    },
    
    /** 
     * check string if it start with pattern
     * 
     * @memberof com_yung_util_StringUtils
     * @param {string} str - string to check
     * @param {string} pattern - string pattern
     * @return {boolean} true or false
     */
    startsWith : function (str, pattern) {
        if (str.indexOf(pattern) === 0) {
            return true;
        } else {
            return false;
        }
    },
    
    /** 
     * check string if it end with pattern
     * 
     * @memberof com_yung_util_StringUtils
     * @param {string} str - string to check
     * @param {string} pattern - string pattern
     * @return {boolean} true or false
     */
    endsWith : function (str, pattern) {
        var idx = str.indexOf(pattern);
        if (idx === (str.length - pattern.length)) {
            return true;
        } else {
            return false;
        }
    },
    
    /** 
     * check string if it contains pattern
     * 
     * @memberof com_yung_util_StringUtils
     * @param {string} str - string to check
     * @param {string} pattern - string pattern
     * @return {boolean} true or false
     */
    contains : function (str, pattern) {
        if (str.indexOf(pattern) >= 0) {
            return true;
        } else {
            return false;
        }
    },
    
    /** 
     * check string and replace string pattern to specific string
     * 
     * @memberof com_yung_util_StringUtils
     * @param {string} targetStr - target string
     * @param {string} strFind - string pattern
     * @param {strReplace} strFind - string to replace pattern
     * @return {string} result string
     */
    replaceAll : function (targetStr, strFind, strReplace) {
        return _replaceAll(targetStr, strFind, strReplace);
    },
    
    /** 
     * check string and remove string pattern
     * 
     * @memberof com_yung_util_StringUtils
     * @param {string} targetStr - target string
     * @param {string} pattern - string pattern
     * @return {string} result string
     */
    remove : function (str, pattern) {
        return this.replaceAll(str, pattern, "");
    },
    
    /** 
     * if string is null then return empty string
     * otherwise return itself
     * 
     * @memberof com_yung_util_StringUtils
     * @param {string} str - check string
     * @return {string} result string
     */
    defaultString : function (str) {
        if (str == null) {
            return "";
        } else {
            return str;
        }
    },
    
    /** 
     * convert string to number
     * 
     * @memberof com_yung_util_StringUtils
     * @param {string} str - string to convert
     * @return {number} result number
     */
    toNumber : function (str) {
        return str * 1.0;
    },
    
    /** 
     * check string if it can convert to number
     * 
     * @memberof com_yung_util_StringUtils
     * @param {string} str - string to check
     * @return {boolean} true or false
     */
    isNumeric : function (str) {
        return jQuery.isNumeric(str);
    },
    
    /** 
     * parse input url string, then convert to object map
     * , parameter key and parameter value, value will decodeURIComponent.
     * Note. if url is null then use window location
     * 
     * @memberof com_yung_util_StringUtils
     * @param {string} url - url string
     * @return {object} object map of parse result
     */
    parseURL : function (url) {
        var map = this.parseURINotDecode(url);
        for (var key in map) {
            var value = map[key];
            try {
                value = decodeURIComponent(value);
            } catch (e) {
                // ignore
            }
            map[key] = value;
        }
        return map;
    },
    
    /** 
     * parse input url string, then convert to object map
     * , parameter key and parameter value, value will not decodeURIComponent.
     * Note. if url is null then use window location
     * 
     * @memberof com_yung_util_StringUtils
     * @param {string} url - url string
     * @return {object} object map of parse result
     */
    parseURINotDecode : function (url) {
        var map = {};
        if (url == null) {
            url = window.location.search.substring(1);
        }
        var idx = url.indexOf("?");
        if (idx > 0) {
            url = url.substring(idx + 1, url.length);
        }
        var sURLVariables = url.split('&');
        var sParameterName;
        for (var i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');
            if (sParameterName[1] === undefined) {
                continue;
            } else {
                map[sParameterName[0]] = sParameterName[1];
            }
        }
        return map;
    },
    
    /** 
     * parse object map, then revert to string url, like '&a=1&b=2'
     * 
     * @memberof com_yung_util_StringUtils
     * @param {object} map - object map
     * @return {string} url string
     */
    mapToURL : function (map) {
        if (map == null) {
            return "";
        }
        var url = "";
        for (key in map) {
            var value = map[key];
            if (value != null) {
                if (url == '') {
                    url = key + "=" + value;
                } else {
                    url = url + "&" + key + "=" + value;
                }
            }
        }
        return url;
    },
    
    /** 
     * calculate string length, for those special string character(non ascii)
     * will be consider as 3 length(default), or specific length
     * 
     * @memberof com_yung_util_StringUtils
     * @param {string} input - target sting
     * @param {number} nonASCIILength - the length of special string character(non ascii)
     * @return {number} total length
     */
    calcLength : function (input, nonASCIILength) {
        if (nonASCIILength == null) {
            nonASCIILength = 3;
        } else {
            nonASCIILength = parseInt(nonASCIILength);
        }
        if (input != null && input != '') {
            var cnt = 0;
            for (var i = 0; i < input.length; i++) {
                var ch = input.charAt(i) + '';
                if (com.yung.util.Validator.ascii(ch)) {
                    cnt = cnt + 1;
                } else {
                    cnt = cnt + nonASCIILength;
                }
            }
            return cnt;
        } else {
            return -1;
        }
    },
    
    /** 
     * substring, for those special string character(non ascii)
     * will be consider as 3 length(default), or specific length
     * 
     * @memberof com_yung_util_StringUtils
     * @param {string} input - target sting
     * @param {number} length - final total length
     * @param {number} nonASCIILength - the length of special string character(non ascii)
     * @return {number} total length
     */
    cutLength : function (input, length, nonASCIILength) {
        if (input == null || input == '') {
            return input;
        }
        if (nonASCIILength == null) {
            nonASCIILength = 3;
        } else {
            nonASCIILength = parseInt(nonASCIILength);
        }
        var count = 0;
        var sb = '';
        for (var i = 0; i < input.length; i++) {
            var c = input.charAt(i) + "";
            if (com.yung.util.Validator.ascii(c)) {
                count = count + 1;
            } else {
                count = count + nonASCIILength;
            }
            if (count > length) {
                break;
            }
            sb = sb + c;
        }
        return sb;
    }
};
$Y.reg(com_yung_util_StringUtils);
/**
 * Create tab tool
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_Tab = $Class.extend({
    
    classProp : { 
        name : "com.yung.util.Tab" 
    },
    
    hashId : '',
    
    /**
     * div id to put tab
     * @member {string}
     * @instance
     * @memberof com_yung_util_Tab
     */
    divId : null,
    
    /**
     * tab title array
     * @member {Array}
     * @instance
     * @memberof com_yung_util_Tab
     */
    titles : null,
    
    /**
     * tab color
     * @member {string}
     * @instance
     * @memberof com_yung_util_Tab
     */
    color : null,
    
    /**
     * tab width
     * @member {number}
     * @instance
     * @memberof com_yung_util_Tab
     */
    width : null,
    
    /**
     * tab span
     * @member {number}
     * @instance
     * @memberof com_yung_util_Tab
     */
    span : null,
    
    /**
     * tab unique id to identify
     * @member {Array}
     * @instance
     * @memberof com_yung_util_Tab
     */
    tabId : null,
    
    srcContent : null,
    
    /**
     * Callback when click tab
     *
     * @callback clickTabCallback
     * @param {number} tabIndex - tab index
     */
    /**
     * tab click callback
     * @member {clickTabCallback}
     * @instance
     * @memberof com_yung_util_Tab
     */
    callback : null,
    
    /**
     * Callback when create tab
     *
     * @callback createTabCallback
     */
    /**
     * constructor
     * @memberof com_yung_util_Tab
     * @param  {string} divId - div id to put tab
     * @param  {Array} titles - tab title array
     * @param  {string} tabId - tab unique id to identify, if empty, system will create one
     * @param  {string} color - tab color, if empty, it will be black
     * @param  {number} width - tab width, if empty, it will be 120
     * @param  {number} span - tab span, if empty, it will be 30
     * @param  {createTabCallback} afterRender - data object to render HTML
     * @param  {number} afterRenderTime - wait millisecond to run afterRender
     */
    init : function (divId, titles, tabId, color, width, span, afterRender, afterRenderTime) {
        $Class.validate(this.classProp.name, divId);
        if (divId == null) {
            throw "Please specify divId";
        }
        this.divId = divId;
        this.hashId = MD5Util.calc(this.classProp.name + "-" + divId);
        if (titles == null) {
            throw "Please specify titles";
        }
        if (jQuery.isArray(titles) == false) {
            throw "titles has to be array";
        }
        this.titles = titles;
        if (color == null || color == '') {
            this.color = "black";
        } else {
            this.color = color;
        }
        if (tabId == null || tabId == '') {
            this.tabId = new Date().getTime() + "";
        } else {
            this.tabId = tabId;
        }
        if (width == null || width == '') {
            this.width = 120;
        } else {
            this.width = width;
        }
        if (span == null || span == '') {
            this.span = 30;
        } else {
            this.span = span;
        }
        this.srcContent = new com_yung_util_BasicList("string");
        this.createTab();
        for (var i = 0; i < this.srcContent.size(); i++) {
            this.setTabContent(i, this.srcContent.get(i));
        }
        this.showTab(0);
        var waitTime = 250;
        if (typeof afterRenderTime == 'number') {
            waitTime = afterRenderTime;
        }
        if (typeof afterRender == 'function') {
            setTimeout(function () {
                afterRender();
            }, waitTime);
        }
        return this;
    },
    
    /** 
     * create tab element
     * 
     * @private
     * @instance
     * @memberof com_yung_util_Tab
     */
    createTab : function () {
        this.getSourceTabContent();
        var headBasicTmp = new com.yung.util.BasicTemplate();
        headBasicTmp.add('<td width="{{width}}">');
        headBasicTmp.add('    <div id="{{tabId}}Tab{{idx}}Show" style="display:none; border-color: {{tabColor}}; background-color: {{tabColor}}; color: white; white-space: nowrap; text-align: center; border-style: solid; border-color: {{tabColor}}; border-width: 2px 1px 0px 1px; padding-top: 5px; padding-bottom: 5px; cursor: pointer;" >');
        headBasicTmp.add('        <span>{{title}}</span>');
        headBasicTmp.add('    </div>');
        headBasicTmp.add('    <div id="{{tabId}}Tab{{idx}}Wait" style="display:none; border-color: {{tabColor}}; background-color: white; color: {{tabColor}}; white-space: nowrap; text-align: center; border-style: solid; border-color: {{tabColor}}; border-width: 2px 2px 0px 2px; padding-top: 5px; padding-bottom: 5px; cursor: pointer;" onclick="fireInstanceMethod(\'' + this.hashId + '\', \'showTab\', {{idx}});">');
        headBasicTmp.add('        <span>{{title}}</span>');
        headBasicTmp.add('    </div>');
        headBasicTmp.add('    </div>');
        headBasicTmp.add('</td>');
        headBasicTmp.add('<td width="{{span}}" style="border-width: 0px 0px 0px 0px; border-color: {{tabColor}}; border-style: solid;" ></td>');
        var headTmp = new com.yung.util.ArrayTemplate();
        headTmp.setBasicTmp(headBasicTmp);
        var contentBasicTmp = new com.yung.util.BasicTemplate();
        contentBasicTmp.add('<div id="{{tabId}}TabContent{{idx}}" style="display:none; border-top: 1px solid {{tabColor}}; padding-left: 10px; padding-right: 10px;">');
        contentBasicTmp.add('</div>');
        var contentTmp = new com.yung.util.ArrayTemplate();
        contentTmp.setBasicTmp(contentBasicTmp);
        var dataArray = this.getDataArray();
        var mainTmp = new com.yung.util.BasicTemplate();
        mainTmp.add('<table border="0" cellpadding="0" cellspacing="0" width="100%">');
        mainTmp.add('    <tr>');
        mainTmp.add('        <td width="15" style="border-width: 0px 0px 0px 0px; border-color: {{tabColor}}; border-style: solid;" ></td>');
        mainTmp.add('        {{headTmp}}');
        mainTmp.add('        <td id="{{tabId}}FinalTd" width="*" style="border-width: 0px 0px 0px 0px; border-color: {{tabColor}}; border-style: solid;" ></td>');
        mainTmp.add('    </tr>');
        mainTmp.add('    <tr>');
        mainTmp.add('        <td colspan="{{colspan}}">');
        mainTmp.add('            {{contentTmp}}');
        mainTmp.add('        </td>');
        mainTmp.add('    </tr>');
        mainTmp.add('</table>');
        var mainData = {};
        mainData["colspan"] = (this.titles.length * 2) + 2;
        mainData["tabColor"] = this.color;
        mainData["tabId"] = this.tabId;
        mainData["headTmp"] = headTmp.toHtml(dataArray);
        mainData["contentTmp"] = contentTmp.toHtml(dataArray);
        jQuery("#" + this.divId).html(mainTmp.toHtml(mainData));
        var browser = com_yung_util_getbrowser();
        if (browser == 'msie') {
            var self = this;
            setTimeout(function(){
                self.adjustRightWidth(self);
            }, 500);
        }
    },
    
    /** 
     * adjust tab width
     * 
     * @private
     * @instance
     * @memberof com_yung_util_Tab
     * @param  {com_yung_util_Tab} tab - tab instance
     */
    adjustRightWidth : function (tab) {
        var divWidth = jQuery("#" + tab.divId).width();
        var leftWidth = tab.titles.length * (tab.width + tab.span) + 15;
        var rightWidth = divWidth - leftWidth;
        if (rightWidth > 0) {
            jQuery("#" + tab.tabId + "FinalTd").attr("width", rightWidth);
        }
    },
    
    /** 
     * get render tab data array
     * 
     * @private
     * @instance
     * @memberof com_yung_util_Tab
     * @return  {Array} data array
     */
    getDataArray : function () {
        var dataArray = [];
        for (var i = 0; i < this.titles.length; i++) {
            var data = {};
            data["title"] = this.titles[i];
            data["tabId"] = this.tabId;
            data["tabColor"] = this.color;
            data["length"] = this.titles.length;
            data["idx"] = i + "";
            data["width"] = this.width + "";
            data["span"] = this.span + "";
            dataArray.push(data);
        }
        return dataArray;
    },
    
    /** 
     * show specified tab
     * 
     * @private
     * @instance
     * @memberof com_yung_util_Tab
     * @param  {number} idx - tab index
     */
    showTab : function (idx) {
        for ( var i = 0; i < this.titles.length; i++) {
            if (idx == i) {
                jQuery("#" + this.tabId + "Tab" + i + "Show").css("display", "block");
                jQuery("#" + this.tabId + "Tab" + i + "Show").css("background-color", this.color);
                jQuery("#" + this.tabId + "Tab" + i + "Show").css("color", "white");
                jQuery("#" + this.tabId + "Tab" + i + "Wait").css("display", "none");
                jQuery("#" + this.tabId + "Tab" + i + "Wait").css("background-color", "");
                jQuery("#" + this.tabId + "Tab" + i + "Wait").css("color", this.color);
                jQuery("#" + this.tabId + "TabContent" + i).css("display", "block");
            } else {
                jQuery("#" + this.tabId + "Tab" + i + "Show").css("display", "none");
                jQuery("#" + this.tabId + "Tab" + i + "Show").css("background-color", "");
                jQuery("#" + this.tabId + "Tab" + i + "Show").css("color", this.color);
                jQuery("#" + this.tabId + "Tab" + i + "Wait").css("display", "block");
                jQuery("#" + this.tabId + "Tab" + i + "Wait").css("background-color", "");
                jQuery("#" + this.tabId + "Tab" + i + "Wait").css("color", this.color);
                jQuery("#" + this.tabId + "TabContent" + i).css("display", "none");
            }
        }
        if (typeof this.callback == 'function') {
            this.callback(idx);
        }
    },
    
    /** 
     * get tab source default HTML code
     * 
     * @private
     * @instance
     * @memberof com_yung_util_Tab
     * @return  {string} source default HTML code
     */
    getSourceTabContent : function () {
        var self = this;
        var found = false;
        jQuery("#" + this.divId + " .yungTabContent").each(function(index) {
            var html = jQuery(this).html();
            self.srcContent.add(html);
            found = true;
        });
        if (found == false) {
            // probably nested tab
            jQuery("#" + this.divId + " .yungTabContent_" + self.tabId).each(function(index) {
                var html = jQuery(this).html();
                self.srcContent.add(html);
                found = true;
            });
        }
    },
    
    /** 
     * set tab content
     * 
     * @private
     * @instance
     * @memberof com_yung_util_Tab
     * @param  {number} idx - tab index
     * @param  {string} html - tab content
     */
    setTabContent : function (idx, html) {
        jQuery("#" + this.tabId + "TabContent" + idx).html(html);
    },
    
    /** 
     * set tab title
     * 
     * @private
     * @instance
     * @memberof com_yung_util_Tab
     * @param  {number} idx - tab index
     * @param  {string} title - tab title
     */
    setTabTitle : function (idx, title) {
    	jQuery("#" + this.tabId + "Tab" + idx + "Show").html("<span>" + title + "</span>");
    	jQuery("#" + this.tabId + "Tab" + idx + "Wait").html("<span>" + title + "</span>");
    },
    
    /** 
     * set tab click callback
     * 
     * @instance
     * @memberof com_yung_util_Tab
     * @param  {clickTabCallback} callbackFunct - tab click callback
     */
    setCallback : function (callbackFunct) {
        if (typeof callbackFunct == 'function') {
            this.callback = callbackFunct;
        } else {
            alert('callbackFunct is not function!');
        }
    }
});

com_yung_util_Tab.instance = function (divId, titles, tabId, color, width, span, afterRender, afterRenderTime) {
    return $Class.getInstance("com.yung.util.Tab", divId, titles, tabId, color, width, span, afterRender, afterRenderTime);
}

/**
 * Smart Render Utility
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_SmartRender = $Class.extend({
    
    classProp : {
        name : "com.yung.util.SmartRender"
    },
    
    /**
     * ratio upper bound to trigger append data
     * @member {number}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    ratioUpper : 0.6, // recommend 0.6
    
    /**
     * ratio lower bound to trigger prepend data
     * @member {number}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    ratioLower : 0.3, // recommend 0.3
    
    /**
     * Pre-loaded data size
     * @member {number}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    preloadSize : 60, // recommend 60
    
    /**
     * buffer data size
     * @member {number}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    bufferSize : 20, // recommend 20
    
    /**
     * row number in window div
     * @member {number}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    winDataSize : 0,
    
    /**
     * window div height
     * @member {number}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    winHeight : 600,
    
    /**
     * window div width
     * @member {number}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    winWidth : 800,
    
    /**
     * display bar size
     * @member {number}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    barSize : 30,
    
    /**
     * bar boundary
     * @member {object}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    bound : {},
    
    /**
     * total data size
     * @member {number}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    dataSize : 0,
    
    /**
     * fix scroll top position, for old IE use only
     * 
     * @private
     * @member {number}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    fixScrollTop : 0,
    
    /**
     * way direction, for old IE use only
     * 
     * @private
     * @member {number}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    way : 0,
    
    /**
     * fix count, for old IE use only
     * 
     * @private
     * @member {number}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    fixCnt : 0,
    
    /**
     * suspend flag
     * 
     * @private
     * @member {boolean}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    suspend : false,
    
    /**
     * switch flag to turn off smart render
     * 
     * @member {boolean}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    off : false,
    
    /**
     * top row index
     * 
     * @private
     * @member {number}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    topIdx : 0,
    
    /**
     * bottom row index
     * 
     * @private
     * @member {number}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    bottomIdx : 0,
    
    /**
     * table content height
     * 
     * @private
     * @member {number}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    contentHeight : 0,
    
    /**
     * table content width
     * 
     * @private
     * @member {number}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    contentWidth : 0,
    
    /**
     * scroll record to detect scroll direction
     * 
     * @private
     * @member {com_yung_util_BasicList}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    scrollPosList : null,
    
    /**
     * display window id
     * 
     * @member {string}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    winId : null,
    winDiv : null,
    contentScrollDivId : null,
    
    /**
     * display content id
     * 
     * @member {string}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    contentDivId : null,
    tableId : null,
    tableCls : '',
    rowElementTypeName : 'div',
    
    /**
     * Callback for render row html
     *
     * @callback RowRenderCallback
     * @param {number} rowId - row id
     * @return {string} row HTML code
     */
    /**
     * Callback for render row html
     * @member {RowRenderCallback}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    rowRenderCallback : null,
    
    /**
     * constructor
     * @memberof com_yung_util_SmartRender
     * @param  {string} id - window id
     * @param  {string} contentId - content div id
     * @param  {number} dataSize - total data size
     * @param  {RowRenderCallback} rowRenderCallback - Callback for render row html
     * @param  {string} tableId - content table id [optional]
     * @param  {string} rowElementTypeName - row element type name [optional, default : 'tr']
     */
    init : function(id, contentId, dataSize, rowRenderCallback, tableId, rowElementTypeName) {
        if (id == null || id == '') {
            throw "id is empty";
        }
        if (_validType('string', id) == false) {
            throw "id is not string";
        }
        this.winId = id;
        this.contentScrollDivId = id;
        this.contentDivId = contentId;
        if (_validType('number', dataSize) == false) {
            throw "dataSize is not number";
        }
        this.dataSize = dataSize;
        if (typeof rowRenderCallback != 'function') {
            throw "rowRenderCallback is not function";
        }
        this.rowRenderCallback = rowRenderCallback;
        if (tableId != null && tableId != '') {
            this.tableId = tableId;
        }
        if (rowElementTypeName != null && rowElementTypeName != '') {
            this.rowElementTypeName = rowElementTypeName;
        }
        this.scrollPosList = new com.yung.util.List("number");
        this.winDiv = jQuery('#' + id);
        if (com_yung_util_getbrowser() == 'msie') {
        	this.winHeight = this.winDiv[0].offsetHeight;
        } else {
        	this.winHeight = this.winDiv.height();
        }
        this.winWidth = this.winDiv.width();
        this.creatDisplayBar();
        this.bindScroll();
        this.enableBarDrag();
        this.bound["maxX"] = 0;
        this.bound["minX"] = 0;
        this.bound["maxY"] = this.winHeight - this.barSize;
        this.bound["minY"] = 0;
        return this;
    },
    
    /** 
     * create display bar to cover original bar
     * 
     * @private
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    creatDisplayBar : function () {
        var template = new com.yung.util.BasicTemplate();
        template.add('<div id="SmartRender-{{winId}}-BarContainer" style="position: absolute; left: 0px; top: 0px; width: {{barWidth}}px; height: {{winHeight}}px; z-index: 2000; background: rgb(241, 241, 241);" onclick="com_yung_util_SmartRender.scroll(this, \'' + this.contentScrollDivId + '\', event);" >');
        template.add('    <div id="SmartRender-{{winId}}-BarY" style="position: relative; left: 0px; top: 0px; width: {{barWidth}}px; height: {{barSize}}px; background: rgb(193, 193, 193);"></div>');
        template.add('</div>');
        var data = {};
        data["winId"] = this.winId;
        data["winHeight"] = this.winHeight;
        data["barSize"] = this.barSize;
        data["barWidth"] = com_yung_util_Position.getScrollbarWidth();
        var html = template.toHtml(data);
        jQuery("body").append(html);
        this.render();
    },
    
    /** 
     * render display bar
     * 
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    render : function () {
        if (this.off == true) {
            jQuery('#SmartRender-' + this.winId + '-BarContainer').css("display", "none");
            return;
        }
        var boundPos = new com.yung.util.Position(jQuery("#" + this.contentScrollDivId));
        var tlPos = boundPos.getTopLeftPosition();
        var brPos = boundPos.getBotRightPosition();
        jQuery('#SmartRender-' + this.winId + '-BarContainer').css("top", tlPos.top + "px");
        var diff = 1;
        if (com_yung_util_getbrowser() == 'msie') {
        	diff = diff - 2;
        }
        jQuery('#SmartRender-' + this.winId + '-BarContainer').css("left", (brPos.left - com_yung_util_Position.getScrollbarWidth() + diff) + "px");
        if (com_yung_util_getbrowser() == 'msie') {
        	this.winHeight = jQuery("#" + this.contentScrollDivId)[0].offsetHeight;
        } else {
        	this.winHeight = jQuery("#" + this.contentScrollDivId).height();
        }
        this.winWidth = jQuery("#" + this.contentScrollDivId).width();
        jQuery('#SmartRender-' + this.winId + '-BarContainer').css("display", "");
    },
    
    /** 
     * bind scroll event
     * 
     * @private
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    bindScroll : function () {
        var self = this;
        jQuery("#" + this.contentScrollDivId).on('scroll', function (evt) {
            if (self.off == true) return;
            evt.preventDefault();
            evt.stopPropagation();
            var sTop = jQuery(this).scrollTop();
            self.insertScroll(sTop);
            var direction = self.scrollDirection();
            var rowId = null;
            var diff = null;
            if (direction > 0 || direction < 0) {
                var posInfo = self.getWindowFirstRowId(sTop);
                rowId = posInfo["rowId"];
                diff = posInfo["diff"];
                var adjTop = rowId / (self.dataSize - self.winDataSize) * (self.winHeight - self.barSize);
                jQuery('#SmartRender-' + self.winId + '-BarY').css("top", adjTop + "px");
            }
            if (self.suspend == true) {
                return;
            }
            var ratio = sTop / self.contentHeight;
            // fix old IE scroll issue
            if (com_yung_util_getbrowser() == 'msie') {
                if (self.fixScrollTop > 0) {
                	if (self.fixCnt > 1) {
                        self.fixCnt = 0;
                        self.fixScrollTop = 0;
                        self.way = 0;
                    } else {
                        if (self.way > 0 && ratio < self.ratioUpper) {
                            self.fixCnt++;
                        } else if (self.way < 0 && ratio > self.ratioLower) {
                            self.fixCnt++;
                        } else {
                            self.fixCnt = 0;
                            jQuery(this).scrollTop(self.fixScrollTop);
                        }
                        return;
                    }
                }
            }
            if (direction > 0 && ratio > self.ratioUpper && self.bottomIdx < self.dataSize) {
                var newTop = self.loadNext(rowId);
                jQuery(this).scrollTop(newTop);
                self.fixScrollTop = newTop;
                self.way = 1;
            }
            if (direction < 0 && ratio < self.ratioLower && self.topIdx > 0) {
                var newTop = self.loadPrev(rowId);
                jQuery(this).scrollTop(newTop);
                self.fixScrollTop = newTop;
                self.way = -1;
            }
        });
    },
    
    /** 
     * enable display bar draggable
     * 
     * @private
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    enableBarDrag : function () {
        var self = this;
        com_yung_util_Movable.enable('SmartRender-' + this.winId + '-BarY', true, false, this.bound, function (startX, startY, endX, endY) {
            if (self.off == true) return;
            var posTop = jQuery("#SmartRender-" + self.winId + "-BarY").css("top");
            posTop = _replaceAll(posTop, "px", "");
            posTop = posTop * 1.0;
            var rowId = parseInt(self.dataSize * (posTop - self.bound["minY"]) / (self.bound["maxY"] - self.bound["minY"]));
            if (rowId > self.dataSize) {
                rowId = self.dataSize;
            }
            var span = self.getSpan(rowId);
            self.topIdx = span[0]["start"];
            self.bottomIdx = span[span.length - 1]["end"];
            var html = self.toContentHtml(span);
            jQuery("#" + self.contentDivId).html(html);
            if (com_yung_util_getbrowser() == 'msie') {
            	self.contentHeight = jQuery('#' + self.contentDivId)[0].offsetHeight;
            } else {
            	self.contentHeight = jQuery('#' + self.contentDivId).height();
            }
            if (rowId == self.dataSize) {
                rowId = rowId - 1;
            }
            var position = new com.yung.util.Position(jQuery("#data-" + rowId));
            var pos = position.getTopLeftPosition(null, null, jQuery('#' + self.winId));
            jQuery('#' + self.winId).scrollTop(pos.top);
            if (com_yung_util_getbrowser() == 'msie') {
                self.fixCnt = 0;
                self.fixScrollTop = 0;
                self.way = 0;
            }
        });
    },
    getSpan : function (rowId) {
        var maxIdx = this.dataSize;
        var ret = [];
        var length = parseInt(this.preloadSize / this.bufferSize);
        var midStart = parseInt(rowId / this.bufferSize) * this.bufferSize;
        var midEnd = (parseInt(rowId / this.bufferSize) + 1) * this.bufferSize;
        if (midEnd >= maxIdx) {
            midEnd = maxIdx;
        }
        if (length % 2 == 0) {
            var expand = length / 2;
            for (var i = (expand - 1); i > 0; i--) {
                var obj = {};
                var start = midStart - (i * this.bufferSize);
                if (start > maxIdx) break;
                if (start < 0) break;
                var end = midStart - ((i - 1) * this.bufferSize);
                obj["start"] = start;
                obj["end"] = end;
                if (obj["end"] >= maxIdx) {
                    obj["end"] = maxIdx;
                }
                ret.push(obj);
            }
            var temp = {};
            temp["start"] = midStart;
            temp["end"] = midEnd;
            ret.push(temp);
            for (var i = 0; i < expand; i++) {
                var obj = {};
                var start = midStart + ((i + 1) * this.bufferSize);
                if (start > maxIdx) break;
                if (start < 0) break;
                var end = midStart + ((i + 2) * this.bufferSize);
                obj["start"] = start;
                obj["end"] = end;
                if (obj["end"] > maxIdx) {
                    obj["end"] = maxIdx;
                }
                ret.push(obj);
            }
        } else {
            var expand = parseInt(length / 2);
            for (var i = expand; i > 0; i--) {
                var obj = {};
                var start = midStart - (i * this.bufferSize);
                if (start > maxIdx) break;
                if (start < 0) break;
                var end = midStart - ((i - 1) * this.bufferSize);
                obj["start"] = start;
                obj["end"] = end;
                if (obj["end"] > maxIdx) {
                    obj["end"] = maxIdx;
                }
                ret.push(obj);
            }
            var temp = {};
            temp["start"] = midStart;
            temp["end"] = midEnd;
            ret.push(temp);
            for (var i = 0; i < expand; i++) {
                var obj = {};
                var start = midStart + ((i + 1) * this.bufferSize);
                if (start > maxIdx) break;
                if (start < 0) break;
                var end = midStart + ((i + 2) * this.bufferSize);
                obj["start"] = start;
                obj["end"] = end;
                if (obj["end"] > maxIdx) {
                    obj["end"] = maxIdx;
                }
                ret.push(obj);
            }
        }
        return this.makeup(length, maxIdx, ret);
    },
    makeup : function (length, maxIdx, array) {
        if (array.length < length) {
            var plusSize = length - array.length;
            if (array[0]["start"] == 0) {
                var nextIdx = array[array.length - 1]["end"];
                for (var i = 0; i < plusSize; i++) {
                    var obj = {};
                    var start = nextIdx + i * this.bufferSize;
                    if (start > maxIdx) break;
                    if (start < 0) break;
                    var end = start + this.bufferSize;
                    obj["start"] = start;
                    obj["end"] = end;
                    if (obj["end"] > maxIdx) {
                        obj["end"] = maxIdx;
                    }
                    array.push(obj);
                }
            } else if (array[array.length - 1]["end"] == maxIdx) {
                var nextIdx = array[0]["start"];
                for (var i = 0; i < plusSize; i++) {
                    var obj = {};
                    var end = nextIdx - (i * this.bufferSize);
                    var start = end - this.bufferSize;
                    if (start > maxIdx) break;
                    if (start < 0) break;
                    obj["start"] = start;
                    obj["end"] = end;
                    if (obj["end"] > maxIdx) {
                        obj["end"] = maxIdx;
                    }
                    array.splice(0, 0, obj);
                }
            }
        }
        return array;
    },
    
    /** 
     * insert scroll position
     * 
     * @private
     * @instance
     * @memberof com_yung_util_SmartRender
     * @param {number} sTop - scroll top position
     */
    insertScroll : function (sTop) {
        if (this.scrollPosList.size() < 3) {
            this.scrollPosList.add(sTop);
            return;
        } else {
            this.scrollPosList.removeByIndex(0);
            this.scrollPosList.add(sTop);
        }
    },
    
    /** 
     * return scroll direction
     * 
     * @private
     * @instance
     * @memberof com_yung_util_SmartRender
     * @return {number} scroll direction, up : -1, down : 1, unknown : 0
     */
    scrollDirection : function () {
        if (this.scrollPosList.size() == 3) {
            if (this.scrollPosList.get(2) > this.scrollPosList.get(1) && this.scrollPosList.get(1) > this.scrollPosList.get(0)) {
                return 1;
            } else if (this.scrollPosList.get(2) < this.scrollPosList.get(1) && this.scrollPosList.get(1) < this.scrollPosList.get(0)) {
                return -1;
            }
        }
        return 0;
    },
    
    /** 
     * return window div first row id
     * 
     * @private
     * @instance
     * @memberof com_yung_util_SmartRender
     * @return {number} row id
     */
    getWindowFirstRowId : function (sTop) {
        var ret = {};
        var height = 0;
        var found = false;
        var selector = "";
        if (this.tableId != null) {
            selector = selector + this.tableId + " tbody";
        } else {
            selector = selector + this.contentDivId;
        }
        jQuery("#" + selector).children().each(function( index ) {
            if (found == true) return;
            var rowHeight = 0;
            if (com_yung_util_getbrowser() == 'msie') {
            	rowHeight = this.offsetHeight;
            } else {
            	rowHeight = jQuery(this).height();
            }
            if (sTop < rowHeight) {
                ret["rowId"] = 0;
                ret["diff"] = sTop;
                found = true;
                return;
            }
            if ((height + rowHeight) > sTop) {
                ret["rowId"] = jQuery(this).attr("data-rowno");
                ret["diff"] = height + rowHeight - sTop;
                found = true;
                return;
            }
            height = height + rowHeight;
        });
        if (ret["rowId"] > (this.dataSize - this.winDataSize)) {
            ret["rowId"] = (this.dataSize - this.winDataSize);
        }
        return ret;
    },
    
    /** 
     * load next buffer
     * 
     * @private
     * @instance
     * @memberof com_yung_util_SmartRender
     * @return {number} new scroll top position
     */
    loadNext : function (rowId) {
        if (this.suspend == true) {
            return;
        }
        this.suspend = true;
        var start = this.bottomIdx;
        this.bottomIdx = this.bottomIdx + this.bufferSize;
        if (this.bottomIdx > this.dataSize) {
            this.bottomIdx = this.dataSize;
        }
        var end = this.bottomIdx;
        var html = this.getHtml(start, end);
        var contentId = this.contentDivId;
        if (this.tableId != null) {
            contentId = this.tableId;
        }
        jQuery('#' + contentId).append(html);
        this.removeTop();
        if (com_yung_util_getbrowser() == 'msie') {
        	this.contentHeight = jQuery('#' + this.contentDivId)[0].offsetHeight;
        } else {
        	this.contentHeight = jQuery('#' + this.contentDivId).height();
        }
        this.topIdx = this.topIdx + this.bufferSize;
        this.scrollPosList.clear();
        var position = new com.yung.util.Position(jQuery("#data-" + rowId));
        var pos = position.getBotLeftPosition(null, null, jQuery('#' + this.contentScrollDivId));
        this.suspend = false;
        return pos.top;
    },
    removeTop : function () {
        var cnt = 0;
        var selector = "";
        if (this.tableId != null) {
            selector = selector + this.tableId + " tbody";
        } else {
            selector = selector + this.contentDivId;
        }
        var self = this;
        jQuery("#" + selector).children().each(function( index ) {
            if (cnt >= self.bufferSize) return;
            jQuery(this).remove();
            cnt++;
        });
    },
    
    /** 
     * load previous buffer
     * 
     * @private
     * @instance
     * @memberof com_yung_util_SmartRender
     * @return {number} new scroll top position
     */
    loadPrev : function (rowId) {
        if (this.suspend == true) {
            return;
        }
        this.suspend = true;
        var start = this.topIdx - this.bufferSize;
        if (start < 0) {
            start = 0;
        }
        var end = this.topIdx;
        this.topIdx = this.topIdx - this.bufferSize;
        if (this.topIdx < 0) {
            this.topIdx = 0;
        }
        var html = this.getHtml(start, end);
        var contentId = this.contentDivId;
        if (this.tableId != null) {
            contentId = this.tableId;
        }
        jQuery('#' + contentId).prepend(html);
        this.removeBottom();
        if (com_yung_util_getbrowser() == 'msie') {
        	this.contentHeight = jQuery('#' + this.contentDivId)[0].offsetHeight;
        } else {
        	this.contentHeight = jQuery('#' + this.contentDivId).height();
        }
        if (this.bottomIdx % this.bufferSize == 0) {
            this.bottomIdx = this.bottomIdx - this.bufferSize;
        } else {
            this.bottomIdx = parseInt(this.bottomIdx / this.bufferSize) * this.bufferSize;
        }
        this.scrollPosList.clear();
        var position = new com.yung.util.Position(jQuery("#data-" + rowId));
        var pos = position.getBotLeftPosition(null, null, jQuery('#' + this.contentScrollDivId));
        this.suspend = false;
        return pos.top;
    },
    removeBottom : function () {
        var selector = "";
        if (this.tableId != null) {
            selector = selector + this.tableId + " tbody";
        } else {
            selector = selector + this.contentDivId;
        }
        var length = jQuery("#" + selector).children().length;
        var maxIdx = length - this.bufferSize;
        if (length % this.bufferSize != 0) {
            maxIdx = parseInt(length / this.bufferSize) * this.bufferSize;
        }
        jQuery("#" + selector).children().each(function( index ) {
            if (index >= maxIdx) {
                jQuery(this).remove();
            }
        });
    },
    
    /** 
     * get HTML code from start and end row id
     * 
     * @private
     * @instance
     * @memberof com_yung_util_SmartRender
     * @param {number} start - start row id
     * @param {number} end - end row id
     * @return {string} html code
     */
    getHtml : function (start, end) {
        var span = this.split(start, end);
        return this.toRowHtml(span);
    },
    
    /** 
     * get HTML code from span array
     * 
     * @private
     * @instance
     * @memberof com_yung_util_SmartRender
     * @param {Array} span - span array
     * @return {string} html code
     */
    toRowHtml : function (span) {
        var html = "";
        for (var i = 0; i < span.length; i++) {
            var begin = span[i]["start"];
            var finish = span[i]["end"];
            for (var j = begin; j < finish; j++) {
                html = html + this.getRowHtml(j);
            }
        }
        return html;
    },
    
    /** 
     * split start row id and end row id to array
     * 
     * @private
     * @instance
     * @memberof com_yung_util_SmartRender
     * @param {number} start - start row id
     * @param {number} end - end row id
     * @return {Array} span array
     */
    split : function (start, end) {
        var length = (end - start) / this.bufferSize;
        var mod = (end - start) % this.bufferSize;
        if (mod != 0) {
            length = length + 1;
        }
        var ret = [];
        for (var i = 0; i < length; i++) {
            var obj = {};
            obj["start"] = i * this.bufferSize + start;
            obj["end"] = (i + 1) * this.bufferSize + start;
            if (obj["end"] > end) {
                obj["end"] = end;
            }
            ret.push(obj);
        }
        return ret;
    },
    
    /** 
     * convert span array to HTML code
     * 
     * @private
     * @instance
     * @memberof com_yung_util_SmartRender
     * @param {Array} span - span array
     * @return {string} HTML code
     */
    toContentHtml : function (span) {
        var html = this.toRowHtml(span);
        if (this.tableId != null) {
            html = "<table id='" + this.tableId + "' class='" + this.tableCls + "'>" + html + "</table>";
        }
        return html;
    },
    
    /** 
     * preload content html
     * 
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    preload : function () {
        if (this.off == true) return;
        this.bottomIdx = this.preloadSize;
        var span = this.split(this.topIdx, this.bottomIdx);
        var html = this.toContentHtml(span);
        jQuery("#" + this.contentDivId).html(html);
        if (com_yung_util_getbrowser() == 'msie') {
        	this.contentHeight = jQuery('#' + this.contentDivId)[0].offsetHeight;
        } else {
        	this.contentHeight = jQuery('#' + this.contentDivId).height();
        }
        var found = false;
        var height = 0;
        var selector = "";
        if (this.tableId != null) {
            selector = selector + this.tableId + " tbody";
        } else {
            selector = selector + this.contentDivId;
        }
        var self = this;
        jQuery("#" + selector).children().each(function( index ) {
            if (found == true) return;
            var rowHeight = 0;
            if (com_yung_util_getbrowser() == 'msie') {
            	rowHeight = this.offsetHeight;
            } else {
            	rowHeight = jQuery(this).height();
            }
            if ((height + rowHeight) > self.winHeight) {
                self.winDataSize = index + 1;
                found = true;
                return;
            }
            height = height + rowHeight;
        });
        this.adjustBuffer();
    },
    
    /** 
     * adjust buffer and preload size to keep better performance
     * 
     * @private
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    adjustBuffer : function () {
        if ((this.bufferSize / this.winDataSize) < 1.5) {
            // adjust buffer
            while ((this.bufferSize / this.winDataSize) < 1.5) {
                this.bufferSize = this.bufferSize + 10;
            }
            this.preloadSize = this.bufferSize * 3;
            this.preload();
        }
    },
    
    /** 
     * get row HTML code by row id
     * Note: override method or use callback
     * 
     * @instance
     * @memberof com_yung_util_SmartRender
     * @param {number} rowId - row id
     * @return {string} HTML code
     */
    getRowHtml : function (rowId) {
        return this.rowRenderCallback(rowId);
    }
    
});

com_yung_util_SmartRender.scroll = function (container, contentScrollDivId, event) {
    event.preventDefault();
    event.stopPropagation();
    var barY = jQuery(container).children();
    var barSize = barY.height();
    var position = new com.yung.util.Position(barY);
    var baseTop = position.getBaseTop();
    var clickY = baseTop + event.clientY;
    var pos = position.getTopLeftPosition();
    if (clickY < pos.top) {
        var sTop = jQuery("#" + contentScrollDivId).scrollTop();
        jQuery("#" + contentScrollDivId).scrollTop(sTop - barSize);
    } else if (clickY > (pos.top + barSize)) {
        var sTop = jQuery("#" + contentScrollDivId).scrollTop();
        jQuery("#" + contentScrollDivId).scrollTop(sTop + barSize);
    }
}

/**
 * Table Utility Schema Data Object
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_TableGridSchema = $Class.extend({
    
    classProp : { 
        name : "com.yung.util.TableGridSchema",
        typeArray : ['edtxt','ed','edn','co','select','ch', 'date']
    },
    
    /**
     * td align
     * @member {string}
     * @instance
     * @memberof com_yung_util_TableGridSchema
     */
    align : "center",
    
    /**
     * td vertical align
     * @member {string}
     * @instance
     * @memberof com_yung_util_TableGridSchema
     */
    vAlign : "",
    
    /**
     * allow null value
     * @member {boolean}
     * @instance
     * @memberof com_yung_util_TableGridSchema
     */
    allowNull : true,
    
    /**
     * column color code
     * @member {string}
     * @instance
     * @memberof com_yung_util_TableGridSchema
     */
    columnColor : "",
    
    /**
     * column hidden
     * @member {boolean}
     * @instance
     * @memberof com_yung_util_TableGridSchema
     */
    columnHidden : false,
    
    /**
     * column id
     * @member {string}
     * @instance
     * @memberof com_yung_util_TableGridSchema
     */
    columnId : "",
    
    /**
     * column validator, a drop list to select
     * @member {Array}
     * @instance
     * @memberof com_yung_util_TableGridSchema
     */
    columnValidator : [],
    
    /**
     * column combo map, a drop key/value to select
     * @member {object}
     * @instance
     * @memberof com_yung_util_TableGridSchema
     */
    comboMap : {},
    
    /**
     * date format, ex. MM/dd/yyyy
     * @member {string}
     * @instance
     * @memberof com_yung_util_TableGridSchema
     */
    dateFormat : "MM/dd/yyyy",
    
    /**
     * column header style
     * @member {string}
     * @instance
     * @memberof com_yung_util_TableGridSchema
     */
    headStyle : "",
    
    /**
     * column header text
     * @member {string}
     * @instance
     * @memberof com_yung_util_TableGridSchema
     */
    header : "",
    
    /**
     * column index
     * @member {number}
     * @instance
     * @memberof com_yung_util_TableGridSchema
     */
    index : 0,
    
    /**
     * column type
     * @member {string}
     * @instance
     * @memberof com_yung_util_TableGridSchema
     */
    type : "ro",
    
    /**
     * column width
     * @member {string}
     * @instance
     * @memberof com_yung_util_TableGridSchema
     */
    width : "80",
    
    /** 
     * constructor
     * 
     * @instance
     * @memberof com_yung_util_TableGridSchema
     * @param {string} columnId - column id
     * @param {string} width - column width
     * @param {string} header - column header text
     */
    init: function(columnId, width, header){
        if (_validType('string', columnId) == false) {
            throw "columnId is not string";
        }
        this.columnId = columnId;
        if (width != null) {
            this.setWidth(width);
        }
        if (header != null) {
            this.setHeader(header);
        }
        return this;
    },
    
    /** 
     * set property value
     * 
     * @instance
     * @memberof com_yung_util_TableGridSchema
     * @param {string} attribute - column id
     * @param {all} value - value
     */
    setProperty : function (attribute, value) {
        if (_validType('string', attribute) == false) {
            throw "attribute is not string";
        }
        var ch = attribute.charAt(0) + "";
        var method = "set" + ch.toUpperCase() + attribute.substring(1);
        if (typeof this[method] == 'function') {
            this[method](value);
        }
    },
    
    /** 
     * get property value
     * 
     * @instance
     * @memberof com_yung_util_TableGridSchema
     * @param {string} attribute - column id
     * @return {all} value
     */
    getProperty : function (attribute) {
        if (_validType('string', attribute) == false) {
            throw "attribute is not string";
        }
        var ch = attribute.charAt(0) + "";
        var method = "get" + ch.toUpperCase() + attribute.substring(1);
        if (typeof this[method] == 'function') {
            return this[method]();
        }
        return null;
    },
    
    setAlign : function (align) {
        if (_validType('string', align) == false) {
            throw "align is not string";
        }
        if (align != 'left' && align != 'center' && align != 'right') {
            throw "unknown vAlign: " + vAlign;
        }
        this.align = align;
    },

    getAlign : function () {
        return this.align;
    },

    setVAlign : function (vAlign) {
        if (_validType('string', vAlign) == false) {
            throw "vAlign is not string";
        }
        if (vAlign != 'top' && vAlign != 'middle' && vAlign != 'bottom') {
            throw "unknown vAlign: " + vAlign;
        }
        this.vAlign = vAlign;
    },

    getVAlign : function () {
        return this.vAlign;
    },

    setAllowNull : function (allowNull) {
        if (allowNull == true) {
            this.allowNull = true;
        } else {
            this.allowNull = false;
        }
    },

    getAllowNull : function () {
        return this.allowNull;
    },

    setColumnColor : function (columnColor) {
        if (_validType('string', columnColor) == false) {
            throw "columnColor is not string";
        }
        this.columnColor = columnColor;
    },

    getColumnColor : function () {
        return this.columnColor;
    },

    setColumnHidden : function (columnHidden) {
        if (columnHidden == true) {
            this.columnHidden = true;
        } else {
            this.columnHidden = false;
        }
    },

    getColumnHidden : function () {
        return this.columnHidden;
    },

    setColumnId : function (columnId) {
        if (_validType('string', columnId) == false) {
            throw "columnId is not string";
        }
        this.columnId = columnId;
    },

    getColumnId : function () {
        return this.columnId;
    },

    setColumnValidator : function (columnValidator) {
        if (_validType(Array, columnValidator) == false) {
            throw "columnValidator is not Array";
        }
        this.columnValidator = columnValidator;
    },

    getColumnValidator : function () {
        return this.columnValidator;
    },

    setComboMap : function (comboMap) {
        if (_validType('object', comboMap) == false) {
            throw "comboMap is not object";
        }
        this.comboMap = comboMap;
    },

    getComboMap : function () {
        return this.comboMap;
    },

    setDateFormat : function (dateFormat) {
        if (_validType('string', dateFormat) == false) {
            throw "dateFormat is not string";
        }
        this.dateFormat = dateFormat;
    },

    getDateFormat : function () {
        return this.dateFormat;
    },

    setHeadStyle : function (headStyle) {
        if (_validType('string', headStyle) == false) {
            throw "headStyle is not string";
        }
        this.headStyle = headStyle;
    },

    getHeadStyle : function () {
        return this.headStyle;
    },

    setHeader : function (header) {
        if (_validType('string', header) == false) {
            throw "header is not string";
        }
        this.header = header;
    },

    getHeader : function () {
        return this.header;
    },

    setIndex : function (index) {
        if (_validType('number', index) == false) {
            throw "index is not number";
        }
        index = parseInt(index);
        this.index = index;
    },

    getIndex : function () {
        return this.index;
    },

    setType : function (type) {
        if (jQuery.inArray(type, this.classProp.typeArray) < 0) {
            throw "unknown type: " + type;
        }
        this.type = type;
    },

    getType : function () {
        return this.type;
    },

    setWidth : function (width) {
        if (jQuery.isNumeric(width) == false) {
            throw "width is not number";
        }
        this.width = width;
    },

    getWidth : function () {
        return this.width;
    }
    
});
com_yung_util_TableGridSchema.convertToLZString = function (SchemaArray) {
    if (_validType(Array, SchemaArray) == false) {
        throw "SchemaArray is not Array";
    }
    if (SchemaArray.length > 0) {
        if (_validType(com_yung_util_TableGridSchema, SchemaArray[0]) == false) {
            throw "SchemaArray element is not com.yung.util.TableGridSchema";
        }
    }
    // reset index and check columnId
    var id = [];
    for (var i = 0; i < SchemaArray.length; i++) {
        if (jQuery.inArray(SchemaArray[i].columnId, id) < 0) {
            id.push(SchemaArray[i].columnId);
        } else {
            throw "columnId: " + SchemaArray[i].columnId + " duplicate";
        }
        SchemaArray[i].setIndex(i);
    }
    var columnData = {};
    columnData["columns"] = SchemaArray;
    var json = JSON.stringify(columnData);
    return com.yung.util.LZString.encode(json);
}

/**
 * Column Definition of Table Utility 
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_ColumnUtil = $Class.extend({
    
    classProp : { name : "com.yung.util.ColumnUtil" },
    
    /**
     * column schema array
     * @member {Array}
     * @instance
     * @memberof com_yung_util_ColumnUtil
     */
    columnArray: null,
    columnMap : null,
    
    /** 
     * constructor
     * 
     * @instance
     * @memberof com_yung_util_ColumnUtil
     * @param {string} lzColumnInfo - column information encoded to LZString 
     */
    init: function(lzColumnInfo){
        var json = com.yung.util.LZString.decode(lzColumnInfo);
        var columnInfo = JSON.parse(json);
        this.columnMap = new Object();
        this.columnArray = columnInfo.columns;
        for (var i = 0; i < this.columnArray.length; i++) {
            this.columnMap['' + this.columnArray[i].columnId] = this.columnArray[i];
        }
        return this;
    },
    
    /** 
     * get column array
     * 
     * @instance
     * @memberof com_yung_util_ColumnUtil
     * @return {Array} column array
     */
    getColumnArray : function() {
        return this.columnArray;
    },
    
    /** 
     * get column schema
     * 
     * @instance
     * @memberof com_yung_util_ColumnUtil
     * @param {string} columnId - column id
     * @return {object} column schema object
     */
    getColumn : function (columnId){
        return this.columnMap[columnId];
    },
    
    /** 
     * get column index
     * 
     * @instance
     * @memberof com_yung_util_ColumnUtil
     * @param {string} columnId - column id
     * @return {number} column index
     */
    getColumnIndex : function (columnId){
        var column = this.columnMap[columnId];
        if (column != null) {
            return column.index;
        } else {
            return null;
        }
    },
    
    /** 
     * get column attribute value
     * 
     * @instance
     * @memberof com_yung_util_ColumnUtil
     * @param {string} columnId - column id
     * @param {string} attribute - column attribute
     * @return {all} column attribute value
     */
    getColumnAttribute : function (columnId, attribute){
        var column = this.getColumn(columnId);
        if (column == null) {
            return null;
        }
        return column[attribute];
    },
    
    /** 
     * get column attribute value string array
     * 
     * @instance
     * @memberof com_yung_util_ColumnUtil
     * @param {string} attribute - column attribute
     * @return {string} array of column attribute value separate by ','
     */
    getColumnAttrArrayString : function (attribute){
        var result = '';
        for (var i = 0; i < this.columnArray.length; i++) {
            var column = this.columnArray[i];
            result = result + column[attribute];
            if (i != (this.columnArray.length - 1)) {
                result = result + ',';
            }
        }
        return result;
    },
    
    /** 
     * get column attribute array
     * 
     * @instance
     * @memberof com_yung_util_ColumnUtil
     * @param {string} attribute - column attribute
     * @return {Array} array of column attribute value
     */
    getColumnAttrArray : function (attribute){
        var result = [];
        for (var i = 0; i < this.columnArray.length; i++) {
            var column = this.columnArray[i];
            result.push(column[attribute]);
        }
        return result;
    }
});

// specific smart render for table utility
var com_yung_util_GridSmartRender = com_yung_util_SmartRender.extend({
    
    classProp : {
        name : "com.yung.util.GridSmartRender"
    },
    
    grid : null,
    rowElementTypeName : 'tr',
    off : true,
    
    init : function(grid) {
        this.grid = grid;
        this.winId = grid.gridDivId;
        this.contentScrollDivId = grid.gridDivId + "-contentScrollDiv";
        this.contentDivId = grid.gridDivId + "-contentDiv";
        this.tableId = grid.gridDivId + "-contentTable";
        this.scrollPosList = new com.yung.util.List("number");
        this.winDiv = jQuery('#' + this.winId);
        if (com_yung_util_getbrowser() == 'msie') {
        	this.winHeight = jQuery('#' + this.contentScrollDivId)[0].offsetHeight;	
        } else {
        	this.winHeight = jQuery('#' + this.contentScrollDivId).height();
        }
        this.winWidth = jQuery('#' + this.contentScrollDivId).width();
        this.creatDisplayBar();
        this.bindScroll();
        this.enableBarDrag();
        this.bound["maxX"] = 0;
        this.bound["minX"] = 0;
        this.bound["maxY"] = this.winHeight - this.barSize;
        this.bound["minY"] = 0;
        if (com_yung_util_getbrowser() == 'msie') {
        	this.preloadSize = 40;
        	this.bufferSize = 10;
        }
        return this;
    },
    
    enableBarDrag : function () {
        var self = this;
        com_yung_util_Movable.enable('SmartRender-' + this.winId + '-BarY', true, false, this.bound, function (startX, startY, endX, endY) {
            if (self.off == true) return;
            var posTop = jQuery("#SmartRender-" + self.winId + "-BarY").css("top");
            posTop = _replaceAll(posTop, "px", "");
            posTop = posTop * 1.0;
            var rowId = parseInt(self.dataSize * (posTop - self.bound["minY"]) / (self.bound["maxY"] - self.bound["minY"]));
            if (rowId > self.dataSize) {
                rowId = self.dataSize;
            }
            var span = self.getSpan(rowId);
            self.topIdx = span[0]["start"];
            self.bottomIdx = span[span.length - 1]["end"];
            var html = self.toContentHtml(span);
            jQuery("#" + self.contentDivId).html(html);
            // grid render
            self.grid.render();
            if (rowId == self.dataSize) {
                rowId = rowId - 1;
            }
            var position = new com.yung.util.Position(jQuery("#data-" + rowId));
            var pos = position.getTopLeftPosition(null, null, jQuery('#' + self.contentScrollDivId));
            var headerHeight = 0;
            if (com_yung_util_getbrowser() == 'msie') {
            	headerHeight = jQuery('#' + self.tableId + " tr:first")[0].offsetHeight;
            } else {
            	headerHeight = jQuery('#' + self.tableId + " tr:first").height();
            }
            jQuery('#' + self.contentScrollDivId).scrollTop(pos.top - headerHeight);
            if (com_yung_util_getbrowser() == 'msie') {
            	self.contentHeight = jQuery('#' + self.contentDivId)[0].offsetHeight;
            } else {
            	self.contentHeight = jQuery('#' + self.contentDivId).height();
            }
            if (com_yung_util_getbrowser() == 'msie') {
                self.fixCnt = 0;
                self.fixScrollTop = 0;
                self.way = 0;
            }
        });
    },
    
    getRowHtml : function (rowId) {
        var rowIdx = this.grid.rowOrderList.get(rowId);
        var rowData = this.grid.cacheRowMap.get(rowIdx);
        var columnArray = this.grid.columnUtil.getColumnArray();
        var html = "";
        html = html + "<tr id='data-" + rowId + "' data-rowNo='" + rowId + "' class='" + this.grid.rowCls + "' >";
        for (var c = 0; c < columnArray.length; c++) {
            var column = columnArray[c];
            var content = rowData[column.columnId];
            if (content == null) {
                content = '';
            }
            var comment = this.grid.getComment(rowIdx, column.columnId);
            if (comment != null && comment != '') {
                content = "<img id='" + this.grid.gridDivId + "-comment-" + rowIdx + "-" + c + "' src='" + this.grid.classProp.commentBase64 + "' width='12' style='position: absolute; top: 0px; left: 0px;' title='" + comment + "' />" + content;
            }
            var clickFunt = '';
            if ('edtxt' == column.type || 'ed' == column.type || 'edn' == column.type) {
                clickFunt = "fireInstanceMethod('" + this.grid.hashId + "', 'edit', '" + this.grid.gridDivId + "-" + rowIdx + "-" + c + "');";
            } else if ('co' == column.type || 'select' == column.type) {
                clickFunt = "fireInstanceMethod('" + this.grid.hashId + "', 'edit', '" + this.grid.gridDivId + "-" + rowIdx + "-" + c + "');";
            } else if ('ch' == column.type) {
                clickFunt = "fireInstanceMethod('" + this.grid.hashId + "', 'edit', '" + this.grid.gridDivId + "-" + rowIdx + "-" + c + "');";
                if (content == '1' || content == 1) {
                    content = '<input type="checkbox" checked />';
                } else {
                    content = '<input type="checkbox" />'
                }
            } else if ('date' == column.type) {
                clickFunt = "fireInstanceMethod('" + this.grid.hashId + "', 'edit', '" + this.grid.gridDivId + "-" + rowIdx + "-" + c + "');";
            } 
            html = html + '<td id="' + this.grid.gridDivId + '-' + rowIdx + '-' + c + '" data-columnId="' + column.columnId + '" class="' + this.grid.cellCls + '" width="' + column.width + '" align="' + column.align + '" style="position: relative; background-clip: padding-box;" onclick="' + clickFunt + '" >' + content + '</td>';
        }
        html = html + "</tr>";
        return html;
    },
    
    preload : function () {
        if (this.off == true) return;
        this.bottomIdx = this.preloadSize;
        var span = this.split(this.topIdx, this.bottomIdx);
        var html = this.toContentHtml(span);
        jQuery("#" + this.contentDivId).html(html);
        // grid render
        this.grid.render();
        var found = false;
        var height = 0;
        var selector = this.tableId;
        selector = selector + " " + this.rowElementTypeName;
        var self = this;
        jQuery("#" + selector).each(function( index ) {
            if (found == true) return;
            var rowHeight = 0;
            if (com_yung_util_getbrowser() == 'msie') {
            	rowHeight = jQuery(this)[0].offsetHeight;
            } else {
            	rowHeight = jQuery(this).height();
            }
            if ((height + rowHeight) > self.winHeight) {
                self.winDataSize = index + 1;
                found = true;
                return;
            }
            height = height + rowHeight;
        });
        if (com_yung_util_getbrowser() == 'msie') {
        	this.contentHeight = jQuery('#' + this.contentDivId)[0].offsetHeight;
        } else {
        	this.contentHeight = jQuery('#' + this.contentDivId).height();
        }
        this.render();
        this.adjustBuffer();
    },
    
    loadNext : function (rowId) {
        if (this.suspend == true) {
            return;
        }
        this.suspend = true;
        var start = this.bottomIdx;
        this.bottomIdx = this.bottomIdx + this.bufferSize;
        if (this.bottomIdx > this.dataSize) {
            this.bottomIdx = this.dataSize;
        }
        var end = this.bottomIdx;
        var html = this.getHtml(start, end);
        jQuery('#' + this.tableId).append(html);
        this.removeTop();
        this.topIdx = this.topIdx + this.bufferSize;
        this.scrollPosList.clear();
        // grid render
        this.grid.render();
        var position = new com.yung.util.Position(jQuery("#data-" + rowId));
        var pos = position.getBotLeftPosition(null, null, jQuery('#' + this.contentScrollDivId));
        if (com_yung_util_getbrowser() == 'msie') {
        	this.contentHeight = jQuery('#' + this.contentDivId)[0].offsetHeight;
        } else {
        	this.contentHeight = jQuery('#' + this.contentDivId).height();
        }
        this.suspend = false;
        return pos.top;
    },
    
    loadPrev : function (rowId) {
        if (this.suspend == true) {
            return;
        }
        this.suspend = true;
        var start = this.topIdx - this.bufferSize;
        if (start < 0) {
            start = 0;
        }
        var end = this.topIdx;
        this.topIdx = this.topIdx - this.bufferSize;
        if (this.topIdx < 0) {
            this.topIdx = 0;
        }
        var html = this.getHtml(start, end);
        // remove header first
        jQuery('#' + this.tableId + " tr:first").remove();
        // add rows
        jQuery('#' + this.tableId).prepend(html);
        // prepend header 
        var headerHtml = this.grid.getHeaderHtml();
        jQuery('#' + this.tableId).prepend(headerHtml);
        this.removeBottom();
        if (this.bottomIdx % this.bufferSize == 0) {
            this.bottomIdx = this.bottomIdx - this.bufferSize;
        } else {
            this.bottomIdx = parseInt(this.bottomIdx / this.bufferSize) * this.bufferSize;
        }
        this.scrollPosList.clear();
        // grid render
        this.grid.render();
        var position = new com.yung.util.Position(jQuery("#data-" + rowId));
        var pos = position.getBotLeftPosition(null, null, jQuery('#' + this.contentScrollDivId));
        if (com_yung_util_getbrowser() == 'msie') {
        	this.contentHeight = jQuery('#' + this.contentDivId)[0].offsetHeight;
        } else {
        	this.contentHeight = jQuery('#' + this.contentDivId).height();
        }
        this.suspend = false;
        return pos.top;
    },
    
    toContentHtml : function (span) {
        var html = this.toRowHtml(span);
        var headerHtml = this.grid.getHeaderHtml();
        html = "<table id='" + this.tableId + "' class='" + this.grid.gridCls + "'>" + headerHtml + html + "</table>";
        return html;
    },
    
    removeTop : function () {
        var cnt = 0;
        var selector = this.tableId + " ." + this.grid.rowCls;
        var self = this;
        jQuery("#" + selector).each(function( index ) {
            if (cnt >= self.bufferSize) return;
            jQuery(this).remove();
            cnt++;
        });
    },
    
    removeBottom : function () {
        var selector = this.tableId + " ." + this.grid.rowCls;
        var length = jQuery("#" + selector).length;
        var maxIdx = length - this.bufferSize;
        if (length % this.bufferSize != 0) {
            maxIdx = parseInt(length / this.bufferSize) * this.bufferSize;
        }
        jQuery("#" + selector).each(function( index ) {
            if (index >= maxIdx) {
                jQuery(this).remove();
            }
        });
    }
    
});

/**
 * Table Utility 
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_TableGridUtil = $Class.extend({
    classProp : { 
        name : "com.yung.util.TableGridUtil",
        commentBase64 : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAZdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuMTZEaa/1AAABVElEQVRoQ+2RW2rDQAxFB+KfFEPJn7eTTXT/O0mn+Iq6J03ixzwkyIHDkHgk3UHpltItssngh0h+XS75EPwYRcX/hRci+GcTBi95V7Hv4UXPXs/nfDyAl72quI9hgUc/T6d8vIBF3lTM17DQk6s2YbDYi4q3Hjbw4EdW8dbDJr1VrO2wUU93bcJgs14qzn7YsIeHNmGwaWsV4zhs3NIimzDYvJUaXw4OaGHRTRgcUluNLQ8H1bTKJgwOq6XG1YMDa1h1EwaHllZj6sPBJW2yCYPDS6n27WCAEjbdhMEQR1Xb9jDIEbtswmCYvapdPxhoj103YTDUVtWmPwy2xbncCctgW1S5HxhwjXOZM5YB16gyfzDoM+frTlkGfaau+4WB/3MYhnw4h6GprvmHwZeG2ITB8KY+x4EP+HEcx3wEg4/Q3/FYPmKapvgP0c+4hN/EG5ek9A0zMUHnKiaJVAAAAABJRU5ErkJggg=="    
    },
    gridDivId : null,
    hashId : null,
    columnUtil : null,
    errorCells : null,
    dirtyRowMap : null,
    gridStyle : null,
    headerCls : null,
    rowCls : null,
    cellCls : null,
    tableTemplate : null,
    headerTemplate : null,
    cacheRowMap : null,
    rowOrderList : null,
    commentMap : null,
    paging : false,
    currentPageIndex : null,
    tableAlign : null,
    smartRender : null,
    combo : null,
    datePicker : null,
    
    /**
     * Callback after grid edit value
     *
     * @callback AfterGridEdit
     * @param {jQueryEle} ele - jQuery table cell element
     * @param {string} rowIdx - row id string
     * @param {number} colIdx - column index
     * @param {com_yung_util_TableGridUtil} grid - this table grid
     * @param {string} newContent - new content for after edit
     */
    /**
     * Callback after grid edit value
     * @member {AfterGridEdit}
     * @instance
     * @memberof com_yung_util_TableGridUtil
     */
    afterEdit : null,
    
    /**
     * Callback before grid edit value. return false will block edit
     *
     * @callback BeforeGridEdit
     * @param {jQueryEle} ele - jQuery table cell element
     * @param {string} rowIdx - row id string
     * @param {number} colIdx - column index
     * @param {com_yung_util_TableGridUtil} grid - this table grid
     * @param {string} oldContent - old content for before edit
     */
    /**
     * Callback before grid edit value
     * @member {BeforeGridEdit}
     * @instance
     * @memberof com_yung_util_TableGridUtil
     */
    beforeEdit : null,
    
    /**
     * paging size, if null for not paging
     * @member {number}
     * @instance
     * @memberof com_yung_util_TableGridUtil
     */
    pageSize : null,
    
    /**
     * border width size
     * @member {number}
     * @instance
     * @memberof com_yung_util_TableGridUtil
     */
    borderWidth : 0,
    
    /** 
     * constructor
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {string} gridDivId - grid div id
     * @param {com_yung_util_ColumnUtil} columnUtil - column information schema utility
     * @param {string} gridCls - grid CSS name, default 'yung-tableborder'
     * @param {string} gridStyle - grid style, default 'table-layout:fixed; word-break:break-all;'
     * @param {string} headerCls - grid header CSS name, default 'yung-tableHeader'
     * @param {string} cellCls - grid cell CSS name, default 'yung-tableCell'
     * @param {string} tableAlign - grid align, default left
     * @param {string} rowCls - grid row CSS name, default 'yung-tableRow'
     */
    init: function(gridDivId, columnUtil, gridCls, gridStyle, headerCls, cellCls, tableAlign, rowCls){
        $Class.validate(this.classProp.name, gridDivId);
        if (columnUtil == null || gridDivId == null || gridDivId == '') {
            alert("Please define columnUtil and gridDivId!, see yung-TableGridUtil.js.");
        }
        this.gridDivId = gridDivId;
        this.hashId = MD5Util.calc(this.classProp.name + "-" + gridDivId);
        this.beforeEdit = null;
        this.afterEdit = null;
        this.columnUtil = columnUtil;
        this.errorCells = new com.yung.util.ObjectHashSet(com.yung.util.TableErrorCell);
        this.dirtyRowMap = new com.yung.util.Map('string', 'string');
        this.cacheRowMap = new com.yung.util.Map('string', 'object');
        this.rowOrderList = new com.yung.util.BasicList('string');
        this.commentMap = new com.yung.util.Map('string', com.yung.util.MapInterface);
        var columnArray = columnUtil.getColumnArray();
        if (gridCls == null || gridCls == '') {
            this.gridCls = "yung-tableborder";
        } else {
            this.gridCls = gridCls;
        }
        if (gridStyle == null || gridStyle == '') {
            this.gridStyle = "table-layout:fixed; word-break:break-all;";
        } else {
            this.gridStyle = gridStyle;
        }
        if (headerCls == null || headerCls == '') {
            this.headerCls = "yung-tableHeader";
        } else {
            this.headerCls = headerCls;
        }
        if (rowCls == null || rowCls == '') {
            this.rowCls = "yung-tableRow";
        } else {
            this.rowCls = rowCls;
        }
        if (cellCls == null || cellCls == '') {
            this.cellCls = "yung-tableCell";
        } else {
            this.cellCls = cellCls;
        }
        if (tableAlign == null || tableAlign == '') {
            this.tableAlign = "left";
        } else {
            this.tableAlign = tableAlign;
        }
        this.tableTemplate = new com.yung.util.BasicTemplate();
        this.headerTemplate = new com.yung.util.BasicTemplate();
        this.createGrid();
        this.smartRender = new com.yung.util.GridSmartRender(this);
        var self = this;
        setTimeout(function () {
            self.render();
        }, 500);
        return this;
    },
    
    /** 
     * set after grid edit callback
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {AfterGridEdit} funct - after grid edit callback
     */
    setAfterEdit : function (funct) {
        if (typeof funct == 'function') {
            this.afterEdit = funct;
        }
    },
    
    /** 
     * set before grid edit callback
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {BeforeGridEdit} funct - before grid edit callback
     */
    setBeforeEdit : function (funct) {
        if (typeof funct == 'function') {
            this.beforeEdit = funct;
        }
    },
    
    /** 
     * set paging size
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {number} pageSize - paging size
     */
    setPageSize : function (pageSize) {
        this.pageSize = pageSize * 1.0;
        this.paging = true;
        this.refresh();
    },
    createGrid : function() {
    	if (this.paging == true) {
    		var pageHtml = this.getPageHtml();
            jQuery("#" + this.gridDivId).html('<div id="' + this.gridDivId + '-pageDiv">' + pageHtml + '</div><div id="' + this.gridDivId + '-tableDiv"></div>');
            this.loadPage(0);
        } else {
        	this.setupTableTemplate();
            jQuery("#" + this.gridDivId).html(this.tableTemplate.toHtml());	
            var headerHtml = this.getHeaderHtml();
            jQuery("#" + this.gridDivId + "-headerDiv").html('<table class="' + this.gridCls + '" style="' + this.gridStyle + '" align="' + this.tableAlign + '">' + headerHtml + '</table>');
            var rowHtml = this.renderRows([]);
        	jQuery("#" + this.gridDivId + "-contentDiv").html('<table id="' + this.gridDivId + '-contentTable" class="' + this.gridCls + '" style="' + this.gridStyle + '" align="' + this.tableAlign + '">' + rowHtml + '</table>');
        	var grid = this;
        	jQuery('#' + this.gridDivId + '-contentScrollDiv').on('scroll', function () {
        	    var contentLeft = jQuery(this).scrollLeft();
        	    if (com_yung_util_TableGridUtil.fireScrollManually == true) {
        	        com_yung_util_TableGridUtil.fireScrollManually = false;
        	        return;
        	    }
        	    var txtAreaEdit = jQuery("#" + grid.gridDivId + "-editTxt");
        		var selectDiv = jQuery("#" + grid.gridDivId + "-selectDiv");
        		var txtAreaDatePciker = jQuery("#" + grid.gridDivId + "-datePicker");
        		txtAreaEdit.css("display", "none");
        		selectDiv.css("display", "none");
        		txtAreaDatePciker.css("display", "none");
        		grid.datePicker.floatDiv.showFloatDiv(false);
        		jQuery('#' + grid.gridDivId + '-headerScrollDiv').scrollLeft(contentLeft);
        	});
        }
    	this.createTextEditor();
    	this.createSelectEditor();
    	this.createDatePicker();
    },
    
    /** 
     * hide grid page bar
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     */
    hidePageHtml : function () {
    	jQuery("#" + this.gridDivId + '-pageDiv').css("display", "none");
    },
    
    /** 
     * show grid page bar
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     */
    showPageHtml : function () {
    	jQuery("#" + this.gridDivId + '-pageDiv').css("display", "");
    },
    getHeaderHtml : function () {
    	var html = this.headerTemplate.toHtml();
        if (html == '') {
            var columnArray = this.columnUtil.getColumnArray();
            this.headerTemplate.add('<tr>');
            for (var i = 0; i < columnArray.length; i++) {
                var column = columnArray[i];
                this.headerTemplate.add('<td class="' + this.headerCls + '" data-columnId="' + column.columnId + '" width="' + column.width + '" align="center" >' + column.header + '</td>');
            }
            this.headerTemplate.add('</tr>');
        }
        return this.headerTemplate.toHtml();
    },
    getPageHtml : function (titleData) {
        if (this.paging == true) {
            var pageBarTemplate = new com.yung.util.BasicTemplate();
            pageBarTemplate.add("<span> Page </span> &nbsp; ");
            pageBarTemplate.add("<input id='{{gridDivId}}-pageIndex' type='text' value='{{pageIndex}}' size='3' /> / <span id='{{gridDivId}}-pageMax' >{{pageNum}}</span> &nbsp; ");
            pageBarTemplate.add("<input id='{{gridDivId}}-changePage' type='button' value='{{change}}' onclick='fireInstanceMethod(\"" + this.hashId + "\", \"changePage\");' /> &nbsp; ");
            pageBarTemplate.add("<input id='{{gridDivId}}-toStartPage' type='button' value='{{first}}' onclick='fireInstanceMethod(\"" + this.hashId + "\", \"changePage\", 0)' /> &nbsp; ");
            pageBarTemplate.add("<input id='{{gridDivId}}-toNextPage' type='button' value='{{prev}}' onclick='fireInstanceMethod(\"" + this.hashId + "\", \"changePage\", -1)' /> &nbsp; ");
            pageBarTemplate.add("<input id='{{gridDivId}}-toPrevPage' type='button' value='{{next}}' onclick='fireInstanceMethod(\"" + this.hashId + "\", \"changePage\", -2)' /> &nbsp; ");
            pageBarTemplate.add("<input id='{{gridDivId}}-toEndPage' type='button' value='{{last}}' onclick='fireInstanceMethod(\"" + this.hashId + "\", \"changePage\", -3)' /> &nbsp; ");
            pageBarTemplate.add("<br><br>");
            var data = {};
            data['gridDivId'] = this.gridDivId;
            data['pageIndex'] = '1';
            data['pageNum'] = '1';
            if (titleData != null) {
                data['change'] = titleData["change"];
                data['first'] = titleData["first"];
                data['prev'] = titleData["prev"];
                data['next'] = titleData["next"];
                data['last'] = titleData["last"];
            }
            if (data["change"] == null) {
                data['change'] = "change";
            }
            if (data["first"] == null) {
                data['first'] = "first";
            }
            if (data["prev"] == null) {
                data['prev'] = "prev";
            }
            if (data["next"] == null) {
                data['next'] = "next";
            }
            if (data["last"] == null) {
                data['last'] = "last";
            }
            this.currentPageIndex = 0;
            return pageBarTemplate.toHtml(data);
        } else {
            return "";
        }
    },
    
    /** 
     * get current page number
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @return {number} current page number
     */
    getPageNum : function () {
        var pageNum = this.rowOrderList.size() / this.pageSize;
        pageNum = parseInt(pageNum);
        if ((pageNum * this.pageSize) == this.rowOrderList.size()) {
            return pageNum;
        }
        return pageNum + 1;
    },
    
    /** 
     * get current total record number
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @return {number} current total record number
     */
    getRowCnt : function () {
        return this.rowOrderList.size();
    },
    
    /** 
     * load JSON for url
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {string} dUrl - Ajax Url
     * @param {function} unblock - function after call
     */
    loadJSON : function(dUrl, unblock) {
        var idx = dUrl.indexOf("?");
        if (idx > 0) {
            var dummyForm = jQuery("#dummyForm");
            if (dummyForm.length == 0) {
                jQuery("body").append("<div id='dummyFormDiv' ></div>");
            }
            var formDiv = jQuery("#dummyFormDiv");
            var map = com.yung.util.StringUtils.parseURL(dUrl);
            dUrl = dUrl.substring(0, idx);
            var html = "<form id='dummyForm' method='post' >";
            for (var k in map) {
                html = html + "<input type='hidden' name='" + k + "' value='" + map[k] + "' />";
            }
            html = html + "</form>";
            formDiv.html(html);
        }
        var ajaxhttp = new com.yung.util.AjaxHttp(dUrl, 'dummyForm', function callback(response, self, caller)
        {
            if (response.action == 'error_main') {
                alert("load data error: " + response.errormsg);
                if (unblock != null) unblock();
                return;
            }
            caller.clean();
            caller.loadData(response.rows);
            caller.render();
            if (typeof unblock == 'function') {
                unblock();
            }
        });
        if (unblock != null) ajaxhttp.setLoading(false);
        ajaxhttp.send(this);
    },
    setupTableTemplate : function () {
    	if (this.tableTemplate.template.size() == 0) {
    	    var width = 0;
    	    var widthStr = jQuery("#" + this.gridDivId).css("width");
    	    widthStr = widthStr.toLowerCase();
    	    if (widthStr.indexOf('%') > 0) {
    	        width = 1000;
    	    } else if (widthStr.indexOf('px') > 0) {
    	        width = this.replaceAll(width + "", "px", "") * 1.0;
    	    } else {
    	        width = widthStr * 1.0;
    	    }
            var data = {};
    		var tableWidth = 0;
    		var columnArray = this.columnUtil.getColumnArray();
    		for (var c = 0; c < columnArray.length; c++) {
                var column = columnArray[c];
                if (column.columnHidden == true) {
                	continue;
                }
                tableWidth = tableWidth + (column.width * 1.0) + this.borderWidth;
    		}
    		data["tableWidth"] = tableWidth;
    		if (width > (tableWidth + com_yung_util_Position.getScrollbarWidth())) {
    			width = tableWidth;
    		}
    		data["width"] = width;
    		data["contentWidth"] = width + com_yung_util_Position.getScrollbarWidth();
    		var height = jQuery("#" + this.gridDivId).css("height");
    		height = this.replaceAll(height + "", "px", "") * 1.0;
    		if (height == null || height == 0) {
    			height = 600;
    		}
    		data["height"] = height;
    		this.tableTemplate.setData(data);
    		this.tableTemplate.add('<div id="' + this.gridDivId + '-headerScrollDiv" style="position: relative; top: 0px; width: {{width}}px; overflow: hidden; z-index: 1010;" >');
        	this.tableTemplate.add('    <div id="' + this.gridDivId + '-headerDiv" style="width: {{tableWidth}}px;"></div>');
        	this.tableTemplate.add('</div>');
        	this.tableTemplate.add('<div id="' + this.gridDivId + '-contentScrollDiv" style="position: relative; top: 0px; width: {{contentWidth}}px; height: {{height}}px; overflow: auto; z-index: 1000;" >');
        	this.tableTemplate.add('    <div id="' + this.gridDivId + '-contentDiv" style="width: {{tableWidth}}px;"></div>');
        	this.tableTemplate.add('</div>');
    		return true;
        }
    	return false;
    },
    
    /** 
     * load data
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {Array} rows - data object array
     * @param {number} pageIndex - page index to render
     */
    loadData : function (rows, pageIndex) {
        this.clean();
        this.setupData(rows);
        if (this.paging == true) {
            this.smartRender.off = true;
            this.smartRender.render();
        	if (pageIndex == null) {
        		pageIndex = 0;
        	}
        	this.loadPage(pageIndex);
        } else if (rows.length > 300) {
            this.smartRender.off = false;
            this.smartRender.dataSize = rows.length;
            this.smartRender.preload();
        } else {
            this.smartRender.off = true;
            this.smartRender.render();
        	var headerHtml = this.getHeaderHtml();
        	var rowHtml = this.renderRows(rows);
        	jQuery("#" + this.gridDivId + "-contentDiv").html('<table id="' + this.gridDivId + '-contentTable" class="' + this.gridCls + '" style="' + this.gridStyle + '" align="' + this.tableAlign + '">' + headerHtml + rowHtml + '</table>');
            this.render();	
        }
    },
    
    /** 
     * load page
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {number} pageIndex - page index to render
     */
    loadPage : function (pageIndex) {
        var headerHtml = this.getHeaderHtml();
        var rowHtml = this.renderPageRow(pageIndex);
        jQuery("#" + this.gridDivId + "-tableDiv").html('<table class="' + this.gridCls + '" style="' + this.gridStyle + '" align="' + this.tableAlign + '">' + headerHtml + rowHtml + '</table>');
        this.render();
    },
    
    /** 
     * refresh and re-render grid, use when turn paging on/off, smart-render on/off
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     */
    refresh : function () {
    	// refresh create grid structure
    	if (this.paging == true) {
    		var headerScroll = jQuery("#" + this.gridDivId + "-headerScrollDiv")[0];
    		if (headerScroll != null) {
    			this.createGrid();
    		}
    	} else {
    		var headerScroll = jQuery("#" + this.gridDivId + "-headerScrollDiv")[0];
    		if (headerScroll == null) {
    			this.createGrid();
    		}
    	}
    	if (this.paging == true) {
    		var pageIndex = this.currentPageIndex;
            if (pageIndex != 0) {
            	this.loadPage(pageIndex);
            }
    	} else {
    		var rows = [];
            for (var i = 0; i < this.rowOrderList.size(); i++) {
                var rowIdx = this.rowOrderList.get(i);
                var row = this.cacheRowMap.get(rowIdx);
                if (row != null) {
                    rows.push(row);
                }
            }
            this.loadData(rows);
        }
    	this.render();
    },
    setupData : function (rows) {
    	this.clean();
    	var columnArray = this.columnUtil.getColumnArray();
        if (rows == null || rows.length == 0) {
            return;
        }
        var columnIdx = -1;
        for (var c = 0; c < columnArray.length; c++) {
            var column = columnArray[c];
            if (column.columnId == 'id') {
                columnIdx = c;
                break;
            }
        }
        if (columnIdx == -1) {
            alert('Please define id column!');
            return;
        }
        this.cacheRowMap.clear();
        this.commentMap.clear();
        this.rowOrderList.clear();
        for (var r = 0; r < rows.length; r++) {
            var rowIdx = rows[r]['id'];
            this.cacheRowMap.put(rowIdx, rows[r]);
            this.rowOrderList.add(rowIdx);
        }
    },
    renderPageRow : function (pageIndex) {
        var rows = [];
        var startIdx = pageIndex * this.pageSize;
        var endIdx = (pageIndex + 1) * this.pageSize;
        for (var i = startIdx; i < endIdx; i++) {
            var rowIdx = this.rowOrderList.get(i);
            var row = this.cacheRowMap.get(rowIdx);
            if (row != null) {
                rows.push(row);
            }
        }
        return this.renderRows(rows);
    },
    renderRows : function (rows) {
        if (rows == null || rows.length == 0) {
            return "";
        } else {
            var columnArray = this.columnUtil.getColumnArray();
            var html = "";
            for (var r = 0; r < rows.length ; r++) {
                var rowIdx = rows[r]['id'];
                html = html + "<tr class='" + this.rowCls + "' >";
                for (var c = 0; c < columnArray.length; c++) {
                    var column = columnArray[c];
                    var content = rows[r][column.columnId];
                    if (content == null) {
                        content = '';
                    }
                    var comment = this.getComment(rowIdx, column.columnId);
                    if (comment != null && comment != '') {
                        content = "<img id='" + this.gridDivId + "-comment-" + rowIdx + "-" + c + "' src='" + this.classProp.commentBase64 + "' width='12' style='position: absolute; top: 0px; left: 0px;' title='" + comment + "' />" + content;
                    }
                    var clickFunt = '';
                    if ('edtxt' == column.type || 'ed' == column.type || 'edn' == column.type) {
                        clickFunt = "fireInstanceMethod('" + this.hashId + "', 'edit', '" + this.gridDivId + "-" + rowIdx + "-" + c + "');";
                    } else if ('co' == column.type || 'select' == column.type) {
                        clickFunt = "fireInstanceMethod('" + this.hashId + "', 'edit', '" + this.gridDivId + "-" + rowIdx + "-" + c + "');";
                    } else if ('ch' == column.type) {
                        clickFunt = "fireInstanceMethod('" + this.hashId + "', 'edit', '" + this.gridDivId + "-" + rowIdx + "-" + c + "');";
                        if (content == '1' || content == 1) {
                            content = '<input type="checkbox" checked />';
                        } else {
                            content = '<input type="checkbox" />'
                        }
                    } else if ('date' == column.type) {
                        clickFunt = "fireInstanceMethod('" + this.hashId + "', 'edit', '" + this.gridDivId + "-" + rowIdx + "-" + c + "');";
                    } 
                    html = html + '<td id="' + this.gridDivId + '-' + rowIdx + '-' + c + '" data-columnId="' + column.columnId + '" class="' + this.cellCls + '" width="' + column.width + '" align="' + column.align + '" style="position: relative; background-clip: padding-box;" onclick="' + clickFunt + '" >' + content + '</td>';
                }
                html = html + "</tr>";
            }
            return html;
        }
    },
    
    /** 
     * re-render grid, only adjust width, show/hide column
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     */
    render : function () {
        var columnArray = this.columnUtil.getColumnArray();
        for (var i = 0; i < columnArray.length; i++) {
            var column = columnArray[i];
            var col = "table td[data-columnId='" + column.columnId + "']";
            if (column.columnHidden == true) {
                jQuery(col).hide();
            } else {
                jQuery(col).show();
            }
        }
        var keyArray = this.dirtyRowMap.getKeyArray();
        for (var i = 0; i < keyArray.length; i++) {
            var rowIdx = keyArray[i];
            var state = this.dirtyRowMap.get(rowIdx);
            if (state == 'updated' || state == 'inserted') {
                this.setUpdated(rowIdx, state);
            } else if (state == 'deleted') {
                this.deleteRow(rowIdx);
            }
        }
        if (this.paging == true) {
            var pageNum = this.getPageNum();
            jQuery("#" + this.gridDivId + "-pageMax").html(pageNum);
        } else {
            // align header to top
        	var contentScrollDiv = jQuery("#" + this.gridDivId + "-contentScrollDiv");
        	var headerScrollDiv = jQuery("#" + this.gridDivId + "-headerScrollDiv");
        	var headerHeight = headerScrollDiv.height();
        	contentScrollDiv.css("top", "-" + headerHeight + "px");
        	this.adjustHeight();
        	this.adjustWidth();
        }
        this.smartRender.render();
    },
    adjustHeight : function () {
        var browser = com_yung_util_getbrowser();
        var contentScrollDiv = jQuery("#" + this.gridDivId + "-contentScrollDiv");
        var contentTable = jQuery("#" + this.gridDivId + "-contentTable");
        var contentTableHeight = contentTable.height();
        if (contentTableHeight > 1) {
            var contentScrollDivHeight = contentScrollDiv.height();
            if (contentScrollDivHeight > contentTableHeight) {
                var contentTableWidth = contentTable.width();
                var contentScrollDivWidth = contentScrollDiv.width();
                if (contentTableWidth > contentScrollDivWidth) {
                    // horizontal scroll bar exist
                    contentTableHeight = contentTableHeight + 6;
                    if (browser == 'firefox') {
                        contentTableHeight = contentTableHeight - 4;
                    }
                    contentScrollDiv.css("height", contentTableHeight + com_yung_util_Position.getScrollbarHeight());
                } else {
                    contentTableHeight = contentTableHeight + 6;
                    contentScrollDiv.css("height", contentTableHeight);
                }
            }
        }
    },
    adjustWidth : function () {
        var browser = com_yung_util_getbrowser();
        var width = 0;
        var contentWidth = 0;
        var width = jQuery("#" + this.gridDivId).width();
        var tableWidth = 0;
        var columnArray = this.columnUtil.getColumnArray();
        for (var c = 0; c < columnArray.length; c++) {
            var column = columnArray[c];
            if (column.columnHidden == true) {
                continue;
            }
            tableWidth = tableWidth + (column.width * 1.0) + this.borderWidth;
        }
        if (width >= (tableWidth + com_yung_util_Position.getScrollbarWidth())) {
            width = tableWidth + com_yung_util_Position.getScrollbarWidth();
            contentWidth = tableWidth + com_yung_util_Position.getScrollbarWidth();
            if (browser == 'msie') {
                contentWidth = contentWidth + 5;
            }
        } else {
            contentWidth = width;
            var tableHeight = jQuery("#" + this.gridDivId + "-contentTable").height();
            var scrollHeight = jQuery("#" + this.gridDivId + "-contentScrollDiv").height();
            if (scrollHeight - (tableHeight + com_yung_util_Position.getScrollbarHeight()) < 3) {
                // vertical scroll bar not exist
                width = width - com_yung_util_Position.getScrollbarWidth();
            }
        }
        jQuery("#" + this.gridDivId + "-contentScrollDiv").width(contentWidth);
        jQuery("#" + this.gridDivId + "-contentDiv").width(tableWidth);
        jQuery("#" + this.gridDivId + "-headerScrollDiv").width(width);
        jQuery("#" + this.gridDivId + "-headerDiv").width(tableWidth);
        // fix for table align
        var contentScrollTablePos = new com.yung.util.Position(jQuery("#" + this.gridDivId + "-contentScrollDiv table"));
        var contentPos = contentScrollTablePos.getTopLeftPosition();
        var headerScrollTablePos = new com.yung.util.Position(jQuery("#" + this.gridDivId + "-headerScrollDiv table"));
        var headerPos = headerScrollTablePos.getTopLeftPosition();
        if ((contentPos.left - headerPos.left) > 2 || (contentPos.left - headerPos.left) < -2) {
            jQuery("#" + this.gridDivId + "-headerScrollDiv").css("left", (contentPos.left - headerPos.left) + "px");
        }
    },
    
    /** 
     * hide column
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {string} arg - column id
     */
    hideColumn : function (arg) {
        var column = null;
        if (typeof arg == 'string') {
            column = this.columnUtil.columnMap[arg];
        } else if (typeof arg == 'number') {
            column = this.columnUtil.columnArray[arg];
        }
        var col = "table td[data-columnId='" + column.columnId + "']";
        column.columnHidden = true;
        jQuery(col).hide();
        this.adjustWidth();
    },
    
    /** 
     * show column
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {string} arg - column id
     */
    showColumn : function (arg) {
        var column = null;
        if (typeof arg == 'string') {
            column = this.columnUtil.columnMap[arg];
        } else if (typeof arg == 'number') {
            column = this.columnUtil.columnArray[arg];
        }
        var col = "table td[data-columnId='" + column.columnId + "']";
        column.columnHidden = false;
        jQuery(col).show();
        this.adjustWidth();
    },
    
    /** 
     * set update status to row
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {string} rowId - row id
     * @param {string} state - row state
     */
    setUpdated : function(rowId, state) {
        var originState = this.dirtyRowMap.get(rowId);
        if (originState == null) {
            if (state == null || state == '' || state == 'updated') {
                state = "updated";
            } else if (state == 'inserted') {
                state = 'inserted';
            } else {
                throw "unknown state: " + state;
            }
            this.dirtyRowMap.put(rowId, state);
        }
        var columnArray = this.columnUtil.getColumnArray();
        for (var i = 0; i < columnArray.length; i++) {
            var column = columnArray[i];
            this.setCellBold(rowId, column.columnId);
        }
    },
    
    /** 
     * get all row id by order
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @return {Array} row id array
     */
    getAllRowIds : function() {
        return this.rowOrderList.toArray();
    },
    
    /** 
     * set cell value
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {string} rowId - row id
     * @param {string} columnId - column id
     * @param {string} value - cell value
     */
    setCellValue : function (rowId, columnId, value) {
        var column = this.columnUtil.getColumn(columnId);
        var colIdx = column.index;
        if (column == null) {
            if (window['console'] != null) {
                console.log("Err: columnId: " + columnId + " is not defined!");    
            }
            return;
        }
        var comment = this.getComment(rowId, columnId);
        var rowData = this.cacheRowMap.get(rowId);
        if (column.type != 'ch') {
            rowData[columnId] = value;
            this.insertCommentPng(rowId, colIdx, value, comment);
        } else {
            var rowData = this.cacheRowMap.get(rowId);
            if (value == '1' || value == 1 || value == 0 || value == '0') {
                rowData[columnId] = value + "";
                if (value == '1' || value == 1) {
                    var tdHtml = '<input type="checkbox" checked />';
                    this.insertCommentPng(rowId, colIdx, tdHtml, comment);
                } else {
                    var tdHtml = '<input type="checkbox" />';
                    this.insertCommentPng(rowId, colIdx, tdHtml, comment);
                }
            } else {
                // html
                this.insertCommentPng(rowId, colIdx, value, comment);
                var idx = (value + "").indexOf('checked');
                if (idx < 0) {
                    rowData[columnId] = '0';
                } else {
                    rowData[columnId] = '1';
                }
            }
        }
        this.cacheRowMap.put(rowId, rowData);
    },
    
    /** 
     * mark cell as delete, CSS text-decoration: line-through
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {string} rowId - row id
     * @param {string} columnId - column id
     */
    setCellDelete : function (rowId, columnId) {
        var colIdx = this.columnUtil.getColumnIndex(columnId);
        if (colIdx == null) {
            console.log("Err: columnId: " + columnId + " is not defined!");
            return;
        }
        jQuery("#" + this.gridDivId + '-' + rowId + '-' + colIdx).css('text-decoration', 'line-through');
    },
    
    /** 
     * mark cell as update, CSS font-weight: bold
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {string} rowId - row id
     * @param {string} columnId - column id
     */
    setCellBold : function (rowId, columnId) {
        var colIdx = this.columnUtil.getColumnIndex(columnId);
        if (colIdx == null) {
            console.log("Err: columnId: " + columnId + " is not defined!");
            return;
        }
        jQuery("#" + this.gridDivId + '-' + rowId + '-' + colIdx).css('font-weight', 'bold');
    },
    
    /** 
     * set cell background color
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {string} rowId - row id
     * @param {string} columnId - column id
     * @param {string} color - color code
     */
    setCellColor : function (rowId, columnId, color) {
        var colIdx = this.columnUtil.getColumnIndex(columnId);
        if (colIdx == null) {
            console.log("Err: columnId: " + columnId + " is not defined!");
            return;
        }
        jQuery("#" + this.gridDivId + '-' + rowId + '-' + colIdx).css('background', color);
    },
    
    /** 
     * get cell value
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {string} rowId - row id
     * @param {string} columnId - column id
     * @return {string} cell value
     */
    getCellValue : function (rowId, columnId) {
        var rowData = this.cacheRowMap.get(rowId);
        if (rowData != null) {
            var ret = rowData[columnId];
            if (ret == '&nbsp;') {
                return null;
            }
            return ret;
        } else {
            return null;
        }
    },
    
    /** 
     * get cell html, different with getCellValue, 
     * ex. ch type cell value is '0' or '1' not html 
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {string} rowId - row id
     * @param {string} columnId - column id
     * @return {string} cell html
     */
    getCellHtml : function (rowId, columnId) {
        var colIdx = this.columnUtil.getColumnIndex(columnId);
        return jQuery("#" + this.gridDivId + '-' + rowId + '-' + colIdx).html();
    },
    
    /** 
     * get cell checked or not, only for type ch only 
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {string} rowId - row id
     * @param {string} columnId - column id
     * @return {boolean} checked or not
     */
    getCellCheck : function (rowId, columnId) {
        var value = this.getCellValue(rowId, columnId);
        if (value != null) {
            if (value == '1' || value == 1) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    },
    getNewRowId : function guidGenerator() {
        var d = new Date();
        var timeStampInMs = d.getTime();
        return "ZzZzZ" + parseInt(timeStampInMs);
    },
    
    /** 
     * add a new empty row by index
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {number} index - row index to add
     * @param {string} idColumn - id column name
     * @return {boolean} call render or not
     */
    addEmptyRow : function (index, idColumn, rendor) {
        if (idColumn == null) {
             idColumn = 'id';
        }
        var newRowId = this.getNewRowId();
        var nextRowId = this.getNewRowId();
        while (nextRowId == newRowId) {
            nextRowId = this.getNewRowId();
        }
        this.rowOrderList.addByIndex(newRowId, index);
        this.dirtyRowMap.put(newRowId, "inserted");
        var columnArray = this.columnUtil.getColumnArray();
        var rowData = {};
        for (var c = 0; c < columnArray.length; c++) {
            var column = columnArray[c];
            if (idColumn != column.columnId) {
                rowData[column.columnId] = '&nbsp;';
            } else {
                rowData[idColumn] = newRowId;
            }
        }
        this.cacheRowMap.put(newRowId, rowData);
        if (rendor == true) {
            if (this.paging == false) {
                var tableRef = jQuery("#" + this.gridDivId + " table tbody")[0];
                // Insert a row in the table at the last row
                var newRow   = tableRef.insertRow(index + 1);
                var newRowHtml = ' ';
                for (var c = 0; c < columnArray.length; c++) {
                    var column = columnArray[c];
                    var content = '&nbsp;';
                    if ('id' == column.columnId) {
                        content = newRowId;
                    }
                    var clickFunt = '';
                    if ('edtxt' == column.type || 'ed' == column.type || 'edn' == column.type) {
                        clickFunt = "fireInstanceMethod('" + this.hashId + "', 'edit', '" + this.gridDivId + "-" + newRowId + "-" + c + "');";
                    } else if ('co' == column.type || 'select' == column.type) {
                        clickFunt = "fireInstanceMethod('" + this.hashId + "', 'edit', '" + this.gridDivId + "-" + newRowId + "-" + c + "');";
                    } else if ('ch' == column.type) {
                        clickFunt = "fireInstanceMethod('" + this.hashId + "', 'edit', '" + this.gridDivId + "-" + newRowId + "-" + c + "');";
                    }
                    newRowHtml = newRowHtml + '<td id="' + this.gridDivId + '-' + newRowId + '-' + c + '" data-columnId="' + column.columnId + '" class="' + this.cellCls + '" width="' + column.width + '" align="' + column.align + '" style="position: relative; background-clip: padding-box;" onclick="' + clickFunt + '" >' + content + '</td>';
                }
                jQuery(newRow).html(newRowHtml);
                this.render();
            } else {
                this.changePage(this.gridDivId, this.currentPageIndex);
            }
        }
        return newRowId;
    },
    
    /** 
     * get all rows which state is not empty
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {boolean} validate - run validate or not
     * @return {Array} rows which state is not empty
     */
    getChangedRows : function() {
        var result = [];
        var dirtyRowIds = this.dirtyRowMap.getKeyArray();
        for (var i = 0; i < dirtyRowIds.length; i++) {
            var rowIdx = dirtyRowIds[i];
            var row = this.getRowData(rowIdx);
            result.push(row);
        }
        return result;
    },
    
    /** 
     * delete grid row, if row state is inserted then remove row,
     * if state is not inserted, then mark as delete
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {string} rowId - row id
     * @param {boolean} forceDelete - remove row immediately 
     */
    deleteRow : function (rowId, forceDelete) {
        if (forceDelete == true) {
            this.dirtyRowMap.remove(rowId);
            this.cacheRowMap.remove(rowId);
            this.commentMap.remove(rowId);
            this.rowOrderList.removeElement(rowId);
            this.refresh();
            return;
        }
        var originState = this.dirtyRowMap.get(rowId);
        if (originState == 'inserted') {
            this.dirtyRowMap.remove(rowId);
            this.cacheRowMap.remove(rowId);
            this.commentMap.remove(rowId);
            this.rowOrderList.removeElement(rowId);
            this.refresh();
        } else {
            this.dirtyRowMap.put(rowId, 'deleted');
            var columnArray = this.columnUtil.getColumnArray();
            for (var i = 0; i < columnArray.length; i++) {
                var column = columnArray[i];
                this.setCellDelete(rowId, column.columnId);
            }
        }
    },
    
    /** 
     * get all rows which state is not empty, and validate row
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {boolean} validate - run validate or not
     * @return {Array} rows which state is not empty
     */
    getAllUpdateData : function (validate) {
        var result = this.getChangedRows();
        if (validate == true) {
            var ret = this.validate(result);
            if (ret == false) {
                return;
            }
        }
        return result;
    },
    
    /** 
     * clean all grid state and data
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     */
    clean : function () {
        this.clearErrorCells();
        this.dirtyRowMap.clear();
        this.cacheRowMap.clear();
        this.commentMap.clear();
        this.rowOrderList.clear();
    },
    
    /** 
     * clean all grid state and data, then re-render
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     */
    clearAll : function () {
        var headerHtml = this.getHeaderHtml();
        jQuery("#" + this.gridDivId).html('<div id="' + this.gridDivId + '-pageDiv"></div><div id="' + this.gridDivId + '-tableDiv"><table class="' + this.gridCls + '" style="' + this.gridStyle + '" align="' + this.tableAlign + '">' + headerHtml + '</table></div>');
        this.clean();
        this.render();
    },
    
    /** 
     * clean all grid error cell, rollback yellow color
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     */
    clearErrorCells : function () {
        var errArray = this.errorCells.toArray();
        for (var i = 0; i < errArray.length; i++) {
            var errCell = errArray[i];
            var rowIdx = errCell.rowIdx;
            var columnId = errCell.columnId;
            this.setCellColor(rowIdx, columnId, "");
        }
        this.errorCells.clear();
    },
    
    /** 
     * add error cell information, set cell color yellow
     * and mark cell error
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {number} rowId - row id
     * @param {string} columnId - id column name
     */
    addErrorCells : function(rowId, columnId) {
        var errCell = new com.yung.util.TableErrorCell(rowId, columnId);
        this.errorCells.add(errCell);
        this.setCellColor(rowId, columnId, 'yellow');
    },
    
    /** 
     * validate current grid rows, and put error information into error cells.
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {Array} dataArray - row data array, optional
     * @return {boolean} with error or not
     */
    validate : function (dataArray) {
        this.clearErrorCells();
        if (dataArray == null) {
            dataArray = this.getChangedRows();
        }
        var errormsg = "";
        var colArray = this.columnUtil.getColumnArray();
        if (dataArray != null && dataArray.length > 0) {
            for (var i = 0; i < dataArray.length; i++) {
                var obj = dataArray[i];
                for (var j = 0; j < colArray.length; j++) {
                    var column = colArray[j];
                    if (column.numberFormat != null && column.numberFormat != '') {
                        var value = obj[column.columnId];
                        if (value != null) {
                            value = value + "";
                            if (value != '' && !jQuery.isNumeric(value)) {
                                var rowIdx = obj['_rowIdx'];
                                var rowNo = this.getRowNumber(rowIdx);
                                if (rowNo == null) {
                                    errormsg = errormsg + "rowId[" + rowIdx + "] column[" + column.header + "]: '" + value + "' is not number! \n";
                                } else {
                                    errormsg = errormsg + "row[" + rowNo + "] column[" + column.header + "]: '" + value + "' is not number! \n";
                                }
                                this.addErrorCells(rowIdx, column.columnId);
                            }
                        }
                    }
                    if (column.length != null && column.length > 0) {
                        var value = obj[column.columnId];
                        var rowIdx = obj['_rowIdx'];
                        if (value != null && value != '') {
                            value = value + "";
                            if (value.length > column.length) {
                                errormsg = errormsg + "rowId[" + rowIdx + "] column[" + column.header + "]: '" + value + "' length is over " + column.length + "! \n";
                                this.addErrorCells(rowIdx, column.columnId);
                            }
                        }
                    }
                    if (column.allowNull != null && column.allowNull === false) {
                        var value = obj[column.columnId];
                        var rowIdx = obj['_rowIdx'];
                        if (value == null || value == '') {
                            errormsg = errormsg + "rowId[" + rowIdx + "] column[" + column.header + "]: '" + value + "' value is empty! \n";
                            this.addErrorCells(rowIdx, column.columnId);
                        }
                    }
                }
            }
        }
        if (errormsg != '') {
            alert(errormsg);
            return false;
        }
        return true;
    },
    
    /** 
     * get row data as data object
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {string} rowId - row id
     * @return {object} data object
     */
    getRowData : function (rowIdx) {
        var obj = {};
        obj["_rowIdx"] = rowIdx;
        var state = this.dirtyRowMap.get(rowIdx);
        if (state == null) {
            state = '';
        }
        obj["_state"] = state;
        var colIds = this.columnUtil.getColumnAttrArray('columnId');
        for (var c = 0; c < colIds.length; c++) {
            var val = this.getCellValue(rowIdx, colIds[c]);
            obj[colIds[c]] = val;
        }
        return obj;
    },
    getRowNumber : function (rowIdx) {
        return this.getCellValue(rowIdx, 'rowNo');
    },
    fireAfterEdit : function (ele, rowIdx, colIdx, newContent) {
        if (this.afterEdit != null) {
            this.afterEdit(ele, rowIdx, colIdx, this, newContent);
        }
    },
    fireBeforeEdit : function (ele, rowIdx, colIdx, oldcontent) {
        if (this.beforeEdit != null) {
            return this.beforeEdit(ele, rowIdx, colIdx, this, oldcontent);
        }
    },
    
    /** 
     * get current grid cell value, Note: ch type will return '1' or '0'
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {string} rowId - row id
     * @param {string} columnId - column id
     * @return {string} cell value
     */
    getCurrentCellValue : function (rowId, columnId) {
        var cacheRowData = this.cacheRowMap.get(rowId);
        var content = cacheRowData[columnId];
        return content;
    },
    
    /** 
     * change page index and render html
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {number} index - page index
     */
    changePage : function (index) {
        var pageIndex = jQuery("#" + this.gridDivId + "-pageIndex").val();
        var isNum = com.yung.util.StringUtils.isNumeric(pageIndex);
        if (isNum == false) {
            alert(pageIndex + " is not number!");
            return;
        }
        pageIndex = pageIndex * 1.0 - 1;
        pageIndex = parseInt(pageIndex);
        var maxPage = this.getPageNum() - 1;
        if (index == 0) {
            pageIndex = 0;
        } else if (index == -1) {
            pageIndex = this.currentPageIndex - 1;
            if (pageIndex < 0) {
                return;
            }
        } else if (index == -2) {
            pageIndex = this.currentPageIndex + 1;
            if (pageIndex > maxPage) {
                return;
            }
        } else if (index == -3) {
            pageIndex = maxPage;
        } else {
            if (pageIndex < 0) {
                alert(pageIndex + " is not validate!");
                return;
            }
            if (pageIndex > maxPage) {
                alert(pageIndex + " is not validate!");
                return;
            }
        }
        this.currentPageIndex = pageIndex;
        this.loadPage(pageIndex);
        jQuery("#" + this.gridDivId + "-pageIndex").val(pageIndex + 1);
    },
    edit : function (cellId) {
        var browser = com_yung_util_getbrowser();
        var tokens = cellId.split("-");
        var rowIdx = tokens[1];
        var colIdx = tokens[2] * 1.0;
        var columnArray = this.columnUtil.getColumnArray();
        var columnId = columnArray[colIdx].columnId;
        var columnArray = this.columnUtil.getColumnArray();
        var type = columnArray[colIdx].type;
        var oldContent = jQuery("#" + cellId).text();
        if ('ch' == type) {
            var content = this.getCurrentCellValue(rowIdx, columnId);
            if (content != "1" && content != 1) {
                content = 0;
            }
            oldContent = content + "";
        }
        var ele = jQuery("#" + cellId);
        var ret = this.fireBeforeEdit(ele, rowIdx, colIdx, oldContent);
        if (ret === false) {
            return;
        }
        if (this.paging == false) {
            // adjust scroll bar 
            var jEle = jQuery("#" + cellId);
            var tdWidth = jEle.width();
            var tdHright = jEle.height();
            var headerEle = jQuery('#' + this.gridDivId + '-headerScrollDiv');
            var contentEle = jQuery('#' + this.gridDivId + '-contentScrollDiv');
            var contentScrollTopVal = contentEle.scrollTop();
            var contentScrollLeftVal = contentEle.scrollLeft();
            var position = new com.yung.util.Position(ele);
            var headerPosition = new com.yung.util.Position(headerEle);
            var contentPosition = new com.yung.util.Position(contentEle);
            var leftTopPos = position.getTopLeftPosition();
            var botLeftPos = position.getBotLeftPosition();
            var botRightPos = position.getBotRightPosition();
            var headerHeight = headerEle.height();
            var headerTopLeftPos = headerPosition.getTopLeftPosition();
            var contentBotLeftPos = contentPosition.getBotLeftPosition();
            var contentBotRightPos = contentPosition.getBotRightPosition();
            if (leftTopPos.top < (headerTopLeftPos.top + headerHeight)) {
                var topOffset = (headerTopLeftPos.top + headerHeight) - leftTopPos.top;
                contentEle.scrollTop(contentScrollTopVal - topOffset);
                com_yung_util_TableGridUtil.fireScrollManually = true;
            }
            if (botRightPos.top > contentBotRightPos.top - com_yung_util_Position.getScrollbarHeight()) {
                var topOffset = (contentBotRightPos.top - com_yung_util_Position.getScrollbarHeight()) - botRightPos.top;
                topOffset = topOffset - 5;
                contentEle.scrollTop(contentScrollTopVal - topOffset);
                com_yung_util_TableGridUtil.fireScrollManually = true;
            }
            if (leftTopPos.left < contentBotLeftPos.left) {
                var leftOffset = contentBotLeftPos.left - leftTopPos.left;
                contentEle.scrollLeft(contentScrollLeftVal - leftOffset);
                contentScrollLeftVal = contentEle.scrollLeft();
                headerEle.scrollLeft(contentScrollLeftVal);
                com_yung_util_TableGridUtil.fireScrollManually = true;
            }
            if (botRightPos.left > contentBotRightPos.left - com_yung_util_Position.getScrollbarWidth()) {
                var leftOffset = (contentBotRightPos.left - com_yung_util_Position.getScrollbarWidth()) - botRightPos.left;
                contentEle.scrollLeft(contentScrollLeftVal - leftOffset + 6);
                contentScrollLeftVal = contentEle.scrollLeft();
                headerEle.scrollLeft(contentScrollLeftVal);
                com_yung_util_TableGridUtil.fireScrollManually = true;
            }
        }
        if ('edtxt' == type || 'ed' == type || 'edn' == type) {
            this.editTxt(cellId);
        } else if ('co' == type || 'select' == type) {
            this.select(cellId);
        } else if ('ch' == type) {
            this.clickCheck(cellId);
        } else if ('date' == type) {
            this.pickDate(cellId);
        }
    },
    createTextEditor : function () {
        var txtAreaEdit = jQuery("#" + this.gridDivId + "-editTxt");
        if (txtAreaEdit[0] == null) {
            var html = '<textarea id="' + this.gridDivId + '-editTxt' + '" data-cellId="" data-lzOldContent="" style="position: absolute; display: none; border: 2px; border-color: black; z-index: 5010;" onblur="fireInstanceMethod(\'' + this.hashId + '\', \'editDone\', this);" ></textarea>';
            jQuery("body").append(html);
        }
        this.combo = com.yung.util.SelectCombo.instance(this.gridDivId + '-editTxt');
        var self = this;
        this.combo.setAfterChoose(function (eleId, inputVal) {
            // fire editTxtDone
            var editor = jQuery("#" + eleId);
            var dataCellId = editor.attr("data-cellId");
            var tokens = dataCellId.split("-");
            var rowIdx = tokens[1];
            var colIdx = tokens[2] * 1.0;
            var columnArray = self.columnUtil.getColumnArray();
            var columnId = columnArray[colIdx].columnId;
            var content = inputVal;
            var oldContent = self.getCurrentCellValue(rowIdx, columnId);
            if (content != oldContent) {
                self.setUpdated(rowIdx);
            }
            self.setCellValue(rowIdx, columnId, content);
            editor.css("display", "none");
        });
    },
    editTxt : function (cellId) {
        var browser = com_yung_util_getbrowser();
        var txtAreaEdit = jQuery("#" + this.gridDivId + "-editTxt");
        txtAreaEdit.attr("data-cellId", cellId);
        var tokens = cellId.split("-");
        var rowIdx = tokens[1];
        var colIdx = tokens[2] * 1.0;
        var columnArray = this.columnUtil.getColumnArray();
        var columnId = columnArray[colIdx].columnId;
        var oldContent = this.getCurrentCellValue(rowIdx, columnId);
        var lzOldContent = com_yung_util_LZString.encode(oldContent);
        txtAreaEdit.attr("data-lzOldContent", lzOldContent);
        var width = jQuery("#" + cellId).width() + yung_global_var.widthOffset[browser];
        var height = jQuery("#" + cellId).height() + yung_global_var.heightOffset[browser];
        var position = new com.yung.util.Position(jQuery("#" + cellId));
        var pos = position.getTopLeftPosition();
        pos.left = pos.left + yung_global_var.leftOffset[browser];
        pos.top = pos.top + yung_global_var.topOffset[browser];
        txtAreaEdit.css("width", width + "px");
        txtAreaEdit.css("height", height + "px");
        txtAreaEdit.css("left", pos.left + "px");
        txtAreaEdit.css("top", pos.top + "px");
        txtAreaEdit.val(oldContent);
        txtAreaEdit.css("display", "block");
        var displayValues = columnArray[colIdx].columnValidator;
        this.combo.setSelectValues(displayValues);
        txtAreaEdit.focus();
    },
    createSelectEditor : function () {
        var selectDiv = jQuery("#" + this.gridDivId + "-selectDiv");
        if (selectDiv[0] == null) {
            var html = '<div id="' + this.gridDivId + '-selectDiv' + '" data-cellId="" data-lzOldContent="" style="position: absolute; display: none; z-index: 5000; background: white; box-shadow: 5px 5px 5px #d7d7d7; border: 1px solid #486c99;" ></div>';
            jQuery("body").append(html);
            var self = this;
            jQuery("body").bind("click", function() {
                var div = jQuery("#" + self.gridDivId + "-selectDiv");
                var display = div.css("display");
                if (display == 'block') {
                    div.css("display", "none");
                }
            });
        }
    },
    select : function (cellId) {
        var browser = com_yung_util_getbrowser();
        var tokens = cellId.split("-");
        var rowIdx = tokens[1];
        var colIdx = tokens[2] * 1.0;
        var columnArray = this.columnUtil.getColumnArray();
        var columnId = columnArray[colIdx].columnId;
        var selectDiv = jQuery("#" + this.gridDivId + "-selectDiv");
        selectDiv = jQuery("#" + this.gridDivId + "-selectDiv");
        selectDiv.attr("data-cellId", cellId);
        var oldContent = this.getCurrentCellValue(rowIdx, columnId);
        var lzOldContent = com_yung_util_LZString.encode(oldContent);
        selectDiv.attr("data-lzOldContent", lzOldContent);
        var html = "<table style='width: 100%;'>";
        var width = jQuery("#" + cellId).width();
        var size = 0;
        var comboMap = this.columnUtil.columnArray[colIdx].comboMap;
        for (var key in comboMap) {
            var content = comboMap[key];
            var lzKey = com_yung_util_LZString.encode(key);
            html = html + "<tr>";
            html = html + "<td class='yung-combo' align='left' data-cellId='" + cellId + "' onclick='fireInstanceMethod(\"" + this.hashId + "\", \"editDone\", this, \"" + lzKey + "\");'>" + content + "</td>";
            html = html + "</tr>";
            size++;
            if (comboMap[key] != null) {
                var w = comboMap[key].length * 7.5;
                w = parseInt(w);
                if (w > width) {
                    if (w > 250) {
                        width = 250;
                    } else {
                        width = w;
                    }
                }
            }
        }
        html = html + "</table>";
        selectDiv.html(html);
        width = width + yung_global_var.widthOffset[browser];
        if (browser == 'msie') {
            width = width + 4;
        }
        var position = new com.yung.util.Position(jQuery("#" + cellId));
        var pos = position.getBotLeftPosition();
        pos.left = pos.left + yung_global_var.leftOffset[browser];
        pos.top = pos.top + yung_global_var.topOffset[browser];
        selectDiv.css("width", width + "px");
        selectDiv.css("left", pos.left + "px");
        selectDiv.css("top", pos.top + "px");
        var browser = com_yung_util_getbrowser();
        if (browser == 'msie') {
            jQuery(".yung-combo").each(function() {
                jQuery(this).hover(
                    function() {
                        jQuery(this).css("background-color", "#A4BED4");
                    }, function() {
                        jQuery(this).css("background-color", "");
                    }
                );
            });
        }
        setTimeout(function () {
            selectDiv.css("display", "block");
        }, 100);
    },
    pickDate : function (cellId) {
        var browser = com_yung_util_getbrowser();
        var txtAreaDatePciker = jQuery("#" + this.gridDivId + "-datePicker");
        var nowEdit = this.datePicker.floatDiv.isDisplay();
        if (nowEdit == true) {
            var self = this;
            setTimeout(function () {
                self.prepareTxtDatePicker(cellId);
                txtAreaDatePciker.focus();
            }, this.datePicker.delayFire * 1.1);
        } else {
            this.prepareTxtDatePicker(cellId);
            txtAreaDatePciker.focus();
        }
    },
    prepareTxtDatePicker : function (cellId) {
        var browser = com_yung_util_getbrowser();
        var txtAreaDatePciker = jQuery("#" + this.gridDivId + "-datePicker");
        txtAreaDatePciker.attr("data-cellId", cellId);
        var tokens = cellId.split("-");
        var rowIdx = tokens[1];
        var colIdx = tokens[2] * 1.0;
        var columnArray = this.columnUtil.getColumnArray();
        var columnId = columnArray[colIdx].columnId;
        var oldContent = this.getCurrentCellValue(rowIdx, columnId);
        var lzOldContent = com_yung_util_LZString.encode(oldContent);
        txtAreaDatePciker.attr("data-lzOldContent", lzOldContent);
        var width = jQuery("#" + cellId).width() + yung_global_var.widthOffset[browser];
        var height = jQuery("#" + cellId).height() + yung_global_var.heightOffset[browser];
        var position = new com.yung.util.Position(jQuery("#" + cellId));
        var pos = position.getTopLeftPosition();
        pos.left = pos.left + yung_global_var.leftOffset[browser];
        pos.top = pos.top + yung_global_var.topOffset[browser];
        txtAreaDatePciker.css("width", width + "px");
        txtAreaDatePciker.css("height", height + "px");
        txtAreaDatePciker.css("left", pos.left + "px");
        txtAreaDatePciker.css("top", pos.top + "px");
        txtAreaDatePciker.val(oldContent);
        txtAreaDatePciker.css("display", "block");
    },
    createDatePicker : function () {
        var txtAreaDatePciker = jQuery("#" + this.gridDivId + "-datePicker");
        if (txtAreaDatePciker[0] == null) {
            var html = '<textarea id="' + this.gridDivId + '-datePicker' + '" data-cellId="" data-lzOldContent="" style="position: absolute; display: none; border: 2px; border-color: black; z-index: 5080;" ></textarea>';
            jQuery("body").append(html);
        }
        this.datePicker = com.yung.util.GridDatePicker.instance(this.gridDivId + '-datePicker', 'MM/dd/yyyy', this);
        if (com_yung_util_getbrowser() == 'msie') {
            this.datePicker.adjustX = -1;
            this.datePicker.adjustY = 4;
        }
        var self = this;
        this.datePicker.setAfterChoose(function (eleId, inputVal) {
            // fire editTxtDone
            var editor = jQuery("#" + eleId);
            var dataCellId = editor.attr("data-cellId");
            var tokens = dataCellId.split("-");
            var rowIdx = tokens[1];
            var colIdx = tokens[2] * 1.0;
            var columnArray = self.columnUtil.getColumnArray();
            var columnId = columnArray[colIdx].columnId;
            var content = inputVal;
            var oldContent = self.getCurrentCellValue(rowIdx, columnId);
            if (content != oldContent) {
                self.setUpdated(rowIdx);
            }
            self.setCellValue(rowIdx, columnId, content);
            editor.css("display", "none");
        });
    },
    clickCheck : function (cellId) {
        var tokens = cellId.split("-");
        var rowIdx = tokens[1];
        var colIdx = tokens[2] * 1.0;
        var columnArray = this.columnUtil.getColumnArray();
        var columnId = columnArray[colIdx].columnId;
        var content = "";
        var checkAttr = "";
        var chk = jQuery("#" + cellId).find("input");
        var checked = chk.prop("checked");
        if (checked) {
            checkAttr = "checked";
            content = "1";
        } else {
            content = "0";
        }
        var html = '<input type="checkbox" ' + checkAttr + ' >';
        var oldContent = this.getCurrentCellValue(rowIdx, columnId);
        if (oldContent == 1 || oldContent == '1') {
            oldContent = "1";
        } else {
            oldContent = "0";
        }
        this.setCellValue(rowIdx, columnId, html);
        if (oldContent != content) {
            this.setUpdated(rowIdx);    
        }
    },
    editDone : function (editor, lzContent) {
        var cellId = jQuery(editor).attr("data-cellId");
        var tokens = cellId.split("-");
        var rowIdx = tokens[1];
        var colIdx = tokens[2] * 1.0;
        var columnArray = this.columnUtil.getColumnArray();
        var columnId = columnArray[colIdx].columnId;
        var type = columnArray[colIdx].type;
        var newContent = '';
        if ('edtxt' == type || 'ed' == type || 'edn' == type) {
            newContent = this.editTxtDone(cellId);
        } else if ('co' == type || 'select' == type ) {
            var selectContent = com_yung_util_LZString.decode(lzContent);
            newContent = this.selectDone(cellId, selectContent);
        } else if ('date' == type) {
            newContent = this.editTxtDone(cellId);
        }
        this.fireAfterEdit(jQuery("#" + cellId), rowIdx, colIdx, newContent);
    },
    editTxtDone : function (cellId) {
        var tokens = cellId.split("-");
        var rowIdx = tokens[1];
        var colIdx = tokens[2] * 1.0;
        var columnArray = this.columnUtil.getColumnArray();
        var columnId = columnArray[colIdx].columnId;
        var editor = jQuery("#" + this.gridDivId + "-editTxt");
        var content = editor.val();
        this.setCellValue(rowIdx, columnArray[colIdx].columnId, content);
        var lzOldContent = editor.attr("data-lzOldContent");
        var oldContent = com_yung_util_LZString.decode(lzOldContent);
        if (content != oldContent) {
            this.setUpdated(rowIdx);
        }
        editor.css("display", "none");
        return content;
    },
    selectDone : function (cellId, selectContent) {
        var tokens = cellId.split("-");
        var rowIdx = tokens[1];
        var colIdx = tokens[2] * 1.0;
        var columnArray = this.columnUtil.getColumnArray();
        var columnId = columnArray[colIdx].columnId;
        this.setCellValue(rowIdx, columnId, selectContent);
        var selectDiv = jQuery("#" + this.gridDivId + "-selectDiv");
        var lzOldContent = selectDiv.attr("data-lzOldContent");
        var oldContent = com_yung_util_LZString.decode(lzOldContent);
        if (selectContent != oldContent) {
            this.setUpdated(rowIdx);
        }
        selectDiv.css("display", "none");
        return selectContent;
    },
    
    /** 
     * set comment in grid
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {string} rowId - row id
     * @param {string} columnId - column id
     * @param {string} comment - comment text
     */
    setComment : function (rowId, columnId, comment) {
        var commentRowMap = this.commentMap.get(rowId);
        if (commentRowMap == null) {
            commentRowMap = new com.yung.util.Map('string', 'string');
            commentRowMap.put(columnId, comment);
            this.commentMap.put(rowId, commentRowMap);
        } else {
            commentRowMap.put(columnId, comment);
        }
        this.renderComment(rowId, columnId, comment);
    },
    renderComment : function (rowId, columnId, comment) {
        var colIdx = this.columnUtil.getColumnIndex(columnId);
        var tdHtml = this.getCellHtml(rowId, columnId);
        this.insertCommentPng(rowId, colIdx, tdHtml, comment);
    },
    insertCommentPng : function (rowId, colIdx, tdHtml, comment) {
        var jTdEle = jQuery("#" + this.gridDivId + '-' + rowId + '-' + colIdx);
        if (jTdEle[0] != null) {
            if (comment == null || comment == '') {
                jTdEle.html(tdHtml);
            } else {
                var left = 0;
                if (com_yung_util_getbrowser() == 'edge') {
                    left = -1;
                }
                var jCommentEle = jQuery("#" + this.gridDivId + "-comment-" + rowId + "-" + colIdx);
                if (jCommentEle[0] != null) {
                    jCommentEle.remove();
                    tdHtml = jTdEle.html();
                }
                tdHtml = "<img id='" + this.gridDivId + "-comment-" + rowId + "-" + colIdx + "' src='" + this.classProp.commentBase64 + "' width='12' style='position: absolute; top: 0px; left: " + left + "px;' title='" + comment + "' />" + tdHtml;
                jTdEle.html(tdHtml);
            }
        }
    },
    
    /** 
     * get comment in grid
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {string} rowId - row id
     * @param {string} columnId - column id
     * @return {string} comment text
     */
    getComment : function (rowId, columnId) {
        var commentRowMap = this.commentMap.get(rowId);
        if (commentRowMap != null) {
            return commentRowMap.get(columnId);
        }
        return null;
    },
    
    /** 
     * remove comment in grid
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {string} rowId - row id
     * @param {string} columnId - column id
     */
    removeComment : function (rowId, columnId) {
        var commentRowMap = this.commentMap.get(rowId);
        if (commentRowMap != null) {
            if (commentRowMap.containsKey(columnId) == true) {
                commentRowMap.remove(columnId);
                var colIdx = this.columnUtil.getColumnIndex(columnId);
                this.deleteCommentPng(rowId, colIdx);
            }
        }
    },
    deleteCommentPng : function (rowId, colIdx) {
        var jCommentEle = jQuery("#" + this.gridDivId + "-comment-" + rowId + "-" + colIdx);
        if (jCommentEle[0] != null) {
            jCommentEle.remove();
        }
    }
});
com_yung_util_TableGridUtil.fireScrollManually = false;

com_yung_util_TableGridUtil.instance = function (gridDivId, columnUtil, gridCls, gridStyle, headerCls, cellCls, tableAlign, rowCls) {
    return $Class.getInstance("com.yung.util.TableGridUtil", gridDivId, columnUtil, gridCls, gridStyle, headerCls, cellCls, tableAlign, rowCls);
}

var com_yung_util_TableErrorCell = $Class.extend({
    classProp : { name : "com.yung.util.TableErrorCell" },
    rowIdx : null,
    columnId : null,
    init: function(rowIdx, columnId) {
        this.rowIdx = rowIdx;
        this.columnId = columnId;
        return this;
    }    
});

/**
 * To defined tool bar item schema
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_ToolbarItem = $Class.extend({
    
    classProp : { 
        name : "com.yung.util.ToolbarItem"
    },
    
    /**
     * item image path
     * @member {string}
     * @instance
     * @memberof com_yung_util_ToolbarItem
     */
    itemImg : null,
    
    /**
     * item label text
     * @member {string}
     * @instance
     * @memberof com_yung_util_ToolbarItem
     */
    itemLabel : null,
    
    /**
     * item tooltip text
     * @member {string}
     * @instance
     * @memberof com_yung_util_ToolbarItem
     */
    itemTooltip : "",
    
    /**
     * item function to run
     * @member {function}
     * @instance
     * @memberof com_yung_util_ToolbarItem
     */
    itemFunct : null,
    
    
    /**
     * constructor
     * @memberof com_yung_util_ToolbarItem
     * @param {string} itemImg - item image path
     * @param {string} itemLabel - item label text
     * @param {string} itemTooltip - item tooltip text
     * @param {createTabCallback} itemFunct - item function to run
     */
    init: function(itemImg, itemLabel, itemTooltip, itemFunct){
        if (itemImg == null && itemLabel == null) {
            throw "please specified itemImg or itemLabel";
        }
        this.setProperty("itemImg", itemImg);
        this.setProperty("itemLabel", itemLabel);
        this.setProperty("itemTooltip", itemTooltip);
        this.setProperty("itemFunct", itemFunct);
        return this;
    },
    
    /** 
     * set property value
     * 
     * @instance
     * @memberof com_yung_util_ToolbarItem
     * @param {string} attribute - property key
     * @param {allType} value - property value
     */
    setProperty : function (attribute, value) {
        if (_validType('string', attribute) == false) {
            throw "attribute is not string";
        }
        var ch = attribute.charAt(0) + "";
        var method = "set" + ch.toUpperCase() + attribute.substring(1);
        if (typeof this[method] == 'function') {
            this[method](value);
        }
    },
    
    /** 
     * get property value
     * 
     * @instance
     * @memberof com_yung_util_ToolbarItem
     * @param {string} attribute - property key
     * @return {allType} property value
     */
    getProperty : function (attribute) {
        if (_validType('string', attribute) == false) {
            throw "attribute is not string";
        }
        var ch = attribute.charAt(0) + "";
        var method = "get" + ch.toUpperCase() + attribute.substring(1);
        if (typeof this[method] == 'function') {
            return this[method]();
        }
        return null;
    },
    setItemImg : function (itemImg) {
        if (itemImg != null && _validType('string', itemImg) == false) {
            throw "itemImg is not string";
        }
        this.itemImg = itemImg;
    },
    getItemImg : function () {
        return this.itemImg;
    },
    setItemLabel : function (itemLabel) {
        if (itemLabel != null && _validType('string', itemLabel) == false) {
            throw "itemLabel is not string";
        }
        this.itemLabel = itemLabel;
    },
    getItemLabel : function () {
        return this.itemLabel;
    },
    setItemTooltip : function (itemTooltip) {
        if (itemTooltip != null && _validType('string', itemTooltip) == false) {
            throw "itemTooltip is not string";
        }
        this.itemTooltip = itemTooltip;
    },
    getItemTooltip : function () {
        return this.itemTooltip;
    },
    setItemFunct : function (itemFunct) {
        if (itemFunct != null && _validType('function', itemFunct) == false) {
            throw "itemFunct is not function";
        }
        this.itemFunct = itemFunct;
    },
    getItemFunct : function () {
        return this.itemFunct;
    }

});

/**
 * Create toolbar tool
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_Toolbar = $Class.extend({
    
    classProp : { 
        name : "com.yung.util.Toolbar" 
    },
    
    /**
     * toolbar container
     * @member {HTMLElement}
     * @instance
     * @memberof com_yung_util_Toolbar
     */
    container : null,
    
    /**
     * toolbar button css
     * @member {CSSStyle}
     * @instance
     * @memberof com_yung_util_Toolbar
     */
    btnCSS : null,
    
    /**
     * toolbar background color
     * @member {string}
     * @instance
     * @memberof com_yung_util_Toolbar
     */
    color : "rgb(234, 234, 234)",
    
    /**
     * constructor
     * @memberof com_yung_util_Toolbar
     * @param {string} containerDivId - toolbar container id
     * @param {string} btnCSS - button css style name
     * @param {string} color - toolbar background color
     */
    init: function(containerDivId, btnCSS, color){
        if (containerDivId == null) {
            throw 'please specify container div id';
        }
        var divEle = document.getElementById(containerDivId);
        if (divEle == null) {
            throw 'container div not exists!';
        }
        jQuery("#" + containerDivId).css("width", "100%");
        jQuery("#" + containerDivId).css("height", "24px");
        jQuery("#" + containerDivId).css("padding-top", "5px");
        jQuery("#" + containerDivId).css("padding-bottom", "5px");
        jQuery("#" + containerDivId).css("margin", "1px");
        jQuery("#" + containerDivId).css("background-color", color);
        this.container = divEle;
        if (btnCSS == null) {
            throw 'please specify btnCSS';
        }
        this.btnCSS = document.getElementById(btnCSS);
        if (this.btnCSS == null) {
            var css = document.createElement("style");
            css.type = "text/css";
            document.body.appendChild(css);
            this.btnCSS = css;
        }
        return this;
    },
    
    /** 
     * create and add separator element
     * 
     * @private
     * @instance
     * @memberof com_yung_util_Toolbar
     * @return {HTMLElement} separator element
     */
    addSeparator : function() {
        var elt = document.createElement('span');
        jQuery(elt).css("float", "left");
        jQuery(elt).css("text-decoration", "none");
        jQuery(elt).css("margin", "0px");
        jQuery(elt).css("font-family", "Arial");
        jQuery(elt).css("font-size", "17px");
        jQuery(elt).css("height", "19px");
        jQuery(elt).css("padding-left", "5px");
        jQuery(elt).css("padding-right", "5px");
        jQuery(elt).css("color", "rgb(33, 115, 70)");
        elt.innerHTML = "&vert;";
        this.container.appendChild(elt);
        return elt;
    },
    
    /** 
     * create button by item schema and set button function
     * 
     * @instance
     * @memberof com_yung_util_Toolbar
     * @param  {Array} items - item schema array
     */
    addItems : function(items) {
        for ( var i = 0; i < items.length; i++) {
            var item = items[i];
            var itemImg = item.img;
            var itemLabel = item.label;
            var itemTooltip = item.tooltip;
            var itemFunct = item.funct;
            if (itemImg == '-' || itemLabel == '-') {
                this.addSeparator();
            } else {
            	var divId = jQuery(this.container).attr("id");
            	this.addItem("com_yung_util_Toolbar_" + divId + "_toolbtn-" + i, itemImg, itemLabel, itemTooltip);
            }
        }
        this.initFunction(items);
    },
    
    /** 
     * initialize button function
     * 
     * @private
     * @instance
     * @memberof com_yung_util_Toolbar
     * @param {Array} items - item array
     */
    initFunction : function(items) {
        for ( var i = 0; i < items.length; i++) {
            var item = items[i];
            var itemFunct = item.funct;
            if (itemFunct != null) {
            	var divId = jQuery(this.container).attr("id");
                this.addClickHandler("com_yung_util_Toolbar_" + divId + "_toolbtn-" + i, itemFunct);
            }
        }
    },
    addClickHandler : function(id, funct) {
        if (funct != null) {
            var ele = jQuery( "#" + id )[0];
            if (ele == null) {
            	throw "ele[id='" + id + "'] not found!";
            }
            jQuery( "#" + id ).bind( "click", function() {
                funct(ele);
            });
        }
    },
    
    
    /** 
     * initialize button style, image or label
     * 
     * @private
     * @instance
     * @memberof com_yung_util_Toolbar
     * @param  {string} id - button id
     * @param  {string} itemImg - button image path
     * @param  {string} label - button label text
     * @param  {string} tooltip - button tooltip
     * @return {HTMLElement} button element
     */
    addItem : function(id, itemImg, label, tooltip) {
        var elt = this.addButton(id, itemImg, label, tooltip);
        this.createCSS(itemImg, tooltip);
        return elt;
    },
    addButton : function(id, itemImg, label, tooltip) {
        var elt = this.createButton(itemImg, tooltip);
        this.initElement(id, elt, label, tooltip);
        this.container.appendChild(elt);
        return elt;
    },
    createButton : function(itemImg, tooltip) {
        var inner = document.createElement('div');
        var elt = document.createElement('a');
        elt.setAttribute('href', 'javascript:void(0);');
        elt.className = 'toolButton';
        inner.className = 'toolbtn-' + this.replaceAll(tooltip, ' ', '_');
        inner.setAttribute('style', 'width: 19px; height: 19px;');
        elt.appendChild(inner);
        return elt;
    },
    initElement : function(id, elt, label, tooltip) {
        elt.setAttribute('id', id);
        if (label != null && label != '') {
            elt.innerHTML = label;
            elt.setAttribute('style', 'height: 19px;');
        } else {
            elt.setAttribute('style', 'width: 20px;');
        }
        // Adds tooltip
        if (tooltip != null) {
            elt.setAttribute('title', tooltip);
        }
    },
    createCSS : function(itemImg, tooltip) {
        var html = this.btnCSS.innerHTML;
        var append = '.toolbtn-' + this.replaceAll(tooltip, ' ', '_') + '{ background-image: url(' + itemImg + '); background-size: 19px 19px; }';
        this.btnCSS.innerHTML = html + append;
    },
    createLabel : function(label) {
        var elt = document.createElement('a');
        elt.setAttribute('href', 'javascript:void(0);');
        elt.className = 'toolLabel';
        mxUtils.write(elt, label);
        return elt;
    }
    
});
/**
 * Convenient validate tool to check string
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_Validator = {
    
    classProp : { 
        name : "com.yung.util.Validator"
    },
    
    /** 
     * to check input string if it is valid email
     * 
     * @memberof com_yung_util_Validator
     * @param {string} input - string to check
     * @return {boolean} valid or not
     */
    email : function (input) {
        if (input == null || input == '') {
            return true;
        }
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(input)) {  
            return true;
        }  
        return false;
    },
    
    /** 
     * to check input string if it is number
     * 
     * @memberof com_yung_util_Validator
     * @param {string} input - string to check
     * @return {boolean} valid or not
     */
    digit : function (input) {
        if (input == null || input == '') {
            return true;
        }
        if (/^\d+$/.test(input)) {
            return true;
        }
        return false;
    },
    
    /** 
     * to check input string if it is an account type
     * account type only allow digit[0-9] and letter[A-Z, a-z],
     * not allow special symbol, like '@','$'
     * 
     * @memberof com_yung_util_Validator
     * @param {string} input - string to check
     * @return {boolean} valid or not
     */
    account : function (input) {
        if (input == null || input == '') {
            return true;
        }
        if (/^[A-Za-z0-9_.]+$/.test(input)) {
            return true;
        }
        return false;
    },
    
    /** 
     * to check input string if it is letter[A-Z, a-z]
     * 
     * @memberof com_yung_util_Validator
     * @param {string} input - string to check
     * @return {boolean} valid or not
     */
    letter : function (input) {
        if (input == null || input == '') {
            return true;
        }
        if (/^[A-Za-z_.]+$/.test(input)) {
            return true;
        }
        return false;
    },
    
    /** 
     * to check input string if it is letter[A-Z, a-z]
     * or a special symbol in an array
     * 
     * @memberof com_yung_util_Validator
     * @param {string} input - string to check
     * @param {string} specialChars - special character array
     * @return {boolean} valid or not
     */
    letterExtchar : function (input, specialChars) {
        if (input == null || input == '') {
            return true;
        }
        if (this.ascii(input) == false) {
            return false;
        }
        for (var i = 0; i < input.length; i++) {
            var c = input.charAt(i) + "";
            if (this.letter(c) == false) {
                var valid = false;
                for (var j = 0; j < specialChars.length; j++) {
                    if (c == specialChars[j]) {
                        valid = true;
                        break;
                    }
                }
                if (valid == false) {
                    return false;
                }
            }
        }
        return true;
    },
    
    /** 
     * to check input string if it is ascii code
     * 
     * @memberof com_yung_util_Validator
     * @param {string} input - string to check
     * @return {boolean} valid or not
     */
    ascii : function (input) {
        if (input == null || input == '') {
            return true;
        }
        if (/^[\x00-\x7F]*$/.test(input)) {
            return true;
        }
        return false;
    },
    
    /** 
     * to check input string if it is account type[0-9, A-Z, a-z]
     * or a special symbol in an array
     * 
     * @memberof com_yung_util_Validator
     * @param {string} input - string to check
     * @param {string} specialChars - special character array
     * @return {boolean} valid or not
     */
    accountExtchar : function (input, specialChars) {
        if (input == null || input == '') {
            return true;
        }
        if (this.ascii(input) == false) {
            return false;
        }
        for (var i = 0; i < input.length; i++) {
            var c = input.charAt(i) + "";
            if (this.account(c) == false) {
                var valid = false;
                for (var j = 0; j < specialChars.length; j++) {
                    if (c == specialChars[j]) {
                        valid = true;
                        break;
                    }
                }
                if (valid == false) {
                    return false;
                }
            }
        }
        return true;
    }
};
$Y.reg(com_yung_util_Validator);
